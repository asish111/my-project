# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception,content_disposition
import base64

class Binary(http.Controller):
    @http.route('/web/binary/download_document', type='http', auth="public")
    @serialize_exception
    def download_document(self,model,field,id,filename,**kw):
        # return 'hello nikunja'
        filecontent = base64.b64decode(filename)
        if not filecontent:
            return request.not_found()
        else:
            # return filename
            # return filecontent
            # return 'file not found'
            if not filename:
                filename = '%s_%s' % (model.replace('.', '_'), id)
                return request.make_response(filecontent,
                                [('Content-Type', 'application/octet-stream'),
                                ('Content-Disposition', content_disposition(filename))])                        

class Ternary(http.Controller):
    @http.route('/web/binary/download_document/files', type='http', auth="public")
    def download_document_files(self,**kw):
        record = request.env['kw_onboarding_handbook'].sudo().search([],order='id desc', limit=1)  
        pdf_file = record.title
        print(pdf_file)
        return request.render("kw_handbook.pdf_record",
                              {
                                  'pdf_file': record,
                              })

