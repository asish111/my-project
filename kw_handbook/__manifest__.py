# -*- coding: utf-8 -*-
{
    'name': "Kwantify Employee Handbook",
    'summary': "Employee Handbook",
    'description': "Employee Handbook",
    'author': "CSM Technologies",
    'website': "http://www.csm.co.in",
    'category': 'Kwantify',
    'version': '0.1',

    # any module necessary for this one to work correctly   , 'kw_employee', 'hr'
    'depends': ['base','kw_web_notify'],

    # always loaded
    'data': [
        'security/kw_handbook_security_manager.xml',
        'security/ir.model.access.csv',
        'views/kw_attachment_view.xml',
        'views/kw_onboarding_handbook.xml',
        'views/kw_onboarding_handbook_category.xml',
        'views/kw_onboarding_handbook_form.xml',
        'views/kw_onboarding_handbook_category_form.xml',
        'views/kw_handbook_menu_items.xml',

       
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'data/handbook_data.xml',
        # 'data/handbook_file.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}