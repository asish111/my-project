# -*- coding: utf-8 -*-
from odoo import models, fields, api, tools
from datetime import datetime
from odoo.exceptions import ValidationError
from odoo.modules import get_module_resource
import base64
from PIL import Image
from odoo.tools.mimetypes import guess_mimetype


class kw_onboarding_handbook(models.Model):
    _name = 'kw_onboarding_handbook'
    _description = 'Policy Documents'
    _rec_name = 'title'
    _order = 'title'

    location = fields.Selection([('0', 'Select Location')], default='0')
    category = fields.Many2one('kw_onboarding_handbook_category')
    title = fields.Char(string="Title", required=True)
    description = fields.Text(string="Description", required=True)
    attachment = fields.Binary(string='Attachment', attachment=True)
    expires_on = fields.Date(string="Expires on")
    count_downloads = fields.Integer()
    icon = fields.Binary(string="Upload Icon")
    display_description = fields.Text(string='Description', compute='_compute_display_description')
    active = fields.Boolean(default=True, help="The active field allows you to hide the employee handbook without removing it.")
    
    # Function to Display the attachment of handbook  :Start    
    @api.multi
    def get_stock_file(self):
        form_view_id = self.env.ref("kw_handbook.kw_onboarding_attachment_form").id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'kw_onboarding_handbook',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'view_id': form_view_id,
            'target': 'new',
        }

    # Function to Display the attachment of handbook  :End
    # @api.constrains('expires_on')
    # def check_expires(self):
    #     current_date = str(datetime.now().date())
    #     for record in self:
    #         if record.expires_on:
    #             if str(record.expires_on) < current_date:
    #                 raise ValidationError("Expire date should be greater than current date")

    @api.constrains('title')
    def check_title(self):
        exists_title = self.env['kw_onboarding_handbook'].search([('title', '=', self.title), ('id', '!=', self.id)])
        if exists_title:
            raise ValueError('Exists ! Already a Title exists in this name')

    @api.model
    def get_default_image(self):
        icon = False
        img_path = get_module_resource('kw_handbook', 'static/img', 'no_icon_image.jpg')
        if img_path:
            with open(img_path, 'rb') as f:
                icon = f.read()
        return base64.b64encode(icon)

    @api.model
    def create(self, vals):
        if not vals.get('icon'):
            vals['icon'] = self.get_default_image()
        record = super(kw_onboarding_handbook, self).create(vals)
        if record:
            self.env.user.notify_success(message='Handbook created successfully.')
        else:
            self.env.user.notify_danger(message='Handbook creation failed.')
        return record

    @api.multi
    def write(self, vals):
        res = super(kw_onboarding_handbook, self).write(vals)
        if not self.icon:
            self.icon = self.get_default_image()
        if res:
            self.env.user.notify_success(message='Handbook updated successfully.')
        else:
            self.env.user.notify_danger(message='Handbook update failed.')
        return res

    @api.constrains('icon')
    def check_icon(self):
        icon_type = ['image/jpeg', 'image/jpg', 'image/png']
        if self.icon:
            mimetype = guess_mimetype(base64.b64decode(self.icon))
            if str(mimetype) not in icon_type:
                raise ValidationError("Unsupported file format ! allowed file formats are .jpg , .jpeg , .png ")
            elif ((len(self.icon) * 3 / 4) / 1024) / 1024 > 0.5:
                raise ValidationError("Maximum file size should be less than 500 kb.")

    @api.constrains('title')
    def check_name(self):
        exists_name = self.env['kw_onboarding_handbook'].search(
            [('title', '=', self.title), ('id', '!=', self.id)])
        if exists_name:
            raise ValueError("The category name" + '"' + self.title + '"' + " already exists.")

    @api.depends('description')
    def _compute_display_description(self):
        for rec in self:
            rec.display_description = (rec.description[:120] + '..') if len(rec.description) > 100 else rec.description
