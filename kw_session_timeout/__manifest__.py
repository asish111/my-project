{
    'name': "Kwantify Inactive Sessions Timeout",
    'summary': """
         Module to disable all inactive sessions since
         a given delay as well as the date changes""",
    'author': "CSM Technologies",
    'website': "http://www.csmpl.com",
    'category': 'Kwantify',
    'version': '0.1',
    'data': [
        'data/ir_config_parameter_data.xml'
    ],
    'installable': True,
}
