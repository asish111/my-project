import base64
import datetime
import json
import logging
import werkzeug
import werkzeug.exceptions
import werkzeug.utils
import werkzeug.wrappers
import werkzeug.wsgi
import requests

import odoo
import pytz
import odoo.modules.registry
from odoo import SUPERUSER_ID
from odoo import http
from odoo.http import content_disposition, dispatch_rpc, request
from odoo.tools.translate import _
from Crypto.Cipher import AES
from Crypto import Random
import odoo.addons.web.controllers.main as main
from odoo.addons.kw_utility_tools import kw_helpers

_logger = logging.getLogger(__name__)


class Home(main.Home):
    @http.route('/web/login', type='http', auth="none", sitemap=False)
    def web_login(self, redirect=None, **kw):
        print("auth called")
        main.ensure_db()
        request.params['login_success'] = False
        if request.httprequest.method == 'GET' and redirect and request.session.uid:
            return http.redirect_with_hash(redirect)
        if not request.uid:
            request.uid = odoo.SUPERUSER_ID
        values = request.params.copy()
        try:
            values['databases'] = http.db_list()
        except odoo.exceptions.AccessDenied:
            values['databases'] = None

        if request.httprequest.method == 'POST':
            old_uid = request.uid
            try:
                uid = request.session.authenticate(request.session.db, request.params['login'], request.params['password'])
                login_username = request.params['login']
                # request.params['login_success'] = True  
                # print(f"USER ID IS {uid} == {login_username}")
                # ---odoo predefined ---##
                # request.params['login_success'] = True
                # http.redirect_with_hash(self._login_redirect(uid, redirect=redirect))
                # ---odoo predefined ---##

                user_status = {}
                print("executed")
                sync_enabled = http.request.env['ir.config_parameter'].sudo().get_param('kw_auth.module_kw_auth_mode_status')
                # sync_enabled = False
                if sync_enabled:
                    ip_address = request.httprequest.environ['REMOTE_ADDR']
                    browser = request.httprequest.user_agent.browser
                    user_emp_kw_id = http.request.env['hr.employee'].sudo().search([("user_id","=",uid)]).kw_id
                    print(f"Kw_id of user is {user_emp_kw_id}")
                    url = http.request.env['ir.config_parameter'].sudo().get_param('kwantify_attendance_url')
                    if url:
                        late_entry_check_url = url
                    else:
                        late_entry_check_url = "http://192.168.201.65/prd.service.portalV6.csmpl.com/OdooSynSVC.svc/UserAttendance"
                    header = {'Content-type': 'application/json', 'Accept': 'text/plain',}
                    json_data = {
                            "UserName": login_username,
                            "Browser": browser,
                            "IPAddress": ip_address
                            }
                    data = json.dumps(json_data)
                    print(data)
                    resp_result = requests.post(late_entry_check_url, headers=header,data=data)
                    print("resp_result is ", resp_result)
                    try:
                        resp = json.loads(resp_result.text)
                        print("Response from .net is..", resp)
                    except Exception as e:
                        print(e)
                        resp = False
                    if resp:
                        url = http.request.env['ir.config_parameter'].sudo().get_param('kwantify_common_url')
                        if not url:
                            url = "http://192.168.201.65/kwantify_v6"
                        raw = self._userstring(login_username)
                        print(raw)
                        qs = self._encrypt(raw)
                        enc_uid = qs.decode('utf8')
                        if resp[0]['LateReasonPopUpOutput'] == '1':
                            return werkzeug.utils.redirect(f"{url}/Odoologin.aspx?q={enc_uid}&type=1")
                        else:
                            # user_status = {}
                            for key,value in resp[0].items():
                                if value=='1':
                                    # enc_uid=qs.decode('utf8')
                                    print(f"E")
                                    if key == 'WhatsappPopupOutput':
                                        user_status[key]=f"{url}/Odoologin.aspx?q={enc_uid}&type=3"
                                    elif key == "EPFPopupOutput":
                                        user_status[key]=f"{url}/Odoologin.aspx?q={enc_uid}&type=2"
                                    # elif key == "LateReasonPopUpOutput":
                                    #     user_status[key]=f"{url}/Odoologin.aspx?q={enc_uid}&type=1"
                                    elif key == "AttenSyncOutput":
                                        user_status[key] = True
                                    else:
                                        user_status[key]=f"/auto-login-kwantify"
                #             print("Generated user_status is ",user_status)
                #             if len(user_status):
                #                 return request.render("kw_auth.user_status",user_status)
                #             else:
                #                 request.params['login_success'] = True
                #                 return http.redirect_with_hash(self._login_redirect(uid, redirect=redirect))
                #     else:
                #         request.params['login_success'] = True
                #         return http.redirect_with_hash(self._login_redirect(uid, redirect=redirect))
                # else:

                # training start
                training_feedback_enabled = http.request.env['ir.config_parameter'].sudo(
                ).get_param('kw_training.module_kw_training_feedback_status')
                print(training_feedback_enabled)
                if training_feedback_enabled:
                    try:
                        training_feedback_url = request.env['kw_training'].with_context(
                            _uid=uid).sudo().check_pending_feedback(user_id=uid)
                        print("Training feedback url is",
                              training_feedback_url)
                        if training_feedback_url:
                            user_status['training_feedback_url'] = training_feedback_url
                    except Exception:
                        pass
                # training end

                # attendance start
                attendance_enabled = http.request.env['ir.config_parameter'].sudo().get_param('kw_hr_attendance.module_kw_hr_attendance_status')
                late_entry_enabled = http.request.env['ir.config_parameter'].sudo().get_param('kw_hr_attendance.late_entry_screen_enable')
                if attendance_enabled and request.env.user.employee_ids:
                    request.env.user.employee_ids.ensure_one()
                    try:
                        request.env['kw_hr_attendance_log'].employee_check_in(
                            employee_id=request.env.user.employee_ids.id)
                        if late_entry_enabled:
                            late_entry_url = request.env['kw_hr_attendance_log'].show_late_entry_reason(employee_id = request.env.user.employee_ids.id )
                            if late_entry_url:
                                return http.local_redirect(late_entry_url, keep_hash=True)
                    except Exception as e:
                        values['error'] = e.args[0]
                #attendance end

                if len(user_status):
                    return request.render("kw_auth.user_status", user_status)
                request.params['login_success'] = True
                return http.redirect_with_hash(self._login_redirect(uid, redirect=redirect))

            except odoo.exceptions.AccessDenied as e:
                failed_uid = request.uid
                request.uid = old_uid
                if e.args[0] == "already_logged_in":
                    values['error'] = "User already logged in. Log out from " \
                                      "other devices and try again."
                    values['logout_all'] = True
                    values['failed_uid'] = failed_uid if failed_uid != SUPERUSER_ID else False

                    print("inside kw auth--------")
                    # print(values)
                elif e.args == odoo.exceptions.AccessDenied().args:
                    values['error'] = _("Wrong login/password")
                else:
                    values['error'] = e.args[0]
        else:
            if 'error' in request.params and request.params.get('error') == 'access':
                values['error'] = _('Only employee can access this database. Please contact the administrator.')

        if 'login' not in values and request.session.get('auth_login'):
            values['login'] = request.session.get('auth_login')

        if not odoo.tools.config['list_db']:
            values['disable_database_manager'] = True

        # otherwise no real way to test debug mode in template as ?debug =>
        # values['debug'] = '' but that's also the fallback value when
        # missing variables in qweb
        if 'debug' in values:
            values['debug'] = True

        response = request.render("web.login",values)
        response.headers['X-Frame-Options'] = 'DENY'
        return response

    def _userstring(self,username):
        tz_india = pytz.timezone('Asia/Kolkata')
        datetime_object = datetime.datetime.now(tz_india)
        datetime_object = datetime_object.strftime("%Y-%m-%d %I:%M:%S %p")
        raw = f"username#{username}|timestamp#{datetime_object}"
        return raw

    def _encrypt(self, raw):
        # private_key = http.request.env['ir.config_parameter'].sudo().get_param('kwantify_private_key')
        private_key = b"6ef93e5ca5f40780aee35aee6bf765aa"
        BLOCK_SIZE = 16
        pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)

        raw = pad('b'+raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        enc = base64.b64encode(iv + cipher.encrypt(raw.encode("utf8")))
        # print(enc)
        return enc

class Session(main.Session):
    @http.route('/web/session/logout', type='http', auth="none")
    def logout(self, redirect='/web'):
        try:  
            attendance_enabled = http.request.env['ir.config_parameter'].sudo().get_param('kw_hr_attendance.module_kw_hr_attendance_status')
            if attendance_enabled and request.session.uid:
                # print('check out---------------')
                request.env['kw_hr_attendance_log'].employee_check_out(user_id=request.session.uid)
        
        except Exception as e:
            # values['error'] = e.args[0]
            print(str(e.args[0]))
            pass
        request.session.logout(keep_db=True)       
        return werkzeug.utils.redirect(redirect, 303)
