# -*- coding: utf-8 -*-
from odoo import http


class UserStatus(http.Controller):
    @http.route('/user-status/', auth='public', website=True)
    def kw_user_status(self, **args):
        return http.request.render('kw_auth.user_status')
