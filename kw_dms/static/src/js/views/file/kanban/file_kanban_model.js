

odoo.define('kw_dms.FileKanbanModel', function (require) {
"use strict";

var core = require('web.core');
var session = require('web.session');

var Domain = require('web.Domain');
var KanbanModel = require('web.KanbanModel');

var _t = core._t;
var QWeb = core.qweb;

var FileKanbanModel = KanbanModel.extend({
	
});

return FileKanbanModel;

});
