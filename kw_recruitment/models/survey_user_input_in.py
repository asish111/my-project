# -*- coding: utf-8 -*-
# import datetime
# import logging
# import re
# from collections import Counter, OrderedDict
# from itertools import product
# from werkzeug import urls
# from odoo import tools, SUPERUSER_ID, _
# from odoo.addons.http_routing.models.ir_http import slug
# from odoo.exceptions import UserError, ValidationError

# email_validator = re.compile(r"[^@]+@[^@]+\.[^@]+")
# _logger = logging.getLogger(__name__)
from odoo import api, fields, models


class SurveyUserInput(models.Model):
    _inherit = "survey.user_input"

    applicant_id = fields.Many2one('hr.applicant', string="Applicant ID")
    meeting_id = fields.Many2one('calendar.event', string="Meeting ID")
    score = fields.Integer(compute='_compute_score_remark', help='Total score of the applicant.')
    remark = fields.Char(compute='_compute_score_remark', help='Remarks of the applicant.')
    current_user = fields.Boolean(compute='_get_current_user')
    applicant_name = fields.Char("Applicant Name", related="applicant_id.partner_name")

    @api.multi
    def view_feedback(self, data=None):
        return self.survey_id.with_context(survey_token=self.token).action_print_recruitment_survey()
        # report = self.env['ir.actions.report']._get_report_from_name('kw_recruitment.kw_print_feedback_report')
        # return report.report_action(self, config=False)

    @api.multi
    def print_feedback(self, data=None):
        report = self.env['ir.actions.report']._get_report_from_name('kw_recruitment.kw_print_feedback_report')
        return report.report_action(self, config=False)

    @api.multi
    def give_feedback(self):
        return self.survey_id.with_context(survey_token=self.token).action_start_kw_recruitment_survey()

    @api.multi
    def _compute_score_remark(self):
        for feedback in self:
            for input in feedback.user_input_line_ids:
                if input.question_id.question == 'Remark':
                    feedback.remark = input.value_free_text
                if input.question_id.question == 'Grand Total':
                    feedback.score = input.value_number

    @api.multi
    def _get_current_user(self):
        for record in self:
            if self.env.user.has_group('hr_recruitment.group_hr_recruitment_manager'):
                record.current_user = True
            else:
                if record.partner_id.id == self.env.user.partner_id.id:
                    record.current_user = True
                else:
                    record.current_user = False
