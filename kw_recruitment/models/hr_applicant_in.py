# -*- coding: utf-8 -*-
import re
import os
import shutil
import base64
from os import system
from PyPDF2 import PdfFileMerger
from mimetypes import guess_extension
from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError
from kw_utility_tools import kw_validations
from odoo.tools.mimetypes import guess_mimetype



class HrApplicant(models.Model):
    _inherit = "hr.applicant"
    _rec_name = "name"

    # #----------- Modified fields----------------
    name = fields.Char("Subject / Application Name", required=False)
    partner_name = fields.Char("Applicant's Name", required=True)
    partner_phone = fields.Char("Phone", size=15)
    partner_mobile = fields.Char("Mobile", size=15, required=False)
    email_from = fields.Char("Email", size=128, help="These people will receive email.", required=False)
    job_position = fields.Many2one('kw_hr_job_positions', "Job Position", required=False)
    job_code = fields.Char('Job Code', related="job_position.job_code", copy=False)
    company_id = fields.Many2one('res.company', string='Company', index=True, required=True, default=lambda self: self.env.user.company_id)
    job_location_id = fields.Many2one('kw_recruitment_location', string="Job Location")
    # #----------- Modified fields----------------

    # #-----------Additional Fields--------------

    erroll_reference = fields.Char("Reference Number")
    experience = fields.Char("Experience")
    exp_year = fields.Integer(string="Year(s)", required=False)
    exp_month = fields.Integer(string="Month(s)", required=False)
    qualification = fields.Many2one("kw_qualification_master", "Qualification OLD", groups="base.group_no_one")
    qualification_ids = fields.Many2many("kw_qualification_master", string="Qualification")
    meeting_count = fields.Integer(compute='_compute_meeting_count', help='Meeting Count')
    applicant_feedback_url = fields.Char('Applicant Feedback URL')
    applicant_response_id = fields.Many2one('survey.user_input', "Response", ondelete="set null", oldname="response")
    applicant_response_state = fields.Selection(related="applicant_response_id.state",
                                                string="Applicant Feedback Stage", store=True)
    # title = fields.Char(string='Job Title')
    other_qualification = fields.Char("Other Qualification", )
    other_position = fields.Char("Other", )
    current_location = fields.Char("Current Location", )
    relevant_job = fields.Many2one('hr.job', string='Profile Relevant For', )

    # changes added on 4-Feb-2020 by Avinas as per feedback of HR
    hiring_type = fields.Many2one('kwemp_employment_type', string='Hiring Type', )
    employee_type = fields.Selection(string='Employee Type', selection=[('new', 'New'), ('replacement', 'Replacement')],
                                     default='new', help="Set recruitment process is employee type.")
    employee_id = fields.Many2one('hr.employee', string='Replacement for', )
    budget_type = fields.Selection(string='Budget', selection=[('treasury', 'Treasury'), ('project', 'Project')],
                                   default='treasury', help="Set recruitment process is employee budget.")
    budget_from_date = fields.Date('From Date', default=fields.Date.context_today)
    budget_to_date = fields.Date('To Date', default=fields.Date.context_today)

    # [Modification on 04-June-2020 (SALMA)]
    certification = fields.Char('Certification')
    current_ctc = fields.Float('Current CTC(Annum)')
    notice_period = fields.Integer('Notice Period')
    employee_referred = fields.Many2one('hr.employee', string='Referred By')
    service_partner_id = fields.Many2one('res.partner', string='Partner')
    media_id = fields.Many2one('kw.social.media', string='Social Media')
    institute_id = fields.Many2one('res.partner', string='Institute')
    consultancy_id = fields.Many2one('res.partner', string='Consultancy')
    jportal_id = fields.Many2one('kw.job.portal', string='Job Portal')
    reference = fields.Char("Client Name")
    reference_walkindrive = fields.Char("Walk-in Drive")
    reference_print_media = fields.Char("Print Media")
    reference_job_fair = fields.Char("Job Fair")
    code_ref = fields.Char('Code')
    user_id = fields.Many2one('res.users', "Responsible", 
        default=lambda self: int(self.env['ir.config_parameter'].sudo().get_param('kw_recruitment.user_id')))

    # duration = fields.Char("Duration")

    # #-----------Additional Fields for report--------------
    gender = fields.Selection(string='Gender',
                              selection=[('male', 'Male'), ('female', 'Female'), ('others', 'Others'), ], required=False)
    date_of_requisition = fields.Many2one('kw_recruitment_requisition', "date_requisition")
    requisition_date = fields.Date(string='requisition_date', related='date_of_requisition.date_requisition',
                                   readonly=True, store=True)

    date_of_offer = fields.Date(string='Date of Offer', default=fields.Date.context_today)
    evaluators = fields.Text(string='Evaluators', compute="_compute_evaluators")
    time_hire = fields.Date(string='Time to Hire')
    time_fill = fields.Date(string='Time to Fill')
    salary_replaced = fields.Text(string='Salary of Replaced')
    project_name = fields.Many2one('project.project', string='Project Name')
    project_pm = fields.Text(string='PM/PL/TL')

    source_id = fields.Many2one('utm.source', required=False)
    division = fields.Many2one('hr.department', string="Division")
    section = fields.Many2one('hr.department', string="Section")
    practise = fields.Many2one('hr.department', string="Practise")

    @api.onchange('department_id')
    def onchange_department(self):
        domain = {}
        for rec in self:
            domain['division'] = [('parent_id', '=', rec.department_id.id), ('dept_type.code', '=', 'division')]
            return {'domain': domain}

    @api.onchange('division')
    def onchange_division(self):
        domain = {}
        for rec in self:
            if rec.department_id:
                domain['section'] = [('parent_id', '=', rec.division.id), ('dept_type.code', '=', 'section')]
                return {'domain': domain}

    @api.onchange('section')
    def onchange_section(self):
        domain = {}
        for rec in self:
            if rec.section:
                domain['practise'] = [('parent_id', '=', rec.section.id), ('dept_type.code', '=', 'practice')]
                return {'domain': domain}

    # #attachement##
    attachment_doc = fields.Integer(compute='_get_attachment', string="Number of Attachments")
    # attachment_doc_ids = fields.One2many('recruitment_attachment','attachment_doc_id',string='Attachments')
    # applicant_ids = fields.One2many('recruitment_attachment', 'applicant_id',ondelete="set null")

    @api.multi
    def export_feedbacks(self):
        ''' This method has following requirements ,
            1.unoconv 0.7
            2.python 3.6.9
            3.LibreOffice 6.0.7.3
        '''
        survey_feedbacks = self.env['survey.user_input'].sudo().search([('applicant_id','=',self.id)])
        given_feedbacks = survey_feedbacks.filtered(lambda r:r.state != 'new')

        if not given_feedbacks:
            raise ValidationError("No feedbacks found.")

        report = self.env.ref('kw_recruitment.kw_recruitment_interview_feedback_report')
        ctx = self.env.context.copy()
        ctx['flag'] = True

        applicant_path = f'applicant_docs/{self.id}'
        if not os.path.exists(applicant_path):
            os.makedirs(applicant_path)

        full_path = os.path.join(os.getcwd() + f'/{applicant_path}')
        try:
            merge_list = []
            for index,feedback in enumerate(given_feedbacks,start=1):

                feedback_b64_string = report.with_context(ctx).render_qweb_pdf(feedback.id)[0]
                feedback_path = f'{full_path}/{index}.pdf'

                with open(os.path.expanduser(feedback_path), 'wb') as fp:
                    fp.write(feedback_b64_string)
                    merge_list.append(feedback_path)
                    
            # Assuming all feedback pdfs are generated
            # convert applicant document to pdf format if this is other than pdf format
            # Loop over the cvs of applicant and convert them one by one
            for index,cv in enumerate(self.attachment_ids):
                
                if cv.content_file:
                    cv_b64_string = base64.b64decode(cv.content_file)
                    # get extension of cv
                    cv_extension = guess_extension(guess_mimetype(cv_b64_string))

                    # If cv is in pdf no need to convert just write the file
                    if cv_extension == '.pdf':
                        cv_store_path = f'{full_path}/cv{index}.pdf'
                        with open(os.path.expanduser(cv_store_path), 'wb') as fp:
                            fp.write(cv_b64_string)
                            merge_list.insert(0,cv_store_path)

                    else:
                        # first write the file with its extension i.e cv.docx,cv.doc etc
                        with open(os.path.expanduser(f'{full_path}/cv{index}{cv_extension}'), 'wb') as fp:
                            fp.write(cv_b64_string)

                        # convert to pdf using unoconv with system command
                        cv_store_path = f'{full_path}/cv{index}{cv_extension}'
                        cv_converted_path = f'{full_path}/cv{index}.pdf'
                        try:
                            system(f'unoconv -f pdf {cv_store_path}') # system coammand that will generate cv.pdf internally
                            merge_list.insert(0, cv_converted_path)
                        except Exception:
                            pass

            # merge all pdf to a single file named finaloutput.pdf
            merger = PdfFileMerger()
            full_path_output = f'{full_path}/finaloutput.pdf'

            for pdf in merge_list:
                merger.append(pdf)

            merger.write(full_path_output)
            merger.close()

            feedback = self.env['kw_recruitment_consolidated_feedback'].create({
                'binary_file': base64.b64encode(open(full_path_output, "rb") .read()),
                'file_name':f'{self.partner_name}_feedback_consolidated.pdf',
                'applicant_id':self.id
            })

            # Clean the directory
            shutil.rmtree(applicant_path)
            
            return {
                'view_mode': 'form',
                'res_model': 'kw_recruitment_consolidated_feedback',
                'res_id': feedback.id,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': self.env.context,
                'nodestroy': True,
            }
        except Exception:
            # Clean the directory
            shutil.rmtree(applicant_path)
            raise ValidationError(f"Unable to convert applicant document to pdf.\nPlease try with a different extension.\n")


    @api.multi
    def name_get(self):
        result = []
        for record in self:
            record_name = str(record.partner_name)
            result.append((record.id, record_name))
        return result

    @api.multi
    def _compute_evaluators(self):
        for record in self:
            meeting_ids = self.env['calendar.event'].search([('applicant_id', '=', record.id)])
            evaluators = self.env['res.partner']
            if meeting_ids:
                for meeting in meeting_ids:
                    evaluators |= meeting.partner_ids
            if evaluators:
                record.evaluators = ','.join(evaluators.mapped('name'))
                # record.evaluators = '\n'.join(evaluators.mapped('name'))
            else:
                record.evaluators = ''

    # #attachement##
    @api.multi
    def _get_attachment(self):
        read_group_res = self.env['recruitment_attachment'].read_group(
            [('res_model', '=', 'hr.applicant'), ('res_id', 'in', self.ids)],
            ['res_id'], ['res_id'])
        attach_data = dict((res['res_id'], res['res_id_count']) for res in read_group_res)

        for record in self:
            record.attachment_doc = attach_data.get(record.id, 0)

    # #attachement##
    @api.multi
    def action_get_attachment_view(self):
        attachment_action = self.env.ref('kw_recruitment.action_recruitement_attachment')
        action = attachment_action.read()[0]
        action['context'] = {'default_res_model': self._name, 'default_res_id': self.ids[0], 'default_name': self.name, 'default_id': self.id}
        action['domain'] = str(['&', ('res_model', '=', self._name), ('res_id', 'in', self.ids)])
        action['search_view_id'] = (self.env.ref('kw_recruitment.view_attachment_search').id, )
        return action

    # @api.multi
    # def name_get(self):
    #     return [(record.id, f"{record.job_position.title} {record.job_position.job_code or ' '}") for record in self]

    @api.onchange('source_id')
    def onchange_source(self):
        self.code_ref = self.source_id.code

    @api.onchange('employee_type')
    def enable_employee_type(self):
        if self.employee_type and self.employee_type == "new":
            self.employee_id = False

    @api.onchange('budget_type')
    def enable_budget_type(self):
        if self.budget_type and self.budget_type == "treasury":
            # self.duration = False
            self.budget_from_date = False
            self.budget_to_date = False

    @api.onchange('job_position')
    def _set_default_field(self):
        self.other_position = False
        if self.job_position:
            job = self.job_position
            if job.job_code == "others":
                self.other_position = ""
            self.job_id = job.job_id.id if job.job_id else False
            self.department_id = job.department_id.id if job.department_id else False
            self.user_id = job.hr_responsible_id.id if job.hr_responsible_id else False
            return {'domain': {'job_location_id': [('id', 'in', job.address_id.ids)]}}

    @api.onchange('qualification_ids')
    def enable_other_qualification(self):
        self.other_qualification = False
        for x in self.qualification_ids:
            if x and x.code == "others":
                self.other_qualification = ""

    @api.constrains('partner_mobile')
    def check_partner_mobile(self):
        for record in self:
            if record.partner_mobile:
                if not re.match("^[0-9+/-]*$", str(record.partner_mobile)) is not None:
                    raise ValidationError("Mobile number is invalid for: %s" % record.partner_mobile)

    @api.constrains('partner_phone')
    def check_partner_phone(self):
        for record in self:
            if record.partner_phone:
                if not re.match("^[0-9+/-]*$", str(record.partner_phone)) is not None:
                    raise ValidationError("Phone number is invalid for: %s" % record.partner_phone)

    @api.constrains('email_from')
    def check_email_from(self):
        for record in self:
            kw_validations.validate_email(record.email_from)

    @api.constrains('qualification_ids', 'other_qualification')
    def validate_other_qualification(self):
        for record in self:
            for x in record.qualification_ids:
                if x and x.code == "others" and not record.other_qualification:
                    raise ValidationError("Please give other qualification.")

    # @api.constrains('exp_year', 'exp_month')
    # def validate_experience(self):
    #     for record in self:
    #         if record.exp_year == 0 and record.exp_month == 0:
    #             raise ValidationError("Please add experience.")

    @api.constrains('source_id')
    def validate_referral(self):
        for record in self:
            if record.source_id.code == 'employee' and not record.employee_referred:
                raise ValidationError("Please enter referred by employee.")
            elif record.source_id.code == 'job' and not record.jportal_id:
                raise ValidationError("Please enter referred by job portal.")
            elif record.source_id.code == 'institute' and not record.institute_id:
                raise ValidationError("Please enter referred by institute.")
            elif record.source_id.code == 'partners' and not record.service_partner_id:
                raise ValidationError("Please enter referred by partner name.")
            elif record.source_id.code == 'social' and not record.media_id:
                raise ValidationError("Please enter referred by social media.")
            elif record.source_id.code == 'consultancy' and not record.consultancy_id:
                raise ValidationError("Please enter referred by consultancy.")
            elif record.source_id.code == 'walkindrive' and not record.reference_walkindrive:
                raise ValidationError("Please enter Walk-in Drive.")
            elif record.source_id.code == 'printmedia' and not record.reference_print_media:
                raise ValidationError("Please enter Print Media.")
            elif record.source_id.code == 'jobfair' and not record.reference_job_fair:
                raise ValidationError("Please enter Job Fair.")
            elif record.source_id.code == 'client' and not record.reference:
                raise ValidationError("Please enter Client Name.")

    @api.constrains('other_position', 'job_position')
    def validate_other_position(self):
        for record in self:
            if record.job_position and record.job_position.job_code == "others" and not record.other_position:
                raise ValidationError("Please enter other job position name.")

    @api.constrains('employee_type', 'employee_id')
    def validate_employee_type(self):
        for record in self:
            if record.employee_type and record.employee_type == "replacement" and not record.employee_id:
                raise ValidationError("Please give Employee Replacement for.")

    @api.constrains('budget_type', 'budget_from_date', 'budget_to_date')
    def validate_budget(self):
        for record in self:
            if record.budget_type and record.budget_type == "project":
                if record.budget_from_date and record.budget_to_date and record.budget_from_date > record.budget_to_date:
                    # if not record.budget_from_date:
                    #     raise ValidationError("Please choose budget from date.")
                    # if not record.budget_to_date:
                    #     raise ValidationError("Please choose budget to date.")
                    # if record.budget_from_date > record.budget_to_date:
                    raise ValidationError("Budget to date must be equal or greater than budget from date.")

    def _compute_meeting_count(self):
        for applicant in self:
            applicant.meeting_count = self.env['calendar.event'].search_count([('applicant_id', '=', applicant.id)])

    @api.multi
    def show_applicant_answers(self):
        self.ensure_one()
        return self.job_id.applicant_feedback_form.with_context(
            survey_token=self.applicant_response_id.token).action_start_kw_recruitment_survey()

    @api.multi
    def action_view_all_feedback(self):
        self.ensure_one()
        applicant_id = self.id
        meetings = self.env['calendar.event'].search(
            ['&', ('applicant_id', '=', applicant_id), ('response_id', '!=', False)])
        return {
            'name': self.name,
            'type': 'ir.actions.act_window',
            'res_model': 'survey.user_input',
            'view_mode': 'tree,form',
            'view_type': 'form',
            'target': 'self',
            'domain': [('id', 'in', [meeting.response_id.id for meeting in meetings])],
        }

    @api.multi
    def action_makeMeeting(self):
        """ This opens Meeting's calendar view to schedule meeting on current applicant
            @return: Dictionary value for created Meeting view
        """
        self.ensure_one()
        partners = self.user_id.partner_id | self.department_id.manager_id.user_id.partner_id
        # self.partner_id | self.user_id.partner_id | self.department_id.manager_id.user_id.partner_id

        category = self.env.ref('hr_recruitment.categ_meet_interview')
        res = self.env['ir.actions.act_window'].for_xml_id('kw_recruitment', 'action_calendar_form')
        # res['domain'] = [('partner_ids', 'in', self.env.user.partner_id.ids)]
        res['domain'] = [('applicant_id', '=', self.id)]
        res['context'] = {
            'visible': False,
            'default_applicant_id': self.id,
            # 'search_default_partner_ids': self.partner_id.name,
            'default_partner_ids': partners.ids,
            'default_user_id': self.env.uid,
            'default_name': self.name,
            'default_categ_ids': category and [category.id] or False,
        }
        return res

    @api.model
    def _get_exp_years(self):
        return [(str(i), i) for i in range(1, 36)]

    @api.multi
    def create_enrollment(self):
        for record in self:
            if not record.job_position.mrf_id:
                raise UserError(_('No MRF found for this Job Position.'))

            if record.partner_name:
                vals = {'name': record.partner_name}
                if record.email_from:
                    vals['email'] = record.email_from
                if record.partner_mobile:
                    vals['mobile'] = record.partner_mobile
                if record.department_id:
                    vals['dept_name'] = record.department_id.id
                if record.job_id:
                    vals['job_id'] = record.job_id.id
                if record.division:
                    vals['division'] = record.division.id
                if record.section:
                    vals['section'] = record.section.id
                if record.practise:
                    vals['practise'] = record.practise.id
                if record.job_position.mrf_id:
                    vals['employment_type'] = record.job_position.mrf_id.type_employment.id
                    vals['emp_role'] = record.job_position.mrf_id.role_id.id
                    vals['emp_category'] = record.job_position.mrf_id.categ_id.id
                if record.gender:
                    vals['gender'] = record.gender

                enrollment_record = self.env['kwonboard_enrollment'].sudo().create(vals)
                if enrollment_record:
                    self.env.user.notify_success(message='Enrollment created successfully.')
                    record.write({'erroll_reference': enrollment_record.reference_no})
                    record.send_feedback_form_to_applicant()
                    if record.job_id:
                        record.job_id.write({'no_of_hired_employee': record.job_id.no_of_hired_employee + 1})
                        record.job_id.message_post(
                            body=_('New Employee %s Hired') % record.partner_name if record.partner_name else record.name,
                            subtype="hr_recruitment.mt_job_applicant_hired")
                    if record.job_position:
                        record.job_position.no_of_hired_employee = 1 if record.job_position.no_of_hired_employee == 0 else record.job_position.no_of_hired_employee + 1
                    else:
                        raise UserError(_('You must define an Applied Job and a Contact Name for this applicant.'))
                    record.active = False
            return {
                "type": "ir.actions.client",
                'tag': "reload",
            }

    @api.model
    def send_feedback_form_to_applicant(self):
        self.ensure_one()
        if self.job_id.applicant_feedback_form:
            applicant_job_survey_id = self.job_id.applicant_feedback_form
            url = applicant_job_survey_id.public_url
            if not self.applicant_response_id:
                response = self.env['survey.user_input'].create(
                    {'survey_id': applicant_job_survey_id.id, 'partner_id': self.user_id.partner_id.id})
                self.applicant_response_id = response.id
            else:
                response = self.applicant_response_id
            token = response.token
            self.applicant_feedback_url = f"{url}/{token}"
            template_id = self.env.ref('kw_recruitment.kw_hr_applicant_feedback_template')
            template_id.send_mail(self.id, force_send=True)
            return True

    @api.model
    def create(self, vals):
        if vals.get('partner_name'):
            vals['name'] = vals.get('partner_name') + '\'s' + ' Application'
        phone = vals.get('partner_phone', False)
        mobile = vals.get('partner_mobile', False)
        if phone and not mobile:
            vals['partner_mobile'] = phone
        if not vals.get('user_id'):
            vals['user_id'] = int(self.env['ir.config_parameter'].sudo().get_param('kw_recruitment.user_id'))
        rec = super(HrApplicant, self).create(vals)

        # channel message post
        if rec.user_id:
            tagged_user = rec.user_id
            ch_obj = self.env['mail.channel']
            channel1 = tagged_user.name + ', ' + self.env.user.name
            channel2 = self.env.user.name + ', ' + tagged_user.name
            channel = ch_obj.sudo().search(["|", ('name', 'ilike', str(channel1)), ('name', 'ilike', str(channel2))])
            if not channel:
                channel_id = ch_obj.channel_get([tagged_user.partner_id.id])
                channel = ch_obj.browse([channel_id['id']])
            channel.message_post(body=f"{rec.create_uid.name} assigned you to take interview of {rec.partner_name}",
                                 message_type='comment', subtype='mail.mt_comment',
                                 author_id=self.env.user.partner_id.id,
                                 notif_layout='mail.mail_notification_light')

        self.env.user.notify_success("Applicant details saved successfully.")

        # email to hr responsible New Job Applied
        if rec.job_position.hr_responsible_id:
            template_id = self.env.ref('kw_recruitment.kw_job_applied_mail_template')
            template_id.send_mail(rec.id, force_send=True)

        # email sent to Applicant Thanks mail Template
        if rec.email_from:
            template_id = self.env.ref('kw_recruitment.kw_job_applicant_applied_mail_template')
            template_id.send_mail(rec.id, force_send=True)

        return rec

    @api.multi
    def write(self, vals):
        result = super(HrApplicant, self).write(vals)
        user_id = vals.get("user_id", False)
        for applicant in self:
            if 'stage_id' in vals and vals['stage_id'] and applicant.stage_id.code in ['OR', 'OA']:
                if not  applicant.job_position:
                    raise ValidationError("Please tag applicant with a job position.")
                if  not applicant.job_position.mrf_id or applicant.job_position.mrf_id.state != 'approve':
                    raise ValidationError("Applicant should be tagged to a approved MRF.")
        if user_id:
            tagged_user = self.env['res.users'].browse(int(user_id))
            ch_obj = self.env['mail.channel']
            channel1 = tagged_user.name + ', ' + self.env.user.name
            channel2 = self.env.user.name + ', ' + tagged_user.name
            channel = ch_obj.sudo().search(["|", ('name', 'ilike', str(channel1)), ('name', 'ilike', str(channel2))])
            if not channel:
                channel_id = ch_obj.channel_get([tagged_user.partner_id.id])
                channel = ch_obj.browse([channel_id['id']])
            channel.message_post(body=f"{self.create_uid.name} assigned you to take interview of {self.partner_name}",
                                 message_type='comment', subtype='mail.mt_comment',
                                 author_id=self.env.user.partner_id.id,
                                 notif_layout='mail.mail_notification_light')
        self.env.user.notify_success("Applicant details updated successfully.")
        return result

    # @api.multi
    # def unlink(self):
    #     for record in self:
    #         record.active = False
    #     return True

    @api.multi
    def start_recruitment_survey(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if not self.response_id:
            response = self.env['survey.user_input'].create({
                'survey_id': self.survey_id.id,
                'partner_id': self.partner_id.id,
                'applicant_id': self.id,
            })
            self.response_id = response.id
        else:
            response = self.response_id
        # grab the token of the response and start surveying
        return self.survey_id.with_context(survey_token=response.token,
                                           # applicant_id=self.id
                                           ).action_start_kw_recruitment_survey()

    @api.multi
    def view_recruitment_survey(self):
        """ If response is available then print this response otherwise print survey form (print template of the survey) """
        self.ensure_one()
        if not self.response_id:
            return self.survey_id.action_print_recruitment_survey()
        else:
            response = self.response_id
            return self.survey_id.with_context(survey_token=response.token).action_print_recruitment_survey()

    # @api.multi
    # def action_get_attachment_tree_view(self):
    #     attachment_action = self.env.ref('base.action_attachment')
    #     action = attachment_action.read()[0]
    #     action['context'] = {'default_res_model': self._name, 'default_res_id': self.ids[0], 'default_name': self.name, }
    #     action['domain'] = str(['&', ('res_model', '=', self._name), ('res_id', 'in', self.ids)])
    #     action['search_view_id'] = (self.env.ref('hr_recruitment.ir_attachment_view_search_inherit_hr_recruitment').id,)
    #     return action

    def update_qualifications(self):
        applicants = self.env['hr.applicant'].search([('qualification','!=',''),('qualification_ids','=', False)])
        if applicants:
            for app in applicants:
                qid = self.env['kw_qualification_master'].search([('name','=ilike', app.qualification)])
                if qid:
                    app.write({'qualification_ids':[(6, 0, [qid.id])]})
