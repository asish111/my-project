# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from ast import literal_eval
class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    first_level_approver_ids = fields.Many2many('hr.job', 'first_level_job_approval_rel', 'job_id', 'approver_id', string='First Level Approval')
    second_level_approver_ids = fields.Many2many('hr.job', 'sec_level_job_approval_rel', 'job2_id', 'approver2_id', string='Second Level Approval')
    user_id = fields.Many2one('res.users', "Responsible",)
    approval_user_ids = fields.Many2many("hr.employee",string="Approval Notification")


    def set_values(self):
        res = super(ResConfigSettings, self).set_values()
        self.env['ir.config_parameter'].set_param('kw_recruitment.first_level_approver_ids', self.first_level_approver_ids.ids)
        self.env['ir.config_parameter'].set_param('kw_recruitment.second_level_approver_ids', self.second_level_approver_ids.ids)
        self.env['ir.config_parameter'].set_param('kw_recruitment.user_id', self.user_id.id)
        self.env['ir.config_parameter'].set_param('kw_recruitment.approval_user_ids', self.approval_user_ids.ids)
        return res


    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        first_level_approver_ids = self.env['ir.config_parameter'].sudo().get_param('kw_recruitment.first_level_approver_ids')
        lines = False
        if first_level_approver_ids:
        	lines = [(6, 0, literal_eval(first_level_approver_ids))]
        second_level_approver_ids = self.env['ir.config_parameter'].sudo().get_param('kw_recruitment.second_level_approver_ids')
        sec_lines = False
        if second_level_approver_ids:
        	sec_lines = [(6, 0, literal_eval(second_level_approver_ids))]
        user = self.env['ir.config_parameter'].sudo().get_param('kw_recruitment.user_id', default=False)

        approval_user_ids = self.env['ir.config_parameter'].sudo().get_param('kw_recruitment.approval_user_ids')
        approval_users = approval_user_ids and [(6, 0, literal_eval(approval_user_ids))] or False

        res.update(
            first_level_approver_ids = lines,
            second_level_approver_ids = sec_lines,
            user_id = int(user),
            approval_user_ids = approval_users
        )
        return res
