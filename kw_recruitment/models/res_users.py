from odoo import models, fields,api
from ast import literal_eval

class Users(models.Model):
    _inherit = 'res.users'

    # helper methods

    def in_second_level_approval(self):
        result = False
        Parameters = self.env['ir.config_parameter'].sudo()
        second_approver_list = literal_eval(Parameters.get_param('kw_recruitment.second_level_approver_ids'))
        if self.employee_ids and self.employee_ids[-1].job_id and \
            self.employee_ids[-1].job_id.id in second_approver_list:
            result = True
        return result
    
    def in_first_level_approval(self):
        result = False
        Parameters = self.env['ir.config_parameter'].sudo()
        first_approver_list = literal_eval(Parameters.get_param(
            'kw_recruitment.first_level_approver_ids'))
        if self.employee_ids and self.employee_ids[-1].job_id and \
                self.employee_ids[-1].job_id.id in first_approver_list:
            result = True
        return result
