import re
from datetime import date
from ast import literal_eval
from odoo import tools, _
from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Requisition(models.Model):
    _name = "kw_recruitment_requisition"
    _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin']
    _description = "Manpower Requisition"
    _rec_name = "code"
    _order = 'id desc'

    @api.depends('current_user')
    def _compute_visibility_approver(self):
        for mrf in self:
            if mrf.current_user.id:
                job = self.env['hr.job']
                Parameters = self.env['ir.config_parameter'].sudo()
                first_approver_list = literal_eval(Parameters.get_param('kw_recruitment.first_level_approver_ids'))
                second_approver_list = literal_eval(Parameters.get_param('kw_recruitment.second_level_approver_ids'))
                all_jobs = job.browse(first_approver_list) + job.browse(second_approver_list)
                # approver_list = first_approver_list.strip('][').split(', ')
                # approver_list.append(second_approver_list.strip('][').split(', '))
                employee = self.env['hr.employee'].search([('job_id', 'in', all_jobs.ids), ('user_id', '=', mrf.current_user.id)])
                if employee:
                    mrf.approver_field = True
                else:
                    mrf.approver_field = False

    def _compute_visibility_action(self):
        for mrf in self:
            if mrf.current_user.id == self.env.user.id:
                mrf.user_inv = True
            else:
                mrf.user_inv = False

    def _compute_is_final_approver(self):
        for mrf in self:
            emp = self.env['hr.employee'].search([('user_id','=', self.env.user.id)])
            if emp.id in self.last_approver_ids.ids:
                mrf.is_final_approver=True
            else:
                mrf.is_final_approver = False

    @api.depends('current_user')
    def _get_raisedby(self):
        for mrf in self:
            emp = self.env['hr.employee'].search([('user_id', '=', mrf.env.user.id)])
            if emp.job_id:
                mrf.req_raised_by = emp.display_name + " - " + emp.job_id.name
            else:
                mrf.req_raised_by = emp.display_name

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    # #####------------------------Fields----------------------######
    # Modified on 27th May 2020 (Salma)
    name_of_business = fields.Many2one('kw_business_unit',string='Name of the Business Unit')
    current_user = fields.Many2one('res.users', default=lambda self: self.env.user)
    req_raised_by = fields.Char('Raised By', compute='_get_raisedby', store=True)
    req_raised_by_id = fields.Many2one('hr.employee', string="Request raised by Employee", default=_default_employee)
    user_inv = fields.Boolean(compute='_compute_visibility_action', string='Field', help='Make buttons invisible once sent for approval button clicked.')
    approver_field = fields.Boolean(compute='_compute_visibility_approver', string='Field', help='Make approver field invisible.')
    approver_id = fields.Many2one('hr.employee', string="Approver")
    note = fields.Text('Comment', track_visibility='always', track_sequence=6)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('sent', 'Pending'),
        ('hold', 'Hold'),
        ('revise', 'Revise'),
        ('forward', 'Forwarded'),
        ('approve', 'Approved'),
        ('reject', 'Rejected'),
    ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', track_sequence=3,
        default='draft')
    forwarded = fields.Boolean(default=False)
    final_approver_ids = fields.Many2many('hr.employee','emp_approver_rel','mrf_id','emp_id', string="Final Approvers")
    last_approver_ids = fields.Many2many('hr.employee','emp_approver_rel_last','mrf_id','emp_id', string="Last Approvers")
    is_final_approver = fields.Boolean(compute='_compute_is_final_approver',)
    # #End
    requisition_type = fields.Selection(string='Budget Type',
                                        selection=[('treasury', 'Treasury Budget'), ('project', 'Project Budget')],
                                        default='treasury',
                                        help="1. Treasury budget. \n2. Project Budget (It will ask for project selection)")
    project = fields.Many2one('crm.lead', string='Project')
    resource = fields.Selection(string='Resource Type', selection=[('new', 'New'), ('replacement', 'Replacement')],
                                default='new', help="Set recruitment process is employee type.")
    employee_id = fields.Many2many('hr.employee', string='Employee Name')
    # modified on 30 april 2020 (gouranga)
    # category_id = fields.Many2one('kw_recruitment_requisition_categories', required=True)
    # skill_id = fields.Many2one('kw_recruitment_requisition_skills', string='Type', required=True)
    role_id = fields.Many2one('kwmaster_role_name', ondelete='cascade', string="Employee Role")
    categ_id = fields.Many2one('kwmaster_category_name', ondelete='cascade', string="Employee Category")
    # modified on 30 april 2020 (gouranga)
    date_requisition = fields.Date(string='Date of Requisition', default=fields.Date.context_today)
    no_of_resource = fields.Integer('No. of Resource Required', default=1)
    skill_set = fields.Text('Expertise(Skill Set)')
    qualification = fields.Text('Qualification and Certification ')
    experience = fields.Char("Work Experience")
    min_exp_year = fields.Selection(string='Min. Years', selection='_get_year_list', default="0")
    max_exp_year = fields.Selection(string='Max. Years', selection='_get_year_list', default="0")
    job_position = fields.Many2one('hr.job')
    salary_month = fields.Char("Salary per Month")
    min_sal = fields.Integer('Min. Salary')
    max_sal = fields.Integer('Max. Salary')
    company_id = fields.Many2one('res.company', string='Company', index=True,
                                 default=lambda self: self.env.user.company_id)
    # branch_id = fields.Char(string='Work Location', )
    # branch_id = fields.Many2one('kw_recruitment_location',string="Work Location")
    branch_id = fields.Many2many('kw_recruitment_location', string="Work Location")
    type_project = fields.Selection(string='Type Of Project',
                                    selection=[('work', 'Work Order'), ('opportunity', 'Opportunity')], default='work',
                                    help="Set recruitment process is work order.")
    project_name = fields.Many2one('crm.lead', string='Project')
    date_of_joining = fields.Date(string=' Tentative Date of Joining', default=fields.Date.context_today)
    duration = fields.Integer('Engagement Period(Months)')
    name_of_workorder = fields.Char(string='Name of the Work Order')
    billing_date = fields.Date(string='Effective date of billing', default=fields.Date.context_today)
    end_date_contract = fields.Date(string='End date of the contract  ', default=fields.Date.context_today)
    type_employment = fields.Many2one('kwemp_employment_type', string='Type of Employment', )
    actual_joining = fields.Date(string='Date of Actual Joining ', default=fields.Date.context_today)
    end_service = fields.Date(string='End Date of Service', default=fields.Date.context_today)
    actual_sal = fields.Integer('Actual Salary (CTC) per Month')
    code = fields.Char(string="Reference No.", default="New", readonly="1")
    user_in_second_label = fields.Boolean(compute='compute_user_in_second_label')

    dept_name = fields.Many2one('hr.department', string='Department', domain=[('parent_id', '=', False)])
    kw_job_id = fields.Many2one('kw_hr_job_positions', string='Job Ref#')
    pending_at = fields.Char(string="Pending At",compute="compute_pending_user")
    hold_user_id = fields.Many2one("res.users",string="Hold By")
    active = fields.Boolean(string="Active",default=True)
    approved_user_id = fields.Many2one("res.users", string="Approved By")
    order_sequence = fields.Char(string="Order", compute="compute_order",store=True)

    @api.depends('state')
    def compute_order(self):
        state_mapping = {'draft':'a','sent':'b','hold':'c','forward':'d','approve':'e','reject':'f'}
        for mrf in self:
            mrf.order_sequence = state_mapping[mrf.state]

    @api.multi
    def compute_pending_user(self):
        Parameters = self.env['ir.config_parameter'].sudo()
        second_level_approver = literal_eval(Parameters.get_param('kw_recruitment.second_level_approver_ids'))
        second_level_employees = self.env['hr.employee'].search([('job_id','in',second_level_approver)])
        for mrf in self:
            if mrf.state == "forward":
                mrf.pending_at = second_level_employees and ','.join(
                    second_level_employees.mapped('display_name')) or ''
            elif mrf.state == "sent":
                mrf.pending_at = mrf.approver_id and mrf.approver_id.name or (
                    second_level_employees and ','.join(second_level_employees.mapped('display_name')) or '')
            elif mrf.state == "hold":
                mrf.pending_at = mrf.hold_user_id.employee_ids and mrf.hold_user_id.employee_ids[
                    -1].display_name or mrf.hold_user_id.name

    @api.depends('current_user')
    def compute_user_in_second_label(self):
        Parameters = self.env['ir.config_parameter'].sudo()
        second_approver_list = literal_eval(Parameters.get_param('kw_recruitment.second_level_approver_ids'))
        for mrf in self:
            if mrf.current_user.employee_ids and mrf.current_user.employee_ids[-1].job_id and \
                    mrf.current_user.employee_ids[-1].job_id.id in second_approver_list:
                mrf.user_in_second_label = True
    @api.multi
    def get_hr_email(self):
        email = False
        hr_user = self.env['ir.config_parameter'].sudo().get_param(
            'kw_recruitment.user_id', default=False)
        print("hr user is",hr_user)
        if hr_user:
            user_rec = self.env['res.users'].sudo().browse(int(hr_user))
            email = user_rec.employee_ids and user_rec.employee_ids[-1].work_email or user_rec.email
        print("Final email is",email)
        return email

    def _get_all_child_department(self, dept_ids):
        child_recs = dept_ids.mapped('child_ids')
        if child_recs:
            return child_recs | self._get_all_child_department(child_recs)
        else:
            return child_recs

    @api.onchange('dept_name', 'approver_id', 'employee_id')
    def _get_dept_approver(self):
        for rec in self:
            employees_list = []
            if rec.dept_name:
                Parameters = self.env['ir.config_parameter'].sudo()
                first_approver_list = Parameters.get_param('kw_recruitment.first_level_approver_ids')
                approver_list = first_approver_list.strip('][').split(', ')

                dept_child_ids = self._get_all_child_department(rec.dept_name)
                dept_child_ids += rec.dept_name
                # print(dept_child_ids)

                employees = self.env['hr.employee'].search([('department_id', 'in', dept_child_ids.ids),
                                                            ('job_id', 'in', [int(job_id) for job_id in approver_list])])
                if employees:
                    employees_list = [emp.id for emp in employees]
                return {'domain': {'approver_id': [('id', 'in', employees_list)],
                                   'employee_id': [('department_id', 'in', dept_child_ids.ids),
                                                   ('user_id', '!=', self.env.user.id)]}, }

    @api.onchange('type_project')
    def _onchange_project_type(self):
        self.project = False
        if self.type_project == 'work':
            return {'domain': {'project': [('stage_id.code', '=', 'workorder')]}}
        elif self.type_project == 'opportunity':
            return {'domain': {'project': [('stage_id.code', '=', 'opportunity')]}}

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        if self._context.get('approval_label_check'):
            Parameters = self.env['ir.config_parameter'].sudo()
            first_approver_list = literal_eval(Parameters.get_param(
                 'kw_recruitment.first_level_approver_ids'))
            if self.env.user.in_second_level_approval():
                args += ['|', '|', ('state', '=', 'forward'), '&', ('state', '=', 'sent'),
                         ('current_user.employee_ids.job_id', 'in', first_approver_list),'&',('state', '=', 'hold'),('hold_user_id','=',self.env.user.id)]
            elif self.env.user.in_first_level_approval():
                args += ['|','&',('state', '=', 'sent'), ('approver_id', 'in', self.env.user.employee_ids.ids),
                         '&', ('state', '=', 'hold'), ('hold_user_id', '=', self.env.user.id)]
            else:
                args += [('id','in',[])]
        return super(Requisition, self)._search(args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)
    
    
    
    @api.onchange('role_id')
    def _get_categories(self):
        role_id = self.role_id.id
        self.categ_id = False
        return {'domain': {'categ_id': [('role_ids', '=', role_id)], }}

    @api.model
    def _get_year_list(self):
        years = 30
        return [(str(i), i) for i in range(years + 1)]

    @api.onchange('resource')
    def enable_resource_type(self):
        if self.resource and self.resource == "new":
            self.employee_id = False

    @api.onchange('category_id')
    def enable_category_type(self):
        if self.category_id:
            self.skill_id = False
            return {'domain': {'skill_id': [('category_id', '=', self.category_id.id)], }}

    @api.onchange('requisition_type')
    def check_requisition_type(self):
        if self.requisition_type and self.requisition_type == "treasury":
            self.type_employment = False
            self.role_id = False
            self.type_project = False
            # self.project_name = False
            self.project = False
            self.duration = False
            # self.date_of_joining = False
            self.name_of_business = False
            self.name_of_workorder = False
            self.billing_date = False
            self.end_date_contract = False
            return {'domain': {'type_employment': [('code', '=', 'P')], 'role_id': []}}
        elif self.requisition_type and self.requisition_type == "project":
            self.type_employment = False
            lst_roles = []
            roles = self.env['kwmaster_role_name'].search([('code', '!=', 'O')])
            for role in roles:
                lst_roles.append(role.id)
            return {'domain': {'type_employment': [('code', 'in', ['O', 'C'])], 'role_id': [('id', '=', lst_roles)]}}

    @api.model
    def create(self, values):
        values['code'] = self.env['ir.sequence'].next_by_code('self.mrf_seq') or 'New'
        result = super(Requisition, self).create(values)
        # Parameters = self.env['ir.config_parameter'].sudo()
        # second_approver_list = literal_eval(Parameters.get_param('kw_recruitment.second_level_approver_ids'))
        # print("second approver list is",second_approver_list)
        # print(result.current_user, result.current_user.employee_ids)
        # if result.current_user.employee_ids and result.current_user.employee_ids[-1].job_id and \
        #     result.current_user.employee_ids[-1].job_id.id in second_approver_list:
        if result.user_in_second_label:
            result.state = 'approve'
            template = self.env.ref('kw_recruitment.action_approval_final_template')
            template.send_mail(result.id)
        self.env.user.notify_success("Manpower Requisition Form created successfully.")
        return result

    @api.multi
    def write(self, values):
        if not self.req_raised_by:
            emp = self.env['hr.employee'].search([('user_id', '=', self.env.user.id)])
            if emp.job_id:
                values['req_raised_by'] = emp.display_name + " - " + emp.job_id.name
            else:
                values['req_raised_by'] = emp.name
        result = super(Requisition, self).write(values)
        self.env.user.notify_success("Manpower requisition Form updated successfully.")
        return result

    @api.constrains('resource', 'employee_id')
    def validate_employee(self):
        for record in self:
            if record.resource and record.resource == "replacement" and not record.employee_id:
                raise ValidationError("Please give Employee name.")

    @api.constrains('requisition_type', 'project')
    def validate_project(self):
        for record in self:
            if record.requisition_type and record.requisition_type == "project" and not record.project:
                raise ValidationError("Please give Project name")

    @api.constrains('min_exp_year', 'max_exp_year')
    def validate_experience(self):
        if int(self.max_exp_year) < int(self.min_exp_year):
            raise ValidationError("Max. experience year could not be less than Min. experience year")

    @api.constrains('min_sal', 'max_sal')
    def validate_sal(self):
        if self.max_sal < self.min_sal:
            raise ValidationError("Max. salary could not be less than Maximum salary")

    @api.constrains('no_of_resource')
    def validate_sal(self):
        if self.no_of_resource == 0:
            raise ValidationError("Number of resource can not be 0")

    def get_mail_activity_details(self):
        res_id = self.id
        res_model_id = self.env['ir.model'].sudo().search([('model','=',self._name)]).id
        date_deadline = date.today()
        note = "Action pending on mrf request."
        activity_type_id = self.env.ref('mail.mail_activity_data_todo').id # todo is default
        # print(locals())
        return res_model_id, res_id, date_deadline, note, activity_type_id

    @api.multi
    def send_for_approval(self):
        # self.get_mail_activity_details()
        mail_activity = self.env['mail.activity']
        self.write({'state': 'sent'})
        if self.approver_id:
            # Mark done activity if found for current user
            res_model_id,res_id,_,_,_= self.get_mail_activity_details()
            activity = mail_activity.search([('res_id', '=', res_id), ('res_model_id', '=', res_model_id),('user_id','=',self._uid)])
            if activity:
                activity.action_feedback() # makes done activity
            # create activity for approver
            if self.approver_id.user_id:
                res_model_id, res_id, date_deadline, note, activity_type_id = self.get_mail_activity_details()
                mail_activity.create({
                    'res_model_id': res_model_id,
                    'activity_type_id': activity_type_id,
                    'res_id': res_id,
                    'date_deadline': date_deadline,
                    'user_id': self.approver_id.user_id.id,
                    'note': note})
            template_obj = self.env.ref('kw_recruitment.template_for_approval')
            if template_obj:
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.id, email_values={'email_to': self.approver_id.work_email})
                self.env.user.notify_success("Mail sent successfully.")
        else:
            # # Fist approver to Second Approver where First Approver is the MRF Creator
            Parameters = self.env['ir.config_parameter'].sudo()
            sec_level_approver_config = Parameters.get_param('kw_recruitment.second_level_approver_ids')
            sec_level_approver_list = sec_level_approver_config.strip('][').split(', ')
            if sec_level_approver_list:
                emps = self.env['hr.employee'].search([('job_id', 'in', [int(i) for i in sec_level_approver_list])])
                self.write({'last_approver_ids': [(6, 0, emps.ids)]})
                template_obj = self.env.ref('kw_recruitment.template_for_approval_witout_approver')
                jobs = self.env['hr.job'].search([('id', 'in', [int(rec) for rec in sec_level_approver_list])])
                if jobs:
                    approvers = self.env['hr.employee'].search([('job_id', 'in', jobs.ids)])
                    for approver in approvers:
                        # start :- activity code
                        res_model_id, res_id, date_deadline, note, activity_type_id = self.get_mail_activity_details()
                        if approver.user_id:
                            mail_activity.create({
                                'res_model_id': res_model_id,
                                'activity_type_id': activity_type_id,
                                'res_id': res_id,
                                'date_deadline': date_deadline,
                                'user_id': approver.user_id.id,
                                'note': note})
                        # end :- activity code
                        body = template_obj.body_html
                        body = body.replace('--approver--', approver.name)
                        if template_obj:
                            mail_values = {
                                'email_to': approver.work_email,
                                'body_html': body,
                            }
                            template_obj.write(mail_values)
                            mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.id, force_send=True)
                            body = body.replace(approver.name, '--approver--')
                            mail_values = {
                                'body_html': body,
                            }
                            template_obj.write(mail_values)
                            self.env.user.notify_success("Mail sent successfully.")
        return True

    def get_notification_user_emails(self):
        ir_config_params = self.env['ir.config_parameter'].sudo()
        approval_user_ids = ir_config_params.get_param('kw_recruitment.approval_user_ids') or False
        emails = len(literal_eval(approval_user_ids)) and ','.join(self.env['hr.employee'].sudo().browse(literal_eval(approval_user_ids)).mapped('work_email')) or ""
        # if self.approver_id:
        #     emails += f',{self.approver_id.work_email}' if len(emails) else self.approver_id.work_email
        return emails


    @api.multi
    def action_approved(self):
        ## Final Approval
        self.write({'state': 'approve','approved_user_id':self.env.user.id})
        # Mark done activity if found for current user
        mail_activity = self.env['mail.activity']
        res_model_id, res_id, _, _, _ = self.get_mail_activity_details()
        activity = mail_activity.search(
            [('res_id', '=', res_id), ('res_model_id', '=', res_model_id), ('user_id', '=', self._uid)])
        if activity:
                activity.action_feedback()  # makes done activity
        if self.approver_id:
            template_obj = self.env.ref('kw_recruitment.action_approval_final_template')
        else:
            template_obj = self.env.ref('kw_recruitment.action_approval_template')
        mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.id, force_send=True)
        self.env.user.notify_success("Mail sent successfully.")
        return True

    @api.multi
    def action_approved_forward(self):
        ## Approve & Forward
        mail_activity = self.env['mail.activity']
        Parameters = self.env['ir.config_parameter'].sudo()
        sec_approver_list = Parameters.get_param('kw_recruitment.second_level_approver_ids')
        coverted_sec = sec_approver_list.strip('][').split(', ')

        if coverted_sec:
            emps = self.env['hr.employee'].search([('job_id', 'in', [int(i) for i in coverted_sec])])

            self.write({'state': 'forward', 'last_approver_ids': [(6, 0, emps.ids)]})
            mail_activity = self.env['mail.activity']
            res_model_id, res_id, _, _, _ = self.get_mail_activity_details()
            activity = mail_activity.search(
                [('res_id', '=', res_id), ('res_model_id', '=', res_model_id), ('user_id', '=', self._uid)])
            if activity:
                activity.action_feedback()  # makes done activity
            template_obj = self.env.ref('kw_recruitment.approve_forward_request_template')
            jobs = self.env['hr.job'].search([('id', 'in', [int(rec) for rec in coverted_sec])])
            if jobs:
                approvers = self.env['hr.employee'].search([('job_id', 'in', jobs.ids)])
                for approver in approvers:
                    res_model_id, res_id, date_deadline, note, activity_type_id = self.get_mail_activity_details()
                    if approver.user_id:
                        mail_activity.create({
                            'res_model_id': res_model_id,
                            'activity_type_id': activity_type_id,
                            'res_id': res_id,
                            'date_deadline': date_deadline,
                            'user_id': approver.user_id.id,
                            'note': note})
                    body = template_obj.body_html
                    body = body.replace('--approver--', approver.name) 
                    if template_obj:
                        mail_values = {
                            'email_to': approver.work_email,
                            'body_html': body,
                        }
                        template_obj.write(mail_values)
                    mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.id, force_send=True)
                    body = body.replace(approver.name, '--approver--')
                    mail_values = {
                        'body_html': body,
                    }
                    template_obj.write(mail_values)
                    self.env.user.notify_success("Mail sent successfully.")
        return True

    @api.multi
    def action_revise(self):
        wizard_form = self.env.ref('kw_recruitment.mrf_revise_view', False)
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': wizard_form.id,
            'res_model': 'kw.mrf.revise',
            'target': 'new',
            'context': {'default_mrf_id': self.id}
        }

    @api.multi
    def action_hold(self):
        wizard_form = self.env.ref('kw_recruitment.mrf_hold_view', False)
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': wizard_form.id,
            'res_model': 'kw.mrf.hold',
            'target': 'new',
            'context': {'default_mrf_id': self.id}
        }

    def action_reject(self):
        wizard_form = self.env.ref('kw_recruitment.mrf_reject_view', False)
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': wizard_form.id,
            'res_model': 'kw.mrf.reject',
            'target': 'new',
            'context': {'default_mrf_id': self.id}
        }

    def action_create_job(self):
        createjob = self.env['kw_hr_job_positions']
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'view_id': createjob,
            'res_model': 'kw_hr_job_positions',
            'target': 'self',
            'context': {'default_department_id': self.dept_name.id,
                        'default_job_id': self.job_position.id ,
                        'default_address_id': [self.branch_id.id],
                        'default_min_exp_year': self.min_exp_year,
                        'default_max_exp_year': self.max_exp_year,
                        'default_no_of_recruitment': self.no_of_resource,
                        }
        }
