# -*- coding: utf-8 -*-
from . import res_config
from . import hr_job_in
from . import hr_applicant_in
from . import calender_event_in
# from . import kw_survey_type_master
from . import survey_survey_in
from . import survey_user_input_in
from . import kw_hr_job_positions
from . import kw_job_category
from . import kw_industry_type
from . import kw_qualification_master
from . import kw_recruitment_location
from . import kw_ir_attachment
from . import calendar_attendee_in
from . import requisition_category
from . import requisition_skill
from . import kw_requisition
from . import kw_source_master
from . import kw_recruitment_doc_store
from . import res_users