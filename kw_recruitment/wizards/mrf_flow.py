# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import time

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError
from datetime import date


class MRFholdConfirmation(models.TransientModel):
    _name = "kw.mrf.hold"
    _description = "MRF Hold"

    mrf_id = fields.Many2one('kw_recruitment_requisition', 'MRF')
    note = fields.Text('Comment', size=40)

    def save_message(self):
        mail_activity = self.env['mail.activity']
        res_model_id, res_id, _, _, _ = self.mrf_id.get_mail_activity_details()
        activity = mail_activity.search(
            [('res_id', '=', res_id), ('res_model_id', '=', res_model_id), ('user_id', '=', self._uid)])
        if activity:
                activity.action_feedback()
        template_obj = self.env.ref('kw_recruitment.template_for_hold_mrf')
        if self.mrf_id.approver_id and self.mrf_id.approver_id.user_id.id == self.env.user.id:
            # # PM --> AVP Flow
            body = template_obj.body_html
            body = body.replace('--name--', self.mrf_id.approver_id.name)
            body = body.replace('--receiver--', self.mrf_id.create_uid.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.create_uid.email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)

                body = body.replace(self.mrf_id.approver_id.name, '--name--')
                body = body.replace(self.mrf_id.create_uid.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'hold', 'note': self.note,'hold_user_id':self.env.user.id})
                self.env.user.notify_success("Mail sent successfully.")

        elif self.mrf_id.approver_id and self.mrf_id.approver_id.user_id.id != self.env.user.id:
            # # PM --> AVP --> CEO Flow
            body = template_obj.body_html
            body = body.replace('--name--', self.env.user.name)
            body = body.replace('--receiver--', self.mrf_id.approver_id.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.approver_id.work_email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)
                body = body.replace(self.env.user.name, '--name--')
                body = body.replace(self.mrf_id.approver_id.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'hold', 'note': self.note,'hold_user_id':self.env.user.id})
                self.env.user.notify_success("Mail sent successfully.")
        else:
            # # AVP --> CEO Flow (direct)
            body = template_obj.body_html
            body = body.replace('--receiver--', self.mrf_id.create_uid.name)
            body = body.replace('--name--', self.env.user.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.create_uid.email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)
                body = body.replace(self.env.user.name, '--name--')
                body = body.replace(self.mrf_id.create_uid.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'hold', 'note': self.note})
                self.env.user.notify_success("Mail sent successfully.")

        return {'type': 'ir.actions.act_window_close'}

    def cancel(self):
        return {'type': 'ir.actions.act_window_close'}


class MRFRejectConfirmation(models.TransientModel):
    _name = "kw.mrf.reject"
    _description = "MRF Reject"

    mrf_id = fields.Many2one('kw_recruitment_requisition', 'MRF')
    note = fields.Text('Comment', size=40)

    def save_message(self):
        mail_activity = self.env['mail.activity']
        res_model_id, res_id, _, _, _ = self.mrf_id.get_mail_activity_details()
        activity = mail_activity.search(
            [('res_id', '=', res_id), ('res_model_id', '=', res_model_id), ('user_id', '=', self._uid)])
        if activity:
                activity.action_feedback()
        if self.mrf_id.approver_id and self.mrf_id.approver_id.user_id.id == self.env.user.id:
            # # PM --> AVP Flow
            template_obj = self.env.ref('kw_recruitment.template_for_reject_first_mrf')
            body = template_obj.body_html
            body = body.replace('--name--', self.mrf_id.approver_id.name)
            body = body.replace('--receiver--', self.mrf_id.create_uid.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.create_uid.email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)
                body = body.replace(self.mrf_id.approver_id.name, '--name--')
                body = body.replace(self.mrf_id.create_uid.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'reject', 'note': self.note})
                self.env.user.notify_success("Mail sent successfully.")

        elif self.mrf_id.approver_id and self.mrf_id.approver_id.user_id.id != self.env.user.id:
            # # PM --> AVP --> CEO Flow
            template_obj = self.env.ref('kw_recruitment.template_for_reject_first_mrf')
            body = template_obj.body_html
            body = body.replace('--name--', self.env.user.name)
            body = body.replace('--receiver--', self.mrf_id.approver_id.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.approver_id.work_email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)

                body = body.replace(self.env.user.name, '--name--')
                body = body.replace(self.mrf_id.approver_id.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'reject', 'note': self.note})
                self.env.user.notify_success("Mail sent successfully.")
        else:
            # # AVP --> CEO Flow (direct)
            template_obj = self.env.ref('kw_recruitment.template_for_reject_first_mrf')
            body = template_obj.body_html
            body = body.replace('--receiver--', self.mrf_id.create_uid.name)
            body = body.replace('--name--', self.env.user.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.create_uid.email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)

                body = body.replace(self.env.user.name, '--name--')
                body = body.replace(self.mrf_id.create_uid.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'reject', 'note': self.note})
                self.env.user.notify_success("Mail sent successfully.")

        return {'type': 'ir.actions.act_window_close'}

    def cancel(self):
        return {'type': 'ir.actions.act_window_close'}


class MRFReviseConfirmation(models.TransientModel):
    _name = "kw.mrf.revise"
    _description = "MRF Revise"

    mrf_id = fields.Many2one('kw_recruitment_requisition', 'MRF')
    note = fields.Text('Comment', size=40)

    def save_message(self):
        mail_activity = self.env['mail.activity']
        res_model_id, res_id, _, _, _= self.mrf_id.get_mail_activity_details()
        activity = mail_activity.search(
            [('res_id', '=', res_id), ('res_model_id', '=', res_model_id), ('user_id', '=', self._uid)])
        if activity:
                activity.action_feedback()
        if self.mrf_id.approver_id and self.mrf_id.approver_id.user_id.id == self.env.user.id:
            # # PM --> AVP Flow
            template_obj = self.env.ref('kw_recruitment.template_for_revise_mrf')
            body = template_obj.body_html
            body = body.replace('--name--', self.mrf_id.approver_id.name)
            body = body.replace('--receiver--', self.mrf_id.create_uid.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.create_uid.email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)

                body = body.replace(self.mrf_id.approver_id.name, '--name--')
                body = body.replace(self.mrf_id.create_uid.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'draft', 'note': self.note})
                self.env.user.notify_success("Mail sent successfully.")

        elif self.mrf_id.approver_id and self.mrf_id.approver_id.user_id.id != self.env.user.id:
            # # PM --> AVP --> CEO Flow
            template_obj = self.env.ref('kw_recruitment.template_for_revise_mrf')
            body = template_obj.body_html
            body = body.replace('--name--', self.env.user.name)
            body = body.replace('--receiver--', self.mrf_id.approver_id.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.approver_id.work_email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)

                body = body.replace(self.env.user.name, '--name--')
                body = body.replace(self.mrf_id.approver_id.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'draft', 'note': self.note})
                self.env.user.notify_success("Mail sent successfully.")
        else:
            # # AVP --> CEO Flow (direct)
            template_obj = self.env.ref('kw_recruitment.template_for_revise_mrf')
            body = template_obj.body_html
            body = body.replace('--receiver--', self.mrf_id.create_uid.name)
            body = body.replace('--name--', self.env.user.name)
            if template_obj:
                mail_values = {
                    'email_to': self.mrf_id.create_uid.email,
                    'body_html': body,
                }
                template_obj.write(mail_values)
                mail = self.env['mail.template'].browse(template_obj.id).send_mail(self.mrf_id.id, force_send=True)

                body = body.replace(self.env.user.name, '--name--')
                body = body.replace(self.mrf_id.create_uid.name, '--receiver--')
                mail_values = {
                    'body_html': body,
                }
                template_obj.write(mail_values)
                self.mrf_id.write({'state': 'draft', 'note': self.note})
                self.env.user.notify_success("Mail sent successfully.")
        return {'type': 'ir.actions.act_window_close'}

    def cancel(self):
        return {'type': 'ir.actions.act_window_close'}

