# -*- coding: utf-8 -*-
{
    'name': "Kwantify Meeting DMS Integration",
    'summary': """Store meeting materials & MOMs at dms""",
    'description': """Store meeting materials & MOMs at dms""",
    'author': "CSM Technologies",
    'website': "http://www.csm.co.in",
    'category': 'DMS/Integration',
    'version': '0.1',
    'depends': ['base', 'kw_meeting_schedule', 'kw_dms'],
    'data': [
        'data/data_kw_meeting_dms_integration.xml',
        'views/res_config_setting.xml',
    ],
    "application": False,
    "installable": True,
    'auto_install': False,
}
