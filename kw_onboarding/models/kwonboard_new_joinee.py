from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import ValidationError



class kwonboard_new_joinee(models.Model):
    _name='kwonboard_new_joinee'
    _description='Onboarding New Joinee'
    _rec_name='enrollment_id'

    employee_code = fields.Char('Employee Code')
    enrollment_id=fields.Many2one('kwonboard_enrollment', string="New Joinee", domain=[('state','=','5')])
    fullname=fields.Char(string="Full Name")
    username=fields.Char(string="Username")
    dept_id=fields.Many2one('hr.department', string="Department", domain=[('dept_type.code', '=', 'department')])
    division_id=fields.Many2one('hr.department', string="Division", domain=[('dept_type.code', '=', 'division')])
    section_id=fields.Many2one('hr.department', string="Section", domain=[('dept_type.code', '=', 'section')])
    practise_id=fields.Many2one('hr.department', string="Practise", domain=[('dept_type.code', '=', 'practise')])
    designation=fields.Many2one('hr.job', string="Designation")
    tagged_to=fields.Many2one('kw_tag_master', string="Tagged To")
    proj_bill_amnt=fields.Integer(string="Project Billing Amount")
    func_area=fields.Many2one('kw_industry_type', string="Functional Area")
    job_loc_id=fields.Many2one('kw_recruitment_location', string="Job Location")
    office_unit=fields.Many2one('kw_res_branch', string="Office Unit")
    grade=fields.Many2one('kwemp_grade', string="Grade")
    band=fields.Many2one('kwemp_band_master', string="Band")
    join_date=fields.Date(string="Joining Date")
    prob_compl_date=fields.Date(string="Probation Complete Date")
    confirm_status=fields.Selection(selection=[('on_probation', 'On Probation'),
                                                ('confirmed','Confirmed')], string="Confirmation Status", default='on_probation')
    emp_role = fields.Many2one('kwmaster_role_name', ondelete='cascade', string="Employee Role")
    emp_category = fields.Many2one('kwmaster_category_name', ondelete='cascade', string="Employee Category")
    employ_type=fields.Many2one('kwemp_employment_type', ondelete='cascade', string="Type of Employment")
    id_card=fields.Selection([('required','Required'),('not required','Not Required')],
                                string="ID Card")
    image=fields.Binary(string="Upload Photo", attachment=True,
                          help="Only .jpeg,.png,.jpg format are allowed. Maximum file size is 1 MB",store=True)
    ## Personal Information ##
    dob=fields.Date(string="Date of Birth")
    gender=fields.Selection(selection=[('male', 'Male'), ('female', 'Female'), ('others', 'Others')], string="Gender")
    religion=fields.Many2one('kwemp_religion_master', string="Religion")
    ##Modification by Salma
    source_id = fields.Many2one('utm.source', string="Reference Mode")
    employee_referred = fields.Many2one('hr.employee', string='Referred By')
    service_partner_id = fields.Many2one('res.partner', string='Partner')
    media_id = fields.Many2one('kw.social.media', string='Social Media')
    institute_id = fields.Many2one('res.partner', string='Institute')
    consultancy_id = fields.Many2one('res.partner', string='Consultancy')
    jportal_id = fields.Many2one('kw.job.portal', string='Job Portal')
    reference = fields.Char("Client Name")
    reference_walkindrive = fields.Char("Walk-in Drive")
    reference_print_media = fields.Char("Print Media")
    reference_job_fair = fields.Char("Job Fair")
    code_ref = fields.Char('Code')
    marital_ref = fields.Char('M Code')
    ## Reporting Authority ##
    admin_auth=fields.Many2one('hr.employee', string="Administrative Authority")
    func_auth=fields.Many2one('hr.employee', string="Functional Authority")
    ### field update on 28th May ###
    ## Account Information ##
    password=fields.Char(string="Password", )
    confirm_password=fields.Char(string="Confirm Password",)
    domain_name=fields.Char(string="Domain Name")
    email_id=fields.Char(string="Email ID", )
    card_no=fields.Char(string="Card No", readonly=True)
    project_id=fields.Many2one('crm.lead', string="Project")
    # job_profile=fields.Char(string="Job Profile")
    job_position=fields.Many2one('hr.job', string="Job Position")
    shift=fields.Many2one('resource.calendar', string="Shift")
    super_admin_access=fields.Boolean(string="Super Admin Previlege")
    ## Payroll Information ##
    enable_payroll=fields.Selection([('yes','Yes'),('no','No')], string="Enable Payroll", default='no')
    enable_epf=fields.Selection([('yes','Yes'),('no','No')], string="EPF")
    enable_gratuity=fields.Selection([('yes','Yes'),('no','No')], string="Gratuity")
    ## Salary Information ##
    at_join_time=fields.Integer(string="At Joining Time", )
    current=fields.Integer(string="Current")
    ## Basic Amount ##
    basic_at_join_time=fields.Integer(string="At Joining Time", readonly=True)
    bank=fields.Many2one('res.partner.bank', string="Bank Name")
    account_no=fields.Integer(string="Account No")
    ## Allowances Information ##
    hra=fields.Integer(string="HRA(%)")
    conveyance=fields.Integer(string="Conveyance")
    medical_reimb=fields.Integer(string="Medical Reimbursement")
    transport=fields.Integer(string="Transport")
    productivity=fields.Integer(string="Productivity Allowance")
    commitment=fields.Integer(string="Commitment Allowance")
    ## Personnel Information ##
    marital_stat=fields.Many2one('kwemp_maritial_master', string="Marital Status")
    refered_by_emp=fields.Many2one('hr.employee', string="Refered By")
    ## Attendance Information ##
    enable_attendance=fields.Selection([('yes','Yes'),('no','No')], string="Enable Attendance", default='no')
    attendance_mode=fields.Many2many('kw_attendance_mode_master', string="Attendance Mode")
    ### field update on 29th May ###
    ## Personnel Information ##
    marriage_date=fields.Date(string="Marriage Date")
    ## Account Information ##
    workstation=fields.Many2one('kw_workstation_master', string="Workstation")

    state=fields.Selection([('draft','Draft'),('authenticated','Authenticated'),
                            ('rejected','Rejected')], default='draft', track_visibility='onchange')

    ## Fields by Salma
    emp_user_created = fields.Boolean('Emp/User Created',default=False)
    emp_user_synced = fields.Boolean('Emp/User Synced',default=False)
    user_count = fields.Integer('Users',compute='_compute_user_emp')


    _sql_constraints = [
        ('username_uniq', 'unique(username)', 'Username must be unique!'),
    ]

    def _compute_user_emp(self):
        for record in self:
            user = self.env['res.users'].search_count([('email','=', record.email_id)])
            empl = self.env['hr.employee'].search_count([('work_email','=', record.email_id)])
            record.user_count = user

    @api.onchange('enrollment_id')
    def onchange_enrollment_id(self):
        enrollment = self.env['kwonboard_enrollment'].search([('name','=', self.enrollment_id.name)])
        self.fullname = enrollment.name
        self.dept_id = enrollment.dept_name
        self.division_id = enrollment.division
        self.section_id = enrollment.section
        self.practise_id = enrollment.practise
        self.designation = enrollment.job_id
        self.emp_role = enrollment.emp_role
        self.emp_category = enrollment.emp_category
        self.employ_type = enrollment.employement_type
        self.office_unit = enrollment.location_id
        self.dob = enrollment.birthday
        self.gender = enrollment.gender
        self.religion = enrollment.emp_religion
        self.email_id = enrollment.csm_email_id
        self.card_no = enrollment.id_card_no
        self.marital_stat = enrollment.marital
        self.marriage_date = enrollment.wedding_anniversary
        self.image = enrollment.image
        self.job_position = enrollment.job_id

    @api.onchange('at_join_time')
    def onchange_at_join_time(self):
        self.basic_at_join_time = self.at_join_time

    @api.onchange('source_id')
    def onchange_source(self):
        self.code_ref = self.source_id.code

    @api.onchange('marital_stat')
    def onchange_marital_stat(self):
        self.marital_ref = self.marital_stat.code

    @api.one
    @api.constrains('password', 'confirm_password')
    def _check_passwords(self):
        if self.password != self.confirm_password:
            raise ValidationError("Password and Confirm Password must be same")

    @api.model
    def create(self, values):
        record = super(kwonboard_new_joinee, self).create(values)
        if record:
            self.env.user.notify_success(message='New Joinee has been Created Successfully.')
        return record

    @api.multi
    def write(self, values):
        self.ensure_one()
        record = super(kwonboard_new_joinee, self).write(values)
        self.env.user.notify_success(message='New Joinee has been Updated Successfully.')
        return record

    @api.multi
    def new_joinee_authenticate(self):
        for rec in self:
            rec.write({'state': 'authenticated'})
        return True

    @api.multi
    def new_joinee_cancel(self):
        for rec in self:
            rec.write({'state': 'cancelled'})
        return True

    @api.multi
    def create_employee_user(self):
        self.ensure_one()
        emp_vals = {}
        user_vals = {}
        for rec in self:
            ##Create User
            user = self.env['res.users'].search(['|',('login','=', rec.username),('email','=', rec.email_id)])
            if user:
                raise ValidationError("User already exists!")
            else:
                user_vals['image'] = rec.image
                user_vals['name'] = rec.fullname
                user_vals['email'] = rec.email_id
                user_vals['password'] = rec.password
                user_vals['login'] = rec.username
                user_vals['branch_id'] = rec.office_unit.id
                user_vals['new_joinee_id'] = rec.id

                new_user_rec = self.env["res.users"].sudo().create(user_vals)
                if new_user_rec or new_emp_rec:
                    rec.write({
                        # 'user_id': new_user_rec.id if new_user_rec else False ,
                        'emp_user_created':True,
                        # 'emp_id': new_emp_rec.id if new_emp_rec else False
                        })
            ##Create Employee
            emp = self.env['hr.employee'].search([('work_email','=', rec.email_id)])
            if emp:
                raise ValidationError("Employee already exists!")
            else:
                emp_vals['image'] = rec.image
                emp_vals['name'] = rec.fullname
                emp_vals['work_email'] = rec.email_id
                emp_vals['department_id'] = rec.dept_id.id
                emp_vals['division'] = rec.division_id.id
                emp_vals['section'] = rec.section_id.id
                emp_vals['practise'] = rec.practise_id.id
                emp_vals['job_id'] = rec.designation.id
                emp_vals['work_location'] = rec.job_loc_id.id
                emp_vals['resource_calender_id'] = rec.shift.id
                emp_vals['birthday'] = rec.dob
                emp_vals['gender'] = rec.gender
                emp_vals['marital'] = rec.marital_stat.id
                emp_vals['bank_account_id'] = rec.bank.id

                new_emp_rec = self.env["hr.employee"].sudo().create(emp_vals)
                
            
            self.env.user.notify_success(message='Congrats ! User Creation completed.')



    @api.multi
    def sync_employee_user(self):
        return
            