# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import ValidationError
from odoo.http import request
import re, random, string
import time
import datetime
from datetime import date, datetime
from lxml import etree
import os, base64
# import mimetypes
# from odoo.tools.mimetypes import guess_mimetype
# from datetime import date, datetime

from kw_utility_tools import kw_validations


# Model Name    : kwonboard_enrollment
# Description   :  For the enrollment of the new employees , a reference number is created and Email will be sent to the employee
# Created By    :
# Created On    : 13-Jun-2019

class kwonboard_enrollment(models.Model):
    _name = 'kwonboard_enrollment'
    _description = "Candidate Enrollment"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    otp_number = fields.Char()
    generate_time = fields.Datetime(string="Expire Datetime")
    dept_name = fields.Many2one('hr.department', string="Department", required=True,
                                domain=[('dept_type.code', '=', 'department')], help="")
    division = fields.Many2one('hr.department', string="Division", domain=[('dept_type.code', '=', 'division')])
    section = fields.Many2one('hr.department', string="Section", domain=[('dept_type.code', '=', 'section')])
    practise = fields.Many2one('hr.department', string="Practise", domain=[('dept_type.code', '=', 'practise')])

    job_id = fields.Many2one('hr.job', string="Designation", help="")
    name = fields.Char(string="Candidate Name", required=True, size=100, help="",
                       read=['base.group_user', 'kw_employee.group_hr_finance', 'kw_employee.group_hr_admin',
                             'kw_employee.group_hr_nsa'])
    firstname = fields.Char(string="Candidate first name", help="")
    middlename = fields.Char(string="Candidate middle name", help="")
    lastname = fields.Char(string="Candidate last name", help="")
    email = fields.Char(string="Personal Email Id", size=50, required=True, help="")
    mobile = fields.Char(String="Mobile No", size=15, required=True, help="")
    whatsapp_no = fields.Char(String="Whatsapp No", size=15)
    reference_no = fields.Char()
    emp_id = fields.Many2one('hr.employee')
    company_id = fields.Many2one('res.company', string='Company', index=True, required=True,
                                 default=lambda self: self.env.user.company_id)
    location_id = fields.Many2one('kw_res_branch', string="Work Location")
    work_location_id = fields.Many2one('res.partner', string="Office Location",
                                       domain="[('parent_id', '=', company_id)]")
    work_location = fields.Char(string="Work Location", related='work_location_id.city', readonly=True)

    state = fields.Selection(
        [('1', 'Enrolled'), ('2', 'Profile Created'), ('3', 'Configuration Mapping'), ('4', 'Configuration Done'),
         ('5', 'Approved')], required=True, default='1', track_visibility='onchange')

    # START : APPROVAL FIELDS#
    system_configuration = fields.Many2many('kwonboard_config_type', string="Environment Configuration")
    admin_setting_status = fields.Boolean("Admin Setting", default=False)
    nsa_setting_status = fields.Boolean("IT Setting", default=False)
    finance_setting_status = fields.Boolean("Finance Setting", default=False)
    create_full_profile = fields.Boolean("Create Full Profile", default=False)
    display_nsa_btn = fields.Boolean("IT Button Display", default=False, compute="_compute_config_setting")

    # user status field if the user beongs to the selected service group
    user_grp_id_card = fields.Boolean("ID Card Group User", default=False, compute="_compute_config_setting")
    user_grp_budget = fields.Boolean("Budget Group User", default=False, compute="_compute_config_setting")
    user_grp_outlook = fields.Boolean("Outlook Group User", default=False, compute="_compute_config_setting")
    user_grp_biometric = fields.Boolean("Biometric Group User", default=False, compute="_compute_config_setting")
    user_grp_domain_pwd = fields.Boolean("Domain Group User", default=False, compute="_compute_config_setting")
    user_grp_epbx = fields.Boolean("EPBX Group User", default=False, compute="_compute_config_setting")
    other_user_status = fields.Boolean("NOT HR MANAGER Group User", default=False, compute="_compute_config_setting")
    emp_role = fields.Many2one('kwmaster_role_name', ondelete='cascade', string="Employee Role")
    emp_category = fields.Many2one('kwmaster_category_name', ondelete='cascade', string="Employee Category")
    employement_type = fields.Many2one('kwemp_employment_type', ondelete='cascade', string="Type of Employment")
    id_card_no = fields.Char(string=u'ID Card No', )
    csm_email_id = fields.Char(string=u'Outlook ID', )
    biometric_id = fields.Char(string=u'Biometric ID', )
    outlook_pwd = fields.Char(string=u'Outlook Password', )
    epbx_no = fields.Char(string=u'EPBX', )
    domain_login_id = fields.Char(string=u'Domain ID ', )
    domain_login_pwd = fields.Char(string=u'Domain Password ', )
    # END: APPROVAL FIELDS#

    # # Personal Details ##
    birthday = fields.Date(string="Date of Birth", )
    blood_group = fields.Many2one('kwemp_blood_group_master', string="Blood Group")
    gender = fields.Selection(string="Gender",
                              selection=[('male', 'Male'), ('female', 'Female'), ('others', 'Others')], )
    country_id = fields.Many2one('res.country', string='Nationality')
    emp_religion = fields.Many2one('kwemp_religion_master', string="Religion")
    marital = fields.Many2one('kwemp_maritial_master', string='Marital Status')
    marital_code = fields.Char(string=u'Marital Status Code ', related='marital.code')

    wedding_anniversary = fields.Date(string=u'Wedding Anniversary', )
    image = fields.Binary(string="Upload Photo", attachment=True,
                          help="Only .jpeg,.png,.jpg format are allowed. Maximum file size is 1 MB",
                          inverse="_inverse_field")
    image_name = fields.Char(string=u'Image Name', )

    present_addr_country_id = fields.Many2one('res.country', string="Present Address Country")
    present_addr_street = fields.Text(string="Present Address Line 1", size=500)
    present_addr_street2 = fields.Text(string="Present Address Line 2", size=500)
    present_addr_city = fields.Char(string="Present Address City", size=100)
    present_addr_state_id = fields.Many2one('res.country.state', string="Present Address State")
    present_addr_zip = fields.Char(string="Present Address ZIP", size=10)
    # Present address : End
    same_address = fields.Boolean(string=u'Same as Present Address', default=False)
    # Permanent address : start
    permanent_addr_country_id = fields.Many2one('res.country', string="Permanent Address Country")
    permanent_addr_street = fields.Text(string="Permanent Address Line 1", size=500)
    permanent_addr_street2 = fields.Text(string="Permanent Address Line 2", size=500)
    permanent_addr_city = fields.Char(string="Permanent Address City", size=100)
    permanent_addr_state_id = fields.Many2one('res.country.state', string="Permanent Address State")
    permanent_addr_zip = fields.Char(string="Permanent Address ZIP", size=10)

    mother_tounge_ids = fields.Many2one('kwemp_language_master', string='Mother Tongue')
    # Permanent address : End
    known_language_ids = fields.One2many('kwonboard_language_known', 'enrole_id', string='Language Known')
    # START : Educational Details##
    educational_ids = fields.One2many('kwonboard_edu_qualification', 'enrole_id', string="Educational Details")
    # END : Educational Details##

    # START : Work Experience details ###
    experience_sts = fields.Selection(string="Work Experience ", selection=[('1', 'Fresher'), ('2', 'Experience')], )
    work_experience_ids = fields.One2many('kwonboard_work_experience', 'enrole_id', string='Work Experience Details')
    # END : Work Experience details ###

    # START :for identification details ###
    identification_ids = fields.One2many('kwonboard_identity_docs', 'enrole_id', string='Indentification Documents')

    applicant_id = fields.Many2one('hr.applicant', string="Applicant", domain=[('kw_enrollment_id','=', False),('stage_id.code','=','OA')])

    # END : for identification details ###

    #
    @api.model
    def default_get(self, fields):
        result = super(kwonboard_enrollment, self).default_get(fields)
        configuration_type_lines = []
        types = self.env['kwonboard_config_type'].search([])
        for typ in types:
            configuration_type_lines.append(typ.id)
        result.update({
                    'system_configuration':[(6, 0, configuration_type_lines)],
                   })
        return result

    @api.onchange('applicant_id')
    def onchange_applicant_id(self):
        self.name = self.applicant_id.partner_name
        self.dept_name = self.applicant_id.department_id
        self.job_id = self.applicant_id.job_id
        self.email = self.applicant_id.email_from
        self.mobile = self.applicant_id.partner_mobile
        self.division = self.applicant_id.division
        self.section = self.applicant_id.section
        self.practise = self.applicant_id.practise
        if self.applicant_id.job_position.mrf_id:
            self.emp_role = self.applicant_id.job_position.mrf_id.role_id 
            self.emp_category = self.applicant_id.job_position.mrf_id.categ_id 
            self.employment_type = self.applicant_id.job_position.mrf_id.type_employment 

    @api.onchange('dept_name')
    def onchange_department(self):
        domain = {}
        for rec in self:
            domain['division'] = [('parent_id', '=', rec.dept_name.id),('dept_type.code', '=', 'division')]
            return {'domain': domain}

    @api.onchange('division')
    def onchange_division(self):
        domain = {}
        for rec in self:
            if rec.dept_name:
                domain['section'] = [('parent_id', '=', rec.division.id),('dept_type.code', '=', 'section')]
                return {'domain': domain}

    @api.onchange('section')
    def onchange_section(self):
        domain = {}
        for rec in self:
            if rec.section:
                domain['practise'] = [('parent_id', '=', rec.section.id),('dept_type.code', '=', 'practice')]
                return {'domain': domain}
                
    @api.onchange('company_id')
    def _onchange_company_id(self):
        self.work_location_id = False
        return {'domain': {'work_location_id': [('parent_id', '=', self.company_id.partner_id.id)], }}

    @api.model
    def _inverse_field(self):
        if self.image:
            bin_value = base64.b64decode(self.image)
            if not os.path.exists('onboarding_docs/' + str(self.id)):
                os.makedirs('onboarding_docs/' + str(self.id))
            full_path = os.path.join(os.getcwd() + '/onboarding_docs/' + str(self.id), self.image_name)
            # if os.path.exists(full_path):
            #     raise ValidationError("The file name "+self.filename+" exists.Please change your file name.")
            try:
                with open(os.path.expanduser(full_path), 'wb') as fp:
                    fp.write(bin_value)
                    fp.close()
            except Exception as e:
                print(e)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(kwonboard_enrollment, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                                submenu=submenu)
        # print(self.env.user.groups_id)
        # print(self._context.get('params')) web.base.url
        doc = etree.XML(res['arch'])
        # #condition for readonly fields, for other user groups
        if not self.env.user.multi_has_groups(['kw_onboarding.group_kw_onboarding_manager', ]):
            # print(doc.xpath("//group[@name='employee_enrollment']"))
            for node in doc.xpath("//field[@name='company_id']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='name']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='image']"):
                node.set('modifiers', '{"readonly": true}')
            # for node in doc.xpath("//group[@name='company_id']"):
            #     node.set('modifiers', '{"readonly": true}')

            for node in doc.xpath("//field[@name='work_location_id']"):
                node.set('modifiers', '{"readonly": true}')

            for node in doc.xpath("//field[@name='dept_name']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='job_id']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='email']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='mobile']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='create_full_profile']"):
                node.set('modifiers', '{"readonly": true}')
        res['arch'] = etree.tostring(doc)
        return res

    @api.onchange('same_address')
    def _change_permanent_address(self):
        if self.same_address:
            self.permanent_addr_country_id = self.present_addr_country_id
            self.permanent_addr_street = self.present_addr_street
            self.permanent_addr_street2 = self.present_addr_street2
            self.permanent_addr_city = self.present_addr_city
            self.permanent_addr_state_id = self.present_addr_state_id
            self.permanent_addr_zip = self.present_addr_zip

    # on change of marital status compute marital status code
    # @api.onchange('marital')
    # def _compute_marital_status_code(self):
    #     if self.marital:
    #         self.marital_code = self.marital.code
    #     else:
    #         self.marital_code = ''

    @api.depends('system_configuration')
    def _compute_config_setting(self):
        for config_rec in self.system_configuration:
            # print(self.env.user.groups_id)
            for sel_group in config_rec.authorized_group:
                # #if the group is selected grouyp or HR manage group
                if sel_group in self.env.user.groups_id:
                    self.other_user_status = True

            if self.env.user.has_group('kw_onboarding.group_kw_onboarding_manager'):
                self.other_user_status = False

            if config_rec.configuration_type == '1':
                self.user_grp_id_card = True
            elif config_rec.configuration_type == '2':
                self.user_grp_budget = True
            elif config_rec.configuration_type == '3':
                self.user_grp_outlook = True
            elif config_rec.configuration_type == '4':
                self.user_grp_biometric = True
            elif config_rec.configuration_type == '5':
                self.user_grp_domain_pwd = True
            elif config_rec.configuration_type == '6':
                self.user_grp_epbx = True

        if (self.user_grp_outlook or self.user_grp_biometric or self.user_grp_domain_pwd or self.user_grp_epbx) and (
                not self.nsa_setting_status and self.state == '3'):
            self.display_nsa_btn = True

    # onchange of present address country change the state
    @api.onchange('present_addr_country_id')
    def _change_present_address_state(self):
        country_id = self.present_addr_country_id.id
        self.present_addr_state_id = False
        return {'domain': {'present_addr_state_id': [('country_id', '=', country_id)], }}

    # onchange of employee category change the role
    @api.onchange('emp_role')
    def _get_categories(self):
        role_id = self.emp_role.id
        self.emp_category = False
        return {'domain': {'emp_category': [('role_ids', '=', role_id)], }}

    # onchange of permanent address country change the state
    @api.onchange('permanent_addr_country_id')
    def _change_permanent_address_state(self):
        country_id = self.permanent_addr_country_id.id
        self.permanent_addr_state_id = False
        if self.same_address and self.present_addr_state_id and (self.permanent_addr_country_id == self.present_addr_country_id):
            self.permanent_addr_state_id = self.present_addr_state_id
        return {'domain': {'permanent_addr_state_id': [('country_id', '=', country_id)], }}

    # START : Button Actions associated ##
    @api.multi
    def complete_env_mapping(self):
        for rec in self:
            if rec.system_configuration:
                admin_setting_status = True
                finance_setting_status = True
                nsa_setting_status = True
                # config_user_id_list = []
                for config_rec in rec.system_configuration:
                    if config_rec.configuration_type == '1':
                        admin_setting_status = False
                    elif config_rec.configuration_type == '2':
                        finance_setting_status = False
                    else:
                        nsa_setting_status = False

                    for group in config_rec.authorized_group:
                        for user in group.users:
                            # if user.id not in config_user_id_list:
                            if config_rec.configuration_type == '1':
                                rec.activity_schedule('kw_onboarding.mail_act_env_config_admin', fields.Date.today(), user_id=user.id)
                            elif config_rec.configuration_type == '2':
                                rec.activity_schedule('kw_onboarding.mail_act_env_config_finance', fields.Date.today(), user_id=user.id)
                            else:
                                rec.activity_schedule('kw_onboarding.mail_act_env_config_nsa', fields.Date.today(), user_id=user.id)
                                # config_user_id_list.append(user.id)
                            # ,author_id=self.env.user.partner_id.id
                # change the status into mapped
                rec.write({'state': '3', 'admin_setting_status': admin_setting_status, 'finance_setting_status': finance_setting_status, 'nsa_setting_status': nsa_setting_status})
            else:
                raise ValidationError(_('Please choose the required configurations'))
        return True

    @api.multi
    def button_take_action(self):
        view_id = self.env.ref('kw_onboarding.kwonboard_enrollment_view_form').id
        target_id = self.id
        return {
            'name': 'Configure Environment',
            'type': 'ir.actions.act_window',
            'res_model': 'kwonboard_enrollment',
            'res_id': target_id,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'target': 'new',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'flags': {'action_buttons': True},
        }

    @api.multi
    def complete_nsa_setting(self):
        for rec in self:
            for config_rec in rec.system_configuration:
                if config_rec.configuration_type == '3' and not (rec.csm_email_id and rec.outlook_pwd):
                    raise ValidationError(_(' IT settings can not be completed without the outlook configuration details'))

                elif config_rec.configuration_type == '4' and not (rec.csm_email_id and rec.outlook_pwd):
                    raise ValidationError(_(' IT settings can not be completed without the outlook configuration details'))

                elif config_rec.configuration_type == '5' and not (rec.domain_login_id or rec.domain_login_pwd):
                    raise ValidationError(_('IT settings can not be completed without system domain configuration details'))

                elif config_rec.configuration_type == '6' and not rec.epbx_no:
                    raise ValidationError(_('IT settings can not be completed without EPBX configuration details'))
            new_state = '3'

            if rec.admin_setting_status and rec.finance_setting_status:
                new_state = '4'

            self.write({'state': new_state, 'nsa_setting_status': True})
            if rec.nsa_setting_status == True:
                self.env.user.notify_info(message='IT settings Completed')
                # code to complte the activity
                rec.activity_feedback(['kw_onboarding.mail_act_env_config_nsa'])
        return True

    @api.multi
    def complete_finance_setting(self):
        for rec in self:
            if rec.emp_role and rec.emp_category and rec.employement_type:
                new_state = '3'
                if rec.nsa_setting_status and rec.admin_setting_status:
                    new_state = '4'
                rec.write({'state': new_state, 'finance_setting_status': True})

                # code to complte the activity
                rec.activity_feedback(['kw_onboarding.mail_act_env_config_finance'])
                self.env.user.notify_info(message='Finance settings Completed')
            else:
                raise ValidationError(_('Please enter all the setting details'))
        return True

    @api.multi
    def complete_admin_setting(self):
        for rec in self:
            if rec.id_card_no:
                new_state = '3'
                if rec.nsa_setting_status and rec.finance_setting_status:
                    new_state = '4'
                rec.write({'state': new_state, 'admin_setting_status': True})
                # code to complte the activity
                rec.activity_feedback(['kw_onboarding.mail_act_env_config_admin'])
                self.env.user.notify_info(message='Admin settings Completed')
            else:
                raise ValidationError(_('Please enter ID Card No'))
        return True

    @api.multi
    def make_enrole_to_employee(self):
        self.ensure_one()
        emp_vals = {}
        for rec in self:
            # check for validations for selected services
            for config_rec in rec.system_configuration:
                if config_rec.configuration_type == '1' and not rec.id_card_no:
                    raise ValidationError(_('Please provide all admin setting details before proceeding for approval'))
                elif config_rec.configuration_type == '2' and not (rec.emp_role or rec.emp_category or rec.employement_type):
                    raise ValidationError(_('Please provide all finance setting details before proceeding for approval'))
                elif config_rec.configuration_type == '3' and not (rec.csm_email_id or rec.outlook_pwd):
                    raise ValidationError(_('Please provide all IT setting details before proceeding for approval'))
                elif config_rec.configuration_type == '4' and not rec.biometric_id:
                    raise ValidationError(_('Please provide all IT setting details before proceeding for approval'))
                elif config_rec.configuration_type == '5' and not (rec.domain_login_id or rec.domain_login_pwd):
                    raise ValidationError(_('Please provide all IT setting details before proceeding for approval'))
                elif config_rec.configuration_type == '6' and not rec.epbx_no:
                    raise ValidationError(_('Please provide all IT setting details before proceeding for approval'))
            # End : validation check

            emp_vals['name'] = rec.name
            emp_vals['job_id'] = rec.job_id.id
            emp_vals['department_id'] = rec.dept_name.id
            emp_vals['mobile_phone'] = rec.mobile
            emp_vals['personal_email'] = rec.email
            emp_vals['birthday'] = rec.birthday
            emp_vals['blood_group'] = rec.blood_group.id

            emp_vals['gender'] = rec.gender
            emp_vals['country_id'] = rec.country_id.id
            emp_vals['emp_religion'] = rec.emp_religion.id
            emp_vals['marital'] = rec.marital.id
            emp_vals['marital_code'] = rec.marital_code
            emp_vals['wedding_anniversary'] = rec.wedding_anniversary
            emp_vals['image'] = rec.image
            emp_vals['company_id'] = rec.company_id.id
            emp_vals['work_location_id'] = rec.work_location_id.id
            emp_vals['work_location'] = rec.work_location
            emp_vals['experience_sts'] = rec.experience_sts

            emp_vals['present_addr_country_id'] = rec.present_addr_country_id.id
            emp_vals['present_addr_street'] = rec.present_addr_street
            emp_vals['present_addr_street2'] = rec.present_addr_street2
            emp_vals['present_addr_city'] = rec.present_addr_city
            emp_vals['present_addr_state_id'] = rec.present_addr_state_id.id
            emp_vals['present_addr_zip'] = rec.present_addr_zip

            emp_vals['same_address'] = rec.same_address
            emp_vals['permanent_addr_street'] = rec.permanent_addr_street
            emp_vals['permanent_addr_street2'] = rec.permanent_addr_street2
            emp_vals['permanent_addr_city'] = rec.permanent_addr_city
            emp_vals['permanent_addr_state_id'] = rec.permanent_addr_state_id.id
            emp_vals['permanent_addr_zip'] = rec.permanent_addr_zip
            emp_vals['permanent_addr_country_id'] = rec.permanent_addr_country_id.id

            emp_vals['emp_role'] = rec.emp_role.id
            emp_vals['emp_category'] = rec.emp_category.id
            emp_vals['employement_type'] = rec.employement_type.id

            emp_vals['id_card_no'] = rec.id_card_no

            emp_vals['work_email'] = rec.csm_email_id
            emp_vals['outlook_pwd'] = rec.outlook_pwd

            emp_vals['biometric_id'] = rec.biometric_id
            emp_vals['epbx_no'] = rec.epbx_no

            emp_vals['domain_login_id'] = rec.domain_login_id
            emp_vals['domain_login_pwd'] = rec.domain_login_pwd
            # added on 24 april
            emp_vals['mother_tongue_id'] = rec.mother_tounge_ids.id if rec.mother_tounge_ids else False

            # language details
            emp_lang_details = []
            for lang_rec in rec.known_language_ids:
                emp_lang_details.append((0, 0, {
                    'language_id': lang_rec.language_id.id,
                    'reading_status': lang_rec.reading_status,
                    'writing_status': lang_rec.writing_status,
                    'speaking_status': lang_rec.speaking_status,
                    'understanding_status': lang_rec.understanding_status,
                }))

            # Fetch the educational details
            emp_edu_details = []
            for edu_rec in rec.educational_ids:
                specilization_details = []
                for specilization in edu_rec.passing_details:
                    specilization_details.append(specilization.id)

                emp_edu_details.append((0, 0, {
                    'course_type': edu_rec.course_type,
                    'course_id': edu_rec.course_id.id,
                    'stream_id': edu_rec.stream_id.id,
                    'university_name': edu_rec.university_name.id,
                    'passing_details': [[6, 'false', specilization_details]],
                    'passing_year': edu_rec.passing_year,
                    'division': edu_rec.division,
                    'marks_obtained': edu_rec.marks_obtained,
                    'uploaded_doc': edu_rec.uploaded_doc,
                    'doc_file_name': edu_rec.filename
                }))

            # Fetch the work experience details
            emp_work_details = []
            for work_rec in rec.work_experience_ids:
                emp_work_details.append((0, 0, {
                    'country_id': work_rec.country_id.id,
                    'name': work_rec.name,
                    'designation_name': work_rec.designation_name,
                    'organization_type': work_rec.organization_type.id,
                    'industry_type': work_rec.industry_type.id,
                    'effective_from': work_rec.effective_from,
                    'effective_to': work_rec.effective_to,
                    'uploaded_doc': work_rec.uploaded_doc,
                    'doc_file_name': work_rec.filename
                }))

            # identification details
            emp_id_details = []
            for id_rec in rec.identification_ids:
                emp_id_details.append((0, 0, {
                    'name': id_rec.name,
                    'doc_number': id_rec.doc_number,
                    'date_of_issue': id_rec.date_of_issue,
                    'date_of_expiry': id_rec.date_of_expiry,
                    'renewal_sts': id_rec.renewal_sts,
                    'uploaded_doc': id_rec.uploaded_doc,
                    'doc_file_name': id_rec.filename
                }))

            emp_vals['work_experience_ids'] = emp_work_details
            emp_vals['educational_details_ids'] = emp_edu_details
            emp_vals['identification_ids'] = emp_id_details
            emp_vals['known_language_ids'] = emp_lang_details

            # new_emp_rec = self.env["hr.employee"].sudo().create(emp_vals)
            # if new_emp_rec:
            #     # update the status and employee id
            #     rec.write({'state': '5', 'emp_id': new_emp_rec.id})

            #     self.env.user.notify_success(message='Congrats ! Employee enrollment completed.')
            # # print(new_emp_rec)
            # return new_emp_rec
            ##Create Authenticate User
            uservals = {
                'enrollment_id':self.id,
                'fullname': self.name,
                'domain_name': self.domain_login_id,
                'email_id': self.csm_email_id,
                'dept_id': self.dept_name.id,
                'division_id':self.division.id,
                'section_id':self.section.id,
                'practise_id':self.practise.id,
                'designation': self.job_id.id,
                'emp_role': self.emp_role.id,
                'emp_category': self.emp_category.id,
                'employ_type': self.employement_type.id,
                'office_unit': self.location_id.id,
                'dob':self.birthday,
                'gender': self.gender,
                'religion': self.emp_religion.id,
                'card_no': self.id_card_no,
                'marital_stat': self.marital.id,
                'marriage_date': self.wedding_anniversary,
                'image': self.image,
                'job_position': self.job_id.id
            }
            user_auth = self.env["kwonboard_new_joinee"].sudo().create(uservals)
            self.env.user.notify_success(message='Congrats ! Employee enrollment completed.')
            rec.write({'state': '5'})
            # view_ref = self.env['ir.model.data'].get_object_reference('kw_onboarding', 'kwonboard_new_joinee_form')
            # view_id = view_ref[1] if view_ref else False
            # import pdb
            # pdb.set_trace()
            # return {
            #     'name': 'Authenticate User',
            #     'view_type': 'form',
            #     'view_mode': 'form',
            #     'res_model': 'kwonboard_new_joinee',
            #     'view_id':  view_id,
            #     'type': 'ir.actions.act_window',
            #     'context': uservals, 
            #     'target': 'new',
            #     'flags': {'initial_mode': 'edit'},
            #     }

    @api.constrains('work_experience_ids','birthday')
    def validate_experience(self):
        if self.work_experience_ids:
            if not self.birthday:
                raise ValidationError("Please enter your date of birthday.")
            for experience in self.work_experience_ids:
                if str(experience.effective_from) < str(self.birthday):
                    raise ValidationError("Work experience date should not be less than date of birth.")
                except_experience = self.work_experience_ids - experience
                overlap_experience = except_experience.filtered(
                    lambda r: r.effective_from <= experience.effective_from <= r.effective_to or r.effective_from <= experience.effective_to <= r.effective_to )
                if overlap_experience:
                    raise ValidationError(f"Overlapping experiences are not allowed.")

    @api.constrains('educational_ids')
    def validate_edu_data(self):
        if self.educational_ids and self.birthday:
            for record in self.educational_ids:
                if str(record.passing_year) < str(self.birthday):
                    raise ValidationError("Passing year should not be less than date of birth.")
        if self.educational_ids and not self.birthday:
            raise ValidationError("Please enter your date of birth.")

    @api.constrains('identification_ids')
    def validate_issue_date(self):
        if self.identification_ids and self.birthday:
            for record in self.identification_ids:
                if str(record.date_of_issue) < str(self.birthday):
                    raise ValidationError("Date of issue should not be less than date of birth.")
        if self.identification_ids and not self.birthday:
            raise ValidationError("Please enter your date of birth.")

    # validate work email
    @api.constrains('csm_email_id')
    def check_work_email(self):
        for record in self:
            kw_validations.validate_email(record.csm_email_id)

    @api.constrains('email')
    def check_email(self):
        for record in self:
            kw_validations.validate_email(record.email)

        records = self.env['kwonboard_enrollment'].search([]) - self
        for info in records:
            if info.email == self.email:
                raise ValidationError("This  Email id is already existing..")

    @api.constrains('present_addr_zip')
    def check_present_pincode(self):
        for record in self:
            if record.present_addr_zip:
                if not re.match("^[0-9]*$", str(record.present_addr_zip)) != None:
                    raise ValidationError("Present pincode is not valid")

    @api.constrains('permanent_addr_zip')
    def check_permanent_pincode(self):
        for record in self:
            if record.permanent_addr_zip:
                if not re.match("^[0-9]*$", str(record.permanent_addr_zip)) != None:
                    raise ValidationError("Permanent pincode is not valid")

    @api.constrains('mobile')
    def check_mobile(self):
        for record in self:
            if record.mobile:
                if not len(record.mobile) == 10:
                    raise ValidationError("Your number is invalid for: %s" % record.mobile)
                if not re.match("^[0-9]*$", str(record.mobile)) != None:
                    raise ValidationError("Your number is invalid for: %s" % record.mobile)
        records = self.env['kwonboard_enrollment'].search([]) - self
        for info in records:
            if info.mobile == self.mobile:
                raise ValidationError("This Mobile number already exists.")

    @api.constrains('image')
    def _check_filename(self):
        allowed_file_list = ['image/jpeg', 'image/jpg', 'image/png']
        if self.image:
            kw_validations.validate_file_mimetype(self.image, allowed_file_list)

            kw_validations.validate_file_size(self.image, 1)
                     

    @api.constrains('wedding_anniversary', 'birthday')
    def validate_Birthdate_data(self):
        current_date = str(datetime.now().date())
        today = date.today()
        for record in self:
            if record.birthday:
                if today.year - record.birthday.year - (
                        (today.month, today.day) < (record.birthday.month, record.birthday.day)) < 18:
                    raise ValidationError("You must be 18 years old.")
                if str(record.birthday) >= current_date:
                    raise ValidationError("The date of birth should be less than current date.")
                if record.wedding_anniversary:
                    if str(record.wedding_anniversary) <= str(record.birthday):
                        raise ValidationError("Wedding Anniversary date should not be less than Birth date.")

    # @api.constrains('birthday')
    # def validate_weddingdata(self):
    #     d = datetime.date(2010, 4, 13)
    #     for record in self:
    #         if record.birthday:
    #             if record.birthday > d:
    #                 raise ValidationError("Age must be greater than 18")

    # method to override the create method and generate a random reference number
    @api.model
    def create(self, vals):
        tm = time.strftime('%Y')
        enroll_record_ids = self.search([], order='id desc', limit=1)
        last_id = enroll_record_ids.id
        reference_number = 'CSM' + tm[2:] + str(last_id + 1).zfill(4)
        reference_msg = 'Hello ' + str(vals['name']) + ', your reference number is :' + str(reference_number) + '. Keep it for future reference. Please visit the link ' + self.env['ir.config_parameter'].sudo().get_param('web.base.url') + ' and fill up your details. '
        vals['reference_no'] = reference_number
        new_record = super(kwonboard_enrollment, self).create(vals)
        ir_config_params = request.env['ir.config_parameter'].sudo()
        demo_mode_config = ir_config_params.get_param('kw_onboarding.module_onboarding_mode_status') or False
        send_mail_config = ir_config_params.get_param('kw_onboarding.module_onboarding_mail_status') or False
        send_sms_config = ir_config_params.get_param('kw_onboarding.module_onboarding_sms_status') or False
        # For sending SMS
        if not demo_mode_config:
            if send_sms_config:
                template = self.env['send_sms'].sudo().search([('name', '=', 'Onboarding_SMS')])
                record_id = new_record.id
                self.env['send_sms'].send_sms(template, record_id)
            # For sending Email
        if send_mail_config:
            template = self.env.ref('kw_onboarding.employee_email_template')
            mail_status = self.env['mail.template'].browse(template.id).send_mail(new_record.id, force_send=True)
        if new_record:
            if vals.get('applicant_id'):
                self.env['hr.applicant'].search([('id','=', vals.get('applicant_id'))]).write({'enrollment_id': new_record.id})
            self.env.user.notify_success(message='Enrollment created successfully.')
        return new_record
            
    @api.multi
    def write(self, values):
        result = super(kwonboard_enrollment, self).write(values)
        self.env.user.notify_success(message='Enrollment updated successfully.')
        return result
