# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions


# class hr_employee_in(models.Model):
#     _inherit = 'hr.employee'

#     state = fields.Selection(
#         [('1', 'Profile Created'), ('2', 'Environment Mapping'), ('3', 'Environment Config'), ('4', 'Approved')],
#         required=True, default='1')

#     @api.multi
#     def button_take_action(self):
#         # for rec in self:
#         #     rec.write({'state': '2'})
#         return True

class HrApplicant(models.Model):
    _inherit = "hr.applicant"

    kw_enrollment_id = fields.Many2one('kwonboard_enrollment', string="Enrollment Ref#")