# -*- coding: utf-8 -*-

from odoo import api,fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    module_onboarding_mail_status = fields.Boolean(string="Send E-mail")
    module_onboarding_sms_status = fields.Boolean(string="Send SMS")
    module_onboarding_mode_status = fields.Boolean(string="Demo Mode")

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            module_onboarding_mail_status = self.env['ir.config_parameter'].sudo().get_param('kw_onboarding.module_onboarding_mail_status'),
            module_onboarding_sms_status = self.env['ir.config_parameter'].sudo().get_param('kw_onboarding.module_onboarding_sms_status'),
            module_onboarding_mode_status = self.env['ir.config_parameter'].sudo().get_param('kw_onboarding.module_onboarding_mode_status'),
        )
        return res

    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()

        field1 = self.module_onboarding_mail_status or False
        field2 = self.module_onboarding_sms_status or False
        field3 = self.module_onboarding_mode_status or False

        param.set_param('kw_onboarding.module_onboarding_mail_status', field1)
        param.set_param('kw_onboarding.module_onboarding_sms_status', field2)
        param.set_param('kw_onboarding.module_onboarding_mode_status', field3)
