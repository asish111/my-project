# -*- coding: utf-8 -*-
from .import hr_employee_in
from . import kwonboard_enrollment
# from . import kwonboard_employee_master
# from . import hr_employee_in
from . import kwonboard_otp
from . import kw_email
from . import res_users_in
from . import kwonboard_educational_qualification
from . import kwonboard_identity_docs
from . import kwonboard_work_experience
from . import kwonboard_language_known


from . import res_config_settings
from . import kwonboard_config_type
from . import kwonboard_new_joinee
from . import kw_tag_master
from . import kw_allowance_master
from . import kw_bonus_master