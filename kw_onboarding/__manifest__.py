{
    'name': "Kwantify Onboarding",
    'summary': "New Joinee on-boarding",
    'description': "Employee on-boarding process",
    'author': "CSM Technologies",
    'website': "http://www.csmpl.com",

    'category': 'Kwantify',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'kw_employee', 'kw_sendsms', 'kw_dynamic_workflow', 'kw_recruitment','kw_workstation', 'kw_hr_attendance'],

    # always loaded
    'data': [
        'security/kwonboard_security.xml',
        'security/ir.model.access.csv',
        'data/mail_data.xml',
        'data/kw_config_type_master.xml',
        'data/kw_tag_master_data.xml',
        'data/kwonboard_config_type_data.xml',
        'data/kwonboard_dynamic_workflow_data.xml',
        # 'data/kw_gateway_setup.xml',
        'views/kwonboard_enrollment_views.xml',
        'views/kwonboard_newjoinee_view.xml',
        'views/kwonboard_new_joinee.xml',
        # 'data/kwonboard_new_joinee_workflow.xml',
        'views/kw_tag_master.xml',
        # 'views/kw_allowance_master.xml',
        # 'data/kw_allowance_master_data.xml',
        # 'views/kw_bonus_master.xml',
        # 'data/kw_bonus_master_data.xml',
        # 'views/hr_employees_views.xml',
        'views/kwonboard_menus.xml',
        'views/kwonboard_config_view.xml',

        'views/kw_generate_otp.xml',
        # 'views/kw_gateway_setup.xml',

        'views/kwonboard_message_view.xml',
        'views/kwonboard_personal_details_template.xml',
        'views/kwonboard_educational_details_template.xml',
        'views/kwonboard_work_experience_template.xml',
        'views/kwonboard_identification_details_template.xml',
        'views/kwonboard_onboarding_template.xml',
        'views/email/kw_email_template.xml',
        'views/email/kw_email_otp_template.xml',
        'views/sms/kw_otp_template.xml',
        'views/sms/kw_sms_template.xml',
        'views/kwonboard_assets_frontend.xml',
        'views/res_config_settings_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
# -*- coding: utf-8 -*-
