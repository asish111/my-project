from odoo import models, fields, api


class hr_department(models.Model):
    _inherit = "hr.department"
    _order = "name asc"
    # _rec_name = 'name'

    name = fields.Char(string=u'Department Name', size=100, required=True)
    kw_id = fields.Integer(string='Kwantify ID')
    dept_head = fields.Many2one('hr.employee', string="Department head")
    dept_type = fields.Many2one('kw_hr_department_type', string="Department Type")

    @api.constrains('name')
    def check_name(self):
        exists_name = self.env['hr.department'].search([('name', '=', self.name), ('id', '!=', self.id)])
        if exists_name:
            raise ValueError("This Department name \"" + self.name + "\" already exists.")

    @api.model
    def create(self, vals):
        record = super(hr_department, self).create(vals)
        if record:
            self.env.user.notify_success(message='Department created successfully.')
        else:
            self.env.user.notify_danger(message='Department creation failed.')
        return record

    @api.multi
    def write(self, vals):
        res = super(hr_department, self).write(vals)
        if res:
            self.env.user.notify_success(message='Department updated successfully.')
        else:
            self.env.user.notify_danger(message='Department updation failed.')
        return res
