from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re


class KwEmployeeOnboardingChecklist(models.Model):
    _name = 'kw_employee_onboarding_checklist'
    _description = "A master model to create checklist of onboarding."
    _rec_name= 'doc_collection'

    joining_kit = fields.Selection(string='Joining Kit',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    doc_collection = fields.Selection(string='Documents Collection',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    kw_profile_update = fields.Selection(string='Kwantify Profile Update',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    hard_copy_verification = fields.Selection(string='Hard Copy Verification',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    kw_id_generation = fields.Selection(string='Kwantify ID Generation',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    email_id_creation = fields.Selection(string='Email ID Creation',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    telephone_extention = fields.Selection(string='Telephone Extention',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    health_insurance = fields.Selection(string='Health Insurance',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
                                        
    family_info = fields.Many2one('kwemp_family_info', 'Dependants info',)

    esi = fields.Selection(string='ESI',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    accident_policy = fields.Selection(string='Accident Policy',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    gratuity = fields.Selection(string='Gratutity',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    pf = fields.Selection(string='PF',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    work_station = fields.Selection(string='Work Station',
                                        selection=[('yes', 'Yes'),('no', 'No'),  ('na', 'NA')], default='no')
    
    offsite = fields.Boolean(string='Offsite')
    work_station_link = fields.Char(string="Workstation link")
    onsite = fields.Boolean(string='Onsite')
    client_loc = fields.Many2one('kw_res_branch', 'Client Location',)

    hr_induction = fields.Selection(string='HR Induction',
                                        selection=[('yes', 'Yes'),('no', 'No')], default='no')
    jd = fields.Selection(string='JD',
                                        selection=[('yes', 'Yes'),('no', 'No')], default='no')
    bond_formality = fields.Selection(string='Bond Formality',
                                        selection=[('yes', 'Yes'),('no', 'No'), ('na', 'NA')], default='no')
    appointment_letter = fields.Selection(string='Appointment Letter',
                                        selection=[('yes', 'Yes'),('no', 'No')], default='no')
    id_card = fields.Selection(string='ID Card',
                                        selection=[('yes', 'Yes'),('no', 'No')], default='no')
    criminal_record_verification = fields.Selection(string='Criminal Record Verification',
                                        selection=[('yes', 'Yes'),('no', 'No')], default='no')
    background_verification = fields.Selection(string='Background Verification',
                                        selection=[('yes', 'Yes'),('no', 'No')], default='no')



    @api.model
    def create(self, values):
        result = super(KwEmployeeOnboardingChecklist, self).create(values)
        if 'active_model' and 'active_id' in self._context:
            if self._context['active_model'] == 'hr.employee':
                employee_id = self._context['active_id']
                employee_rec = self.env['hr.employee'].browse(employee_id)
                if employee_rec and not employee_rec.onboarding_checklist:
                    employee_rec.onboarding_checklist = result.id
        return result
