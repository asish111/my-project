# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, SUPERUSER_ID
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
import re
from lxml import etree
from datetime import date, datetime
from dateutil import relativedelta
import requests, json
from kw_utility_tools import kw_validations
from odoo import http

DEFAULT_INTERNAL_GRP = 1


class hr_employee_in(models.Model):
    _inherit = 'hr.employee'

    # START : additional hr setting
    is_wfh = fields.Boolean('WFH', default=False, store=True, readonly=False)
    kw_id = fields.Integer(string='Kwantify ID')
    emp_code = fields.Char(string=u'Employee Code', size=100)
    emp_grade = fields.Many2one(string=u'Grade', comodel_name='kwemp_grade', ondelete='set null')
    emp_band = fields.Many2one(string=u'Band', comodel_name='kwemp_band_master', ondelete='set null')
    date_of_joining = fields.Date(string="Joining Date")  # ,required=True
    date_of_completed_probation = fields.Date(string="Probation Complete Date")
    confirmation_sts = fields.Boolean("Confirmation Status", default=False)

    onboarding_checklist = fields.Many2one(string=u'Onboarding Checklist', comodel_name='kw_employee_onboarding_checklist')
    
    # system_configuration = fields.Many2many('kwemp_config_type', string="Environment Configuration")

    emp_role = fields.Many2one('kwmaster_role_name', ondelete='cascade', string="Employee Role")
    emp_category = fields.Many2one('kwmaster_category_name', ondelete='cascade', string="Employee Category")
    employement_type = fields.Many2one('kwemp_employment_type', ondelete='cascade', string="Type of Employment")

    id_card_no = fields.Char(string=u'ID Card No', size=100)
    # csm_email_id            = fields.Char(string=u'Work Email ID', size=100)

    biometric_id = fields.Char(string=u'Biometric ID', size=100)
    outlook_pwd = fields.Char(string=u'Outlook Password', size=100)
    epbx_no = fields.Char(string=u'EPBX ', size=100)

    domain_login_id = fields.Char(string=u'System Domain ID ', size=100)
    domain_login_pwd = fields.Char(string=u'System Domain Password ', size=100)
    # END : additional hr setting

    # START : Personal Info
    # start: overriding the existing fields
    name = fields.Char(string="Name", size=100)
    mobile_phone = fields.Char(string="Work Mobile No", size=15)
    work_phone = fields.Char(string="Work Phone No", size=18)
    work_location_id = fields.Many2one('res.partner', string="Work Location ID",
                                       domain="[('parent_id', '=', company_id)]",help="Employee's working location." )
    work_location = fields.Char(string="Work Location", related='work_location_id.city', readonly=True)
    work_email = fields.Char(string="Work Email", size=100)
    job_title = fields.Char(string="Job Role", size=100)
    gender = fields.Selection(groups="base.group_user")

    image = fields.Binary(string="Upload Photo",
                          help="Only .jpeg,.png,.jpg format are allowed. Maximum file size is 1 MB")

    # marital = fields.Selection(string='Marital Status', groups="base.group_user", default='single')
    marital = fields.Many2one('kwemp_maritial_master', string='Marital Status')
    marital_code = fields.Char(string=u'Marital Status Code ')

    country_id = fields.Many2one(groups="base.group_user")
    birthday = fields.Date(groups="base.group_user")
    identification_id = fields.Char(string='Identification No (Aadhar No)', groups="base.group_user", size=100)
    passport_id = fields.Char(groups="base.group_user", size=100)

    permit_no = fields.Char(groups="base.group_user", size=100)
    visa_no = fields.Char(groups="base.group_user", size=100)
    visa_expire = fields.Date(groups="base.group_user")

    emergency_contact = fields.Char("Emergency Contact Person", groups="base.group_user", size=100)
    emergency_phone = fields.Char("Emergency Phone", groups="base.group_user", size=15)
    km_home_work = fields.Integer(string="Distance From Home To Work", groups="base.group_user")

    # End: overriding the existing fields

    personal_email = fields.Char(string=u'Personal EMail Id ', size=100)
    whatsapp_no = fields.Char(string=u'WhatsApp No.', size=15)
    present_addr_street = fields.Text(string="Present Address Line 1", size=500)
    present_addr_street2 = fields.Text(string="Present Address Line 2", size=500)
    present_addr_country_id = fields.Many2one('res.country', string="Present Address Country")
    present_addr_city = fields.Char(string="Present Address City", size=100)
    present_addr_state_id = fields.Many2one('res.country.state', string="Present Address State")
    present_addr_zip = fields.Char(string="Present Address ZIP", size=10)

    same_address = fields.Boolean(string=u'Same as Present Address', default=False)

    permanent_addr_country_id = fields.Many2one('res.country', string="Permanent Address Country")
    permanent_addr_street = fields.Text(string="Permanent Address Line 1", size=500)
    permanent_addr_street2 = fields.Text(string="Permanent Address Line 2", size=500)
    permanent_addr_city = fields.Char(string="Permanent Address City", size=100)
    permanent_addr_state_id = fields.Many2one('res.country.state', string="Permanent Address State")
    permanent_addr_zip = fields.Char(string="Permanent Address ZIP", size=10)

    emp_religion = fields.Many2one('kwemp_religion_master', string="Religion")
    emp_refered_from = fields.Many2one('kwemp_reference_mode_master', string="Reference Mode")
    emp_refered_detail = fields.Text(string="Reference Details")

    wedding_anniversary = fields.Date(string=u'Wedding Anniversary', )
    known_language_ids = fields.One2many('kwemp_language_known', 'emp_id', string='Language Known')
    # END : Personal Info

    # START : Work Experience details###
    experience_sts = fields.Selection(string="Work Experience Details ",
                                      selection=[('1', 'Fresher'), ('2', 'Experience')],default="2")
    worked_country_ids = fields.Many2many('res.country', string='Countries Of Work Experience',
                                          groups="hr.group_hr_user")
    work_experience_ids = fields.One2many('kwemp_work_experience', 'emp_id', string='Work Experience Details')
    technical_skill_ids = fields.One2many('kwemp_technical_skills', 'emp_id', string='Technical Skills')
    # END : Work Experience details###

    # START : Educational Details##
    educational_details_ids = fields.One2many('kwemp_educational_qualification', 'emp_id', string="Educational Details")
    # END : Educational Details##

    # START :for identification details ###
    blood_group = fields.Many2one('kwemp_blood_group_master', string="Blood Group")
    identification_ids = fields.One2many('kwemp_identity_docs', 'emp_id', string='Indentification Documents')
    membership_assc_ids = fields.One2many('kwemp_membership_assc', 'emp_id', string='Membership Association Details')
    # END : for identification details ###

    # START : Family info Details##
    family_details_ids = fields.One2many('kwemp_family_info', 'emp_id', string="Family Info Details")
    # END : Family info Details##

    # is always computed.   , default=lambda self: self.env.user.id
    current_user = fields.Boolean("Current User", compute='_get_current_user', default=False)

    total_experience_display = fields.Char('Total Experience', readonly=True, compute='_compute_experience_display',
                                           help="Field allowing to see the total experience in years and months depending upon joining date and experience details")
    color = fields.Char(compute='get_status')
    emp_display_name = fields.Char('Name.', compute='_emp_display_name')
    # added on 20 april 2020
    mother_tongue_id = fields.Many2one('kwemp_language_master',string="Mother Tongue")
    # help content for base location
    address_id = fields.Many2one(
        'res.partner', 'Work Address',help="Employee's base location.")
    division = fields.Many2one('hr.department', string="Division")
    section = fields.Many2one('hr.department', string="Section")
    practise = fields.Many2one('hr.department', string="Practise")
    # added on 14 july 2020 by Gouranga
    base_branch_id = fields.Many2one('kw_res_branch','Base Location')
    job_branch_id = fields.Many2one('kw_res_branch', 'Job Location')
    # added on 14 july 2020 by Gouranga

    def toggle_work(self):
        if self.is_wfh == True:
            self.write({'is_wfh': False})
        else:
            self.write({'is_wfh': True})

    @api.multi
    def view_onboarding_checklist(self):
        onboarding_checklist_view_id = self.env.ref('kw_employee.kw_employee_onboarding_checklist_view_form').id
        _action = {
                'type': 'ir.actions.act_window',
                'res_model': 'kw_employee_onboarding_checklist',
                'view_mode': 'form',
                'view_type': 'form',
                'view_id': onboarding_checklist_view_id,
                'target': 'self',
            }
        if self.onboarding_checklist:
            _action['res_id'] = self.onboarding_checklist.id
        return _action

    @api.onchange('department_id')
    def onchange_department(self):
        domain={}
        for rec in self:
            domain['division'] = [('parent_id', '=', rec.department_id.id),('dept_type.code', '=', 'division')]
            return {'domain': domain}

    @api.onchange('division')
    def onchange_division(self):
        domain={}
        for rec in self:
            if rec.department_id:
                domain['section'] = [('parent_id', '=', rec.division.id),('dept_type.code', '=', 'section')]
                return {'domain': domain}

    @api.onchange('section')
    def onchange_section(self):
        domain={}
        for rec in self:
            if rec.section:
                domain['practise'] = [('parent_id', '=', rec.section.id),('dept_type.code', '=', 'practice')]
                return {'domain': domain}

    @api.model
    def get_status(self):
        try:
            url = http.request.env['ir.config_parameter'].sudo().get_param('kwantify_common_api')
            if url:
                user_presence = url + '/GetEmployeePresence'
            else:
                user_presence = "https://kwportalservice.csmpl.com/OdooSynSVC.svc/GetEmployeePresence"
            # response_obj = requests.post(user_presence)
            # status_content = response_obj.content
            # resp = json.loads(status_content.decode("utf-8"))
            status_colors = ''
            resp = []
            for record in resp:
                for emp_data in self:
                    if emp_data.kw_id == int(record["UserId"]):
                        colors = int(record["UserStatus"])
                        if colors == 1:
                            status_colors = "green"
                        elif colors == 2:
                            status_colors = "yellow"
                        elif colors == 3:
                            status_colors = "info"
                        elif colors == 4:
                            status_colors = "red"
                        elif colors == 5:
                            status_colors = "gray"
                        else:
                            status_colors = "NA"
                        emp_data.color = status_colors
            print("Response success from Service")
        except:
            for record in self:
                record.color = 'NA'
            print("Response failed from Service")
            # get current user

    @api.multi
    def action_employee_permissions(self):
        if not self.user_id:
            raise ValidationError("Employee must tagged to a user to set permissions.")

        user_form_view_id = self.env.ref("base.view_users_form").id
        
        return {
            'name': 'Permissions',
            'model': 'ir.actions.act_window',
            'type': 'ir.actions.act_window',
            'res_model': 'res.users',
            'res_id': self.user_id.id,
            'view_type': 'form',
            'views': [(user_form_view_id, 'form')],
            'view_id': user_form_view_id,
            # 'flags': {'action_buttons': True, 'mode': 'edit'},
        }

    @api.depends('date_of_joining', 'work_experience_ids')
    def _compute_experience_display(self):
        for rec in self:
            total_years, total_months = 0, 0
            if rec.date_of_joining:
                difference = relativedelta.relativedelta(datetime.today(), rec.date_of_joining)
                total_years += difference.years
                total_months += difference.months

            if rec.work_experience_ids:
                for exp_data in rec.work_experience_ids:
                    exp_difference = relativedelta.relativedelta(exp_data.effective_to, exp_data.effective_from)
                    total_years += exp_difference.years
                    total_months += exp_difference.months
                    # print ("Difference is %s year, %s months " %(total_years,total_months))
            # print ("Difference is %s year, %s months " %(total_years,total_months))

            if total_months >= 12:
                total_years += total_months // 12
                total_months = total_months % 12

            if total_years > 0 or total_months > 0:
                rec.total_experience_display = " %s Years and %s Months " % (total_years, total_months)
            else:
                rec.total_experience_display = ''

    # on change of marital status compute marital status code
    @api.onchange('marital')
    def _compute_marital_status_code(self):
        if self.marital:
            self.marital_code = self.marital.code
        else:
            self.marital_code = ''

    # get current user
    @api.depends('user_id')
    def _get_current_user(self):
        for rec in self:
            if self.env.user.id == rec.user_id.id:
                rec.current_user = True
            elif self.env.user.has_group('hr.group_hr_manager'):
                rec.current_user = True

    @api.onchange('emp_role')
    def _get_categories(self):
        role_id = self.emp_role.id
        self.emp_category = False
        return {'domain': {'emp_category': [('role_ids', '=', role_id)], }}

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            emp_code = f'({record.emp_code})' if record.emp_code else ''
            result.append((record.id, f'{record.name} {emp_code}'))
        return result

    @api.model
    def _emp_display_name(self):
        for record in self:
            emp_code = f'({record.emp_code})' if record.emp_code else ''
            record.emp_display_name = f'{record.name} {emp_code}'

    # on change of same address check-box , assign present address to permanent address
    @api.onchange('same_address')
    def _change_permanent_address(self):
        if self.same_address:
            self.permanent_addr_country_id = self.present_addr_country_id
            self.permanent_addr_street = self.present_addr_street
            self.permanent_addr_street2 = self.present_addr_street2
            self.permanent_addr_city = self.present_addr_city
            self.permanent_addr_state_id = self.present_addr_state_id
            self.permanent_addr_zip = self.present_addr_zip

    # onchange of present address country change the state
    @api.onchange('present_addr_country_id')
    def _change_present_address_state(self):
        country_id = self.present_addr_country_id.id
        self.present_addr_state_id = False
        return {'domain': {'present_addr_state_id': [('country_id', '=', country_id)], }}

    # onchange of permanent address country change the state
    @api.onchange('permanent_addr_country_id')
    def _change_permanent_address_state(self):
        country_id = self.permanent_addr_country_id.id
        if self.same_address and self.present_addr_state_id and (
                self.permanent_addr_country_id == self.present_addr_country_id):
            self.permanent_addr_state_id = self.present_addr_state_id
        else:
            self.permanent_addr_state_id = False
        return {'domain': {'permanent_addr_state_id': [('country_id', '=', country_id)], }}

    @api.onchange('emp_refered_from')
    def get_refered_detail(self):
        if not self.emp_refered_from:
            self.emp_refered_detail = False

    def send_wishes(self):
        print("send wishes")
        print(self)

    def create_emp_user(self):
        for rec in self:
            # print(rec.mobile_phone)
            if rec.name and rec.work_email and rec.company_id:
                try:
                    # print(self._context)
                    user_rec = self.env["res.users"].sudo().create(
                        {"name": rec.name, "email": rec.work_email, "login": rec.work_email, "active": True,
                         "company_ids": [[6, False, [rec.company_id.id]]], "company_id": rec.company_id.id,
                         "sel_groups_1_9_10": DEFAULT_INTERNAL_GRP, "lang": self._context.get('lang'),
                         "tz": self._context.get('tz')})
                    if user_rec:
                        rec.write({'user_id': user_rec.id})
                        # print(user_rec.a)
                        mail_status = self.env["res.users"].action_reset_password()

                        # if rec.mobile_phone:
                        #     template = self.env['send_sms'].sudo().search([('name', '=', 'User_SMS')])
                        #     record_id = rec.id
                        #     self.env['send_sms'].send_sms(template, record_id)

                except ValidationError:
                    raise ValidationError(_('User is already created using the same login'))
            else:
                raise ValidationError(_('Please enter name, email address and company'))

    @api.multi
    def write(self, vals):
        experience = vals.get('experience_sts', False)
        if experience == "1":
            if 'work_experience_ids' in vals:
                vals.pop('work_experience_ids')
        gen_emp_code = ''
        for rec in self:
            if (not rec.emp_code) and 'work_location_id' in vals:
                gen_emp_code = self._generate_employee_code(vals['work_location_id'])
                vals['emp_code'] = gen_emp_code

        old_parent_ids = []       
        if 'parent_id' in vals:
            old_parent_ids = self.mapped('parent_id')
            
        # for reporting authority/manager add to hr RA group
        group_hr = self.env.ref('kw_employee.group_hr_ra', False)
        # old_parent_id = self.parent_id

        super(hr_employee_in, self).write(vals)

        for emp_rec in self:
            if emp_rec.parent_id and emp_rec.parent_id.user_id:
                group_hr.write({'users': [(4, emp_rec.parent_id.user_id.id)]})

        # for reporting authority/manager add to hr RA group        
        for old_parent in old_parent_ids:
            if old_parent.user_id and len(old_parent.child_ids) == 0:
                group_hr.write({'users': [(3, old_parent.user_id.id)]})     

        self.env.user.notify_info(message='Employee details updated successfully.')
        return True

    @api.model
    def create(self, vals):
        experience = vals.get('experience_sts', False)
        if experience == "1":
            if 'work_experience_ids' in vals:
                vals.pop('work_experience_ids')
        gen_emp_code = ''
        if ('emp_code' not in vals or not vals['emp_code']) and 'work_location_id' in vals:
            gen_emp_code = self._generate_employee_code(vals['work_location_id'])
            vals['emp_code'] = gen_emp_code

        emp_rec = super(hr_employee_in, self).create(vals)

        # Call Announcement template and make an announcement
        if emp_rec:
            try:
                if emp_rec.active:
                    template = self.env.ref('kw_announcement.kwannouncement_new_joinee_hr_template')
                    if template:
                        self.env['kw_announcement_template'].sudo().make_announcement(template, emp_rec.id)

                # add the user to  RA group
                if emp_rec.parent_id and emp_rec.parent_id.user_id:
                    group_hr = self.env.ref('kw_employee.group_hr_ra', False)
                    group_hr.write({'users': [(4, emp_rec.parent_id.user_id.id)]})

                self.env.user.notify_success(message="Employee created successfully.")
            except Exception as e:
                print(e)
        return emp_rec

    @api.constrains('date_of_joining', 'birthday')
    def validate_data(self):
        current_date = str(datetime.now().date())
        for record in self:
            if record.date_of_joining:
                if str(record.date_of_joining) > current_date:
                    raise ValidationError("The date of joining should not be greater than current date.")
            if record.birthday:
                if str(record.birthday) >= current_date:
                    raise ValidationError("The date of birth should be less than current date.")

    @api.constrains('wedding_anniversary', 'birthday')
    def validate_Birthdate_data(self):
        current_date = str(datetime.now().date())
        today = date.today()
        for record in self:
            if record.birthday:
                if today.year - record.birthday.year - (
                        (today.month, today.day) < (record.birthday.month, record.birthday.day)) < 18:
                    raise ValidationError("You must be 18 years old.")
                if record.wedding_anniversary:
                    if str(record.wedding_anniversary) <= str(record.birthday):
                        raise ValidationError("Wedding Anniversary date should not be less than birth date.")
            if record.birthday:
                if str(record.birthday) >= current_date:
                    raise ValidationError("The date of birth should be less than current date.")

    @api.constrains('visa_expire')
    def validate_visaexpire_data(self):
        for record in self:
            if record.birthday and record.visa_expire:
                if str(record.visa_expire) <= str(record.birthday):
                    raise ValidationError("Visa Expire date should not be less than birth date.")

    @api.constrains('work_email')
    def check_work_email(self):
        for record in self:
            kw_validations.validate_email(record.work_email)
            if record.work_email:
                records = self.env['hr.employee'].search([('work_email', '=', record.work_email)]) - self
                if records:
                    raise ValidationError("This email id is already existing.")

    @api.constrains('work_experience_ids','birthday')
    def validate_experience(self):
        if self.work_experience_ids:
            if not self.birthday:
                raise ValidationError("Please enter your date of birthday.")
            for experience in self.work_experience_ids:
                if str(experience.effective_from) < str(self.birthday):
                    raise ValidationError("Work experience date should not be less than date of birth.")
                except_experience = self.work_experience_ids - experience
                overlap_experience = except_experience.filtered(
                    lambda r: r.effective_from <= experience.effective_from <= r.effective_to or r.effective_from <= experience.effective_to <= r.effective_to )
                if overlap_experience:
                    raise ValidationError(f"Overlapping experiences are not allowed.")
            
    @api.constrains('membership_assc_ids')
    def validate_membership_assc(self):
        for emp_rec in self:
            if emp_rec.membership_assc_ids and emp_rec.birthday:
                for record in emp_rec.membership_assc_ids:
                    if str(record.date_of_issue) <= str(emp_rec.birthday):
                        raise ValidationError("Membership issue date should not be less than date of birth.")

    @api.constrains('educational_details_ids')
    def validate_edu_data(self):
        if self.educational_details_ids:
            for record in self.educational_details_ids:
                if self.birthday:
                    if str(record.passing_year) < str(self.birthday):
                        raise ValidationError("Passing year should not be less than date of birth.")

        # if self.educational_details_ids and not self.birthday:
        #     raise ValidationError("Please enter your date of birth.")

    @api.constrains('identification_ids')
    def validate_issue_date(self):
        if self.identification_ids and self.birthday:
            for record in self.identification_ids:
                if str(record.date_of_issue) < str(self.birthday):
                    raise ValidationError("Date of issue should not be less than date of birth.")
        # if self.identification_ids and not self.birthday:
        #     raise ValidationError("Please enter your date of birth.")

    @api.constrains('mobile_phone')
    def check_mobile(self):
        for record in self:
            if record.mobile_phone:
                if not len(record.mobile_phone) == 10:
                    raise ValidationError("Your mobile number is invalid for: %s" % record.mobile_phone)
                elif not re.match("^[0-9]*$", str(record.mobile_phone)) != None:
                    raise ValidationError("Your mobile number is invalid for: %s" % record.mobile_phone)
        # records = self.env['hr.employee'].search([]) - self
        # for sssinfo in records:
        #     if record.mobile_phone:
        #         if info.mobile_phone == self.mobile_phone:
        #             raise ValidationError("This Mobile Number is already exist..")

    @api.constrains('work_phone')
    def check_phone(self):
        for record in self:
            if record.work_phone:
                if not len(record.work_phone) <= 18:
                    raise ValidationError("Your work phone no is invalid for: %s" % record.work_phone)
                elif re.match("^(\+?[\d ])+$",str(record.work_phone)) == None: #modified to allow + and space 24 april
                    raise ValidationError("Your work phone no is invalid for: %s" % record.work_phone)

    @api.constrains('date_of_joining', 'date_of_completed_probation')
    def validate_probation(self):
        for record in self:
            if record.date_of_joining:
                if str(record.date_of_completed_probation) < str(record.date_of_joining):
                    raise ValidationError("The Probation completion date should not be less than Joining date")
                # elif not record.date_of_completed_probation:
                #     raise ValidationError("Please enter Probation completion date.")

    @api.constrains('emergency_phone')
    def check_emergency_phone(self):
        for record in self:
            if record.emergency_phone:
                if not len(record.emergency_phone) <= 10:
                    raise ValidationError("Your emergency phone no is invalid for: %s" % record.emergency_phone)
                elif not re.match("^[0-9]*$", str(record.emergency_phone)) != None:
                    raise ValidationError("Your Emergency phone no is invalid for: %s" % record.emergency_phone)

    @api.constrains('personal_email')
    def check_personal_email(self):
        for record in self:
            kw_validations.validate_email(record.personal_email)

    @api.constrains('present_addr_zip')
    def check_present_pincode(self):
        for record in self:
            if record.present_addr_zip:
                if not re.match("^[0-9]*$", str(record.present_addr_zip)) != None:
                    raise ValidationError("Present pincode is not Valid")

    @api.constrains('permanent_addr_zip')
    def check_permanent_pincode(self):
        for record in self:
            if record.permanent_addr_zip:
                if not re.match("^[0-9]*$", str(record.permanent_addr_zip)) != None:
                    raise ValidationError("Permanent pincode is not valid")

    @api.onchange('emp_role')
    def _get_categories(self):
        role_id = self.emp_role.id
        self.emp_category = False
        return {'domain': {'emp_category': [('role_ids', '=', role_id)], }}

    # method to create employee code#
    def _generate_employee_code(self, work_location_id):
        emp_record_ids = self.search([], order='id desc', limit=1)
        last_id = emp_record_ids.id
        partner_record = self.env["res.partner"].sudo().browse([work_location_id])
        office_prefix = ''
        if partner_record.office_prefix:
            office_prefix = partner_record.office_prefix
        # print(company_code+" "+str(last_id))
        emp_code = office_prefix + "-" + str(last_id + 1).zfill(4)
        return emp_code

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(hr_employee_in, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                          submenu=submenu)
        # print(self.env.user.groups_id)
        doc = etree.XML(res['arch'])
        for node in doc.xpath("//group[@name='extra_info1']"):
            node.set('modifiers', '{"invisible": true}')
        for node in doc.xpath("//field[@name='extra_info2']"):
            node.set('modifiers', '{"invisible": true}')

        # condition for readonly fields, for other user groups
        if not self.env.user.multi_has_groups(['hr.group_hr_manager', 'hr.group_hr_user']):
            for node in doc.xpath("//field[@name='name']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='image']"):
                node.set('modifiers', '{"readonly": true}')

            for node in doc.xpath("//field[@name='address_id']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='work_location']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='work_email']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='mobile_phone']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='work_phone']"):
                node.set('modifiers', '{"readonly": true}')

            for node in doc.xpath("//field[@name='department_id']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='job_id']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='job_title']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='parent_id']"):
                node.set('modifiers', '{"readonly": true}')

            for node in doc.xpath("//field[@name='coach_id']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='resource_calendar_id']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='tz']"):
                node.set('modifiers', '{"readonly": true}')
            for node in doc.xpath("//field[@name='notes']"):
                node.set('modifiers', '{"readonly": true}')

            for node in doc.xpath("//field[@name='gender']"):
                node.set('modifiers', '{"readonly": true}')

            for node in doc.xpath("//group[@name='extra_info1']"):
                node.set('modifiers', '{"invisible": true}')
            for node in doc.xpath("//field[@name='extra_info2']"):
                node.set('modifiers', '{"invisible": true}')

        res['arch'] = etree.tostring(doc)
        return res

    @api.onchange('company_id')
    def _onchange_company_id(self):
        return {'domain': {'work_location_id': [('parent_id', '=', self.company_id.partner_id.id)], }}

    @api.constrains('image')
    def _check_employee_photo(self):
        allowed_file_list = ['image/jpeg', 'image/jpg', 'image/png']
        for record in self:
            kw_validations.validate_file_mimetype(record.image, allowed_file_list)
            kw_validations.validate_file_size(record.image, 1)

            # @api.multi

    # def open_chat(self):
    #     pass
    @api.multi
    def unlink(self):
        for record in self:
            record.active = False
        return True
