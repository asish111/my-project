# -*- coding: utf-8 -*-

from . import kwemp_grade_master
from . import hr_employee_in
from . import hr_employee_category_in
from . import hr_job_in
from . import hr_department_in

from . import res_partner
# from . import res_company_in

from . import kwemp_grade
from . import kwemp_family_info
from . import kwemp_language_known
from . import kwemp_identity_docs
from . import kwemp_work_experience
from . import kwemp_educational_qualification
from . import kwemp_technical_skills
from . import kwemp_membership_assc

from . import kwemp_reference_mode_master

# from . import kwemp_config_type

from . import kwemp_organization_type
from . import kwemp_industry_type
from . import kwemp_technical_skills_master


from . import kwemp_blood_group_master
from . import kwemp_religion_master
from . import kwemp_maritial_master
from . import kwemp_work_location

from . import kw_category_name
from . import kw_course_name
from . import kw_institute_name
from . import kw_profile_specializations
from . import kw_relationship_name
from . import kw_role_name
from . import kw_stream_name
from . import kwemp_employment_type
from . import kwemp_language_master
from . import kw_course_setting
from . import kw_resource_skill
from . import kwemp_band_master
from . import kw_hr_department_type
from . import kw_employee_onboarding_checklist

