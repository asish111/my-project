# -*- coding: utf-8 -*-
{
    'name': "Kwantify Employee",
    'summary': "Advance features of employee",
    'description': "Employee details module",
    'author': "CSM Tech",
    'website': "http://www.csm.co.in",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Kwantify',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'kw_sendsms', 'hr', 'kw_web_searchpanel', 'kw_skill_assessment', 'kw_branch_master'],

    # always loaded
    'data': [
        'security/kwemp_security.xml',
        'security/ir.model.access.csv',
        'views/kwemp_menus.xml',
        'views/kwemp_grade_views.xml',
        'views/hr_employees_views.xml',
        'views/kwemp_profile_master_views.xml',
        # 'views/kwemp_config_view.xml',
        'views/kwemp_organization_views.xml',
        'views/kwemp_industry_type_views.xml',
        'views/kwemp_technical_skills_master_views.xml',
        'views/kwemp_role_view.xml',
        # 'views/kwemp_emp_category.xml',
        'views/kwemp_relationship_view.xml',
        'views/kwemp_employee_type_view.xml',
        # 'views/res_company_views.xml',
        'views/kwemp_language_known.xml',
        'views/kwemp_work_location_view.xml',
        # 'views/kw_company_name_master.xml',
        'views/kwemp_blood_group_master_views.xml',
        'views/kwemp_religion_master_views.xml',
        'views/kwemp_maritial_status_views.xml',
        'views/sms/user_sms_template.xml',

        'views/res_partner_views.xml',
        'views/kwemp_course_setting_view.xml',
        'views/kw_hr_department_type.xml',
        'views/hr_department_in.xml',

        'data/religion_data.xml',
        'data/language_data.xml',
        'data/blood_group_data.xml',
        'data/family_relation_data.xml',
        # 'data/organization_grade_data.xml',
        'data/maritial_status_data.xml',
        'data/industry_type_data.xml',
        'data/technical_category_data.xml',
        'data/technical_skills_data.xml',
        'data/course_name_data.xml',
        'data/course_streams_data.xml',
        'data/course_institute_data.xml',
        # 'data/course_specializations_data.xml',
        'data/org_type_data.xml',
        'data/emp_type_data.xml',
        'data/reference_mode_data.xml',
        'data/emp_role_data.xml',
        'data/emp_category_data.xml',
        'data/kw_hr_department_type_data.xml',
        # 'data/company_data.xml',
        'data/system_param_data.xml',
        'data/course_setting_data.xml',
        # 'views/hr_employee_inherits.xml',
        'views/kw_resource_skill.xml',
        'views/hr_templates.xml',
        'views/hr_employee_kanban_search_view.xml',
        'views/kwemp_band_master_view.xml',
        'views/kw_employee_onboarding_checklist_view.xml',
        'views/kwemp_band_grade_master_view.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/kwemp_grade_demo.xml',
        # 'demo/employee_data.xml',
        # 'data/dept_data_file.xml',
        # 'data/job_designation_data.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
