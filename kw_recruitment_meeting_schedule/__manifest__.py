# -*- coding: utf-8 -*-
{
    'name': "Kwantify Recruitment Meeting Schedule",

    'summary': """
        Schedule your meetings""",

    'description': """
        Recruitment Meeting Schedule for scheduling meeting for an interview.
    """,

    'author': "CSM Tech",
    'website': "https://csm.co.in",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Kwantify / Extra Tools',
    'version': '1.0',

    # any module necessary for this one to work correctly   ,'board','web_timeline'
    'depends': ['base', 'hr_recruitment', 'kw_meeting_schedule'],

    # always loaded
    'data': [
        'data/kw_recruitment_mail_templates.xml',
        'data/kw_hr_applicant_meeting_template.xml',
        'data/hr_recruitment_data.xml',
        'views/kw_hr_applicant_view.xml',
        'views/kw_meeting_calendar.xml',
        'views/calendar_views.xml',
        'views/kw_meeting_schedule_attendee_view.xml',
        'wizards/kw_applicant_feedback_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
       # 'demo/demo.xml',
    ],
    'qweb': [],
    'application': True,
    'installable': True,
    'auto_install': False,
}
