from odoo import models,fields,api


class ApplicantFeedback(models.TransientModel):

    _name = "kw_applicant_feedback"
    _description = "Applicant Feedback Print Wizard"

    def default_meeting_id(self):
        print("Context Inside",self._context)
        return self.env['kw_meeting_events'].browse(self._context.get('active_id'))

    @api.model
    def get_applicant_domain(self):
        meeting_id = self.env['kw_meeting_events'].browse(self._context.get('active_id'))
        applicant_ids = meeting_id and meeting_id.applicant_ids and meeting_id.applicant_ids.ids or []
        return [('id', 'in', applicant_ids)]

    meeting_id = fields.Many2one("kw_meeting_events",string="Meeting",default=default_meeting_id)
    applicant_id = fields.Many2one("hr.applicant","Applicant",domain=get_applicant_domain)


    @api.multi
    def print_applicant_feedback_report(self):
        applicant_id = self.applicant_id.id
        meeting_id = self.meeting_id.id

        applicant_feedbacks = self.env['survey.user_input'].search(
            [('kw_meeting_id', '=', meeting_id), ('applicant_id','=',applicant_id),('state','!=','new')])
        if applicant_feedbacks:
            report = self.env['ir.actions.report']._get_report_from_name(
                'kw_recruitment.kw_print_feedback_report')
                
            self.env.user.notify_success("Feedback report is downloaded.")
            return report.report_action(applicant_feedbacks, config=False)

        self.env.user.notify_info("No Feedback is given for this applicant.")



