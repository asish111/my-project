# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class MeetingEvent(models.Model):
    _inherit = 'kw_meeting_events'

    
    applicant_ids = fields.Many2many('hr.applicant', string="Applicant", domain=[('stage_id.code', '!=', 'OA')])
    survey_id = fields.Many2one('survey.survey', string="Interview From", domain="[('survey_type.code', '=', 'recr')]")

    response_ids = fields.One2many(string='Meeting Feedback', comodel_name='survey.user_input',
                                   inverse_name='kw_meeting_id',
                                   ondelete='restrict'
                                   )
    mode_of_interview = fields.Selection(string='Mode Of Interview',
                                         selection=[
                                         ('Face to Face', 'Face to Face'), 
                                         ('Telephonic', 'Telephonic'),
                                         ('Video Conference', 'Video Conference')])
    meetingtype_code = fields.Char('code')
    # recruitment_access = fields.Boolean(string="Recruitment Access ?",compute="compute_recruitment_access")

    # @api.multi
    # def compute_recruitment_access(self):
    #     recruitment_group = self.env.user.has_group('')
    #     for meeting in self:

    @api.multi
    def open_feedback_wizard(self):
        print("method called")
        wizard_form_view = self.env.ref(
            'kw_recruitment_meeting_schedule.kw_recruitment_meeting_applicant_feedback_wizard_form')
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'kw_applicant_feedback',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': wizard_form_view.id,
            'target': 'new',
        }


    
    
    

    @api.onchange('meeting_type_id')
    def set_categ_ids(self):
        for record in self:
            record.categ_ids = record.meeting_type_id
            record.meetingtype_code = record.meeting_type_id.code    

    @api.model
    def create(self, vals):
        # print("method called",vals)
        record = super(MeetingEvent, self).create(vals)
        email = self.env.user.email
        # current_user = self.env.user
        if email:
            for meeting in record:
                # attendees = [attendee.name for attendee in meeting.employee_ids]
                if meeting.survey_id and meeting.applicant_ids:
                    for applicant_id in meeting.applicant_ids:
                        
                        template = self.env.ref('kw_recruitment_meeting_schedule.kw_applicant_intimatation_email_template_schedule')
                        calendar_view = self.env.ref('kw_meeting_schedule.view_kw_meeting_schedule_calendar_event_calendar')
                        action_id = self.env['ir.actions.act_window'].search([('view_id', '=', calendar_view.id)], limit=1).id
                        db_name = self._cr.dbname
                        if template:
                            body = template.body_html
                            body = body.replace('--applicantname--', applicant_id.partner_name)
                            body = body.replace('--jobposition--', applicant_id.job_position.title)
                            body = body.replace('--candidateprofile--', applicant_id.job_position.candidate_profile if applicant_id.job_position.candidate_profile else '-')
                            mail_values = {
                                'email_to': applicant_id.email_from,
                                'body_html': body,
                                }          
                            template.write(mail_values)
                            self.env['mail.template'].browse(template.id).send_mail(meeting.id, force_send=False)
                            body = body.replace( applicant_id.partner_name,'--applicantname--')
                            body = body.replace( applicant_id.job_position.title,'--jobposition--')
                            body = body.replace( applicant_id.job_position.candidate_profile if applicant_id.job_position.candidate_profile else '-','--candidateprofile--')
                            mail_values = {
                                'body_html':body
                                }          
                            template.write(mail_values)
                            for attendee in meeting.employee_ids:
                                #Email to Attendies
                                user_input = self.env['survey.user_input'].create({
                                    'kw_meeting_id': meeting.id,
                                    'survey_id': meeting.survey_id.id,
                                    'partner_id': attendee.user_id.partner_id.id,
                                    'type': 'link',
                                    'applicant_id': applicant_id.id
                                })
                                templateattendy = self.env.ref('kw_recruitment_meeting_schedule.template_meeting_invitation_attendees')
                                if templateattendy:
                                    survey_url = f"{user_input.survey_id.recruitment_survey_url}/{user_input.token}"
                                    body = templateattendy.body_html
                                    body = body.replace('--partnername--', applicant_id.partner_name)
                                    mail_values = {
                                        'subject': "Interview Schedule | "+applicant_id.job_position.title,
                                        'email_to': attendee.user_id.partner_id.email,
                                        'body_html': body,
                                        }          
                                    templateattendy.write(mail_values)
                                    
                                    meeting_attendee = self.env['kw_meeting_attendee'].search([
                                                            ('event_id', '=', meeting.id), ('employee_id', '=', attendee.id)])
                                    access_token = meeting_attendee and meeting_attendee.access_token or False
                                    self.env['mail.template'].browse(templateattendy.id).with_context(
                                        survey_url=survey_url,
                                        dbname=db_name,
                                        access_token=access_token,
                                        action_id=action_id).send_mail(meeting.id, force_send=False)
                                    body = body.replace( applicant_id.partner_name,'--partnername--')
                                    mail_values = {
                                                'body_html':body
                                                }          
                                    templateattendy.write(mail_values)
        self.env.user.notify_success("Meeting created successfully.")
        return record

    
    @api.multi
    def write(self, values):
        result = super(MeetingEvent, self).write(values)
        return result
    
    @api.multi
    def action_open_document(self):
        applicant_ids = self.applicant_ids.ids
        tree_view_id = self.env.ref('kw_recruitment.kw_recruitment_view_attachment_tree').id
        return {
            'name': 'Attachments',
            'view_type': 'form',
            'view_mode': 'tree',
            'view_id': tree_view_id,
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'domain': [('res_model', '=', 'hr.applicant'), ('res_id', 'in', applicant_ids)],
            'target': 'current',
        }



class SurveyUserInput(models.Model):
    _inherit = "survey.user_input"

    kw_meeting_id = fields.Many2one('kw_meeting_events', string="Meeting ID")
    score = fields.Integer(compute='_compute_score_remark', help='Total score of the applicant.')
    remark = fields.Char(compute='_compute_score_remark', help='Remarks of the applicant.')
    current_user = fields.Boolean(compute='_get_current_user')
    applicant_id = fields.Many2one('hr.applicant')


class RecruitmentStage(models.Model):
    _inherit = 'hr.recruitment.stage'

    code = fields.Char('Code')
