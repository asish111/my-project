# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class KwResBranch(models.Model):
    _inherit = 'kw_res_branch'


    @api.multi
    def name_get(self):
        result = []
        for record in self:
            record_name = str(record.name)
            if record.city:
                record_name = record_name+" "+record.city
            result.append((record.id, record_name))
        return result