# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _


class HrApplicant(models.Model):
    _inherit = "hr.applicant"
    _rec_name = "partner_name"

    schedule_meeting_count = fields.Integer(compute='_compute_kwmeeting_count', help='Meeting Count')

    def _compute_kwmeeting_count(self):
        for applicant in self:
            applicant.schedule_meeting_count = self.env['kw_meeting_events'].search_count(
                [('applicant_ids', '=', applicant.id)])

    @api.multi
    def action_makeMeeting_schedule(self):
        """ This opens Meeting's calendar view to schedule meeting on current applicant
            @return: Dictionary value for created Meeting view
        """
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id('kw_meeting_schedule', 'action_window_kw_meeting_schedule')
        meetingtype = self.env['calendar.event.type'].search([('code', '=', 'interview')],limit=1)
        res['domain'] = [('applicant_ids', 'in', [self.id])]
        res['context'] = {
            'visible': False,
            'default_applicant_ids': [(6, 0, [self.id])],
            'default_user_id': self.env.uid,
            'default_name': "Interview schedule -",
            'default_meeting_type_id': meetingtype.id,
        }
        return res
