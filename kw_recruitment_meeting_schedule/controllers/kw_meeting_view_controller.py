import werkzeug

from odoo.api import Environment
import odoo.http as http

from odoo.http import request
from odoo import SUPERUSER_ID
from odoo import registry as registry_get
import odoo.addons.calendar.controllers.main as main


class RecruitmentMeetingView(http.Controller):

    @http.route('/recruitment/meeting/view', type='http', auth="public")
    def view(self, db, token, action, id, view='calendar'):
        registry = registry_get(db)
        with registry.cursor() as cr:
            # Since we are in auth=none, create an env with SUPERUSER_ID
            env = Environment(cr, SUPERUSER_ID, {})
            attendee = env['kw_meeting_attendee'].search(
                [('access_token', '=', token), ('event_id', '=', int(id))])
            if not attendee:
                return request.not_found()
            # timezone = attendee.partner_id.tz
            lang = attendee.partner_id.lang or 'en_US'
           
            event = env['kw_meeting_events'].browse(int(id))

            if event.applicant_ids and request.session.uid and request.env['res.users'].browse(request.session.uid).user_has_groups('base.group_user'):
                meeting_action = request.env.ref(
                    'kw_recruitment_meeting_schedule.action_kw_meeting_events_attendee').id
                return werkzeug.utils.redirect('/web?db=%s#id=%s&view_type=form&model=kw_meeting_events&action=%s' % (db, id, meeting_action))
            
            response_content = env['ir.ui.view'].with_context(lang=lang).render_template(
                'kw_meeting_schedule.invitation_page_anonymous', {
                    'event': event,
                    'attendee': attendee,
                })
            return request.make_response(response_content, headers=[('Content-Type', 'text/html')])
