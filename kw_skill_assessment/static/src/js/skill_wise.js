odoo.define('kw_skill_assessment.skill_wise_report', function (require) {
    "use strict";
    
    var core = require('web.core');
    var framework = require('web.framework');
    var session = require('web.session');
    var ajax = require('web.ajax');
    var ActionManager = require('web.ActionManager');
    var view_registry = require('web.view_registry');
    var Widget = require('web.Widget');
    var AbstractAction = require('web.AbstractAction');
    var ControlPanelMixin = require('web.ControlPanelMixin');
    var QWeb = core.qweb;
    
    var _t = core._t;
    var _lt = core._lt;

    
    var SkillWiseReportView = AbstractAction.extend(ControlPanelMixin, {
        dept_id: 0,
        init: function(parent, value) {
            this._super(parent, value);
            var emp_score = [];
            var self = this;
            if (value.tag == 'kw_skill_assessment.skill_wise_report') {
                self._rpc({
                    route: '/skill-employee-report',
                }, []).then(function(result){
                    self.emp_score = result
                    self.render();
                    self.href = window.location.href;
                    self.render_DataTables();
                });
            }
        },
        render_DataTables: function() {

            var skill_employee_table = $('#skill_employee').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel',
                    {
                        extend: 'pdf',
                        footer: 'true',
                        orientation: 'landscape',
                        text: 'PDF',
                        exportOptions: {
                            modifier: {
                                selected: true
                            }
                        }
                    },
                ],
                scrollY:        "400px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                columnDefs: [
                    { width: 200, targets: 0 },
                    { width: 100, targets: 1 }
                ],
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns: 1
                }
            });
            
            $(".buttons-excel").addClass("btn btn-primary");
            $(".buttons-pdf").addClass("btn btn-primary");
            $(".buttons-excel").removeClass("dt-button");
            $(".buttons-pdf").removeClass("dt-button");

            skill_employee_table.columns().every(function () {
                var that = this;
                $('input', this.header()).on('keyup change clear', function () {
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
            });
            setTimeout(function(){
                //skill_employee_table.columns.adjust().draw();
                $('.dataTables_info').hide();
            },2000);
        },
        willStart: function() {
            return $.when(ajax.loadLibs(this), this._super());
        },
        start: function() {
            var self = this;
            return this._super();
        },
        render: function() {
            var super_render = this._super;
            var self = this;
            
            var kw_skill = QWeb.render( 'kw_skill_assessment.skill_wise_report', {
                widget:self.emp_score,
            });
            $(kw_skill).prependTo(self.$el);
            return kw_skill;
        },
        reload: function () {
            window.location.href = this.href;
        },
    });
    core.action_registry.add('kw_skill_assessment.skill_wise_report', SkillWiseReportView);
    return SkillWiseReportView
});
