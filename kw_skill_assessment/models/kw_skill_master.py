# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re


class kw_skill_master(models.Model):
    _name = 'kw_skill_master'
    _description = "A master model to create skills."
    _order = 'name'

    name = fields.Char(string="Name", required=True, size=100)
    description = fields.Text(string="Description")
    skill_type = fields.Many2one('kw_skill_type_master', string="Skill Type", required=True)
    # employee_id = fields.Many2many('hr.employee', string='Responsible Person')
    employee_id = fields.Many2many(
        comodel_name='hr.employee',
        relation='kw_skill_master_hr_employee_rel',
        string="Responsible Person",
        column1='skill_id',
        column2='emp_id')

    @api.constrains('name')
    def validate_skill_name(self):
        if re.match("^[a-zA-Z0-9 ,./()_+-]+", self.name) == None:
            raise ValidationError("Invalid skill! Please provide a valid skill.")

        record = self.env['kw_skill_master'].search([]) - self
        for info in record:
            if info.name.lower() == self.name.lower():
                raise ValidationError(f"The skill {self.name} already exists.")

    @api.model
    def create(self, vals):
        new_record = super(kw_skill_master, self).create(vals)
        self.env.user.notify_success(message='Skill created successfully.')
        return new_record

    @api.multi
    def write(self, vals):
        res = super(kw_skill_master, self).write(vals)
        self.env.user.notify_success(message='Skill updated successfully.')
        return res
