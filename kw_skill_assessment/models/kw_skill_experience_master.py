from odoo import models, fields, api
from odoo.exceptions import ValidationError

class kw_Skill_Experience(models.Model):
    _name = 'kw_skill_experience_master'
    _description = "A model to create skill experience."

    name = fields.Char(string="Title", required=True, size=100)
    min_exp = fields.Integer(string='Minimum Experience',default=0,required=True)
    max_exp = fields.Integer(string='Maximum Experience', default=0,required=True)
    description = fields.Char(string="Description",)
    
