from odoo import api, fields, models

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    skill_manager_emp = fields.Many2one('hr.employee',string="Employee")

    @api.multi
    def set_values(self):
        res     = super(ResConfigSettings, self).set_values()
        param   = self.env['ir.config_parameter'].sudo()

        param.set_param('kw_skill_assessment.skill_manager_emp_name', self.skill_manager_emp.id or False)
        return res

    @api.model
    def get_values(self):
        res     = super(ResConfigSettings, self).get_values()
        params  = self.env['ir.config_parameter'].sudo()

        res.update(
            skill_manager_emp = int(params.get_param('kw_skill_assessment.skill_manager_emp_name')),
        )

        return res