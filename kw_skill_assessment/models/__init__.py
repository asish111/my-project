# -*- coding: utf-8 -*-

from . import kw_question_set_config
from . import kw_skill_type_master
from . import kw_skill_group_master
from . import kw_skill_master
from . import kw_question_bank
from . import kw_question
from . import kw_my_skill
from . import kw_question_weightage
from . import kw_ans_master
from . import kw_answer_child
from . import kw_no_of_questions
from . import kw_user_wise_total_test_report
from . import kw_skill_score_master
from . import kw_skill_experience_master
from . import res_config_settings