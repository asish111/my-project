# -*- coding: utf-8 -*-
import random
import bs4 as bs
import datetime
import time
import itertools
from datetime import date
from odoo import http
from odoo.http import request
from collections import Counter


def emp_count(rec, id):
    temp = []
    for t in rec:
        if t[3] not in temp and id == t[0]:
            temp.append(t[3])
    return len(temp)


def percent_count(rec, id):
    count = emp_count(rec, id)
    if count > 0:
        score_count = 0
        temp = []
        for t in rec:
            if id == t[0]:
                score_count += t[5]
        return score_count / count


def fail(rec, id):
    count = emp_count(rec, id)
    if count > 0:
        pass


class kw_assessment(http.Controller):
    @http.route('/take_test', auth="public", website=True)
    def take_test(self, **args):
        if http.request.session.uid is None:
            return http.request.redirect('/web')

        if 'userid' in args:
            infodict = dict()

            # user_id = args['userid']
            user_id = http.request.session['uid']
            set_id = args['ques_set_id']
            skill_set = http.request.env['kw_skill_question_set_config'].search([('id', '=', set_id)])
            infodict['skill_set'] = skill_set
            emp_record = http.request.env['hr.employee'].search([('user_id', '=', user_id)])
            infodict['employee'] = emp_record
            infodict['questions'] = http.request.env['kw_skill_question_set_config'].search(
                [('id', '=', set_id)]).question_set(skill_set)
            soup = http.request.env['kw_skill_question_set_config'].search(
                [('id', '=', set_id)]).instruction
            infodict['ques_instruction'] = (bs.BeautifulSoup(soup)).text

            if skill_set.applicable_candidate == '1':
                pass
            if skill_set.applicable_candidate == '2':
                emp = http.request.env['hr.employee'].search([('user_id', '=', user_id)])
                mylst = []
                for deg in skill_set.select_deg:
                    mylst.append(deg.id)
                for r in emp:
                    if r.job_id.id not in mylst:
                        return http.request.redirect(
                            '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')
            if skill_set.applicable_candidate == '3':
                lyst = []
                for ind in skill_set.select_individual:
                    lyst.append(ind.user_id.id)
                if user_id not in lyst:
                    return http.request.redirect(
                        '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')

            dict_freq = {'y': 365 * 24, 'h': 180 * 24, 'q': 90 * 24, 'm': 30 * 24}
            frequency = skill_set.frequency
            curr_date = datetime.datetime.now()
            data = http.request.env['kw_skill_answer_master'].sudo().search(
                ['&', ('user_id', '=', user_id), ('set_config_id', '=', skill_set.id)], order="create_date desc",
                limit=1)
            if len(data) > 0:
                date_gap = abs(curr_date - data.create_date).total_seconds() / 3600.0
                if frequency == 'o':
                    return http.request.redirect(
                        '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')
                if frequency == 't':
                    a = []
                    yr = http.request.env['kw_skill_answer_master'].sudo().search(
                        ['&', ('user_id', '=', user_id), ('set_config_id', '=', skill_set.id)])
                    for rc in yr:
                        if rc.create_date.year == curr_date.year:
                            a.append(rc.id)
                    if len(a) >= 2:
                        return http.request.redirect(
                            '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')
                if frequency in dict_freq and date_gap <= dict_freq[frequency]:
                    return http.request.redirect(
                        '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')

            answer_freq = http.request.env['kw_skill_answer_master'].search(
                ['&', '&', '&', ('user_id', '=', user_id), ('set_config_id', '=', skill_set.id),
                 ('status', '!=', 'Completed'), ('created_date', '=', date.today())], order="create_date desc", limit=1)
            if not len(answer_freq) == 0:
                # for re in answer_freq:
                create_date = answer_freq.create_date
                test_duration = int(
                    http.request.env['kw_skill_question_set_config'].sudo().browse(
                        answer_freq.set_config_id.id).duration)
                final_duration = create_date + datetime.timedelta(seconds=test_duration)
                current_time = datetime.datetime.now()
                if final_duration > current_time and answer_freq.status != "Completed":
                    return http.request.redirect(
                        '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')
                if final_duration < current_time and answer_freq.status == "Completed":
                    if skill_set.applicable_candidate == '1':
                        pass
                    if skill_set.applicable_candidate == '2':
                        emp = http.request.env['hr.employee'].search([('user_id', '=', user_id)])
                        mylst = []
                        for deg in skill_set.select_deg:
                            mylst.append(deg.id)
                        for r in emp:
                            if r.job_id.id not in mylst:
                                return http.request.redirect(
                                    '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')
                    if skill_set.applicable_candidate == '3':
                        lyst = []
                        for ind in skill_set.select_individual:
                            lyst.append(ind.user_id.id)
                        if user_id not in lyst:
                            return http.request.redirect(
                                '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')

                    dict_freq = {'y': 365 * 24, 'h': 180 * 24, 'q': 90 * 24, 'm': 30 * 24}
                    frequency = skill_set.frequency
                    curr_date = datetime.datetime.now()
                    data = http.request.env['kw_skill_answer_master'].sudo().search(
                        ['&', ('user_id', '=', user_id), ('set_config_id', '=', skill_set.id)],
                        order="create_date desc", limit=1)
                    if len(data) > 0:
                        date_gap = abs(curr_date - data.create_date).total_seconds() / 3600.0
                        if frequency == 'o':
                            return http.request.redirect(
                                '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')
                        if frequency == 't':
                            a = []
                            yr = http.request.env['kw_skill_answer_master'].sudo().search(
                                ['&', ('user_id', '=', user_id), ('set_config_id', '=', skill_set.id)])
                            for rc in yr:
                                if rc.create_date.year == curr_date.year:
                                    a.append(rc.id)
                            if len(a) >= 2:
                                return http.request.redirect(
                                    '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')
                        if frequency in dict_freq and date_gap <= dict_freq[frequency]:
                            return http.request.redirect(
                                '/web#action=kw_skill_assessment.kw_my_skill_action_window&model=kw_skill_answer_master&view_type=kanban')

            child_ids = []
            for i in infodict['questions']:
                c_ans = http.request.env['kw_skill_question_bank'].sudo().search([('id', '=', i)])
                weightage = http.request.env['kw_skill_question_weightage'].sudo().search(
                    [('id', '=', c_ans.difficulty_level.id)])
                child_ids.append([0, 0, {'ques_id': i, 'correct_option': c_ans.correct_ans,
                                         'difficulty_id': c_ans.difficulty_level, 'weightage': weightage.weightage}])
            master_record = http.request.env['kw_skill_answer_master'].sudo().create({
                'user_id': user_id, 'skill_id': skill_set.skill.id,
                'skill_type_id': skill_set.skill_types.id, 'total_mark': skill_set.total_marks,
                'created_date': date.today(), 'emp_rel': emp_record.id, 'status': 'Initiated',
                'set_config_id': skill_set.id, 'child_ids': child_ids})
            template = http.request.env.ref("kw_skill_assessment.kw_skill_email_template")
            if template and skill_set.assessment_type == 'skill':
                template.send_mail(master_record.id)
            infodict['answer_master_id'] = master_record.id
            http.request.session['master_id'] = master_record.id
            return http.request.render('kw_skill_assessment.kw_skill_assessment_test', infodict)
        else:
            return http.request.redirect('/web')

    @http.route('/skill-employee-report', type='json', auth="user", website=True)
    def skill_employee_report(self, **args):

        dept = []
        departments = http.request.env['hr.department'].sudo().search([])
        for record in departments:
            dept.append({'id': record.id, 'name': record.name})

        stm = []
        skill_type_master = http.request.env['kw_skill_type_master'].sudo().search([])
        for record in skill_type_master:
            stm.append({'id': record.id, 'name': record.skill_type})

        sm = []
        skill_master = http.request.env['kw_skill_master'].sudo().search([])
        for rec in skill_master:
            sm.append({'id': rec.id, 'name': rec.name, 'skill_type': rec.skill_type.id})

        count_child_id = []
        for record in skill_type_master:
            child_id_length = http.request.env['kw_skill_master'].sudo().search([('skill_type.id', '=', record.id)])
            count_child_id.append(len(child_id_length))

        emp = http.request.env['hr.employee'].sudo().search([])
        all_answer_data = http.request.env['kw_skill_answer_master'].sudo().search([])
        percentage = []
        for record in all_answer_data:
            percentage.append(record.percentage_scored)
        emp_skill_answer_data = all_answer_data.mapped(lambda r: r.user_id)
        # question_set_assessment_type = all_answer_data.filtered(lambda r: r.set_config_id.assessment_type == "skill")
        # print(question_set_assessment_type)
        emp_skill_data = []
        for employee in emp:
            # if question_set_assessment_type.set_config_id.assessment_type == 'skill':
            if emp_skill_answer_data:
                skill_answer_master_data = {}
                skill_answer_master_data['emp_name'] = employee.name
                skill_answer_master_data['emp_code'] = employee.emp_code
                skill_answer_master_data['designation'] = employee.job_id.name
                sum = 0
                color = ''
                for skill_master_data in skill_master:
                    filter_emp_answer_data = all_answer_data.filtered(
                        lambda r: r.user_id == employee.user_id and r.skill_id.id == skill_master_data.id and r.set_config_id.assessment_type == "skill")
                    percentage_sc = 0
                    if filter_emp_answer_data:
                        maxm = max([record.id for record in filter_emp_answer_data])
                        for total_mark in filter_emp_answer_data:
                            if total_mark.id == maxm:
                                percentage_sc = float("%.2f" % total_mark.percentage_scored)
                                print(percentage_sc)
                                if round(percentage_sc) <= 40:
                                    color = "Red"
                                elif round(percentage_sc) >= 41 and round(percentage_sc) <= 70:
                                    color = "Orange"
                                else:
                                    color = "Green"

                                t_mark = total_mark.total_mark_obtained
                                sum = sum + t_mark
                    skill_answer_master_data[skill_master_data.name] = (color if filter_emp_answer_data else 'No')
                    skill_answer_master_data[skill_master_data.id] = (percentage_sc if filter_emp_answer_data else 'No')
                skill_answer_master_data['sum_skill_mark'] = sum
                emp_skill_data.append(skill_answer_master_data)
        infodict = dict(department=dept, skill_type=stm, count_child=count_child_id, skill_master=sm,
                        emp_scores=emp_skill_data)
        return infodict

    @http.route('/result', type='http', auth="user", website=True)
    def skill_result(self, **args):
        answer_masterid = args.get('answer_master_id', False)
        infodict = dict()
        infodict['weightage_type'] = {}
        infodict['total'] = {}
        user_id = http.request.session['uid']
        infodict['employee'] = http.request.env['hr.employee'].search([('user_id', '=', user_id)])
        infodict['answer_record'] = http.request.env['kw_skill_answer_master'].search([('id', '=', answer_masterid)])
        weightage_records = http.request.env['kw_skill_question_weightage'].search([])
        alloted_duration = http.request.env['kw_skill_question_set_config'].search(
            [('id', '=', infodict['answer_record'].set_config_id.id)]).duration
        infodict['alloted_time'] = self.convert(int(alloted_duration))

        if infodict['answer_record'].time_taken and str.isdigit(infodict['answer_record'].time_taken):
            infodict['time_taken'] = self.convert(int(infodict['answer_record'].time_taken))
            # infodict['time_taken'] = time.strftime("%Hhr %Mmin %Ssec", time.gmtime(int(infodict['answer_record'].time_taken)))
        else:
            infodict['time_taken'] = time.strftime("%Hhr %Mmin %Ssec",
                                                   time.gmtime(infodict['answer_record'].time_taken))

        total_questions = 0
        total_correct_answer = 0
        for c in weightage_records:
            sec_mark = 0
            correct_answer = 0
            no_ques = 0
            child_rec = http.request.env['kw_skill_answer_child'].search(
                ['&', ('ans_id', '=', int(answer_masterid)), ('difficulty_id', '=', c.id)])
            if len(child_rec) > 0:
                for record in child_rec:
                    sec_mark += record.mark_obtained
                    if record.selected_option == record.correct_option:
                        correct_answer += 1
                quest_config_rec = http.request.env['kw_skill_question_set_config'].search(
                    [('id', '=', infodict['answer_record'].set_config_id.id)]).add_questions
                for q in quest_config_rec:
                    if q.question_type.id == c.id:
                        no_ques = q.no_of_question
                total_questions += no_ques
                total_correct_answer += correct_answer
                infodict['weightage_type'].update({c.name: [no_ques, correct_answer, sec_mark]})
        infodict['total_ques'] = total_questions
        infodict['total_correct_ans'] = total_correct_answer

        return http.request.render('kw_skill_assessment.kw_skill_result', infodict)

    @http.route('/test-instruction/<model("kw_skill_question_set_config"):question_set>', type='http', auth="user",
                website=True)
    def test_instruction(self, question_set, **args):
        if http.request.session.uid is None:
            return http.request.redirect('/web')
        return http.request.render('kw_skill_assessment.kw_skill_assessment_index', {'question_set': question_set})

    # def convert_to_string(self, seconds):
    #     min, sec = divmod(seconds, 60) 
    #     hour, min = divmod(min, 60) 
    #     if hour > 0:
    #         return "%dhr %02dmin %02dsec" % (hour, min, sec) 
    #     elif min > 0:
    #         return "%02dmin %02dsec" % (min, sec) 
    #     else:
    #         return "%02dsec" % (sec)

    def convert(self, seconds):
        seconds = seconds % (24 * 3600)
        hour = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60

        if hour > 0 and minutes == 0:
            return "%dh" % (hour)
        elif minutes > 0 and hour > 0:
            return "%dh %dm" % (hour, minutes)
        elif minutes > 0 and hour == 0:
            return "%dm" % (minutes)
        else:
            return "%ds" % (seconds)

    @http.route('/skill-stack-report', type='json', auth="user", website=True)
    def skill_stack_report(self, **args):
        query = "select gm.id as gm_id, gm.name as gm_name, a.id as a_id, a.user_id as a_user_id, a.skill_id as a_skill_id, a.percentage_scored as a_percentage_scored\
                from kw_skill_group_master gm\
                join kw_skill_group_master_kw_skill_master_rel gmr on gm.id=gmr.kw_skill_group_master_id\
                join kw_skill_master sm on sm.id=gmr.kw_skill_master_id\
                join kw_skill_answer_master a on a.skill_id=sm.id\
                where a.id in (select id from (select row_number() over(partition by user_id, skill_id order by id desc) as sl_no, * from kw_skill_answer_master) adt where sl_no=1)"
        request.cr.execute(query)
        data = request.cr.fetchall()
        sgm = []
        sgm_record = http.request.env['kw_skill_group_master'].sudo().search([])
        for record in sgm_record:
            sgm.append({'id': record.id, 'name': record.name,
                        'skills': [r.name for r in record.skills],
                        'emp_count': emp_count(data, record.id),
                        'percentage': percent_count(data, record.id)})
        infodict = dict(skill_group=sgm)
        return infodict

    @http.route('/skill-report', type='json', auth="user", website=True)
    def skill_report(self, **args):

        query = "\
            with sam as \
            (\
            select skill_id, sum(case when percentage_scored < 40 then 1 else 0 end) as fail, COUNT(a.ID) AS TOTAL\
            , sum(case when percentage_scored between 41 and 70 then 1 else 0 end) as average\
            , sum(case when percentage_scored > 71 then 1 else 0 end) as good\
            , avg(percentage_scored) as percentage_scored\
            from kw_skill_answer_master a\
            where a.id in (select id from \
            (select row_number() over(partition by user_id,skill_id order by id desc) as sl_no, * from kw_skill_answer_master where set_config_id in \
            (select id from kw_skill_question_set_config where assessment_type = 'skill')\
            ) adt where sl_no=1) group by skill_id)\
            \
            select sm.id,sm.name\
            ,COALESCE(TOTAL,0) as TOTAL\
            , COALESCE(fail, 0) as fail\
            , COALESCE(average, 0) as average\
            , COALESCE(good, 0) as good\
            , COALESCE(round( CAST(sam.percentage_scored as numeric), 2), 0) as percentage_scored\
            from kw_skill_master sm\
            LEFT join sam on sm.id = sam.skill_id"
        request.cr.execute(query)
        data = request.cr.fetchall()

        sk = []
        for record in data:
            sk.append(
                {'id': record[0], 'name': record[1], 'emp_count': record[2], 'fail': record[3], 'average': record[4],
                 'good': record[5], 'percentage': record[6]})

        infodict = dict(sk_master=sk)
        return infodict
