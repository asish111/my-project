# -*- coding: utf-8 -*-
{
    'name': "Kwantify Recruitment DMS Integration",
    'summary': """Store recruitment CV materials at Document""",
    'description': """Store recruitment CV materials at Document""",
    'author': "CSM Technologies",
    'website': "http://www.csm.co.in",
    'category': 'DMS/Integration',
    'version': '0.1',
    'depends': ['base', 'kw_recruitment', 'kw_dms'],
    'data': [
        'data/kw_recruitment_dms_integration_data.xml',
        'data/dms_scheduler_data.xml',
        'views/kw_recruitment_cv_material_view.xml',
        'views/res_config_setting.xml',
    ],
    "application": False,
    "installable": True,
    'auto_install': False,
}
