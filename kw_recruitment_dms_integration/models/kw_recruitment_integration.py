# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, SUPERUSER_ID
from odoo.exceptions import ValidationError, AccessError
from odoo import tools, _
from odoo import models, fields, api

class kw_recruitment_cv_dms_integration(models.Model):
    _name = 'ir.attachment'
    ##integration with DMS
    _inherit = [
        'ir.attachment',
        'kw_dms.kw_recruitment.integration',
    ]

    content_file = fields.Binary(required=True, string="Upload Document")

    def auto_document_migration(self):
        applications = self.env['ir.attachment'].search([('res_model','=', 'hr.applicant'),('dms_file_id','=', False)])
        if applications:
            for application in applications:
                if application.datas != None:
                    res_id = self.env['kw_dms.file'].search([('res_id','=',application.id)])
                    if not res_id:
                        dms_id = self.env['kw_dms.kw_recruitment.integration'].create_dms_file_with_resid(application.datas,application.name,application)               
        return True

class kw_recruitment_cv_dms(models.Model):

    _inherit = 'hr.applicant'

    dms_user_doc_dir_id = fields.Many2one('kw_dms.directory', ondelete='restrict',
                                          string="DMS User Document ID",)
    dms_user_doc_dir_access_group = fields.Many2one('kw_dms_security.access_groups', ondelete='restrict',
                                                    string="DMS User Document Access Group",)
    attachment_ids = fields.One2many('ir.attachment', 'res_id', domain=[('res_model', '=', 'hr.applicant')], string='Attachments')

    @api.model
    def create(self, vals):
        if not vals.get('attachment_ids') or vals.get('attachment_ids') == []:
            raise ValidationError("Please Add Documents.")
        else:
            for attachment in vals.get('attachment_ids'):
                if isinstance(attachment[2],dict):
                    attachment[2]['res_model'] = 'hr.applicant'
        return super(kw_recruitment_cv_dms, self.with_context(mail_create_nolog=True)).create(vals)

    @api.multi
    def write(self, vals):
        if not self.attachment_ids or vals.get('attachment_ids') == []:
            raise ValidationError("Please Add Documents.")
        if vals.get('attachment_ids'):
            for attachment in vals.get('attachment_ids'):
                if isinstance(attachment[2],dict):
                    attachment[2]['res_model'] = 'hr.applicant'
        return super(kw_recruitment_cv_dms, self.with_context(mail_create_nolog=True)).write(vals)
    @api.multi
    def _create_recruitment_cv_folder(self):

        config_params = self.env['ir.config_parameter'].sudo()
        dir_model = self.env['kw_dms.directory'].sudo()
        access_group_model = self.env['kw_dms_security.access_groups'].sudo()

        dir_records = dir_model.search([])
        access_group_records = access_group_model.search([])

        recruitment_upload_folder = int(config_params.get_param(
            'kw_dms.recruitment_cv_folder', False))


    
        for record in self:

            dms_user_doc_dir_access_group_id = record.dms_user_doc_dir_access_group.id
            dms_user_doc_dir_id = record.dms_user_doc_dir_id.id

            # upload_doc_dir_access_group_id = record.upload_doc_dir_access_group.id
            # upload_doc_dir_id = record.upload_doc_dir_id.id

            updatests = 0

            if record.create_date:

                folder_name = f"{record.partner_name}-{record.create_date.strftime('%d-%b-%Y')}"

                # upload_access_group_name = '%s_upload_access_group' % (
                #     folder_name)
                dms_user_access_group_name = '%s_user_access_group' % (
                    folder_name)

                if dms_user_doc_dir_id and record.dms_user_doc_dir_id.name != folder_name:
                    record.dms_user_doc_dir_id.write({'name': folder_name})

                ##create main user directory
                if not dms_user_doc_dir_id:

                    updatests = 1

                    existing_dms_user_doc_dir = dir_records.filtered(
                        lambda r: r.name == folder_name)
                    existing_dms_user_access_group = access_group_records.filtered(
                        lambda r: r.name == dms_user_access_group_name)

                    if existing_dms_user_access_group:
                        dms_user_doc_dir_access_group_id = existing_dms_user_access_group.id
                    else:
                        dms_user_doc_dir_access_group_id = access_group_model.create(
                            {'name': dms_user_access_group_name, 'perm_read': True, 'explicit_users': [[4, self.env.user.id, 0]]}).id

                    if existing_dms_user_doc_dir:
                        dms_user_doc_dir_id = existing_dms_user_doc_dir.id
                    else:
                        dms_user_doc_dir_id = dir_model.create({'name': folder_name, 'parent_directory': recruitment_upload_folder, 'groups': [
                                                               [4, dms_user_doc_dir_access_group_id, 0]]}).id


                if updatests == 1:
                   record.write({'dms_user_doc_dir_access_group': dms_user_doc_dir_access_group_id,
                                 'dms_user_doc_dir_id': dms_user_doc_dir_id, })

            else:
                raise ValidationError(
                    "Please select applicant before uploading any document.")

