# -*- coding: utf-8 -*-
import requests, json
import logging
from datetime import date
from odoo import models, fields, api

class JobPositionsSync(models.Model):
    _inherit = "kw_hr_job_positions"

    @api.model
    def sync_job_lists(self):
        curr_date = date.today()
        job_lists = self.env["kw_hr_job_positions"].search(
            ['&', ("website_published", "=", True), ("expiration", '>=', curr_date)])
        if len(job_lists) > 0:
            payload = {"method": "syncJobOpenings", "data": []}
            for job in job_lists:
                payload["data"].append({
                    "id": job.id,
                    "title": job.title if job.title else "",
                    "location": [loc.id for loc in job.address_id] if job.address_id else [],
                    "department_id": job.department_id.id if job.department_id else "",
                    "code": job.job_code if job.job_code else "",
                    "description": job.description if job.description else "",
                    "publish_date": str(job.job_publish_date) if job.job_publish_date else "",
                    "publish_status": 1 if job.website_published else 0,
                    "expiration": str(job.expiration) if job.expiration else "",
                    "no_of_post": job.no_of_recruitment if job.no_of_recruitment else "",
                    "qualification": [qual.id for qual in job.qualification] if job.qualification else [],
                    "min_exp_year": job.min_exp_year if job.min_exp_year else 0,
                    "max_exp_year": job.max_exp_year if job.max_exp_year else 0,
                    "job_category": job.emp_category_id.id if job.emp_category_id else "",
                    "industry_type": job.industry_type.id if job.industry_type else "",
                    "travel": 1 if job.travel else 0,
                    "summary": job.summary if job.summary else "",
                    "candidate_profile": job.candidate_profile if job.candidate_profile else "",
                    "job_image": job.attachment_url if job.attachment else "",
                    "set_homepage": 1 if job.on_homepage else 0
                })
            log = self.env["kw_recruitment_career_sync_log"].sudo()
            data = json.dumps(payload)
            try:
                print(f"Prepared payload inside job opening sync is {payload}")
                url = self.env["ir.config_parameter"].sudo().get_param("kw_recruitment_career_sync_system_parameter")
                print("URL is", url)
                if not url:
                    url = "http://192.168.103.229/CSM/api/consoleServices"
                response_obj = requests.post(
                    url, headers={"Content-Type": "application/json"}, data=data, timeout=30)
                content = response_obj.content
                # print("Content is ",content)
                resp = json.loads(content.decode("utf-8"))
                print(f"Response after request to sync job opening {resp}")
                if resp['status'] == '200':
                    log.create({
                        "name": "Job Sync",
                        "payload": data,
                        'status': 'Success'
                    })
                    print("Job opening sync Successful.")
                    self.env.user.notify_success(message='Sync successful.')
            except Exception as e:
                print("Error : no response from career server", e)
                log.create({
                    "name": "Job Sync",
                    "payload": data,
                    'status': 'Failed'
                })
                self.env.user.notify_info(message='An error occurred while job opening sync.')

    @api.model
    def sync_job_master(self):
        location = self.env["kw_recruitment_location"].search([])
        qualification = self.env['kw_qualification_master'].search([])
        job_category = self.env['kw_job_category'].search([])
        industry_type = self.env['kw_industry_type'].search([])

        payload = {"method": "syncJobMasterData","data": {"location": [],"qualification":[],"job_category":[],"industry_type":[]}}
        payload["data"]["location"] = [{"id": loc.id,"name": loc.name} for loc in location]

        payload["data"]["qualification"] = [{"id": qual.id,"name": qual.name,"code": qual.code if qual.code else "",
                                             "sequence": qual.sequence if qual.sequence else ""} for qual in qualification]

        payload["data"]["job_category"] = [{"id": job.id,"name": job.name} for job in job_category]

        payload["data"]["industry_type"] = [{"id": ind.id,"name": ind.name} for ind in industry_type]
        
        log = self.env["kw_recruitment_career_sync_log"].sudo()
        data = json.dumps(payload)
        print("Prepared payload is", data)
        try:
            url = self.env["ir.config_parameter"].sudo().get_param(
                "kw_recruitment_career_sync_system_parameter")
            if not url:
                url = "http://192.168.103.229/CSM/api/consoleServices"
            response_obj = requests.post(url, headers={"Content-Type": "application/json"}, data=data, timeout=30)
            content = response_obj.content
            # print("Content is ", content)
            resp = json.loads(content.decode("utf-8"))
            print(f"Response after request to sync job master{resp}")
            if resp['status'] == '200':
                log.create({
                    "name": "Master Sync",
                    "payload": data,
                    'status': "Success",
                })
                # print("Sync Successful.")
                self.env.user.notify_success(message='Master sync successful.')
        except Exception as e:
            print("Error : no response from career server", e)
            log.create({
                "name": "Master Sync",
                "payload": data,
                'status': "Failed",
            })
            self.env.user.notify_info(message='An error occurred while master sync.')
