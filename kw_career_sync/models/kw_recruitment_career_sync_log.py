# -*- coding: utf-8 -*-
from odoo import models, fields, api


class JobPositionsSync(models.Model):
    _name = "kw_recruitment_career_sync_log"
    _description = "Stores sync log with career site."
    _order = "create_date desc"

    name = fields.Char(string="Name")
    date = fields.Date(string='Date', default=fields.Date.context_today, )
    payload = fields.Char(string="Payload")
    status = fields.Char("Status")
