# -*- coding: utf-8 -*-
import json
from odoo import http
from odoo.http import request


class APIController(http.Controller):

    @http.route("/submit-applicant/<job_position_id>", type="http", cors='*', auth="none", methods=["POST"], csrf=False)
    def save_applicant(self, job_position_id=None, **payload):
        print("Received Payload Is.", payload)
        data_payload = str(payload)
        log = request.env['kw_recruitment_career_sync_log'].sudo()
        try:
            job_position_id = int(job_position_id)
        except Exception:
            log.create({'name': 'Applicant Create', 'payload': data_payload, 'status': 'Failed', })
            return json.dumps({
                "status": "failed",
                "message": f"Invalid job position id, {job_position_id}."
            })
               
        else:
            job_position = request.env['kw_hr_job_positions'].sudo().search([('id', '=', job_position_id)])
            if len(job_position) == 0:
                return json.dumps({
                    "status": "failed",
                    "message": f"The job id, {job_position_id} is not available in the database."
                })
            if 'partner_name' not in payload:
                return json.dumps({
                    "status": "failed",
                    "message": "Missing required parameter (partner_name)."
                })
            if 'partner_mobile' not in payload:
                return json.dumps({
                    "status": "failed",
                    "message": "Missing Required Parameter (partner_mobile)"
                })
            if 'email_from' not in payload:
                return json.dumps({
                    "status": "failed",
                    "message": "Missing Required Parameter (email_from)"
                })
            else:
                Applicant = request.env['hr.applicant'].sudo()
                payload['job_position'] = job_position_id
                payload['name'] = f"{payload['partner_name']}'s Application"
                payload['job_id'] = job_position.job_id.id if job_position.job_id else False
                if ('job_location_id' in payload and payload['job_location_id'] == '') or ('job_location_id' not in payload):
                    # del payload['job_location_id']
                    if job_position.address_id and len(job_position.address_id) == 1:
                        payload['job_location_id'] = job_position.address_id[0].id
                # payload['job_location_id'] = job_position.address_id.id if job_position.address_id else False
                payload['department_id'] = job_position.department_id.id if job_position.department_id else False
                if 'qualification' in payload:
                    if payload['qualification'] == "":
                        del payload['qualification']
                    else:
                        if payload['qualification'].isdigit():
                            qual_id = request.env['kw_qualification_master'].sudo().search([('id', '=', payload['qualification'])])
                            if len(qual_id) == 0:
                                return json.dumps({"status": "failed", "message": f"The qualification id, {payload['qualification']} is not available in the database."})
                            else:
                                payload['qualification_ids'] = [[6, 0, [qual_id.id]]]
                        else:
                            return json.dumps({"status": "failed", "message": f"The qualification id should be in integer format."})
                try:
                    # Create applicant
                    source_id = request.env['utm.source'].sudo().search([('code', '=', 'website')])
                    payload['source_id'] = source_id.id
                    record = Applicant.create(payload)
                    if 'attachment' in payload:
                        # base64.encodestring(payload['attachment'].read())
                        # filename = payload['attachment'].filename
                        file_name = payload['file_name'] if 'file_name' in payload else False
                        file = payload['attachment']
                        if file_name:
                            record.sudo().write({'attachment_ids': [[0, 0, {'name': file_name,
                                                                            'datas_fname':file_name,
                                                                            'datas': file,
                                                                            'res_name': payload['name'],
                                                                            'res_model': 'hr.applicant',
                                                                            'res_model_name': 'Applicant',
                                                                            'res_id': record.id, }
                                                                    ]]
                                                 })
                        else:
                            record.sudo().write({'attachment_ids': [[0, 0, {'datas': file,
                                                                            'res_name': payload['name'],
                                                                            'res_model': 'hr.applicant',
                                                                            'res_model_name': 'Applicant',
                                                                            'res_id': record.id, }
                                                                     ]]
                                                 })
                    log.create({
                        'name': 'Applicant Create',
                        'payload': data_payload,
                        'status': 'Success',
                    })
                except Exception as e:
                    # print(e)
                    log.create({
                        'name': 'Applicant Create',
                        'payload': data_payload,
                        'status': 'Failed',
                    })
                    return json.dumps({"status": "failed", "message": "Error occurred during submission."})
                else:
                    return json.dumps({"status": "success", "message": "Applicant created successfully."})
