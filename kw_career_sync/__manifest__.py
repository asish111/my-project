# -*- coding: utf-8 -*-
{
    'name': "kwantify career sync",

    'summary': """Sync with career site""",

    'description': """
        
    """,

    'author': "CSM Technologies",
    'website': "www.csm.co.in",

    'category': 'Recruitment',
    'version': '1',

    'depends': ['base','kw_recruitment'],

    'data': [
        'security/ir.model.access.csv',
        'data/kw_sync_with_career_data.xml',
        'data/kw_recruitment_career_sync_system_parameter.xml',
        "views/kw_recruitment_career_sync_log_view.xml",
        'views/kw_sync_with_career_view.xml'
    ],
}
