# -*- coding: utf-8 -*-

from . import kw_meeting_amenity_master
from . import kw_meeting_room_master
from . import calendar_event_type
from . import kw_meeting_reminder
from . import kw_meeting_attendee
from . import kw_meeting_calendar
from . import kw_meeting_agenda
from . import kw_meeting_agenda_proposals
from . import kw_meeting_external_participants
from . import kw_meeting_agenda_activities
from . import ir_http
from . import kw_mymeeting_dategroup
from . import kw_meeting_all_participants
from . import res_partner_in
# from . import calendar_event_integration
