from odoo import models, fields, api
from odoo.exceptions import ValidationError


class kw_appraisal_goal(models.Model):
    _name = 'kw_appraisal_goal'
    _description = 'Appraisal goal'
    _rec_name = 'employee_id'

    appraisal_period = fields.Many2one('kw_assessment_period_master', string='Appraisal Period', )
    employee_id = fields.Many2one('hr.employee', string='Employee', )
    emp_code = fields.Char('Emp Code', related='employee_id.emp_code')
    emp_deg = fields.Char('Designation', related='employee_id.job_id.name')
    name = fields.Char('Goal', required=True)
    milestone_id = fields.One2many('kw_appraisal_milestone', 'goal_id', required=True)
    lm_score = fields.Char(string='LM Score')
    lm_remarks = fields.Char(string='LM Remarks')
    ulm_remarks = fields.Char(string='ULM Remarks')

    # @api.multi
    # @api.constrains('appraisal_period','employee_id')
    # def no_duplicate(self):
    #     goal_records = self.env['kw_appraisal_goal'].sudo().search([])-self
    #     for record in goal_records:
    #         appraisal_periods = record.appraisal_period.ids
    #         employee_ids = record.employee_id.ids
    #         if str(self.appraisal_period.id) in str(appraisal_periods) and str(self.employee_id.id) in str(employee_ids):
    #             raise ValueError("This goal has already exists.")

    @api.model
    def save_datas(self, args):
        kra_data = self.env['kw_appraisal_kra'].search(['&', ('appraisal_period', '=', int(args.get('appraisal_year'))),
                                                        ('employee_id', '=', int(args.get('employee_name')))], limit=1)
        hr_app_data = self.env['hr.appraisal'].search(
            ['&', ('appraisal_year_rel', '=', int(args.get('appraisal_year'))),
             ('emp_id', '=', int(args.get('employee_name')))], limit=1)
        user_input = self.env['survey.user_input'].sudo().search(['&', ('appraisal_id', '=', hr_app_data.id), ('token', '=', args.get('token'))], limit=1)
        values = []
        if user_input.state != 'done':
            if args.get('actual'):
                actual_data = float(args.get('actual'))
            else:
                actual_data = 0
            if len(kra_data):
                kra_data.write(
                    {'appraisal_period': int(args.get('appraisal_year')), 'employee_id': int(args.get('employee_name')),
                     'actual_score': actual_data, 'achievement': args.get('achievement')})
                hr_app_data.write({'kra_score': actual_data})
            else:
                self.env['kw_appraisal_kra'].create(
                    {'appraisal_period': int(args.get('appraisal_year')), 'employee_id': int(args.get('employee_name')),
                     'actual_score': actual_data, 'achievement': args.get('achievement')})
                hr_app_data.write({'kra_score': actual_data})

            prev_year = args.get('someObj', False)
            if prev_year and len(prev_year) > 0 or not prev_year and len(prev_year) == 0:
                prev_reord = self.env['kw_appraisal_goal'].sudo().search(['&',
                                                                          ('appraisal_period', '=',
                                                                           int(args.get('appraisal_year')) - 1),
                                                                          ('employee_id', '=',
                                                                           int(args.get('employee_name')))],
                                                                         limit=1)
                if len(prev_reord) > 0:
                    for data in prev_reord.milestone_id:
                        if str(data.id) in prev_year:
                            data.write({'status': True})
                        else:
                            data.write({'status': False})

            survey_id = args.get('survey')
            token = args.get('token')
            if args.get('draft'):
                user_input.sudo().write({'last_displayed_page_id': 0})
                values = [hr_app_data.state.name, survey_id, token, args.get('self_employee_id')]
            else:
                user_input.sudo().write({'last_displayed_page_id': 0})
                values = [survey_id, token, args.get('self_employee_id')]
            record = self.env['kw_appraisal_goal'].sudo().search(
                ['&', ('appraisal_period', '=', int(args.get('appraisal_year'))),
                 ('employee_id', '=', int(args.get('employee_name')))])
            if len(record) > 0:
                mlstn = []
                if args.get('milestones', False) and len(args.get('milestones')) > 0:
                    recv_milestone = args.get('milestones')
                    for milestones in record.milestone_id:
                        if milestones.name in args.get('milestones'):
                            mlstn.append([4, milestones.id, False])
                            recv_milestone.remove(milestones.name)
                        else:
                            mlstn.append([2, milestones.id, False])
                    for data in recv_milestone:
                        mlstn.append([0, 0, {'name': data}])
                elif not args.get('milestones', False):
                    for milestones in record.milestone_id:
                        mlstn.append([2, milestones.id, False])
                record.write({'appraisal_period': int(args.get('appraisal_year')),
                              'employee_id': int(args.get('employee_name')),
                              'name': args.get('goal_name'), 'milestone_id': mlstn, 'lm_remarks': args.get('lm_remark'),
                              'ulm_remarks': args.get('ulm_remark'), 'lm_score': args.get('lm_score')})
            else:
                vals = []
                if args.get('milestones'):
                    for record in args.get('milestones'):
                        vals.append([0, 0, {'name': record}])
                new_record = self.env['kw_appraisal_goal'].sudo().create(
                    {'appraisal_period': int(args.get('appraisal_year')), 'employee_id': int(args.get('employee_name')),
                     'name': args.get('goal_name'),
                     'milestone_id': vals, 'lm_remarks': args.get('lm_remark'), 'ulm_remarks': args.get('ulm_remark'),
                     'lm_score': args.get('lm_score')
                     })
        return values
