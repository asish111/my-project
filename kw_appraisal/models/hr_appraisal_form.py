# -*- coding: utf-8 -*-

from odoo import models, fields, api, SUPERUSER_ID
from datetime import datetime
from odoo.exceptions import ValidationError
import pytz


class HrAppraisalForm(models.Model):
    _name = 'hr.appraisal'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'emp_id'
    _description = 'Appraisal'

    @api.model
    def _read_group_stage_ids(self, categories, domain, order):
        """ Read all the stages and display it in the kanban view, even if it is empty."""
        category_ids = categories._search([], order=order, access_rights_uid=SUPERUSER_ID)
        return categories.browse(category_ids)

    def _default_stage_id(self):
        """Setting default stage"""
        rec = self.env['hr.appraisal.stages'].search([], limit=1, order='sequence ASC')
        return rec.id if rec else None

    appraisal_year_rel = fields.Many2one('kw_assessment_period_master', string="Appraisal relation")
    appraisal_year = fields.Char(string="Appraisal Year")
    kw_ids = fields.Integer(string="kw ids")
    emp_id = fields.Many2one('hr.employee', string="Employee", required=True)
    employee_ra = fields.Char(related='emp_id.parent_id.name', store=False)
    e_code = fields.Char(string="Employee Code", related="emp_id.emp_code")
    deg_id = fields.Char(related='emp_id.job_id.name',string='Designation')
    dept_id = fields.Many2one('hr.department',compute='_get_department',string='Department',store=False)
    current_dt = fields.Date(string="Today Date", default=fields.Date.today())
    app_period_from = fields.Date("Start Date", required=True, readonly=False)
    appraisal_deadline = fields.Date(string="End Date", required=True)
    final_interview = fields.Date(string="Final Interview", help="After sending survey link,you can"
                                                                 " schedule final interview date")
    company_id = fields.Many2one('res.company', string='Company', default=lambda self: self.env.user.company_id)
    hr_emp = fields.Boolean(string="Self", default=False)
    hr_manager = fields.Boolean(string="LM select", default=False)
    hr_collaborator = fields.Boolean(string="ULM select", default=False)
    hr_colleague = fields.Boolean(string="Colleague(Peer)", default=False)
    hr_manager_id = fields.Many2many('hr.employee', 'manager_appraisal_rel', string="LM")
    hr_collaborator_id = fields.Many2many('hr.employee', 'collaborators_appraisal_rel',
                                          string="ULM")
    manager_survey_id = fields.Many2one('survey.survey', string="Select Manager Opinion Form")
    emp_survey_id = fields.Many2one('survey.survey', string="Appraisal Form")
    collaborator_survey_id = fields.Many2one('survey.survey', string="Select Collaborator Opinion Form")
    colleague_survey_id = fields.Many2one('survey.survey', string="Select Colleague Opinion Form")
    response_id = fields.Many2one('survey.user_input', "Response", ondelete="set null", oldname="response")
    final_evaluation = fields.Text(string="Final Evaluation")
    tot_comp_survey = fields.Integer(string="Count Answers", compute="_compute_completed_survey")
    tot_sent_survey = fields.Integer(string="Count Sent Questions")
    created_by = fields.Many2one('res.users', string="Created By", default=lambda self: self.env.uid)
    state = fields.Many2one('hr.appraisal.stages', string='Stage', track_visibility='onchange', index=True,
                            default=lambda self: self._default_stage_id(),
                            group_expand='_read_group_stage_ids')
    # for coloring the kanban box
    color = fields.Integer(string="Color Index")
    kw_appraisal_id = fields.Many2one('kw_appraisal', 'Appraisal IDS')

    # LM & ULM start and end date field
    hr_lm_start_date = fields.Date(string="LM Start Date", required=True)
    hr_lm_end_date = fields.Date(string='LM End Date', required=True)
    hr_ulm_start_date = fields.Date(string='ULM Start Date', required=True)
    hr_ulm_end_date = fields.Date(string='ULM End Date', required=True)
    survey_state_id = fields.Char(string="Employee Survey Status", compute='_survey_response_id')
    ra_id = fields.Char(string="Assessor Name", compute='_reviewer')
    date_time = fields.Date(string='Current Date', compute='_find_current_date')
    hr_survey_status = fields.Char(string="Status", compute='_hr_survey_state')
    score = fields.Float(string="Appraisal Score", compute='count_score', store=True,
                         help='Score will visible after publish your appraisal')
    compare_self_date = fields.Boolean(string="Self compare date", compute='_self_compare_date')
    compare_lm_date = fields.Boolean(string="LM compare date", compute='_lm_compare_date')
    compare_ulm_date = fields.Boolean(string="ULM compare date", compute='_ulm_compare_date')
    compare_self_end_date = fields.Boolean(string="Self End Date", compute='_self_end_date')
    compare_lm_end_date = fields.Boolean(string="compare lm End Date", compute='_lm_end_date')
    compare_ulm_end_date = fields.Boolean(string="compare ulm End Date", compute='_ulm_end_date')
    state_compare = fields.Boolean(string="State Compare", compute='_state_compare')
    kra_score = fields.Float(string="KRA Score", help='KRA Score will visible after publish your appraisal')
    hr_end_date = fields.Char(string="End Date", compute='_hr_end_date')
    user_input_ids = fields.One2many('survey.user_input', 'appraisal_id')

    reassessment = fields.Boolean(default=False)
    self_input_id = fields.Many2one('survey.user_input')
    lm_input_id = fields.Many2one('survey.user_input')
    ulm_input_id = fields.Many2one('survey.user_input')

    def _get_department(self):
        for record in self:
            if record.emp_id:
                department = self.env['hr.employee'].search([('id','=',record.emp_id.id)],limit=1)
                record.dept_id = department.department_id.id

    @api.model
    def _hr_end_date(self):
        for record in self:
            if record.state.name == 'Self':
                record.hr_end_date = record.appraisal_deadline
            if record.state.name == 'LM':
                record.hr_end_date = record.hr_lm_end_date
            if record.state.name == 'ULM':
                record.hr_end_date = record.hr_ulm_end_date

    @api.model
    def reminder_mail(self):
        apr_deadline_check = self.env['hr.appraisal'].search([])
        dt_now = datetime.now(pytz.timezone('Asia/Calcutta'))
        current_dt = dt_now.strftime("%d")

        # pending_lm_manager_records      =  self.env['hr.employee']
        # pending_lm_appraisal_records    =  self.env['hr.appraisal']

        for records in apr_deadline_check:
            diff_self_end_date = records.appraisal_deadline - records.date_time
            diff_lm_end_date = records.hr_lm_end_date - records.date_time
            diff_ulm_end_date = records.hr_ulm_end_date - records.date_time

            hrself_end_date = records.appraisal_deadline.strftime("%d")
            diff_self_end_date = int(hrself_end_date) - int(current_dt)
            if not records.date_time > records.appraisal_deadline:
                if records.state.sequence == 2 and diff_self_end_date <= 2 and records.survey_state_id != 'Completed':
                    # print("Employee is",records.emp_id.name)
                    template = self.env.ref('kw_appraisal.kw_reminder_self_email_template')
                    self.env['mail.template'].browse(template.id).send_mail(records.id)

            hrlm_end_date = records.hr_lm_end_date.strftime("%d")
            diff_lm_end_date = int(hrlm_end_date) - int(current_dt)

            if not records.date_time > records.hr_lm_end_date:
                if records.state.sequence == 3 and diff_lm_end_date <= 2 and records.hr_survey_status != 'Completed' and records.reassessment == False:
                    template = self.env.ref('kw_appraisal.kw_lm_reminder_email_template')
                    self.env['mail.template'].browse(template.id).send_mail(records.id)

                    ##check if lm manager present in existing record, if not add to the list and appraisal record to the appraisal list
                    # if records.hr_manager_id not in pending_lm_manager_records:
                    #     pending_lm_manager_records |= records.hr_manager_id
                    #     pending_lm_appraisal_records |=records

                # lm_ids = set()
                # lm_incomplete = apr_deadline_check.filtered(lambda rec: rec.state.sequence == 3 \
                #                                                             and diff_lm_end_date <= 2 and rec.hr_survey_status != 'Completed' and rec.reassessment == False)
                # [lm_ids.add(r) for r in lm_incomplete.mapped(lambda r: r.hr_manager_id.id)]

                # if lm_ids:
                #     for id in lm_ids:
                #         rec = lm_incomplete.filtered(lambda record: record.hr_manager_id.id == id)
                #         template = self.env.ref('kw_appraisal.kw_lm_reminder_email_template')
                #         self.env['mail.template'].browse(template.id).send_mail(rec[0].id)

            hrulm_end_date = records.hr_ulm_end_date.strftime("%d")
            diff_ulm_end_date = int(hrulm_end_date) - int(current_dt)
            if not records.date_time > records.hr_ulm_end_date:
                if records.state.sequence == 4 and diff_ulm_end_date <= 2 and records.hr_survey_status != 'Completed':
                    template = self.env.ref('kw_appraisal.kw_ulm_reminder_email_template')
                    self.env['mail.template'].browse(template.id).send_mail(records.id)

                # print("ULM end date condition checked======")
                # hrulm_end_date = records.hr_ulm_end_date.strftime("%d")
                # diff_ulm_end_date = int(hrulm_end_date) - int(current_dt)
                # ulm_ids = set()
                # ulm_incomplete = apr_deadline_check.filtered(lambda recs: recs.state.id == 4 \
                #                                                           and diff_ulm_end_date <= 2 and recs.hr_survey_status != 'Completed')
                # [ulm_ids.add(r) for r in ulm_incomplete.mapped(lambda r: r.hr_collaborator_id.id)]
                # print("ULM Ids are--------", ulm_ids)

                # if ulm_ids:
                #     for ulm_id in ulm_ids:
                #         ulm_rec = ulm_incomplete.filtered(lambda rec: rec.hr_collaborator_id.id == ulm_id)
                #         print(rec, "========state 4=========")
                #         template = self.env.ref('kw_appraisal.kw_ulm_reminder_email_template')
                #         self.env['mail.template'].browse(template.id).send_mail(ulm_rec[0].id)


        ##send mail for pending Line Manager
        # if pending_lm_appraisal_records:
        #     for lm_appraisal_record in pending_lm_appraisal_records:
               
        #         lm_template            = self.env.ref('kw_appraisal.kw_lm_reminder_email_template')
        #         lm_template.send_mail(lm_appraisal_record.id)
               
        
        for record_data in self.env['hr.appraisal'].search(['&', ('state', 'in', [3]), ('reassessment', '=', True)]):
            template = self.env.ref('kw_appraisal.kw_reassessment_reminder_mail_template')
            self.env['mail.template'].browse(template.id).send_mail(record_data.id)

    @api.multi
    def action_send_mail(self):
        print("Mail Send Successfully...........")

    @api.one
    def _ulm_end_date(self):
        if self.hr_ulm_end_date < self.date_time:
            self.compare_ulm_end_date = True

    @api.one
    def _lm_end_date(self):
        if self.hr_lm_end_date < self.date_time:
            self.compare_lm_end_date = True

    @api.one
    def _self_end_date(self):
        if self.appraisal_deadline < self.date_time:
            self.compare_self_end_date = True

    @api.one
    def _state_compare(self):
        if self.hr_survey_status == "Not Started" or self.hr_survey_status == "Completed":
            self.state_compare = True
        else:
            pass

    @api.one
    def _self_compare_date(self):
        if self.app_period_from > self.date_time:
            self.compare_self_date = True

    @api.one
    def _lm_compare_date(self):
        if self.hr_lm_start_date > self.date_time:
            self.compare_lm_date = True

    @api.one
    def _ulm_compare_date(self):
        if self.hr_ulm_start_date > self.date_time:
            self.compare_ulm_date = True

    @api.multi
    @api.depends('state')
    def count_score(self):
        for record in self:
            if record.state.id == 5 or record.state.id == 6:
                total_score = 0
                questions = 0.0
                for records in record.hr_manager_id:
                    user_input_line = self.env['survey.user_input_line'].sudo().search(
                        [('user_input_id', '=', record.lm_input_id.id)])
                    if len(user_input_line):
                        for lines in user_input_line:
                            total_score += lines.quizz_mark
                            for quest_ids in lines.question_id:
                                for labels in quest_ids:
                                    questions += max(float(marks.quizz_mark) for marks in labels.labels_ids)
                if questions != 0:
                    score = (total_score / questions) * 100
                    record.score = '%.3f' % (score)
            else:
                pass

    @api.multi
    def _hr_survey_state(self):
        for record in self:
            survey_emp = record.self_input_id.sudo()
            survey_lm = record.lm_input_id.sudo()
            survey_ulm = record.ulm_input_id.sudo()

            if record.state.id == 2:
                record.hr_survey_status = survey_emp.state
            if record.state.id == 3:
                record.hr_survey_status = survey_lm.state
            if record.state.id == 4:
                record.hr_survey_status = survey_ulm.state

            if record.hr_survey_status == False:
                record.hr_survey_status = "Not Started"
            elif record.hr_survey_status == 'new':
                record.hr_survey_status = "In Progress"
            elif record.hr_survey_status == 'skip':
                record.hr_survey_status = "Draft"
            elif record.hr_survey_status == 'done':
                record.hr_survey_status = "Completed"
            if record.state.id == 5:
                record.hr_survey_status = "Completed"
            if record.state.id == 6:
                record.hr_survey_status = "Published"

    @api.multi
    @api.constrains('app_period_from', 'appraisal_deadline')
    def date_constrains(self):
        for rec in self:
            if rec.appraisal_deadline < rec.app_period_from:
                raise ValidationError('Self Appraisal End Date Must be greater Than Start Date...')

    @api.multi
    @api.constrains('hr_lm_start_date', 'hr_lm_end_date')
    def lmdate_constrains(self):
        for rec in self:
            if rec.hr_lm_end_date < rec.hr_lm_start_date:
                raise ValidationError('LM Appraisal End Date Must be greater Than Start Date...')

    @api.multi
    @api.constrains('hr_ulm_start_date', 'hr_ulm_end_date')
    def ulmdate_constrains(self):
        for rec in self:
            if rec.hr_ulm_end_date < rec.hr_ulm_start_date:
                raise ValidationError('ULM Appraisal End Date Must be greater Than Start Date...')

    @api.one
    def _find_current_date(self):
        dt_now = datetime.now(pytz.timezone('Asia/Calcutta'))
        current_dt = dt_now.strftime("%Y-%m-%d")
        self.date_time = current_dt

    @api.one
    def _reviewer(self):
        if self.state.id == 3:
            self.ra_id = self.hr_manager_id.name
        elif self.state.id == 4:
            self.ra_id = self.hr_collaborator_id.name

    @api.model
    def check_groups(self, vals):
        """here groups_ext_ids is a list of groups(external_id may be)"""
        if not vals.get('user_id'):
            return False
        else:
            user = self.env['res.users'].browse(int(vals['user_id']))
            if user.has_group('kw_appraisal.group_appraisal_manager'):
                return True
            else:
                return False

    @api.one
    def _survey_response_id(self):
        survey_res = self.env['survey.user_input'].sudo().search([('appraisal_id', '=', self.id)])
        if self.survey_state_id == False:
            self.survey_state_id = "Not Started"
        for record in survey_res:
            # print("Record survey is", record)
            self.survey_state_id = record.state
            if self.survey_state_id == 'new':
                self.survey_state_id = "In Progress"
            elif self.survey_state_id == 'skip':
                self.survey_state_id = "Draft"
            elif self.survey_state_id == 'done':
                self.survey_state_id = "Completed"

    @api.multi
    def action_edit_appraisal(self):
        self.state = self.state.sequence - 1
        if self.state.id == 2:
            if self.self_input_id:
                survey_user_input_partner_id = self.self_input_id.sudo()
            else:
                survey_user_input_partner_id = self.env['survey.user_input'].sudo().search(
                    ['&', ('partner_id', '=', self.emp_id.user_id.partner_id.id), ('appraisal_id', '=', self.id)],
                    limit=1)
            if survey_user_input_partner_id:
                survey_user_input_partner_id.state = 'skip'
                survey_user_input_partner_id.last_displayed_page_id = 0
        elif self.state.id == 3:
            if self.lm_input_id:
                survey_user_input_partner_id1 = self.lm_input_id.sudo()
            else:
                survey_user_input_partner_id1 = self.env['survey.user_input'].sudo().search(
                    ['&', ('partner_id', '=', self.hr_manager_id.user_id.partner_id.id),
                     ('appraisal_id', '=', self.id)], limit=1)
            if survey_user_input_partner_id1:
                survey_user_input_partner_id1.state = 'skip'
                survey_user_input_partner_id1.last_displayed_page_id = 0
        elif self.state.id == 4:
            if self.ulm_input_id:
                survey_user_input_partner_id2 = self.ulm_input_id.sudo()
            else:
                survey_user_input_partner_id2 = self.env['survey.user_input'].sudo().search(
                    ['&', ('partner_id', '=', self.hr_collaborator_id.user_id.partner_id.id),
                     ('appraisal_id', '=', self.id)], limit=1)
            if survey_user_input_partner_id2:
                survey_user_input_partner_id2.state = 'skip'
                survey_user_input_partner_id2.last_displayed_page_id = 0
        return True

    @api.multi
    def action_start_appraisal(self):
        if self.emp_id.user_id and self.emp_id.user_id.id == self.env.user.id and self.hr_emp and self.emp_survey_id:
            # print("Self is :", self.emp_id.name)
            if self.self_input_id and self.self_input_id.sudo().partner_id.id == self.emp_id.user_id.partner_id.id:
                self.self_input_id.sudo().write({'deadline': self.appraisal_deadline})
                token = self.self_input_id.sudo().token
            else:
                token = self.get_appraisal_details(self.emp_id, 'SELF')
            return self.emp_survey_id.with_context(survey_token=token,
                                                   employee_id=self.emp_id.id).action_test_kw_survey()
        for lms in self.hr_manager_id:
            if lms.user_id and lms.user_id.id == self.env.user.id and self.hr_manager and self.manager_survey_id:
                # print("Lm is :", lms.name)
                if self.lm_input_id and self.lm_input_id.sudo().partner_id.id == lms.user_id.partner_id.id:
                    self.lm_input_id.sudo().write({'deadline': self.hr_lm_end_date})
                    token = self.lm_input_id.sudo().token
                else:
                    token = self.get_appraisal_details(lms, 'LM')
                return self.manager_survey_id.with_context(survey_token=token,
                                                           employee_id=self.emp_id.id).action_test_kw_survey()
        for ulms in self.hr_collaborator_id:
            if ulms.user_id and ulms.user_id.id == self.env.user.id and self.hr_collaborator and self.collaborator_survey_id:
                # print("Ulm is :", ulms.name)
                if self.ulm_input_id and self.ulm_input_id.sudo().partner_id.id == ulms.user_id.partner_id.id:
                    self.ulm_input_id.sudo().write({'deadline': self.hr_ulm_end_date})
                    token = self.ulm_input_id.sudo().token
                else:
                    token = self.get_appraisal_details(ulms, 'ULM')
                return self.collaborator_survey_id.with_context(survey_token=token,
                                                                employee_id=self.emp_id.id).action_test_kw_survey()

    @api.multi
    def get_appraisal_details(self, values, checker):
        user_input = self.env['survey.user_input'].sudo()
        survey_rel_id = user_input.search(
            ['&', ('appraisal_id', '=', self.id), ('partner_id', '=', values.user_id.partner_id.id)], limit=1)
        if checker == 'SELF':
            if len(survey_rel_id):
                # print("Self")
                self.self_input_id = survey_rel_id.id
                survey_rel_id.sudo().write({'deadline': self.appraisal_deadline})
                token = survey_rel_id.token
            else:
                # print("Else Self")
                response = user_input.sudo().create(
                    {'survey_id': self.emp_survey_id.id,
                     'partner_id': values.user_id.partner_id.id if values.user_id.partner_id else False,
                     'appraisal_id': self.ids[0], 'deadline': self.appraisal_deadline,
                     'email': values.work_email if values.work_email else False,
                     'emp_code': values.emp_code if values.emp_code else False})
                self.self_input_id = response.id
                return response.token
        elif checker == 'LM':
            if len(survey_rel_id):
                # print("LM")
                self.lm_input_id = survey_rel_id.id
                survey_rel_id.sudo().write({'deadline': self.hr_lm_end_date})
                token = survey_rel_id.token
            else:
                # print("Else LM")
                response = user_input.sudo().create(
                    {'survey_id': self.manager_survey_id.id,
                     'partner_id': values.user_id.partner_id.id if values.user_id.partner_id else False,
                     'appraisal_id': self.ids[0], 'deadline': self.hr_lm_end_date,
                     'email': values.work_email if values.work_email else False,
                     'emp_code': values.emp_code if values.emp_code else False})
                self.lm_input_id = response.id
                return response.token
        elif checker == "ULM":
            if len(survey_rel_id):
                # print("ULM")
                self.ulm_input_id = survey_rel_id.id
                survey_rel_id.sudo().write({'deadline': self.hr_ulm_end_date})
                token = survey_rel_id.token
            else:
                # print("Else ULM")
                response = user_input.sudo().create(
                    {'survey_id': self.collaborator_survey_id.id,
                     'partner_id': values.user_id.partner_id.id if values.user_id.partner_id else False,
                     'appraisal_id': self.ids[0], 'deadline': self.hr_ulm_end_date,
                     'email': values.work_email if values.work_email else False,
                     'emp_code': values.emp_code if values.emp_code else False})
                self.ulm_input_id = response.id
                return response.token

    def UpdateExistingRecordCron(self):
        appr = self.env['hr.appraisal'].search([])
        user_input = self.env['survey.user_input'].sudo()
        user_input_line = self.env['survey.user_input_line'].sudo()
        for appr_records in appr:
            # print(appr_records," Appraisal record")
            vals = {}
            user_inputs = user_input.search([('appraisal_id', '=', appr_records.id)])
            for inputs in user_inputs:
                # print(inputs," UserInputs")
                input_lines_employee = user_input_line.search([('user_input_id', '=', inputs.id), (
                'user_input_id.appraisal_id.emp_id.user_id.partner_id.id', '=', inputs.partner_id.id)])
                # print(input_lines_employee," employee")
                input_lines_manager = user_input_line.search(['&', '&', ('user_input_id', '=', inputs.id), (
                'user_input_id.appraisal_id.emp_id.user_id.partner_id.id', '!=', inputs.partner_id.id),
                                                              ('value_suggested', '!=', False)])
                # print(input_lines_manager," LM")
                input_lines_collaborator = user_input_line.search(['&', '&', '&', ('user_input_id', '=', inputs.id), (
                'user_input_id.appraisal_id.emp_id.user_id.partner_id.id', '!=', inputs.partner_id.id), (
                                                                   'user_input_id.appraisal_id.hr_manager_id.user_id.partner_id.id',
                                                                   '!=', inputs.partner_id.id),
                                                                   ('value_suggested', '=', False)])
                # print(input_lines_collaborator," ULM")

                if len(input_lines_employee):
                    # print('emp_came')
                    vals.update({'self_input_id': inputs.id})
                if len(input_lines_manager):
                    # print('lm_came')
                    vals.update({'lm_input_id': inputs.id})
                if len(input_lines_collaborator):
                    # print('ulm_came')
                    vals.update({'ulm_input_id': inputs.id})
                # print(vals)
                appr_records.write(vals)

    @api.multi
    def view_results(self):
        user_input = self.env['survey.user_input'].sudo().search(
            ['&', '&', ('partner_id', '=', self.emp_id.user_id.partner_id.id), ('appraisal_id', '=', self.id),
             ('survey_id', '=', self.emp_survey_id.id)])
        token = user_input.token
        # print(token)
        return self.emp_survey_id.with_context(survey_token=token, employee_id=self.emp_id.id,
                                               appraisal_year_rel=self.appraisal_year_rel.id).action_kw_survey_result()

    @api.multi
    def view_score(self):
        user_input = self.env['survey.user_input'].sudo().search(
            ['&', '&', ('partner_id', '=', self.emp_id.user_id.partner_id.id), ('appraisal_id', '=', self.id),
             ('survey_id', '=', self.emp_survey_id.id)])
        token = user_input.token
        # print(token)
        return self.emp_survey_id.with_context(survey_token=token, employee_id=self.emp_id.id,
                                               appraisal_year_rel=self.appraisal_year_rel.id).action_kw_survey_score()

    @api.multi
    def action_get_answers(self):
        """ This function will return all the answers posted related to this appraisal."""

        tree_res = self.env['ir.model.data'].get_object_reference('survey', 'survey_user_input_tree')
        tree_id = tree_res and tree_res[1] or False
        form_res = self.env['ir.model.data'].get_object_reference('kw_appraisal', 'kw_survey_user_input_form_inherits')
        form_id = form_res and form_res[1] or False
        return {
            'model': 'ir.actions.act_window',
            'name': 'Answers',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'survey.user_input',
            'views': [(tree_id, 'tree'), (form_id, 'form')],
            'domain': [('state', '=', 'done'), ('appraisal_id', '=', self.ids[0])],
        }

    @api.multi
    def _compute_completed_survey(self):
        for record in self:
            # print("Compute field called.............")
            answers = self.env['survey.user_input'].sudo().search(
                [('state', '=', 'done'), ('appraisal_id', '=', record.ids[0])])
            record.tot_comp_survey = len(answers)

    @api.model
    def fields_get(self, fields=None):
        fields_to_hide = ['kw_ids', 'e_code', 'current_dt', 'app_period_from', 'appraisal_deadline', 'final_interview',
                          'company_id', 'hr_emp', 'hr_manager', 'hr_collaborator', 'hr_colleague', 'manager_survey_id',
                          'emp_survey_id', 'collaborator_survey_id', 'colleague_survey_id', 'response_id',
                          'final_evaluation', 'tot_comp_survey', 'tot_sent_survey', 'created_by', 'color',
                          'kw_appraisal_id', 'hr_lm_start_date', 'hr_lm_end_date', 'hr_ulm_start_date',
                          'hr_ulm_end_date', 'ra_id', 'created_by', 'message_needaction', 'create_date',
                          'message_follower_ids', 'message_channel_ids', 'id', 'message_is_follower',
                          'message_main_attachment_id', 'message_has_error', 'message_ids', 'create_uid',
                          'message_partner_ids', 'write_uid', 'website_message_ids', 'id', 'activity_user_id',
                          'activity_type_id', 'activity_summary', 'activity_date_deadline', 'activity_ids',
                          'appraisal_year_rel']
        res = super(HrAppraisalForm, self).fields_get()
        for field in fields_to_hide:
            res[field]['selectable'] = False
        return res

    def UpdateRAManagerRecordCron(self):
        appr = self.env['hr.appraisal'].search([])
        complete_stage = self.env['hr.appraisal.stages'].search([('sequence','=',5)])
        try:
            for appraisal_record in appr:
                if appraisal_record.state.sequence == 4 and appraisal_record.emp_id.parent_id and appraisal_record.emp_id.parent_id.parent_id and not appraisal_record.emp_id.parent_id.parent_id.parent_id:
                    # print(appraisal_record.emp_id.name,"ubiu")
                    appraisal_record.write({'state':complete_stage.id})
        except Exception as e:
            pass
                

class AppraisalStages(models.Model):
    _name = 'hr.appraisal.stages'
    _description = 'Appraisal Stages'

    name = fields.Char(string="Name")
    sequence = fields.Integer(string="Sequence")
    fold = fields.Boolean(string='Folded in Appraisal Pipeline',
                          help='This stage is folded in the kanban view when '
                               'there are no records in that stage to display.')
