from odoo import models, fields, api
from odoo.exceptions import  ValidationError


class kw_appraisal_KRA(models.Model):
    _name           = 'kw_appraisal_kra'
    _description    = 'Appraisal KRA'
    _rec_name       = 'employee_id'
    appraisal_period    = fields.Many2one('kw_assessment_period_master',string='Appraisal Period',)
    employee_id         = fields.Many2one('hr.employee',string='Employee')
    total_score         = fields.Integer('Total Score',default=100)
    actual_score        = fields.Float('Actual Score')
    achievement         = fields.Char('Achievement')
    