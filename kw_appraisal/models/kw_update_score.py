from odoo import api, models,fields
from odoo.exceptions import UserError
from odoo import exceptions,_

class update_score_wizard(models.TransientModel):
    _name='update_score_wizard'
    _description = 'Update score wizard'

    def _get_default_appraisal(self):
        datas = self.env['hr.appraisal'].browse(self.env.context.get('active_ids'))
        return datas

    appr = fields.Many2many('hr.appraisal',readonly=1, default=_get_default_appraisal)

    @api.multi
    def update_score(self):
        for record in self.appr:
            total_score = 0
            questions = 0.0
            for records in record.hr_manager_id:
                if record.lm_input_id:
                    user_input_line = self.env['survey.user_input_line'].search([('user_input_id','=',record.lm_input_id.id),('user_input_id.state','=','done')])
                else:
                    user_input = self.env['survey.user_input'].search(['&','&',('appraisal_id', '=', record.id),('partner_id','=',records.user_id.partner_id.id),('state', '=', 'done')])
                    user_input_line = self.env['survey.user_input_line'].search([('user_input_id','=',user_input.id)])
                if len(user_input_line):
                    for lines in user_input_line:
                        total_score += lines.quizz_mark
                        for quest_ids in lines.question_id:
                            for labels in quest_ids:
                                questions += max(float(marks.quizz_mark) for marks in labels.labels_ids) 
            if questions !=0:
                score = (total_score/questions)*100
                record.score = '%.3f'%(score)
        self.env.user.notify_info(message='Updated Successfully.')