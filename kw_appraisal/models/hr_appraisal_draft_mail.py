# -*- coding: utf-8 -*-
from odoo import models, fields, api

##send mail after creation , BY  T ketaki Debadarshini
class kw_hr_appraisal(models.Model):

    _inherit = 'hr.appraisal'
  

    
    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """
    
        result = super(kw_hr_appraisal, self).create(values)

        if result:
            try:
                mail_template    = self.env.ref('kw_appraisal.kw_employee_appraisal_draft_email_template')
                self.env['mail.template'].browse(mail_template.id).send_mail(result.id)

            except Exception as e:
                pass
    
        return result




##send mail appraisal period  , BY  T ketaki Debadarshini
class kw_appraisal_period(models.Model):

    _inherit = 'kw_appraisal'
  
    
    ##send mail to the employees of the appraisal period
    @api.multi
    def action_send_mail(self):

        for record in self:
            try:
                appraisal_recs = self.env['hr.appraisal'].search([('kw_ids', '=', record.id)])

                if appraisal_recs:
                    mail_template    = self.env.ref('kw_appraisal.kw_employee_appraisal_draft_email_template')
                    for appraisal_rec in appraisal_recs:
                        self.env['mail.template'].browse(mail_template.id).send_mail(appraisal_rec.id)
            except Exception as e:
                pass

        return True






    