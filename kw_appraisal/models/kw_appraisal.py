# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class kw_appraisal(models.Model):
    _name = 'kw_appraisal'
    _description = 'Appraisal'
    _rec_name = 'year'

    emp_strt_date = fields.Date(string="Employee Start Date", required=True, autocomplete="off")
    emp_end_date = fields.Date(string="Employee End Date", required=True, autocomplete="off")
    lm_start_date = fields.Date(string='LM Start Date', required=True, autocomplete="off")
    lm_end_date = fields.Date(string='LM End Date', required=True, autocomplete="off")
    ulm_start_date = fields.Date(string='ULM Start Date', required=True, autocomplete="off")
    ulm_end_date = fields.Date(string='ULM End Date', required=True, autocomplete="off")
    employee = fields.Many2many(
        'kw_appraisal_employee', 'rel_appraisal', string='Employee', required=True)
    year = fields.Many2one('kw_assessment_period_master',
                           string='Appraisal Year', required=True)
    hr_appraisal_ids = fields.One2many(
        'hr.appraisal', 'kw_appraisal_id', 'Hr Appraisal IDS', ondelete='cascade')

    hr_state_id = fields.Char(string="Status", compute='_hr_state_id')

    @api.model
    def _hr_state_id(self):
        hr_state = self.env['hr.appraisal'].search([])
        for records in self:
            if len(hr_state) < 1:
                records.hr_state_id = '0'
            else:
                for record in hr_state:
                    for s_record in record.state:
                        records.hr_state_id = s_record.id

    # @api.multi
    # def unlink(self):
    #     obj = self.env['kw_appraisal_employee'].search([('employee', 'in', self.ids)])
    #     if obj:
    #         raise ValueError("You are trying to delete a record that is still referenced!")
    #     return super(kw_appraisal, self).unlink()​

    @api.multi
    @api.constrains('year')
    def check_year(self):
        existing_record = self.env['kw_appraisal'].sudo().search([]) - self
        for record in existing_record:
            exist_year = record.year.ids
            if str(self.year.id) in str(exist_year):
                raise ValidationError("Appraisal period already exists.")

    @api.multi
    @api.constrains('emp_strt_date', 'emp_end_date')
    def date_constrains(self):
        for rec in self:
            if rec.emp_end_date < rec.emp_strt_date:
                raise ValidationError('Sorry, Self Appraisal End Date Must be greater Than Start Date...')

    @api.multi
    @api.constrains('lm_start_date', 'lm_end_date')
    def lmdate_constrains(self):
        for rec in self:
            if rec.lm_end_date < rec.lm_start_date:
                raise ValidationError('LM Appraisal End Date Must be greater Than Start Date...')

    @api.multi
    @api.constrains('ulm_start_date', 'ulm_end_date')
    def ulmdate_constrains(self):
        for rec in self:
            if rec.ulm_end_date < rec.ulm_start_date:
                raise ValidationError('ULM Appraisal End Date Must be greater Than Start Date...')

    @api.model
    def create(self, values):
        record = super(kw_appraisal, self).create(values)
        if record:
            self.env.user.notify_success(message='Appraisal Period Configure Created Successfully.')
        # record.create_appraisal_employees()
        return record

    @api.multi
    def write(self, values):
        self.ensure_one()
        super(kw_appraisal, self).write(values)
        self.env.user.notify_success(message='Appraisal Period Configure Updated Successfully.')
        # if values.get('hr_appraisal_ids', False):
        #     appraisal_ids = self.create_appraisal_employees()
        return True

    @api.multi
    def action_start_appraisal_all(self):
        self.ensure_one()
        start_date = self.emp_strt_date
        emp_end_date = self.emp_end_date
        lm_start_date = self.lm_start_date
        lm_end_date = self.lm_end_date
        ulm_start_date = self.ulm_start_date
        ulm_end_date = self.ulm_end_date
        appraisal_update = self.env['hr.appraisal'].search(
            [('kw_ids', '=', self.id)])
        if len(appraisal_update) > 0:
            hr_appraisals = [emp.emp_id.id for emp in appraisal_update]
            for record in self.employee:
                survey_form = record.kw_survey_id.id
                for employee in record.employee_id:
                    vals = {
                        'appraisal_year_rel': self.year.id,
                        'kw_ids': self.id,
                        'appraisal_year': self.year.assessment_period,
                        'emp_id': employee.id,
                        'appraisal_deadline': emp_end_date,
                        'app_period_from': start_date,
                        'hr_lm_start_date': lm_start_date,
                        'hr_lm_end_date': lm_end_date,
                        'hr_ulm_start_date': ulm_start_date,
                        'hr_ulm_end_date': ulm_end_date,
                        'hr_emp': True,
                        'emp_survey_id': survey_form,
                    }

                    if employee.parent_id:
                        vals['hr_manager_id'] = [(4, employee.parent_id.id)]
                        vals['hr_manager'] = True
                        vals['manager_survey_id'] = survey_form
                    if employee.parent_id.parent_id.id:
                        vals['hr_collaborator'] = True
                        vals['hr_collaborator_id'] = [
                            (4, employee.parent_id.parent_id.id)]
                        vals['collaborator_survey_id'] = survey_form
                    if employee.id not in hr_appraisals:
                        apr_create = self.env['hr.appraisal'].create(vals)
                        # if apr_create:
                        #     self.env.user.notify_success(message='Appraisal Updated successfully.')

                    else:
                        apr_date = self.env['hr.appraisal'].search(
                            [('emp_id', '=', employee.id), ('kw_ids', '=', self.id)])
                        if start_date != apr_date.app_period_from:
                            apr_date.write({
                                'app_period_from': start_date,
                            })
                        if emp_end_date != apr_date.appraisal_deadline:
                            apr_date.write({
                                'appraisal_deadline': emp_end_date,
                            })
                        if lm_start_date != apr_date.hr_lm_start_date:
                            apr_date.write({
                                'hr_lm_start_date': lm_start_date,
                            })
                        if lm_end_date != apr_date.hr_lm_end_date:
                            apr_date.write({
                                'hr_lm_end_date': lm_end_date,
                            })
                        if ulm_start_date != apr_date.hr_ulm_start_date:
                            apr_date.write({
                                'hr_ulm_start_date': ulm_start_date,
                            })
                        if ulm_end_date != apr_date.hr_ulm_end_date:
                            apr_date.write({
                                'hr_ulm_end_date': ulm_end_date,
                            })
                        if employee.parent_id.id != apr_date.hr_manager_id.id:
                            apr_date.write({
                                'hr_manager_id': (6, 0, [employee.parent_id.ids]),
                            })
                        if employee.parent_id.parent_id.id != apr_date.hr_collaborator_id.id:
                            apr_date.write({
                                'hr_collaborator_id': (6, 0, [employee.parent_id.parent_id.ids]),
                            })
                        hr_appraisals.remove(employee.id)
            for kw_id in hr_appraisals:
                self.env['hr.appraisal'].search([('emp_id', '=', kw_id)]).unlink()

        else:
            for record in self.employee:
                survey_form = record.kw_survey_id.id
                for employee in record.employee_id:
                    vals = {
                        'appraisal_year_rel': self.year.id,
                        'kw_ids': self.id,
                        'appraisal_year': self.year.assessment_period,
                        'emp_id': employee.id,
                        'appraisal_deadline': emp_end_date,
                        'app_period_from': start_date,
                        'hr_lm_start_date': lm_start_date,
                        'hr_lm_end_date': lm_end_date,
                        'hr_ulm_start_date': ulm_start_date,
                        'hr_ulm_end_date': ulm_end_date,
                        'hr_emp': True,
                        'emp_survey_id': survey_form,
                    }
                    if employee.parent_id:
                        vals['hr_manager_id'] = [(4, employee.parent_id.id)]
                        vals['hr_manager'] = True
                        vals['manager_survey_id'] = survey_form
                    if employee.parent_id.parent_id.id:
                        vals['hr_collaborator'] = True
                        vals['hr_collaborator_id'] = [(4, employee.parent_id.parent_id.id)]
                        vals['collaborator_survey_id'] = survey_form
                    hr_appr = self.env['hr.appraisal'].create(vals)
            if hr_appr:
                self.env.user.notify_success(message='Appraisal Updated successfully.')

        # State changing to self------------

        state = self.env['hr.appraisal'].search([('state', '=', 1)])
        state2 = self.env['hr.appraisal.stages'].search([('sequence', '=', 2)])
        for record_state in state:
            if record_state:
                record_state.state = state2
        return True

    @api.multi
    def action_update_appraisal(self):
        self.ensure_one()
        start_date = self.emp_strt_date
        emp_end_date = self.emp_end_date
        lm_start_date = self.lm_start_date
        lm_end_date = self.lm_end_date
        ulm_start_date = self.ulm_start_date
        ulm_end_date = self.ulm_end_date
        appraisal_update = self.env['hr.appraisal'].search([('kw_ids', '=', self.id)])
        if len(appraisal_update) > 0:
            hr_appraisals = [emp.emp_id.id for emp in appraisal_update]
            for record in self.employee:
                survey_form = record.kw_survey_id.id
                for employee in record.employee_id:
                    vals = {
                        'appraisal_year_rel': self.year.id,
                        'kw_ids': self.id,
                        'appraisal_year': self.year.assessment_period,
                        'emp_id': employee.id,
                        'appraisal_deadline': emp_end_date,
                        'app_period_from': start_date,
                        'hr_lm_start_date': lm_start_date,
                        'hr_lm_end_date': lm_end_date,
                        'hr_ulm_start_date': ulm_start_date,
                        'hr_ulm_end_date': ulm_end_date,
                        'hr_emp': True,
                        'emp_survey_id': survey_form,
                    }

                    if employee.parent_id:
                        vals['hr_manager_id'] = [(4, employee.parent_id.id, False)]
                        vals['hr_manager'] = True
                        vals['manager_survey_id'] = survey_form
                    if employee.parent_id.parent_id.id:
                        vals['hr_collaborator'] = True
                        vals['hr_collaborator_id'] = [
                            (4, employee.parent_id.parent_id.id, False)]
                        vals['collaborator_survey_id'] = survey_form
                    if employee.id not in hr_appraisals:
                        apr_create = self.env['hr.appraisal'].create(vals)
                        # if apr_create:
                        #     self.env.user.notify_success(message='Appraisal Updated successfully.')

                    else:
                        apr_date = self.env['hr.appraisal'].search(
                            ['&', ('emp_id', '=', employee.id), ('kw_ids', '=', self.id)])
                        if start_date != apr_date.app_period_from:
                            apr_date.write({
                                'app_period_from': start_date,
                            })
                        if emp_end_date != apr_date.appraisal_deadline:
                            apr_date.write({
                                'appraisal_deadline': emp_end_date,
                            })
                        if lm_start_date != apr_date.hr_lm_start_date:
                            apr_date.write({
                                'hr_lm_start_date': lm_start_date,
                            })
                        if lm_end_date != apr_date.hr_lm_end_date:
                            apr_date.write({
                                'hr_lm_end_date': lm_end_date,
                            })
                        if ulm_start_date != apr_date.hr_ulm_start_date:
                            apr_date.write({
                                'hr_ulm_start_date': ulm_start_date,
                            })
                        if ulm_end_date != apr_date.hr_ulm_end_date:
                            apr_date.write({
                                'hr_ulm_end_date': ulm_end_date,
                            })
                        if survey_form != apr_date.emp_survey_id.id:
                            apr_date.write({
                                'emp_survey_id': survey_form,
                                'manager_survey_id': survey_form,
                                'collaborator_survey_id': survey_form,
                            })
                        if employee.parent_id.id != apr_date.hr_manager_id.id or employee.parent_id.parent_id.id != apr_date.hr_collaborator_id.id:
                            if employee.parent_id.id:
                                apr_date.write({
                                    'hr_manager': True,
                                    'hr_manager_id': [(6, None, employee.parent_id.ids)],
                                })
                            elif not employee.parent_id.id:
                                apr_date.write({
                                    'hr_manager': False,
                                })
                            if employee.parent_id.parent_id.id:
                                apr_date.write({
                                    'hr_manager_id': [(6, None, employee.parent_id.ids)],
                                    'hr_collaborator': True,
                                    'hr_collaborator_id': [(6, None, employee.parent_id.parent_id.ids)],
                                })
                            elif not employee.parent_id.parent_id.id:
                                apr_date.write({
                                    'hr_manager_id': [(6, None, employee.parent_id.ids)],
                                    'hr_collaborator': False,
                                })
                        hr_appraisals.remove(employee.id)
            for kw_id in hr_appraisals:
                self.env['hr.appraisal'].search([('emp_id', '=', kw_id)]).unlink()
            self.env.user.notify_success(message='Appraisal Updated successfully.')
        else:
            for record in self.employee:
                survey_form = record.kw_survey_id.id
                for employee in record.employee_id:
                    vals = {
                        'appraisal_year_rel': self.year.id,
                        'kw_ids': self.id,
                        'appraisal_year': self.year.assessment_period,
                        'emp_id': employee.id,
                        'appraisal_deadline': emp_end_date,
                        'app_period_from': start_date,
                        'hr_lm_start_date': lm_start_date,
                        'hr_lm_end_date': lm_end_date,
                        'hr_ulm_start_date': ulm_start_date,
                        'hr_ulm_end_date': ulm_end_date,
                        'hr_emp': True,
                        'emp_survey_id': survey_form,
                    }

                    if employee.parent_id:
                        vals['hr_manager_id'] = [(4, employee.parent_id.id)]
                        vals['hr_manager'] = True
                        vals['manager_survey_id'] = survey_form
                    if employee.parent_id.parent_id.id:
                        vals['hr_collaborator'] = True
                        vals['hr_collaborator_id'] = [(4, employee.parent_id.parent_id.id)]
                        vals['collaborator_survey_id'] = survey_form
                    hr_appr = self.env['hr.appraisal'].create(vals)
            if hr_appr:
                self.env.user.notify_success(message='Appraisal Updated successfully.')

        # State changing to self------------

        emp_rec = self.env['hr.appraisal'].sudo().search([])
        for emp in emp_rec:
            emp_id = emp.emp_id.id
        resource_compare = self.env['resource.resource'].search([('user_id', '=', self.env.user.id)])
        # employees_find = self.env['hr.employee'].search([('resource_id','=',resource_compare.id)])
        employees = self.env['hr.employee'].search([('resource_id', '=', emp_id)])
        state = self.env['hr.appraisal'].search([('state', '=', 1)])
        state2 = self.env['hr.appraisal.stages'].search([('sequence', '=', 2)])
        for record_state in state:
            if record_state:
                record_state.state = state2
