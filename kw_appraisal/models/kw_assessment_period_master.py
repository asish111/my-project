# -*- coding: utf-8 -*-
from odoo import models, fields, api


class kw_appraisal(models.Model):
    _name = 'kw_assessment_period_master'
    _description = 'Employee Appraisal'
    _rec_name = 'assessment_period'

    assessment_period = fields.Char(string="Assessment Period", required=True)


    @api.constrains('assessment_period')
    def check_period(self):
        exists_name = self.env['kw_assessment_period_master'].search(
            [('assessment_period', '=', self.assessment_period), ('id', '!=', self.id)])
        if exists_name:
            raise ValueError("The Appraisal period" + '"'+self.assessment_period + '"' + " already exists.")