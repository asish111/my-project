from . import hr_appraisal_survey, hr_appraisal_form, kw_appraisal,kw_assessment_period_master, kw_appraisal_employee,kw_survey,kw_appraisal_goal,kw_appraisal_milestone,kw_archives,kw_employee_score,kw_appraisal_log
from . import appraisal_wizard, kw_update_score
from . import kw_appraisal_KRA
##appraisal dend draft mail, By T ketaki Debadarshini
from . import hr_appraisal_draft_mail

from . import kw_appraisal_report
from . import kw_appraisal_page_wise_report