from odoo import api, models,fields
from odoo.exceptions import UserError
from odoo import exceptions

class appraisal_wizard(models.TransientModel):
    _name='appraisal_wizard'
    _description = 'Appraisal wizard'

    def _get_default_appraisal(self):
        return self.env['hr.appraisal'].browse(self.env.context.get('active_ids'))

    appr = fields.Many2many('hr.appraisal',readonly=1, default=_get_default_appraisal)

    @api.multi
    def action_publish_appraisal(self):
        rec = self.env['hr.appraisal.stages'].search([('sequence', '=', 6)])
        
        for record in self:
            for states in record.appr:
                states.state = rec.id
                template = self.env.ref('kw_appraisal.kw_appraisal_result_email_template')
                self.env['mail.template'].browse(template.id).send_mail(states.id)
        self.env.user.notify_info(message='Appraisal Published Sucessfully.')
        
