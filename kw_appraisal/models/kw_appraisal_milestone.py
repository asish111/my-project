from odoo import models, fields, api
from odoo.exceptions import  ValidationError


class kw_appraisal_milestone(models.Model):
    _name = 'kw_appraisal_milestone'
    _description = 'Appraisal milestone'
    _rec_name = 'name'

    name = fields.Char('Milestone Name',required=True)
    goal_id = fields.Many2one('kw_appraisal_goal','milestone_id')
    status = fields.Boolean('Status',default=False)