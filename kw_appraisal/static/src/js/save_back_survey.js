odoo.define('kw_appraisal.not', function (require) {
    'use strict';
    var ajax = require('web.ajax');
    var the_form = $('.js_surveyform');
    var submit_controller = the_form.attr("data-back");
    var survey = {
        init: function () {
            $(document).on("click", '#save_and_back', function () {
                var prevs = ''
                survey.ajax_call(prevs);
            });
            $(document).on("click", '#prevs', function () {
                var prevs = 'prevs'
                survey.ajax_call(prevs);
            });
        },
        ajax_call: function (prevs) {
            // alert(prevs)
            var result = {};
            $.each($('#kw_survey_form').serializeArray(), function () {
                result[this.name] = this.value;
            });
            ajax.jsonRpc(submit_controller, 'call', {
                'kw_survey_form':result,
                'prevs':prevs,
            }).then(function (data) {
                // alert(data)
                if (data['redirect'] == 'self') {
                    window.location.href = '/web#action=kw_appraisal.self_actions_window&amp;model=hr_appraisal&amp;view_type=kanban&amp;menu_id=kw_appraisal.menu_hr_appraisal'
                }
                else if(data['redirect'] == 'lm'){
                    window.location.href =  '/web#action=kw_appraisal.lm_actions_window&amp;model=hr_appraisal&amp;view_type=kanban&amp;menu_id=kw_appraisal.menu_hr_appraisal'                        

                }
                else if(data['redirect'] == 'ulm'){
                    window.location.href = '/web#action=kw_appraisal.ulm_actions_window&amp;model=hr_appraisal&amp;view_type=kanban&amp;menu_id=kw_appraisal.menu_hr_appraisal'                        

                }
                else if(data['redirect'] == 'lm_reassign'){
                    window.location.href = '/web#action=kw_appraisal.lm_actions_window&amp;model=hr_appraisal&amp;view_type=kanban&amp;menu_id=kw_appraisal.menu_hr_appraisal'                                            

                }
                else if(data['redirect'] == 'prevs'){
                    window.location.href ="/kw/survey/start/"+data['survey_id']+'/'+data['token']
                }
            });
        },
    };

    $(function () {
        survey.init();
    });
});