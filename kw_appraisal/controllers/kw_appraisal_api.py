# -*- coding: utf-8 -*-
import json
from odoo import http
from odoo.http import request

class APIController(http.Controller):

    @http.route("/appraisal-result/", type="http", cors='*', auth="none", methods=["POST"], csrf=False)
    def send_appraisal(self):
        employee_id = request.env['hr.appraisal'].sudo().search([])
        if len(employee_id) == 0:
            return json.dumps({
                "status": "no-data",
                "message": f"Sorry, no data found for current year."
            })
        else:
            maxm = max([record.appraisal_year_rel.id for record in employee_id])
            print(maxm)
            data = [{'emp_kw_ids':record.emp_id.kw_id,
                    'kra_score':record.kra_score,
                    'appraisal_score':record.score,} for record in employee_id if record.appraisal_year_rel and record.appraisal_year_rel.id == maxm]
            return json.dumps(data)
                
