# -*- coding: utf-8 -*-
from odoo import models, fields, api


class kw_resignation_master(models.Model):
    _name = "kw_resignation_master" 
    _description = "Resignation master model"
    _rec_name = 'name'

    name = fields.Char("Name")