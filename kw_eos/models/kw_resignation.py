# -*- coding: utf-8 -*-
from odoo import models, fields, api


class kw_resignation(models.Model):
    _name = "kw_resignation"
    _description = "Resignation model"
    _rec_name = 'reg_type'

    reg_type = fields.Many2one('kw_resignation_master',string="Resignation Type")
    reason = fields.Text(string="Reason For Resignation")
    effective_form = fields.Date(string="Effective From")
    last_working_date = fields.Date(string='Last Working Date')
    