# -*- coding: utf-8 -*-
{
    'name': "EOS",
    'summary': """End of service""",
    'description': """ End of service
        
    """,

    'author': "CSM Technologies",
    'website': "www.csm.co.in",

    'category': 'End of service',
    'version': '1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','mail','kw_dynamic_workflow'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/kw_resignation.xml',
        'views/kw_resignation_type_master.xml',
        'views/kw_menu.xml',
        'data/kw_eos_dynamic_workflow.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
}