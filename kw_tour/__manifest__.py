# -*- coding: utf-8 -*-
{
    'name': "Kwantify Tour",
    'summary': """ Kwantify Tour """,
    'description': """ Kwantify Tour """,
    'author': "CSM Technologies",
    'website': "http://www.csm.co.in",
    'category': 'Training',
    'version': '0.1',
    'depends': ['base','kwantify','kw_branch_master','crm'],
    'data': [
        'security/kw_tour_security.xml',
        'security/ir.model.access.csv',
        
        # data file section
        'data/kw_tour_type_data.xml',
        'data/kw_tour_country_type_data.xml',
        'data/kw_tour_expense_type_data.xml',
        'data/kw_tour_travel_mode_data.xml',
        'data/kw_tour_approval_workflow_data.xml',
        'data/kw_tour_settlement_workflow_data.xml',
        'data/kw_tour_settlement_mail_data.xml',
        'data/kw_tour_cancellation_workflow_data.xml',
        'data/kw_tour_sequence_no.xml',
        # data file section

        # view section
        'views/kw_tour_menu.xml',
        'views/kw_tour_city_master_view.xml',
        'views/kw_tour_city_view.xml',
        'views/kw_tour_classification_type_view.xml',
        'views/kw_tour_da_category_view.xml',
        'views/kw_tour_da_type_view.xml',
        'views/kw_tour_type_view.xml',
        'views/kw_tour_view.xml',
        'views/kw_tour_take_action_view.xml',
        'views/kw_tour_details_view.xml',
        'views/kw_tour_accomodation_view.xml',
        'views/kw_tour_expense_type_view.xml',
        # 'views/kw_tour_classfication_expense_type_view.xml',
        'views/kw_tour_ancillary_expenses_view.xml',
        'views/kw_tour_settlement_details_view.xml',
        'views/kw_tour_settlement_docs_view.xml',
        'views/kw_tour_settlement_expenses_view.xml',
        'views/kw_tour_settlement_view.xml',
        'views/kw_tour_settlement_take_action_view.xml',
        'views/kw_tour_cancellation_view.xml',
        # view section
    ],
}
