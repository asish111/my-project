from odoo import models, fields, api
from odoo.exceptions import ValidationError

class TourDetails(models.Model):
    _name = "kw_tour_details"
    _description = "Tour Details"

    tour_id = fields.Many2one("kw_tour","Tour",required=True,ondelete='cascade')
    tour_type = fields.Selection(string="Type",
        selection=[('Domestic', 'Domestic'), ('International', 'International')])
    from_date = fields.Datetime(string="Date")
    from_country_id = fields.Many2one('res.country',string="Country")
    from_city_id = fields.Many2one('kw_tour_city',string="City")

    to_date = fields.Datetime(string="Date")
    to_country_id = fields.Many2one('res.country', string="Country")
    to_city_id = fields.Many2one('kw_tour_city', string="City")

    arrangement = fields.Selection(string="Arrangement",
        selection=[('Self', 'Self'), ('Company', 'Company')],required=True,default="Company")
    travel_mode_id = fields.Many2one('kw_tour_travel_mode',"Travel Mode")
    cost = fields.Integer("Cost")

    @api.onchange('arrangement')
    def change_travel_cost(self):
        if self.arrangement and self.arrangement == "Company":
            self.travel_mode_id = False
            self.cost = False
    
    
    @api.constrains('from_date', 'to_date')
    def validate_date(self):
        for record in self:
            if record.from_date > record.to_date:
                raise ValidationError(f'From date should not greater than to date.')