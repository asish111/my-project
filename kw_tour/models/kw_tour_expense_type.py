# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourExpenseType(models.Model):
    _name = "kw_tour_expense_type"
    _description = "Tour Expense Type"
    _rec_name = "name"

    name = fields.Char('Expense Type',)
    code = fields.Char("Code")
    description = fields.Text("Description")

    @api.model
    def create(self, values):
        result = super(TourExpenseType, self).create(values)
        self.env.user.notify_success("Tour expense type created successfully.")
        return result
                                