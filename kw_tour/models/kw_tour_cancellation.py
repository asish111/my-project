from odoo import models,fields,api
from odoo.exceptions import UserError, ValidationError

class TourCancellation(models.Model):
    _name           = 'kw_tour_cancellation'
    _description    = 'Tour Cancellation'

    def default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    employee_id     = fields.Many2one("hr.employee","Employee",default=default_employee)
    tour_id         = fields.Many2one("kw_tour","Tour",required=True,default=lambda self:self.env.context.get('default_tour_id'))
    reason          = fields.Text("Reason")
    state           = fields.Selection(string="Status",selection=[('Applied','Applied'),('Approved','Approved'),('Rejected','Rejected')])

    ra_access       = fields.Boolean("RA Access", compute="compute_ra_access")

    @api.multi
    def compute_ra_access(self):
        for cancellation in self:
            if cancellation.employee_id and cancellation.employee_id.parent_id and cancellation.employee_id.parent_id.user_id == self.env.user:
                cancellation.ra_access = True


    
    @api.model
    def create(self, values):
        
        result = super(TourCancellation, self).create(values)
        
        if 'active_model' and 'active_id' in self._context and self._context['active_model'] == 'kw_tour':
            tour = self.env['kw_tour'].browse(self._context['active_id'])
            if not tour.cancellation_id:
                tour.cancellation_id = result.id
        return result  # access_check

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        if self._context.get('access_check'):
            if not self.env.user.has_group('kw_tour.group_kw_tour_admin'):
                args += [('create_uid', '=', self._uid)]
        return super(TourCancellation, self)._search(args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)
