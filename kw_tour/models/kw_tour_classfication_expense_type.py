# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourClassificationExpenseType(models.Model):
    _name = "kw_tour_classification_expense"
    _description = "Tour Classification Expense"

    classification_id = fields.Many2one("kw_tour_classification_type", string='Classfication_id')
    expense_type_id = fields.Many2one("kw_tour_expense_type",string="Expense Type")
    currency_type_id = fields.Many2one("res.currency",string="Currency Type")
    amount = fields.Integer("Amount")
    