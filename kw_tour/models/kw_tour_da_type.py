# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourDAType(models.Model):
    _name = "kw_tour_da_type"
    _description = "Tour DA Type"
    _rec_name = "expense_type_id"

    expense_type_id = fields.Many2one('kw_tour_expense_type','Expense Type',ondelete='restrict')
    category_id = fields.Many2one('kw_tour_da_category', 'Da Category',ondelete='restrict')
    percentage = fields.Integer('Percentage')

    @api.model
    def create(self, values):
        result = super(TourDAType, self).create(values)
        self.env.user.notify_success("Tour DA type created successfully.")
        return result
