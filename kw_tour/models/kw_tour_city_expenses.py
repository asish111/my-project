
# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourCityExpense(models.Model):
    _name = "kw_tour_city_expenses"
    _description = "Tour City Expenses"

    city_id = fields.Many2one("kw_tour_city",string="CIty")
    
    @api.model
    def create(self, values):
        result = super(TourCityExpense, self).create(values)
        self.env.user.notify_success("Tour City expenses created successfully.")
        return result
    
    @api.multi
    def write(self, values):
        result = super(TourCityExpense, self).write(values)
        self.env.user.notify_success("Tour City expenses updated successfully.")
        return result