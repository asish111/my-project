from odoo import models,fields,api

class TourSettlementDetails(models.Model):
    _name = 'kw_tour_settlement_details'
    _description = "Tour Settlement Details"

    settlement_id = fields.Many2one("kw_tour_settlement",required=True,ondelete='cascade')
    date = fields.Date(string='Date')
    place = fields.Char("place")
    category = fields.Selection(
        string='Category',
        selection=[('Tour', 'Tour'), ('Local Visit', 'Local Visit'),('Visit','Visit')])
    description = fields.Text("Description")
    currency_id = fields.Many2one('res.currency',"Currency")
    amount = fields.Integer('Amount')
    status = fields.Selection(
        string='Bill Status',
        selection=[('Paid', 'Paid'), ('Due', 'Due')]
    )
    