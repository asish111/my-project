from odoo import models, fields, api


class TourTravelMode(models.Model):
    _name = "kw_tour_travel_mode"
    _description = "Tour Travel Mode"

    name = fields.Char("Mode")
    code = fields.Char("Code")
