# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourCountryType(models.Model):
    _name = "kw_tour_country_type"
    _description = "Tour Country Type"

    name = fields.Char("Name",required=True)
    code = fields.Char("Code")
    description = fields.Char("Description")

    @api.model
    def create(self, values):
        result = super(TourCountryType, self).create(values)
        self.env.user.notify_success("Tour country type created successfully.")
        return result