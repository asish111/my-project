# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourClassificationType(models.Model):
    _name = "kw_tour_classification_type"
    _description = "Tour Classification Type"


    name = fields.Char('Name')
    tour_type_id = fields.Many2one("kw_tour_type","Tour Type")
    expense_type_id = fields.Many2one("kw_tour_expense_type","Expense Type")
    currency_type_id = fields.Many2one("res.currency","Currency Type")
    amount = fields.Integer("Amount")
    description = fields.Text("description")


    @api.model
    def create(self, values):
        result = super(TourClassificationType, self).create(values)
        self.env.user.notify_success("Tour classfication type created successfully.")
        return result