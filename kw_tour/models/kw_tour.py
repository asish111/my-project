from odoo import models,fields,api
from odoo.exceptions import UserError, ValidationError

class Tour(models.Model):
    _name           =  "kw_tour"
    _description    =  "Tour Apply"
    _rec_name       =  "code"

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    employee_id         = fields.Many2one('hr.employee', string="Employee Name",
                                  default=_default_employee, required=True, ondelete='cascade', index=True)
    tour_type_id        = fields.Many2one('kw_tour_type',required=True,string="Type Of Tour")
    project             = fields.Boolean(string="Project Enabled ?",default=False,compute="_compute_project")
    project_type        = fields.Selection(string='Project Type',
                                    selection=[('70', 'Work Order'), ('3', 'Opportunity')], default='70',
                                    )
    project_id          = fields.Many2one('crm.lead', string='Project')
    purpose             = fields.Text("Purpose", )
    date_travel         = fields.Date("Date Of Travel", required=True,)
    city_id             = fields.Many2one('kw_tour_city',string="Originating Place")

    tour_detail_ids     = fields.One2many('kw_tour_details','tour_id',string="Details", required=True)
    total_ticket_cost   = fields.Integer("Total Ticket Cost")
    acc_arrange         = fields.Selection(string="Accomodation Arrangement", selection=[
                                   ('Company', 'Company'), ('Self', 'Self')])
    accomodation_ids    = fields.One2many('kw_tour_accomodation','tour_id',string="Accomodation Details")

    extra_baggage       = fields.Char("Extra Baggage", )
    advance             = fields.Char("Advance(In INR)", )
    advance_usd         = fields.Char("Advance(In USD)", )
    public_view         = fields.Boolean("Enable Public View")
    ra_access           = fields.Boolean("RA Access",compute="compute_ra_access")
    admin_access        = fields.Boolean(string="Admin Access", compute="_compute_approve")
    travel_desk_access  = fields.Boolean(string="Traveldesk Access", compute="_compute_approve")
    finance_access      = fields.Boolean(string="Finance Access", compute="_compute_approve")

    state               = fields.Selection(string='Status',
                            selection=[('Draft', 'Draft'),
                            ('Applied', 'Applied'),
                            ('Traveldesk Approved', 'Traveldesk Approved'),
                            ('Finance Approved', 'Finance Approved'),
                            ('Rejected', 'Rejected')])
    cancellation_id     = fields.Many2one("kw_tour_cancellation","Cancellation",ondelete="set null")
    code                = fields.Char(string="Reference No.", default="New", readonly="1")

    parent_id           = fields.Many2one('kw_tour',"Old Tour",ondelete="cascade")
    child_id            = fields.Many2one('kw_tour',"New Tour",ondelete="set null")
    status              = fields.Selection(string='Status',required=True,
                            selection=[('Tour', 'Tour'),
                                       ('Rescheduled', 'Rescheduled')], default='Tour')

    @api.onchange('tour_type_id')
    def _onchange_project(self):
        if self.tour_type_id and self.tour_type_id.code == 'project':
            self.project        = True
        else:
            self.project        = False
            self.project_type   = False
            self.project_id     = False

    @api.multi
    def action_cancel_tour(self):
        ''' if state is not approved then change to cancelled,
            if state is approved then it will follow the approval process '''
        if self.state == 'Applied':
            self.write({'state':'Cancelled'})
        else:
            # open tour cancellation form view
            form_view_id = self.env.ref('kw_tour.view_kw_tour_cancellation_form').id
            return {
                'type'      : 'ir.actions.act_window',
                'res_model' : 'kw_tour_cancellation',
                'view_mode' : 'form',
                'view_type' : 'form',
                'view_id'   : form_view_id,
                'target'    : 'self',
                'flags'     : {"toolbar": False},
                'context'   : {'default_tour_id':self.id}
            }
            
    @api.multi
    def action_reschedule_tour(self):
        ''' Open a new form view with old data to apply a new tour.
        If state in draft or applied than through error message of can't be scheduled.
        '''
        if self.state in ['Applied','Draft']:
            raise ValidationError(f"{self.state} tour can't be rescheduled.")
        else:
            form_view_id = self.env.ref('kw_tour.view_kw_tour_form').id
            default_data = {
                'default_tour_type_id'      : self.tour_type_id and self.tour_type_id.id or False,
                'default_project_id'        : self.project_id and self.project_id.id or False,
                'default_project_type'      : self.project_type,
                'default_date_travel'       : self.date_travel,
                'default_city_id'           : self.city_id and self.city_id.id or False,
                'default_tour_detail_ids'   : self.tour_detail_ids and [[0, 0,
                        {
                            'tour_type'         : tour_detail.tour_type,
                            'from_date'         : tour_detail.from_date,
                            'from_country_id'   : tour_detail.from_country_id and tour_detail.from_country_id.id or False,
                            'from_city_id'      : tour_detail.from_city_id and tour_detail.from_city_id.id or False,
                            'to_date'           : tour_detail.to_date,
                            'to_country_id'     : tour_detail.to_country_id and tour_detail.to_country_id.id or False,
                            'to_city_id'        : tour_detail.to_city_id and tour_detail.to_city_id.id or False,
                            'arrangement'       : tour_detail.arrangement,
                            'travel_mode_id'    : tour_detail.travel_mode_id and tour_detail.travel_mode_id.id or False,
                            'cost'              : tour_detail.cost,
                        }] for tour_detail in self.tour_detail_ids] or False,
                'default_total_ticket_cost' : self.total_ticket_cost,
                'default_acc_arrange'       : self.acc_arrange,
                'default_accomodation_ids'  : self.accomodation_ids and [[0, 0,
                        {
                            'name'          : acmdtn.name,
                            'contact_person': acmdtn.contact_person,
                            'contact_no'    : acmdtn.contact_no,
                            'tour_type_id'  : acmdtn.tour_type_id and acmdtn.tour_type_id.id or False,
                            'country_id'    : acmdtn.country_id and acmdtn.country_id.id or False,
                            'city_id'       : acmdtn.city_id and acmdtn.city_id.id or False,
                            'hotel_name'    : acmdtn.hotel_name,
                            'telephone_no'  : acmdtn.telephone_no,
                            'check_in_time' : acmdtn.check_in_time,
                            'check_out_time': acmdtn.check_out_time,
                            'no_of_night'   : acmdtn.no_of_night,
                            'advance_paid'  : acmdtn.advance_paid,
                            'currency_id'   : acmdtn.currency_id and acmdtn.currency_id.id or False,
                            'status'        : acmdtn.status,
                        }] for acmdtn in self.accomodation_ids] or False,
                'default_extra_baggage'     : self.extra_baggage,
                'default_advance'           : self.advance,
                'default_advance'           : self.advance,
                'default_advance_usd'       : self.advance_usd,
                'default_parent_id'         : self.id,
                'default_status'            : 'Rescheduled',
                'default_public_view'       : self.public_view,
            }
            return {
                'type'      : 'ir.actions.act_window',
                'res_model' : 'kw_tour',
                'view_mode' : 'form',
                'view_type' : 'form',
                'view_id'   : form_view_id,
                'target'    : 'self',
                'flags'     : {"toolbar": False},
                'context'   : default_data,
            }

    @api.onchange('project_type')
    def change_project(self):
        # self.project_id = False
        if self.project_type and self.project_id and str(self.project_id.stage_id.sequence) != self.project_type:
            self.project_id = False

    @api.multi
    def _compute_project(self):
        for rec in self:
            if rec.tour_type_id.code == 'project':
                rec.project = True

    @api.model
    def create(self, values):
        values['code']  = self.env['ir.sequence'].next_by_code('self.tour_seq') or 'New'
        result          = super(Tour, self).create(values)
        self.env.user.notify_success("Apply Tour created successfully.")
        return result
    
    @api.multi
    def compute_ra_access(self):
        for tour in self:
            if tour.employee_id and tour.employee_id.parent_id and tour.employee_id.parent_id.user_id == self.env.user:
                tour.ra_access = True
            
    @api.multi
    def _compute_approve(self):
        for record in self:
            if self.env.user.has_group('kw_tour.group_kw_tour_admin'):
                record.admin_access         = True
            if self.env.user.has_group('kw_tour.group_kw_tour_travel_desk'):
                record.travel_desk_access   = True
            if self.env.user.has_group('kw_tour.group_kw_tour_finance'):
                record.finance_access       = True

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        if self._context.get('access_label_check'):
            
            if self.env.user.has_group('kw_tour.group_kw_tour_user'):
                args += [('state','=','Applied'),('employee_id','in',self.env.user.employee_ids.child_ids.ids)]
            
            elif self.env.user.has_group('kw_tour.group_kw_tour_travel_desk'):
                print("user in travel desk")
                args += [('state','=','Approved')]
            
            elif self.env.user.has_group('kw_tour.group_kw_tour_finance'):
                args += [('state','=','Traveldesk Approved')]
            
            elif self.env.user.has_group('kw_tour.group_kw_tour_admin'):
                args += [('state','=','Finance Approved')]

        return super(Tour, self)._search(args, offset=offset, limit=limit, order=order, count=count, access_rights_uid=access_rights_uid)
        
    @api.multi
    def button_take_action(self):
        view_id = self.env.ref('kw_tour.view_kw_tour_take_action_form').id
        target_id = self.id
        return {
            'name'      : 'Take Action',
            'type'      : 'ir.actions.act_window',
            'res_model' : 'kw_tour',
            'res_id'    : target_id,
            'view_type' : 'form',
            'view_mode' : 'tree,form',
            'views'     : [(view_id, 'form')],
            'target'    : 'self',
            'view_id'   : view_id,
        }

