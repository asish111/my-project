# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourType(models.Model):
    _name = "kw_tour_type"
    _description = "Tour Type"
    _order = "sequence asc"

    name = fields.Char('Name',required=True)
    code = fields.Char("Code",required=True)
    sequence = fields.Integer("sequence")

    @api.model
    def create(self, values):
        result = super(TourType, self).create(values)
        self.env.user.notify_success("Tour type created successfully.")
        return result