# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourDaCategory(models.Model):
    _name = "kw_tour_da_category"
    _description = "Tour da Category master"
    _rec_name = "name"

    name = fields.Char('Da Category')
   
   