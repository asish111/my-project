# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourCity(models.Model):
    _name = "kw_tour_city"
    _description = "Tour City"

    country_id = fields.Many2one("res.country","Country")
    country_type_id = fields.Many2one("kw_tour_country_type","Country Type")
    name = fields.Many2one("kw_tour_city_master",'City Name')
    office_location_id = fields.Many2one('kw_res_branch', 'Office Location',)
    ha_eligible = fields.Boolean("Eligible For Hardship Allowance")
    expense_type_id = fields.Many2one("kw_tour_expense_type","Expense Type")
    eligibility_percent = fields.Integer("Percentage Of Eligibility")
    classification_type_id = fields.Many2one("kw_tour_classification_type","Classification Type")
    expense_ids = fields.One2many("kw_tour_city_expenses","city_id",string="Expenses")

    @api.model
    def create(self, values):
        result = super(TourCity, self).create(values)
        self.env.user.notify_success("Tour City configuration created successfully.")
        return result
    
    @api.multi
    def write(self, values):
        result = super(TourCity, self).write(values)
        self.env.user.notify_success("Tour City configuration updated successfully.")
        return result