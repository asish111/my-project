from odoo import models,fields,api

class TourSettlement(models.Model):
    _name                 = 'kw_tour_settlement'
    _description          = "Tour Settlement"
    _rec_name             = 'employee_id'

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    employee_id           = fields.Many2one('hr.employee', string="Applied By",
                                  default=_default_employee, required=True, ondelete='cascade', index=True)

    tour_id               = fields.Many2one(string='Tour', comodel_name='kw_tour', ondelete='cascade', required=True)
    expense_ids           = fields.One2many("kw_tour_settlement_expenses","settlement_id",string="Expenses")
    settlement_detail_ids = fields.One2many("kw_tour_settlement_details","settlement_id",string="Settlement Details")
    doc_ids               = fields.One2many("kw_tour_settlement_docs","settlement_id",string="Documents")
    comment               = fields.Text("Comment")

    state                 = fields.Selection(string="Status",selection=[('Draft','Draft'),
                            ('Applied','Applied'),('Approved','Approved'),('Rejec','Rejected')])
    finance_access        = fields.Boolean("Finance Access ?",compute="compute_finaace_access")

    @api.multi
    def compute_finaace_access(self):
        res = self.env.user.has_group('kw_tour.group_kw_tour_finance')
        for settlement in self:
            settlement.finance_access = res
    
