from odoo import models, fields, api


class TourSettlementExpenses(models.Model):
    _name = 'kw_tour_settlement_expenses'
    _description = "Tour Settlement Expenses"

    settlement_id = fields.Many2one("kw_tour_settlement", required=True,ondelete='cascade')
    expense_id = fields.Many2one("kw_tour_expense_type")
    inr_currency = fields.Integer("Currency Inr")
    usd_currency = fields.Integer("Currency USD")
