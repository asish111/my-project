
# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourancillaryExpense(models.Model):
    _name = "kw_tour_ancillary_expenses"
    _description = "Tour City Ancillary"

    ancillary_expense = fields.Char("Ancillary", required=True)
    description = fields.Text("Description")

    @api.model
    def create(self, values):
        result = super(TourancillaryExpense, self).create(values)
        self.env.user.notify_success("Tour Ancillary expenses created successfully.")
        return result