from odoo import models, fields, api


class TourSettlementDocs(models.Model):
    _name           = 'kw_tour_settlement_docs'
    _description    = "Tour Settlement Docs"

    settlement_id   = fields.Many2one("kw_tour_settlement", required=True,ondelete='cascade')
    doc             = fields.Binary("Document")
    file_name       = fields.Char("File Name")
