# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TourcityMaster(models.Model):
    _name = "kw_tour_city_master"
    _description = "Tour City Master"
    _rec_name = "name"
    _order = "name asc"

    name = fields.Char(string='City Name')

    @api.model
    def create(self, values):
        result = super(TourcityMaster, self).create(values)
        self.env.user.notify_success("City created successfully.")
        return result
    
    @api.multi
    def write(self, values):
        result = super(TourcityMaster, self).write(values)
        self.env.user.notify_success("City updated successfully.")
        return result