from odoo import models, fields, api


class TourAccomodationDetails(models.Model):
    _name = "kw_tour_accomodation"
    _description = "Tour Accomodation"

    tour_id = fields.Many2one('kw_tour',required=True,ondelete="cascade")
    name = fields.Char('Name Of The Agency')
    contact_person = fields.Char('Contact Person')
    contact_no = fields.Char('Contact No.')
    tour_type_id = fields.Many2one('kw_tour_type',"Tour Type", required=True)
    country_id = fields.Many2one("res.country")
    city_id = fields.Many2one('kw_tour_city',"City")
    hotel_name = fields.Char("Hotel Name")
    telephone_no = fields.Char("Tel. No.")
    check_in_time = fields.Datetime("Check In Time")
    check_out_time = fields.Datetime("Check Out Time")
    no_of_night = fields.Integer("No. Of Nights")
    advance_paid = fields.Char("Advance Paid")
    currency_id = fields.Many2one("res.currency","Currency")
    status = fields.Char("Status")

