# -*- coding: utf-8 -*-


from . import hr_employee

from . import hr_emp_integration_file


from . import kwemp_identity_docs
from . import kwemp_membership_assc

from . import kwemp_work_experience
from . import kwemp_educational_qualification
