# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import kw_validations
from . import kw_whatsapp_integration
from . import kw_helpers

from . import models