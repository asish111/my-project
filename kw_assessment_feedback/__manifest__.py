# -*- coding: utf-8 -*-
{
    'name': "Kwantify Assessment Feedback",
    'version': '12.0.1.0.0',
    'summary': """Roll out monthly assessment feedback and get the best of your workforce""",
    'description': """Roll out monthly assessment feedback and get the best of your workforce""",
    'category': 'Generic Modules/Human Resources',
    'author': 'CSM technology pvt.ltd.',
    'company': 'CSM technology pvt.ltd.',
    'maintainer': 'CSM technology pvt.ltd.',
    'website': "https://www.csm.co.in",
    # any module necessary for this one to work correctly
    'depends': ['base','hr','kwantify'],

    # always loaded
    'data': [
        'security/kw_assessment_feedback_security.xml',
        'security/ir.model.access.csv',
        'views/kw_feedback_question_set.xml',
        'views/kw_feedback_map_resources.xml',
        'views/kw_feedback_masters_view.xml',
        'views/kw_feedback_add_feedback.xml',
        'views/kw_feedback_competencies.xml',
        'views/kw_feedback_form.xml',
        'views/kw_feedback_templates.xml',
        'views/kw_feedback_view_feedback.xml',
        'views/kw_feedback_publish_feedback.xml',
        'views/wizard/kw_feedback_publish_wizard.xml',
        'views/kw_feedback_answers_views.xml',
        'views/kw_feedback_menus.xml',
        'views/kw_assessment_feedback_assets.xml',

        #data files
        'data/kw_feedback_parameter_data.xml',
        'data/kw_feedback_year_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}