from odoo import api, models,fields
from odoo.exceptions import UserError
from odoo import exceptions,_

class kw_feedback_publish_wizard(models.TransientModel):
    _name       ='kw_feedback_publish_wizard'
    _description= 'Publish Feedback wizard'

    def _get_default_assessment_feedback(self):
        datas   = self.env['kw_feedback_details'].browse(self.env.context.get('active_ids'))
        return datas

    feedback    = fields.Many2many('kw_feedback_details',readonly=1, default=_get_default_assessment_feedback)

    @api.multi
    def publish_feedback(self):
        for record in self.feedback:
            if record.feedback_status == '3':
                record.feedback_status = '4'