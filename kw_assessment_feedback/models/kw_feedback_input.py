from odoo import models, fields, api
import uuid
from odoo.addons.http_routing.models.ir_http import slug
from werkzeug import urls
from odoo.http import request

class kw_feedback_input_line(models.Model):
    _name       = 'kw_feedback_input_line'
    _description= ' Input line of Assessment feedback'

    assessee_id         = fields.Many2one('hr.employee')
    question_set_id     = fields.Many2one('kw_feedback_set_questionier')
    feedback_details_id = fields.Many2one('kw_feedback_details')
    score               = fields.Char('Score')
    remarks             = fields.Text('Remarks')
    state               = fields.Boolean()
    competencies        = fields.Many2one('kw_feedback_competencies')
    parameter_id        = fields.Many2one('kw_feedback_parameter_master')
    
    @api.model
    def save_feedback_lines(self, details_id, post, answer_tag,parameters,competencies):
        vals = {
            'assessee_id': details_id.assessee_id.id,
            'question_set_id': details_id.question_set_id.id,
            'competencies':competencies.id,
            'feedback_details_id': details_id.id,
            'parameter_id':parameters.id,
            'state':False
        }

        old_vals = self.search(['&','&','&',
            ('assessee_id', '=', details_id.assessee_id.id),
            ('question_set_id', '=', details_id.question_set_id.id),
            ('feedback_details_id', '=', details_id.id),
            ('competencies','=',competencies.id)
        ])
        
        comment_answer_tag = "%s_%s" %(answer_tag, 'remark')
        if answer_tag in post and post[answer_tag].strip():
            comment_answer = post.pop(("%s_%s" % (answer_tag, 'remark')), '').strip()
            vals.update({'score': post[answer_tag],'remarks': comment_answer,'state': True})
        if comment_answer_tag in post and post[comment_answer_tag].strip():
            comment_answer = post.pop(("%s_%s" % (answer_tag, 'remark')), '').strip()
            vals.update({'score': '','remarks': comment_answer,'state': True})
        else:
            vals.update({'state': True})

        if not old_vals:
            self.create(vals)
        else:
            old_vals.write(vals)

        return True