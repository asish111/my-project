# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
class kw_feedback_question_set(models.Model):
    _name       = 'kw_feedback_question_set'
    _description= 'Assessment feedback Question set model'
    _rec_name   = 'year'

    year        = fields.Many2one(comodel_name='kw_feedback_year_master',required=True,)
    month       = fields.Selection(
        string='Month',
        selection=[('01', 'January'), ('02', 'February'),('03', 'March'),('04', 'April'),('05', 'May'),('06', 'June'),('07', 'July'),('08', 'August'),('09', 'September')
        ,('10', 'October'),('11', 'November'),('12', 'December')],
        default='01',required=True)
    department  = fields.Many2one(comodel_name='hr.department',string='Department',required=True)
    parameters  = fields.One2many('kw_feedback_set_questionier','quest_id')

    @api.constrains('department','year','month')
    def _check_name(self):
        record = self.env['kw_feedback_question_set'].sudo().search([]) - self
        for data in record:
            if self.year.id == data.year.id and self.month == data.month and self.department.id == data.department.id:
                raise ValidationError("This record is already exists , try a different one.")


class kw_feedback_set_questionier(models.Model):
    _name       = 'kw_feedback_set_questionier'
    _description= 'Assessment feedback Questionier Model'
    _rec_name   = 'parameters'
    
    @api.model
    def _get_domain(self):
        # print(self._context)
        param_id = self._context.get('default_parameter_id',False)
        active_id = self._context.get('default_acitve_id',False)
        # print(param_id)
        # print("Active id is ",active_id)
        if active_id:
            data = self.env['kw_feedback_question_set'].browse(active_id)
            # print(data.parameters)
            dom = [('id','not in',[i.parameters.id for i in data.parameters])]
            # print(dom," before append")
            if param_id:
                for i in param_id:
                    if i[2]:
                        dom[0][2].append(i[2]['parameters'])
            # print(dom)
            return dom
        elif param_id:
            # print([('id','not in',[i[2]['parameters'] for i in param_id])])
            return [('id','not in',[i[2]['parameters'] for i in param_id])]

            
    quest_id    = fields.Many2one('kw_feedback_question_set')
    parameters  = fields.Many2one('kw_feedback_parameter_master',required=True,domain=_get_domain)
    competencies = fields.One2many('kw_feedback_competencies','parameters')




class kw_feedback_competencies(models.Model):
    _name = 'kw_feedback_competencies'
    _description = 'Assessment feedback Competencies model'
    _rec_name = 'competencies'

    parameters = fields.Many2one('kw_feedback_set_questionier')
    competencies = fields.Text('Competencies',required=True)