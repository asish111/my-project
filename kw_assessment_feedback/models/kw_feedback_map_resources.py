# -*- coding: utf-8 -*-

from odoo import models, fields, api
import time
from odoo.exceptions import  ValidationError

class kw_feedback_map_resources(models.Model):
    _name       = 'kw_feedback_map_resources'
    _description= 'Assessment feedback Map resouces model'
    _rec_name   = 'year'

    department  = fields.Many2one(comodel_name='hr.department',string='Department',required=True)
    year        = fields.Many2one(comodel_name='kw_feedback_year_master',required=True,)
    month       = fields.Selection(
        string='Month',
        selection=[('01', 'January'), ('02', 'February'),('03', 'March'),('04', 'April'),('05', 'May'),('06', 'June'),('07', 'July'),('08', 'August'),('09', 'September')
        ,('10', 'October'),('11', 'November'),('12', 'December')],default='01',required=True)

    assessment_form_date= fields.Date(string='Assessment From Date ',required=True)
    assessment_to_date  = fields.Date(string='Assessment To Date ',required=True)
    select_assessor     = fields.Many2one('hr.employee',required=True)
    select_assessee     = fields.Many2many('hr.employee','kw_feedback_assessee_rel','feedback_id','emp_id')
    question_set_id     = fields.Many2one('kw_feedback_question_set',compute='get_questions_id',store=True)
    total_answers       = fields.Integer(string="Count Answers", compute="_compute_completed_answers")


    @api.depends('department','year','month')
    def get_questions_id(self):
        for record in self:
            quest_id = self.env['kw_feedback_question_set'].search(['&','&',('department','=',record.department.id),('year','=',record.year.id),('month','=',record.month)],limit=1)
            print(quest_id)
            if len(quest_id) > 0:
                record.question_set_id = quest_id.id
            else:
                month_dict = {"01": "January","02": "February","03": "March","04": "April","05": "May","06": "June","07": "July","08": "August","09": "September","10": "October","11": "November","12": "December"}
                final_month = month_dict[record.month]
                raise ValidationError("You don't have any template against : \n \n Department = %s \n Year = %s \n Month = %s \n \n Create a template before map the resources."%(record.department.name,record.year.year,final_month))

    @api.multi
    @api.constrains('assessment_form_date', 'assessment_to_date')
    def feedback_date_constrains(self):
        for record in self:
            if record.assessment_to_date < record.assessment_form_date:
                raise ValidationError('Feedback assessment from date must be less than to date...!')
    
    @api.multi
    @api.constrains('select_assessor', 'select_assessee')
    def feedback_assessor_constrains(self):
        for record in self:
            if record.select_assessor.id in record.select_assessee.ids:
                raise ValidationError("Assessor should not be in assessees list...!")
            if len(record.select_assessee.ids) == 0:
                raise ValidationError("please add at least one asseessee against the asseessor...!")

    
    @api.model
    def create(self,vals):
        new_record = super(kw_feedback_map_resources,self).create(vals)
        values = {
            'map_resource_id':new_record.id,
            'assessment_form_date':new_record.assessment_form_date,
            'assessment_to_date':new_record.assessment_to_date,
            'question_set_id':new_record.question_set_id.id if new_record.question_set_id else False,
        }
        for assessor in new_record.select_assessor:
            values['assessor_id'] = assessor.id
            for assessee in new_record.select_assessee:
                values['assessee_id'] = assessee.id
                self.env['kw_feedback_details'].create(values)
        return new_record

    @api.multi
    def write(self,vals):
        print(vals)
        new_record = super(kw_feedback_map_resources,self).write(vals)
        feedback_detail = self.env['kw_feedback_details'].search([('map_resource_id','=',self.id)])
        accesor_ids = feedback_detail.mapped(lambda r: r.assessor_id.id)
        accessee_ids = feedback_detail.mapped(lambda r: r.assessee_id.id)

        from_date = self.assessment_form_date
        if 'assessment_form_date' in vals:
            from_date = vals['assessment_form_date']

        to_date = self.assessment_to_date
        if 'assessment_to_date' in vals:
            to_date = vals['assessment_to_date']

        values = {
            'map_resource_id':self.id,
            'assessment_form_date':from_date,
            'assessment_to_date':to_date,
            'question_set_id':self.question_set_id.id if self.question_set_id else False,
            }

        if 'select_assessor' in vals and 'select_assessee' not in vals:
            datas = self.env['kw_feedback_details'].search([('assessor_id','in',accesor_ids)])
            for records in datas:
                values['assessor_id'] = vals['select_assessor']
                records.write(values)

        if 'select_assessee' in vals and 'select_assessor' not in vals:
            print(len(vals['select_assessee'][0][2])," Vals")
            print(len(accessee_ids)," Database")
            if len(vals['select_assessee'][0][2]) > 0:
                for a in vals['select_assessee'][0][2]:
                    if a not in accessee_ids:
                        values['assessor_id'] = self.select_assessor.id
                        values['assessee_id'] = a
                        self.env['kw_feedback_details'].create(values)
                    if a in accessee_ids:
                        for records in feedback_detail:
                            records.write(values)
            if len(vals['select_assessee'][0][2]) < len(accessee_ids):
                for accessees in accessee_ids:
                    print(accessees)
                    if accessees not in vals['select_assessee'][0][2]:
                        print(accessees," Not in less than")
                        self.env['kw_feedback_details'].search(['&',('map_resource_id','=',self.id),('assessee_id','=',accessees)],limit=1).unlink()

        if 'select_assessee' in vals and 'select_assessor' in vals:
            if 'select_assessor' in vals:
                datas = self.env['kw_feedback_details'].search([('assessor_id','in',accesor_ids)])
                for records in datas:
                    values['assessor_id'] = vals['select_assessor']
                    records.write(values)
            if 'select_assessee' in vals:
                print(len(vals['select_assessee'][0][2])," Vals")
                print(len(accessee_ids)," Database")
                if len(vals['select_assessee'][0][2]) > 0:
                    for a in vals['select_assessee'][0][2]:
                        if a not in accessee_ids:
                            values['assessor_id'] = self.select_assessor.id
                            values['assessee_id'] = a
                            self.env['kw_feedback_details'].create(values)
                        if a in accessee_ids:
                            for records in feedback_detail:
                                records.write(values)
                if len(vals['select_assessee'][0][2]) < len(accessee_ids):
                    for accessees in accessee_ids:
                        print(accessees)
                        if accessees not in vals['select_assessee'][0][2]:
                            print(accessees," Not in less than")
                            self.env['kw_feedback_details'].search(['&',('map_resource_id','=',self.id),('assessee_id','=',accessees)],limit=1).unlink()   
            
        return new_record
    
    @api.multi
    def _compute_completed_answers(self):
        for record in self:
            answers = self.env['kw_feedback_details'].search(['&',('feedback_status', 'in', ['3','4']),('map_resource_id', '=', record.ids[0])])
            record.total_answers = len(answers)

    @api.multi
    def action_get_answers(self):
        tree_res = self.env['ir.model.data'].get_object_reference('kw_assessment_feedback', 'kw_feedback_feedback_answers_tree_view')
        tree_id = tree_res and tree_res[1] or False
        form_res = self.env['ir.model.data'].get_object_reference('kw_assessment_feedback', 'kw_feedback_feedback_answers_form_view')
        form_id = form_res and form_res[1] or False
        return {
            'model': 'ir.actions.act_window',
            'name': 'Answers',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form,tree',
            'res_model': 'kw_feedback_details',
            'views': [(tree_id, 'tree'), (form_id, 'form')],
            'domain': ['&',('feedback_status', 'in', ['3','4']), ('map_resource_id', '=', self.ids[0])],
        }