from odoo import models, fields, api
import uuid
from odoo.addons.http_routing.models.ir_http import slug
from werkzeug import urls
from odoo.http import request
from odoo.exceptions import ValidationError
from datetime import datetime
import pytz


class kw_feedback_details(models.Model):
    _name = 'kw_feedback_details'
    _description = 'Assessment feedback Assessor and Assessee details'
    _rec_name = 'map_resource_id'

    assessor_id = fields.Many2one('hr.employee')
    assessee_id = fields.Many2one('hr.employee')
    image_small = fields.Binary(related='assessee_id.image', store=False)
    map_resource_id = fields.Many2one('kw_feedback_map_resources')
    year = fields.Char(related='map_resource_id.year.year', store=False)
    month = fields.Selection(related='map_resource_id.month', store=False)
    question_set_id = fields.Many2one('kw_feedback_question_set')
    assessment_form_date = fields.Date(string='Assessment From Date')
    assessment_to_date = fields.Date(string='Assessment To Date')
    feedback_status = fields.Selection(string='Status',
                                       selection=[('1', 'Not Started'), ('2', 'Draft'), ('3', 'Completed'),
                                                  ('4', 'Published')], default='1')
    token = fields.Char('Token', default=lambda self: str(uuid.uuid4()), readonly=True, copy=False)
    start_feedback_url = fields.Char("Assessment Feedback Public link", compute="_compute_feedback_url")
    view_feedback_url = fields.Char("Assessment Feedback View Result", compute="_compute_feedback_url")
    final_remark = fields.Text(string='Final Remark')
    from_date = fields.Boolean(string="Compare from date", compute='_from_date', store=False)
    to_date = fields.Boolean(string="Compare to date", compute='_to_date', store=False)
    date_time = fields.Date(string='Current Date', compute='_find_current_date', store=False)
    total_score = fields.Float('Total Score', compute='count_score', store=True,
                               help='Score will visible after complete your feedback')
    color = fields.Integer("Color Index", compute="change_color", store=False)
    feedback_detail_ids = fields.One2many('kw_feedback_input_line', 'feedback_details_id', string='Answers', copy=True)

    @api.model
    def fields_get(self, fields=None):
        fields_to_hide = ['map_resource_id', 'question_set_id', 'token', 'final_remark', 'create_date', 'id',
                          'create_uid', 'write_uid']
        res = super(kw_feedback_details, self).fields_get()
        for field in fields_to_hide:
            res[field]['selectable'] = False
        return res

    @api.depends('feedback_status')
    def change_color(self):
        for record in self:
            color = 0
            if record.feedback_status == '1':
                color = 1
            elif record.feedback_status == '2':
                color = 3
            elif record.feedback_status == '3':
                color = 4
            elif record.feedback_status == '4':
                color = 10
            record.color = color

    @api.multi
    def _find_current_date(self):
        for record in self:
            dt_now = datetime.now(pytz.timezone('Asia/Calcutta'))
            current_dt = dt_now.strftime("%Y-%m-%d")
            record.date_time = current_dt

    @api.multi
    def _from_date(self):
        for record in self:
            if record.assessment_form_date > record.date_time:
                record.from_date = True

    @api.multi
    def _to_date(self):
        for record in self:
            if record.assessment_to_date < record.date_time:
                record.to_date = True

    def _compute_feedback_url(self):
        base_url = '/' if self.env.context.get('relative_url') else \
            self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        for record in self:
            record.start_feedback_url = urls.url_join(base_url, "kw/feedback/begin/%s" % (slug(record)))
            record.view_feedback_url = urls.url_join(base_url, "kw/feedback/results/%s" % (slug(record)))

    @api.multi
    def give_feedback(self):
        token = self.token
        u_token = "/%s" % token if token else ""
        return {
            'type': 'ir.actions.act_url',
            'name': 'Give Assessment Feedback',
            'target': 'self',
            'url': self.with_context(relative_url=True).start_feedback_url + u_token
        }

    _sql_constraints = [
        ('unique_token', 'UNIQUE (token)', 'A token must be unique!'),
    ]

    @api.multi
    def view_feedback(self):
        token = self.token
        u_token = "/%s" % token if token else ""
        return {
            'type': 'ir.actions.act_url',
            'name': 'View Assessment Feedback',
            'target': 'self',
            'url': self.with_context(relative_url=True).view_feedback_url + u_token
        }

    @api.multi
    @api.depends('feedback_status')
    def count_score(self):
        for record in self:
            if record.feedback_status in ['3', '4']:
                total = 0.0
                inputs = self.env['kw_feedback_input_line'].search([('feedback_details_id', '=', record.id)])
                for input_lines in inputs:
                    total += float(input_lines.score)
                record.total_score = total
            else:
                record.total_score = 0.0
