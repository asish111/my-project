# -*- coding: utf-8 -*-

from odoo import models, fields, api

class kw_feedback_year_master(models.Model):
    _name       = 'kw_feedback_year_master'
    _description= 'Assessment feedback Year Master'
    _rec_name   = 'year'

    year        = fields.Char("Feedback Year",required=True)

class kw_feedback_parameter_master(models.Model):
    _name       = 'kw_feedback_parameter_master'
    _description= 'Assessment feedback Parameter Master'
    _rec_name   = 'parameters'

    parameters  = fields.Char("Parameters",required=True)
