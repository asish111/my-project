import json
import logging
import werkzeug
from datetime import datetime
from math import ceil
import pytz
from odoo import fields, http, SUPERUSER_ID
from odoo.http import request
from odoo.tools import ustr

_logger = logging.getLogger(__name__)
class kw_assessment_feedback(http.Controller):

    def get_feedback_details(self,feedback_details,token,post):
        try:
            details = feedback_details.search([('token','=',token)])
        except KeyError:
            return '<h1 class="text-center">Invalid Token...!!</h1>'
        if details.feedback_status != '4':
            for record in details:
                for questions in record.question_set_id:
                    for parameters in questions.parameters:
                        for competencies in parameters.competencies:
                            answer_tag = "%s_%s_%s" % (details.assessee_id.id, parameters.id, competencies.id)
                            datas = request.env['kw_feedback_input_line'].sudo().save_feedback_lines(details, post, answer_tag,parameters,competencies)
        return datas

    @http.route(['/kw/feedback/begin/<model("kw_feedback_details"):feedback_details>/<string:token>'],type='http', auth='public', website=True)
    def kw_feedback_begin(self, feedback_details, token=None, **post):
        if feedback_details.feedback_status not in ['4']:
            if feedback_details.feedback_status =='1':
                feedback_details.write({'feedback_status':'2'})
            data={'feedback':feedback_details,'token':token}
            return request.render('kw_assessment_feedback.kw_feedback_form',data)
        else:
            return request.render('kw_assessment_feedback.kw_feedback_thanks_page')

    @http.route(['/kw/feedback/prefill/<model("kw_feedback_details"):feedback_details>/<string:token>'],
                type='http', auth='public', website=True)
    def kw_feedback_prefill(self, feedback_details, token, **post):
        feedback_input_line = request.env['kw_feedback_input_line']
        ret = {}
        feedback_answers = feedback_input_line.sudo().search([('feedback_details_id.token', '=', token)])
        final_remark_tag = '%s_%s'%(feedback_details.id,'final_remark')
        for answer in feedback_answers:
            answer_value = None
            answer_tag = '%s_%s_%s' % (answer.assessee_id.id, answer.parameter_id.id, answer.competencies.id)
            answer_value = answer.score
            if answer_value:
                ret.setdefault(answer_tag, []).append(answer_value)
            answer_tag = "%s_%s_%s_%s" % (answer.assessee_id.id, answer.parameter_id.id, answer.competencies.id,'remark')
            answer_value = answer.remarks
            if answer_value:
                ret.setdefault(answer_tag, []).append(answer_value)
        if feedback_details.final_remark:
            ret.setdefault(final_remark_tag, []).append(feedback_details.final_remark)
        return json.dumps(ret, default=str)

    @http.route(['/kw/feedback/submit/<model("kw_feedback_details"):feedback_details>/<string:token>'],type='http',methods=['POST'], auth='public', website=True)
    def kw_feedback_finish(self, feedback_details, token, **post):
        final_remark = str(feedback_details.id)+'_final_remark'
        self.get_feedback_details(feedback_details,token,post)
        feedback_details.write({'feedback_status':'3','final_remark':post[final_remark]})          
        data={'feedback':feedback_details,'token':token}
        return request.render('kw_assessment_feedback.kw_feedback_thanks_page',data)

    @http.route(['/kw/feedback/back/<model("kw_feedback_details"):feedback_details>/<string:token>'], type='json', methods=['POST'], auth='public', website=True)
    def kw_feedback_back(self,feedback_details, token, **kw):
        final_remark = str(feedback_details.id)+'_final_remark'
        post = kw['kw_feedback_form']
        ret={}
        self.get_feedback_details(feedback_details,token,post)
        if kw['prevs'] == 'prevs':
            feedback_details.write({'feedback_status':'2','final_remark':post[final_remark]})  
            ret['redirect'] = 'prevs'
        return ret

    @http.route(['/kw/feedback/results/<model("kw_feedback_details"):feedback_details>/<string:token>'],type='http', auth='public', website=True)
    def kw_feedback_results(self, feedback_details, token=None, **post):
        if feedback_details.feedback_status in ['3','4']:
            data={'feedback':feedback_details,'token':token}
            return request.render('kw_assessment_feedback.kw_feedback_result_form',data)
        else:
            return "Not Published"

        