odoo.define('kw_assessment_feedback.feedback_form', function (require) {
    'use strict';

    require('web.dom_ready');
    var ajax = require('web.ajax');
    var base = require('web_editor.base');
    var context = require('web_editor.context');

    var the_form = $('#kw_assessment_feedback_form');
    if (!the_form.length) {
        return $.Deferred().reject("DOM doesn't contain '#kw_assessment_feedback_form'");
    }
    console.debug("[Assessment Feedback] Custom JS for Assessment Feedback is loading...");
    var prefill_controller = the_form.attr("data-prefill");

    // Pre-filling of the form with previous answers
    function prefill() {
        if (!_.isUndefined(prefill_controller)) {
            // alert('Called')
            var prefill_def = $.ajax(prefill_controller, { dataType: "json" })
                .done(function (json_data) {
                    _.each(json_data, function (value, key) {
                        var input = the_form.find(".form-control[name=" + key + "]");
                        input.val(value);
                        var paragraph = the_form.find(".result[name=" + key + "]")
                        paragraph.text(value)
                    });
                })
                .fail(function () {
                    console.warn("[Assessment Feedback] Unable to load prefill data");
                });
            return prefill_def;
        }
    }

    function load_locale() {
        var url = "/web/webclient/locale/" + context.get().lang || 'en_US';
        return ajax.loadJS(url);
    }

    var ready_with_locale = $.when(base.ready(), load_locale());
    ready_with_locale.then(function () {
        prefill();
    });
    $('#finish').on('click', function (e) {
        e.preventDefault();
        var form = $(this).parents('form');
        if (form.valid()) {
            swal({
                text: "Are you sure want to submit ?",
                icon: "warning",
                dangerMode: true,
                closeOnClickOutside: false,
                closeModal: false,
                buttons: {
                    confirm: { text: 'Yes, Submit', className: 'btn-info' },
                    cancel: 'Cancel'
                },
            }).then(function (isConfirm) {
                if (isConfirm) {
                    form.submit();
                } else {
                    swal.close();
                }
            });
        }
    });
    // $('#kw_assessment_feedback_form').on('submit', function (e) {
    //     alert('Submition Attempt')
    // });

});


