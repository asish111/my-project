odoo.define('kw_assessment_feedback.tour', function(require) {
"use strict";

var core = require('web.core');
var tour = require('web_tour.tour');

var _t = core._t;

tour.register('Assessment Feedback', {
    url: "/web",
}, [tour.STEPS.SHOW_APPS_MENU_ITEM, {
    trigger: '.o_app[data-menu-xmlid="kw_assessment_feedback.kw_assessment_feedback_menu_root"]',
    content: _t('Want a better way to <b>manage your Assessment Feedback</b>? <i>It starts here.</i>'),
    position: 'right',
    edition: 'community',
},{
    trigger: '.o_app[data-menu-xmlid="kw_assessment_feedback.kw_assessment_feedback_configuration"]',
    content: _t('Want a better way to <b>Configure your Template and Tag resources</b>? <i>It starts here.</i>'),
    position: 'buttom',
    edition: 'community',
},  {
    trigger: '.create_template',
    content: _t('Let\'s first create your Template.'),
    position: 'bottom',
    width: 200,
}, {
    trigger: 'input.create_competencies',
    content: _t('Choose <b>Parameters and competencies</b>. (e.g. one parameter : Core skills and Competencies : more than one.)'),
    position: 'right',
}, {
    trigger: '.create_map_resource',
    content: _t('This will Map the resources and tag the templates to them to give the feedbacks.'),
    position: 'bottom',
    run: function (actions) {
        actions.auto('.modal:visible .btn.btn-primary');
    },
}]);

});
