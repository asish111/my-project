# -*- coding: utf-8 -*-


from . import kw_announcement_category, kw_announcement, kw_announcement_template
from . import kw_announcement_comments
from . import kw_announcement_groups
