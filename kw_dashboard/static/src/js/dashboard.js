odoo.define('kw_dashboard.dashboard', function (require) {
    "use strict";
    
    var core = require('web.core');
    var framework = require('web.framework');
    var session = require('web.session');
    var ajax = require('web.ajax');
    var ActionManager = require('web.ActionManager');
    var view_registry = require('web.view_registry');
    var Widget = require('web.Widget');
    var AbstractAction = require('web.AbstractAction');
    var ControlPanelMixin = require('web.ControlPanelMixin');
    var QWeb = core.qweb;
    
    var _t = core._t;
    var _lt = core._lt;
    var offset=0;
    
    // var base64=require('kw_dashboard._decode');

    
    var KwantifyDashboardView = AbstractAction.extend(ControlPanelMixin, {
        events: {
            // 'click .my_profile': 'action_my_profile',
            'click .loadmore': 'action_load_more',
            // 'keyup .searchemp': 'action_search_employee',
        },
        init: function(parent, value) {
            this._super(parent, value);
            var employee_data = [];
            var self = this;
            if (value.tag == 'kw_dashboard.dashboard') {
                self._rpc({
                    model: 'kw.dashboard',
                    method: 'get_employee_info',
                }, []).then(function(result){
                    // console.log(result)
                    self.employee_data = result
                }).done(function(){
                    self.render();
                    self.href = window.location.href;
                });
            }
        },
        willStart: function() {
             return $.when(ajax.loadLibs(this), this._super());
        },
        start: function() {
            var self = this;
            return this._super();
        },
        render: function() {
            var super_render = this._super;
            var self = this;
            var kw_dashboard = QWeb.render( 'kw_dashboard.dashboard', {
                widget: self,
            });
            $( ".o_control_panel" ).addClass( "o_hidden" );
            $(kw_dashboard).prependTo(self.$el);
            return kw_dashboard
        },
        reload: function () {
                window.location.href = this.href;
        },
        action_my_profile: function(event) {
            var self = this;
            // console.log(employee_data.id);
            event.stopPropagation();
            event.preventDefault();
            this.do_action({
                name: _t("My Profile"),
                type: 'ir.actions.act_window',
                res_model: 'hr.employee',
                res_id: self.employee_data.id,
                view_mode: 'form',
                view_type: 'form',
                views: [[false, 'form']],
                domain: [],
                flags: {'toolbar':false},
                target: self.reload(),
            },{on_reverse_breadcrumb: function(){ return self.reload();}})
        },
        //Load More Employee Da
        
        action_load_more: function(event) {
            var self = this;
            offset = offset + 20;
            // console.log(offset)
            self._rpc({
                model: 'kw.dashboard',
                method: 'get_emp_offset',
                args :[{
                    'offset': offset,
                }],
                
                // params:('5'),
            }).then(function(result){
                    
                // console.log(result);
                $.each( result, function( key, value) {
                    // var image =  base64.decode(value.image);
                    // console.log(value.work_phone)
                    $(".emp_panel:last").after("<div class='emp_panel'>\
                            <div class='profile_img '>\
                                <img class='img-responsive pic' src='data:image/png;base64,"+value.image+"'/>\
                            </div>\
                            <div class='profile_name'>\
                                <span>\
                                    "+value.name+"\
                                </span>\
                                <a href='/web#id="+value.id+"&amp;model=hr.employee&amp;view_type=form&amp;menu_id=90' class='usericon'>\
                                <span class='fa fa-user anchor'/> </a>\
                            </div>\
                            <div class='work_phone'>\
                                <i class='fa fa-phone'> &nbsp; </i>\
                                <span> "+value.work_phone+"</span> &emsp;\
                                <i class='fa fa-mobile'> &nbsp; </i>\
                                <span> "+value.mobile_phone+"</span>\
                            </div>\
                        </div>");
                    });
            })
        },

        // action_search_employee: function(event) {
        //     var self=this;
        //     self._rpc({
        //         model: 'kw.dashboard',
        //         method: 'get_emp_offset',
        //     }).then(function(key, value){
        //         $("#searchEmp").on("keyup", function() {
        //             var value = $(this).val().toLowerCase();
        //             $("#dash *").filter(function() {
        //               $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        //             });
        //           });
        //     }) 
        // },
    });
    core.action_registry.add('kw_dashboard.dashboard', KwantifyDashboardView);
    return KwantifyDashboardView
});