from odoo import models, fields, api
from odoo.http import request

class KwantifyDashboard(models.Model):
    _name = "kw.dashboard"
    _description = "Kwantify Dashboard"

    name = fields.Char("")

    @api.model
    def get_employee_info(self):
        empData=[]
        employees = self.env['hr.employee'].sudo().search([],limit=20)
        for record in employees: 
            for employee in record:
                empData.append({'id' : employee.id, 'name' : employee.name, 'work_location': employee.work_location, 'work_email': employee.work_email, 'image': employee.image, 'work_phone': employee.work_phone,
                        'mobile_phone': employee.mobile_phone, 'job_id': employee.job_id})
        return empData
        
    @api.model   
    def get_emp_offset(self,vals):
        offset = vals['offset']
        # print(offset)
        # print(vals)
        # print("Method Called")
        
        empData=[]
        employees = self.env['hr.employee'].sudo().search([],limit=20,offset=offset)
        for record in employees: 
            for employee in record:
                empData.append({'id' : employee.id, 'name' : employee.name, 'work_location': employee.work_location, 'image': employee.image, 'work_phone': employee.work_phone,
                        'mobile_phone': employee.mobile_phone})
        return empData
        
       