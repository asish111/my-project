# -*- coding: utf-8 -*-
from datetime import datetime

from odoo import http
from odoo.http import request

class BioAttendance(http.Controller):

    @http.route('/sync_bio_attendance/', methods=['POST'],auth='public', csrf=False, type='json', cors='*')
    def sync_bio_attendance(self, **args):
        """ @params : json
                bio_attendance ; string
            @returns : json
            
                status :int
        """
        
        try:
            print('The START  ::::::::::',datetime.now())
            # print(args)
            
            if 'bio_attendance' in args and args['bio_attendance']:
                # print(args['bio_attendance'].split('~'))
                # 7,7,307,7,26,'2020-07-02 18:22',3230451~11,11,1430,11,38,'2020-07-02 18:22',3230452~11,11,1428,11,38,'2020-07-02 18:22',3230453~11,11,26,11,38,'2020-07-02 18:22',3230454~11,11,1149,11,37,'2020-07-02 18:23',3230456~                

                req_bio_attendance = args['bio_attendance'].strip('~')

                request.env['kw_bio_atd_request_response'].sudo().create({'name':args['guid'] if 'guid' in args and args['guid'] else '','request_info':req_bio_attendance})

                # all_bio_attendance = [bio_rec.split(',') if bio_rec else None for bio_rec in req_bio_attendance.split('~')]
                # print(all_bio_attendance)
                

            print('The END  ::::::::::',datetime.now())

            return {'status':200}
        except Exception as e:
            return {'status': 500, 'error_log': str(e)}

        
      
