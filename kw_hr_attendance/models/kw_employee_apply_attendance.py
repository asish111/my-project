# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta
import re
from lxml import etree

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class EmployeeApplyAttendance(models.Model):
    _name           = 'kw_employee_apply_attendance'
    _description    = 'Applying attendance for current or back date'
    _rec_name       = 'employee_id'
    _inherit        = ["mail.thread"]

    applied_for          = fields.Selection(string="Applied By",selection=[('self','Self'),('others','Others')],required=True,default='self',track_visibility='onchange')
    employee_id          = fields.Many2one('hr.employee',string='Employee', required=True,track_visibility='onchange')

    attendance_date      = fields.Date(string='Attendance Date', required=True,track_visibility='onchange',index=True)
    check_in_datetime    = fields.Datetime(string='Office-in Date & Time', required=True,track_visibility='onchange',autocomplete="off")
    check_out_datetime   = fields.Datetime(string='Office-out Date & Time', required=True,track_visibility='onchange',autocomplete="off")
    reason               = fields.Text(string="Reason", required=True,track_visibility='onchange')

    

    action_taken_by      = fields.Many2one('hr.employee', string="Action Taken By",ondelete='cascade',track_visibility='onchange')
    authority_remark     = fields.Text(string='Remark',track_visibility='onchange')
    action_taken_on      = fields.Datetime(string='Action Taken On',track_visibility='onchange')

    state                = fields.Selection(string="Status",selection=[('1','Draft'),('2','Applied'),('3','Approved'),('5','Rejected'),('6','Cancelled')], required=True,default='1',track_visibility='onchange') #,('4','Approved by HR')
  
    
    btn_visibility_status = fields.Boolean(string="Button Visibility",compute="_compute_button_visibility_status")
    cancel_btn_visibility = fields.Boolean(string="Cancel button visibility",compute="_compute_button_visibility_status")  

    attendance_history_id = fields.Many2one(
        string='Daily Attendance History Id',
        comodel_name='kw_daily_employee_attendance',compute="_compute_attendance_entries"       
    )

    shift_id             = fields.Many2one('resource.calendar',string="Assigned Shift ",related="attendance_history_id.shift_id")
    shift_in_time        = fields.Float(string="Shift In Time",related="attendance_history_id.shift_in_time")
    shift_out_time       = fields.Float(string="Shift Out Time",related="attendance_history_id.shift_out_time")
    is_cross_shift       = fields.Boolean(string="Is Cross Shift",related="attendance_history_id.is_cross_shift")

    attendance_log_ids = fields.Many2many(
        string='Daily Attendance Log',
        comodel_name='hr.attendance',compute="_compute_attendance_entries",
        
    )

    def _compute_button_visibility_status(self):
        for rec in self:
            emp_ids                   = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1).ids  
      
            rec.btn_visibility_status = True if rec.employee_id.parent_id.id in emp_ids and rec.state== '2' else False #self.env.user.has_group('hr_attendance.group_hr_attendance_manager') and rec.state== '3' or

            rec.cancel_btn_visibility = True if rec.create_uid.id == self.env.uid else False
    
    @api.depends('attendance_date','employee_id')
    def _compute_attendance_entries(self):
        for rec in self:
            if rec.attendance_date and rec.employee_id:
                rec.attendance_history_id = self.env['kw_daily_employee_attendance'].search([('employee_id','=',rec.employee_id.id),('attendance_recorded_date','=',rec.attendance_date)],limit=1).id

                rec.attendance_log_ids = self.env['hr.attendance'].search([('employee_id','=',rec.employee_id.id),('check_in','>=',rec.attendance_date),('check_in','<=',rec.attendance_date)]).ids


    @api.constrains('attendance_date','employee_id')
    def date_validation(self):
        
        for record in self:
            if record.attendance_date and record.employee_id:

                attendance_record_exist = self.env['kw_employee_apply_attendance'].search([('employee_id','=',record.employee_id.id),('attendance_date','=',record.attendance_date),('state','not in',['5','6'])]) - self

                if attendance_record_exist:
                    raise ValidationError(f'Attendance record already exists on the requested date for "{record.employee_id.name}".')           

            if record.attendance_date >= datetime.now().date():
                raise ValidationError("Attendance date should be earlier than today's date")

    @api.constrains('check_in_datetime', 'check_out_datetime','attendance_date')
    def _check_validity_check_in_check_out(self):
        """ verifies if check_in is earlier than check_out. """
        for attendance in self:
            if attendance.check_in_datetime and attendance.check_out_datetime:
                if attendance.check_out_datetime < attendance.check_in_datetime:
                    raise ValidationError('"Office Out" time cannot be earlier than "Office In" time.')

                 
                if attendance.attendance_date and not attendance.is_cross_shift:
                    if attendance.check_in_datetime.date() != attendance.attendance_date or attendance.check_out_datetime.date() != attendance.attendance_date:
                        raise ValidationError('"Office In/ Out" time must be same as the "attendance requested date".')
                elif attendance.attendance_date and attendance.is_cross_shift:
                    if (attendance.check_in_datetime.date() - attendance.attendance_date).days >1 or (attendance.check_out_datetime.date() - attendance.attendance_date).days >1:
                        raise ValidationError('Invalid "Office In/ Out" time .')

            if attendance.check_in_datetime and attendance.attendance_date:
                if attendance.check_out_datetime.date() < attendance.attendance_date:
                    raise ValidationError('"Office In" time can be on or next day of "attendance requested date".')
         

    @api.onchange('applied_for')
    def _get_employee(self):
        emp_ids = self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)
        if self.applied_for == "others":
            self.employee_id = False
            if not self.env.user.has_group('hr_attendance.group_hr_attendance_manager') :
                return {'domain': {'employee_id': ([('parent_id','in', emp_ids.ids)])}}
            else:
                return {'domain': {'employee_id': ([])}}
        else:
            
            self.employee_id = emp_ids.id 
            return {'domain': {'employee_id': ([('id','in', emp_ids.ids)])}}
            

    @api.constrains('reason')
    def validate_reason(self):
        for record in self:
            if re.match("^[a-zA-Z/\s\+-.()]+$", self.reason) == None:
                raise ValidationError("Please enter valid Reason")

    
    
    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """       

    
        result = super(EmployeeApplyAttendance, self).create(values)

        # for attendance_request in result:
        # if self.env.user.has_group('hr_attendance.group_hr_attendance_manager') :
        #     result.write({'state':'4','action_taken_by':self.env.user.employee_ids.id,'authority_remark':'Application auto-approved by Attendance Manager','action_taken_on':datetime.now()})

        #     ##create daily attendance for those info
        #     result.create_daily_attendance()
        # else:
        for attendance_request in result:
            if self.env.user.has_group('hr_attendance.group_hr_attendance_manager') or (attendance_request.employee_id.parent_id.user_id == self.env.user and attendance_request.applied_for == 'others'):
                attendance_request.write({'state':'3','action_taken_by':self.env.user.employee_ids.id,'authority_remark':'Application auto-approved.','action_taken_on':datetime.now()})

                ##create daily attendance for those info
                result.create_daily_attendance()
                
            else:
                attendance_request.write({'state':'2'})
    
        return result


    @api.multi
    def create_daily_attendance(self):

        daily_attendance                = self.env['kw_daily_employee_attendance']

        for rec in self:

            attendance_date                 = rec.attendance_date
            employee                        = rec.employee_id

            
            if employee.resource_calendar_id:
                emp_day_rec                     = daily_attendance.search([('attendance_recorded_date','=',attendance_date),('employee_id','=',employee.id)])    

                #daily_emp_attendance    = self.env['kw_daily_employee_attendance']
                shift_info              = daily_attendance._get_employee_shift(employee,attendance_date)
                emp_tz                  = employee.tz or employee.resource_calendar_id.tz or 'UTC' 

                # day_shift               = ofc_hours.mapped('calendar_id')[0]
                day_shift               = shift_info[3]
                
                attendance_data         = {'employee_id':employee.id,'shift_id':day_shift.id,'shift_name':day_shift.name,'is_cross_shift':day_shift.cross_shift,'shift_in_time':shift_info[4],'shift_out_time':shift_info[5],'attendance_recorded_date':attendance_date,'check_in':rec.check_in_datetime,'check_in_mode':2,'check_out':rec.check_out_datetime,'check_out_mode':2,'tz':emp_tz}

                if not emp_day_rec:      
                    daily_attendance.create(attendance_data)   
                else:
                    emp_day_rec.write(attendance_data)
            else:
                raise ValidationError(f'There is no shift assigned for "{employee.name}". \n Please assign shift to the employee(s) before proceeding for manual attendance')

   




    


   