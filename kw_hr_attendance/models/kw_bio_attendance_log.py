# -*- coding: utf-8 -*-
## BIo attendance service request/ processing :: Created BY : T Ketaki Debadrashini,  On :10-July-2020
import pytz
from datetime import datetime,timedelta

from odoo import models, fields, api
from odoo.exceptions import ValidationError

class BioAttendanceRequest(models.Model):
    _name           = 'kw_bio_atd_request_response'
    _description    = 'Bio Attendance Request and Response'

    name            = fields.Char(string="Request Id",)
    request_info    = fields.Text(string="Received Bio Info",)
    response        = fields.Text(string="Generated Bio Response",)

    
    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """
    
        result = super(BioAttendanceRequest, self).create(values)

        result._process_bio_request()
        # print('after atd processing ----------')
        return result


    @api.multi
    def _process_bio_request(self):
        """
            Create a new record for a bio-attendnace enrollment
            @param values: provides a data for new record
    
            @return: returns True
        """

        for record in self:

            all_bio_attendance = [bio_rec.split(',') if bio_rec else None for bio_rec in record.request_info.split('~')]
            # print(all_bio_attendance)
            self.env['kw_bio_atd_enroll_info']._prepare_enroll_info(all_bio_attendance)

            # print('inside atd processing ----------')

class BioAttendanceEnrollInfo(models.Model):
    _name           = 'kw_bio_atd_enroll_info'
    _description    = 'Bio Attendance Request Processing as per the employee enrollment no'
    _rec_name       = 'enroll_no'
    
    machine_no          = fields.Integer(string="Machine No",)
    t_machine_no        = fields.Integer(string="T Machine No",)
    enroll_no           = fields.Integer(string="Enroll No",)
    enroll_machine_no   = fields.Integer(string="Enroll Machine No",)

    verify_mode         = fields.Integer(string="Verify Mode",)
    date                = fields.Datetime(string="Date with TimeZone",)
    event_type          = fields.Integer(string="Event Type",)

    utc_date_time       = fields.Datetime(string="Date in UTC",)  
    sync_status         = fields.Integer(string="Sync Status",default=0)   


    @api.model
    def _prepare_enroll_info(self,bio_info_request):
        enrollement_info = []
        for enrolle_info in bio_info_request:

            str_date = "".join((enrolle_info[5].strip("'"), ":00")) 

            enrollement_info.append(self._get_enroll_values(machine_no=enrolle_info[0],t_machine_no=enrolle_info[1],enroll_no=enrolle_info[2],enroll_machine_no=enrolle_info[3],verify_mode=enrolle_info[4],date=str_date,event_type=enrolle_info[6]))

        # print(enrollement_info)

        if enrollement_info:
            self.create(enrollement_info)

    def _get_enroll_values(self,machine_no=0,t_machine_no=0,enroll_no=0,enroll_machine_no=0,verify_mode=0,date=None,event_type=0):

        enroll_datetime     = datetime.strptime(date, "%Y-%m-%d %H:%M:%S") if date else None

        return {
            'machine_no'    :int(machine_no),
            't_machine_no'  :int(t_machine_no),
            'enroll_no'     :int(enroll_no),
            'enroll_machine_no':int(enroll_machine_no),
            'verify_mode'   :int(verify_mode),
            'date'          :enroll_datetime,
            'event_type'    :int(event_type),
        }

    
    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """
    
        result = super(BioAttendanceEnrollInfo, self).create(values)

        
        result.create_bio_attendance_log()
        
        return result

    ##process bio-attendance info
    @api.multi
    def create_bio_attendance_log(self):
        for record in self:
            #utc_date_time sync_status  date

            employee_rec = self.env['hr.employee'].search([('biometric_id','=',str(record.enroll_no))])
            if employee_rec:

                emp_tz                  = employee_rec.tz or employee_rec.resource_calendar_id.tz or 'UTC' 
                datetime_emp_tz         = pytz.timezone(emp_tz).localize(record.date)
            
                # Convert to US/Pacific time zone
                utc_in_datetime         = datetime_emp_tz.astimezone(pytz.timezone('UTC'))
                # print(utc_in_datetime)

                
                latest_bio_log_rec          = self.env['kw_hr_attendance_log'].sudo().search([('employee_id', '=', employee_rec.id),('check_in_mode','=',1)], limit=1)

                if latest_bio_log_rec and latest_bio_log_rec.check_in and not latest_bio_log_rec.check_out :

                    latest_bio_log_rec.sudo().write({'check_out':utc_in_datetime,'check_out_mode':1})
                else:
                    self.env['kw_hr_attendance_log'].sudo().create({'employee_id': employee_rec.id,'check_in': utc_in_datetime, 'check_in_mode':1})

                record.sync_status = 1
            







    
    

    
    
