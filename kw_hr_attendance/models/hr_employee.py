# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import date,datetime,timedelta


class HrEmployee(models.Model):
    _inherit                = "hr.employee"
    _description            = "Employee"

    attendance_mode_ids     = fields.Many2many('kw_attendance_mode_master',string="Attendance mode")
    no_attendance           = fields.Boolean(string='No Attendance')
    branch_id               = fields.Many2one('kw_res_branch', string="Branch/SBU", related="user_id.branch_id", store=True)
    attendance_mode         = fields.Char("Attendance Mode", compute="get_attendance_mode")
    
    kw_attendance_ids       = fields.One2many(
        string='Daily Attendace Details',
        comodel_name='kw_daily_employee_attendance',
        inverse_name='employee_id',
    )
    kw_attendance_log_ids   = fields.One2many(
        string='Attendance Log Details',
        comodel_name='kw_hr_attendance_log',
        inverse_name='employee_id',
    )
    roaster_group_ids       = fields.One2many(
        'kw_roaster_group_config', 'group_head_id',string="Roaster Groups")

    shift_change_log_ids = fields.One2many(comodel_name='kw_attendance_shift_log',inverse_name='employee_id')
    def default_effective_from(self):
        return self.date_of_joining if self.date_of_joining else self.create_date.date()
    effective_from = fields.Date(string='Effective From') #,default=default_effective_from

    @api.model
    def get_attendance_mode(self):
        for emp in self:
            if emp.no_attendance:
                emp.attendance_mode = "No Attendance"
            else:
                attendance = emp.attendance_mode_ids.mapped('name')
                emp.attendance_mode =', '.join(attendance)

    # last_attendance_log_id = fields.Many2one('hr.attendance', compute='_compute_last_attendance_log_id',)


    # @api.depends('kw_attendance_log_ids')
    # def _compute_last_attendance_id(self):
    #     for employee in self:
    #         employee.last_attendance_id = self.env['hr.attendance'].search([
    #             ('employee_id', '=', employee.id),
    #         ], limit=1)
    

    @api.onchange("no_attendance")
    def _change_attendance_status(self):
        for record in self:
            if record.no_attendance:
                record.attendance_mode_ids = [(5, record.attendance_mode_ids.ids, False)]
    
    @api.multi
    def write(self, values):
        """
            Update all record(s) in recordset, with new value comes as {values}
            return True on success, False otherwise
    
            @param values: dict of new values to be set
    
            @return: True on success, False otherwise
        """
       
        if 'no_attendance' in values:
            if values['no_attendance']:
                values.update({'attendance_mode_ids':[(5, values['attendance_mode_ids'], False)]})  

        shift_log = self.env['kw_attendance_shift_log']
        for record in self:
            if 'resource_calendar_id' in values:
                vals = {}
                last_record = shift_log.search([('employee_id','=',record.id)],order='id asc')
                effective_date = date.today()
                if 'effective_from' in values:
                    if values['effective_from']:
                        effective_date = datetime.strptime(str(values['effective_from']), '%Y-%m-%d')
                elif record.effective_from:
                    effective_date = record.effective_from
                
                if not last_record:
                    shift_log.create({
                        'effective_from':record.create_date.date(),
                        'effective_to':effective_date - timedelta(days=1),
                        'shift_id':record.resource_calendar_id.id or False,
                        'employee_id':record.id
                    })
                else:
                    last_record[-1].write({
                        'effective_to':effective_date - timedelta(days=1)
                        })

                shift_log.create({
                    'effective_from':effective_date,
                    'shift_id':values['resource_calendar_id'],
                    'employee_id':record.id
                })

            if 'effective_from' in values:
                if values['effective_from']:
                    shift_records = shift_log.search([('employee_id','=',record.id)])
                    if shift_records:
                        shift_records[-2].write({
                        'effective_to':datetime.strptime(str(values['effective_from']), '%Y-%m-%d') - timedelta(days=1)
                        })
                        shift_records[-1].write({
                        'effective_from':values['effective_from'],
                        })
        
        result = super(HrEmployee, self).write(values)
    
        return result

    
    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """ 
        if 'no_attendance' in values:
            if values['no_attendance']:
                values.update({'attendance_mode_ids':[(5, values['attendance_mode_ids'], False)]})  

        result = super(HrEmployee, self).create(values)
    
        return result

