# -*- coding: utf-8 -*-
## Daily employee attendance consolidated data for payroll :: Created BY : T Ketaki Debadrashini,  
from datetime import datetime,timezone,timedelta  
import pytz
from dateutil.relativedelta import relativedelta

from odoo import models, fields, api, exceptions
from odoo.exceptions import ValidationError
from odoo.addons.resource.models import resource
from odoo.addons.base.models.res_partner import _tz_get
from odoo.tools.float_utils import float_round
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, pycompat

DAY_STATUS_WORKING,DAY_STATUS_LEAVE,DAY_STATUS_HOLIDAY,DAY_STATUS_RWORKING,DAY_STATUS_RHOLIDAY = '0','1','2','3','4'

IN_STATUS_EARLY_ENTRY,IN_STATUS_ON_TIME,IN_STATUS_LE,IN_STATUS_EXTRA_LE,IN_STATUS_LE_HALF_DAY,IN_STATUS_LE_FULL_DAY = '0','1','2','3','4','5'
OUT_STATUS_EARLY_EXIT,OUT_STATUS_ON_TIME,OUT_STATUS_LE,OUT_STATUS_EXTRA_LE,OUT_STATUS_EE_HALF_DAY = '0','1','2','3','4'

ATD_STATUS_PRESENT,ATD_STATUS_ABSENT,ATD_STATUS_TOUR,ATD_STATUS_LEAVE,ATD_STATUS_FHALF_LEAVE,ATD_STATUS_SHALF_LEAVE,ATD_STATUS_FHALF_ABSENT,ATD_STATUS_SHALF_ABSENT = '1','2','3','4','5','6','7','8'

ATD_MODE_BIO,ATD_MODE_MANUAL,ATD_MODE_PORTAL = 'bio_metric','manual','portal'

RECOMPUTE_START_DAY = 1

def float_time_to_seconds(float_time):
    """ Convert a float time timedelta object. """
    
    time_obj            = resource.float_to_time(float_time)
    ho, mi, se          = str(time_obj).split(':')
    time_delta_obj      = timedelta(hours=int(ho), minutes=int(mi), seconds=int(se))

    return time_delta_obj.seconds

class DailyHrAttendance(models.Model):
    _name           = "kw_daily_employee_attendance"
    _description    = "Daily Attendance"
    _order          = "attendance_recorded_date desc"
    _rec_name       = 'employee_id'

    _inherit        = ["mail.thread"]

    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    employee_id         = fields.Many2one('hr.employee', string="Employee", default=_default_employee, required=True, ondelete='cascade', index=True,track_visibility='onchange')
    attendance_recorded_date    = fields.Date('Attendance Date', required=True,index=True,track_visibility='onchange')
    
    check_in            = fields.Datetime(string="Check In",track_visibility='onchange') ##optional as the record can be inserted for holiday
    check_out           = fields.Datetime(string="Check Out",track_visibility='onchange')
    check_in_mode       = fields.Integer(string='Log-in Mode',default=0) ##0- web, 1- bio, 2-manual
    check_out_mode      = fields.Integer(string='Log-out Mode',default=0) 

    emp_tz_check_in     = fields.Char('Check In Emp. Timezone', compute='_compute_display_time')
    emp_tz_check_out    = fields.Char('Check Out Emp. Timezone', compute='_compute_display_time')

    tz                  = fields.Selection(
        _tz_get, string='Timezone',
        default=lambda self: self._context.get('tz') or self.env.user.employee_ids.tz or 'UTC',
        help="This field is used in order to define in which timezone the resources will work.")  

    shift_id            = fields.Many2one('resource.calendar',string="Shift ",track_visibility='onchange')
    is_cross_shift      = fields.Boolean(string="Is Cross Shift",)
    shift_name          = fields.Char(string='Shift Name', )
    shift_in_time       = fields.Float(string="Shift In Time",track_visibility='onchange', )
    shift_out_time      = fields.Float(string="Shift Out Time",track_visibility='onchange', )
    shift_rest_time     = fields.Float(string='Shift Rest Hour', compute="_compute_shift_details", store=True )

    check_in_status     = fields.Selection(string='Office-in Status',selection=[(IN_STATUS_EARLY_ENTRY, 'Early Entry'),(IN_STATUS_ON_TIME, 'On Time'),(IN_STATUS_LE, 'Late Entry'),(IN_STATUS_EXTRA_LE, 'Extra Late Entry'),(IN_STATUS_LE_HALF_DAY, 'Late Entry Half Day Absent'),(IN_STATUS_LE_FULL_DAY, 'Late Entry Full Day Absent')],readonly=True,compute='_compute_checkin_status',store=True,track_visibility='onchange')  ##??Late Entry

    late_entry_duration = fields.Float(string="Late Entry Duration",)
    check_out_status    = fields.Selection(string='Office-out Status',selection=[(OUT_STATUS_EARLY_EXIT, 'Early Exit'),(OUT_STATUS_ON_TIME, 'On Time'),(OUT_STATUS_LE, 'Late Exit'),(OUT_STATUS_EXTRA_LE, 'Extra Late Exit'),(OUT_STATUS_EE_HALF_DAY, 'Early Exit Half Day Absent')],readonly=True,compute='_compute_attendance_check_out_status',store=True,track_visibility='onchange')  ##??Late Exit

    lunch_in            = fields.Datetime(string="Lunch In") ## ???Float
    lunch_out           = fields.Datetime(string="Lunch Out")  
    worked_hours        = fields.Float(string='Worked Hours', compute='_compute_worked_hours', store=True, readonly=True)#

    state               = fields.Selection(string="Attendance Status" ,selection=[(ATD_STATUS_PRESENT, 'Present'), (ATD_STATUS_ABSENT, 'Absent'),  (ATD_STATUS_TOUR, 'On Tour'), (ATD_STATUS_LEAVE, 'On-leave'),  (ATD_STATUS_FHALF_ABSENT, 'First Half Absent'), (ATD_STATUS_SHALF_ABSENT, 'Second Half Absent')],readonly=True,track_visibility='onchange', compute="_compute_attendance_status", store=True) ##Late Present ,(ATD_STATUS_FHALF_LEAVE, 'First Half Leave'), (ATD_STATUS_SHALF_LEAVE, 'Second Half Leave')

    day_status          = fields.Selection(string="Day Status" ,selection=[(DAY_STATUS_LEAVE, 'On Leave'), (DAY_STATUS_WORKING, 'Working Day'),(DAY_STATUS_HOLIDAY, 'Holiday'),(DAY_STATUS_RWORKING,'Roaster Working Day'),(DAY_STATUS_RHOLIDAY, 'Roaster Week Holiday')],readonly=True,default='0', compute="_compute_shift_details", store=True,track_visibility='onchange') ##WeekHoliday                   
    
    late_entry_reason       = fields.Text(string='Late Entry Reason',)
    le_forward_to           = fields.Many2one('hr.employee', string="Late Entry Forwarded To",ondelete='cascade',compute='_compute_leapproval_status',store=True)
    le_forward_reason       = fields.Text(string='Late Entry Forward Reason',)

    le_authority_remark     = fields.Text(string='Late Entry Remark',track_visibility='onchange')
    le_action_taken_by      = fields.Many2one('hr.employee', string="Late Entry Action Taken By",ondelete='cascade',track_visibility='onchange')
    le_action_taken_on      = fields.Datetime(string="Late Entry Action Taken On",track_visibility='onchange')
    le_state                = fields.Selection(string="Late Entry Status" ,selection=[('0', 'Draft'),('1', 'Applied'), ('2', 'Granted'),('3', 'Forwarded')],track_visibility='onchange')
    # le_actions              = fields.Selection(string="Late Entry Action" ,selection=[('1', 'Approve'),('2', 'Reject'),('3', 'Forward')],track_visibility='onchange')
    le_action_status        = fields.Selection(string="Late Entry Action Status" ,selection=[('1', 'LateWPC'), ('2', 'LateWOPC')],compute='_compute_leapproval_status',store=True) ##LateWOPC  LateWPC

    
    le_approval_log_ids     = fields.One2many(
        string='Approval Logs',
        comodel_name='kw_late_entry_approval_log',
        inverse_name='daily_attendance_id',
    )
    

    _sql_constraints = [
        ('daily_attendance_unique', 'unique (employee_id,attendance_recorded_date)', 'The attendance record exists for the selected date.'),
    ]

    

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            record_name = record.employee_id.name
            result.append((record.id, record_name))
        return result

    @api.multi
    def _compute_display_time(self):
        for attendance in self:
            
            attendance.emp_tz_check_in = self._get_display_time(attendance.tz,attendance.check_in) if attendance.tz and attendance.check_in else False 
            attendance.emp_tz_check_out= self._get_display_time(attendance.tz,attendance.check_out) if attendance.tz and attendance.check_out else False 

    @api.model
    def _get_display_time(self,emp_tz,display_datetime):
        """ Return date and time (from to from) based on duration with timezone in string. Eg :
            return : August-23-2013 at (04-30 To 06-30) (Europe/Brussels)
                
        """
        timezone = emp_tz or 'UTC'
        timezone = pycompat.to_native(emp_tz)  # make safe for str{p,f}time()

        # get date/time format according to context
        format_date, format_time = self._get_date_formats()

        # convert date and time into user timezone
        self_tz     = self.with_context(tz=timezone)
        date        = fields.Datetime.context_timestamp(self_tz, fields.Datetime.from_string(display_datetime))
        to_text     = pycompat.to_text
        date_str    = to_text(date.strftime(format_date))
        time_str    = to_text(date.strftime(format_time))
        display_time = (u"%s %s (%s)") % (date_str,time_str,timezone,)

        return display_time

    @api.depends('shift_rest_time','lunch_in','lunch_out','check_in','check_out','day_status','state')
    def _compute_worked_hours(self):
        for rec in self:
            if rec.check_in and rec.check_out:
                worked_hour_in_sec      = (rec.check_out - rec.check_in).total_seconds()
                # print('working hour calculation --------------')
                # print(worked_hour_in_sec)
                rest_time_in_sec            = 0
                if rec.day_status in [DAY_STATUS_WORKING,DAY_STATUS_RWORKING] and rec.state in [ATD_STATUS_PRESENT,ATD_STATUS_FHALF_ABSENT,ATD_STATUS_SHALF_ABSENT]:
                    if rec.lunch_in and rec.lunch_out:
                        rest_time_in_sec    = (rec.lunch_out - rec.lunch_in).total_seconds()
                    else:
                        rest_time_in_sec    = float_time_to_seconds(rec.shift_rest_time)

                rec.worked_hours        = float_round((worked_hour_in_sec-rest_time_in_sec)/ 3600, precision_digits=2)
    

    @api.depends('employee_id','attendance_recorded_date','check_in')
    def _compute_shift_details(self):
        for attendance_rec in self:
            in_date              = attendance_rec.attendance_recorded_date if not attendance_rec.check_in else attendance_rec.check_in.date()
            
            shift_info           = self._get_employee_shift(attendance_rec.employee_id,in_date)
            if shift_info:
                # print(ofc_hours.mapped('id'))  
                shift_type                      = shift_info[0] ##default/flexi/roaster
                roaster_shift                   = shift_info[2] 
                ofc_hours                       = shift_info[1] 

                attendance_rec.shift_rest_time  = max(ofc_hours.mapped('rest_time'))

                ###day status               
                if shift_type == 'roaster' or roaster_shift:
                    day_status              = DAY_STATUS_RHOLIDAY if roaster_shift.week_off_status else DAY_STATUS_RWORKING #3, rosterworking day, 4: roaster week off
                else:
                    # week_offs               = set(ofc_hours.mapped('week_off'))
                    # unique_week_offs        = (list(week_offs)) 
                    day_status              = DAY_STATUS_WORKING #'1' if len(unique_week_offs) == 1 and True in unique_week_offs else '0'
                   
                    timezone                = pytz.timezone(attendance_rec.tz or attendance_rec.employee_id.tz or 'UTC')
                    #in_date                 = attendance_rec.check_in.strftime("%Y-%m-%d")
                    naive_in_datetime       = datetime.combine(in_date,resource.float_to_time(attendance_rec.shift_in_time))
                    naive_out_datetime      = datetime.combine(in_date,resource.float_to_time(attendance_rec.shift_out_time))

                    ##check if the shift is flexi:: if flexi shift then get the work intervals from employee's default shift
                    work_shift_employee     = attendance_rec.shift_id
                    if work_shift_employee.employee_id:
                        work_shift_employee = attendance_rec.employee_id.resource_calendar_id
                                                            
                    work_interval           = work_shift_employee.with_context(employee_id=attendance_rec.employee_id.id)._work_intervals(naive_in_datetime.replace(tzinfo=timezone), naive_out_datetime.replace(tzinfo=timezone), resource=None, domain=None)
                    # print('work interval --------------')
                    # print(len(work_interval))    
                    
                    ##if there are no working attedance record found then holiday
                    if len(work_interval) == 0 : #or is_public_holiday
                        day_status      = DAY_STATUS_HOLIDAY

                attendance_rec.day_status = day_status
    
    @api.depends('check_in','day_status','shift_in_time')
    def _compute_checkin_status(self):
        for attendance_rec in self: 
            
            ##check for attendance mode, for no attednace mode enabled , late entry status will not be calculated :: and if the day is not holiday/weekoff
            
            if not attendance_rec.employee_id.no_attendance and attendance_rec.day_status in [DAY_STATUS_WORKING,DAY_STATUS_RWORKING] and attendance_rec.check_in:
                
                timezone                = pytz.timezone(attendance_rec.tz or attendance_rec.employee_id.tz or 'UTC')
                check_in_status         = IN_STATUS_ON_TIME
                ##Start chec-in time status updation
                
                check_in_time_tz        = attendance_rec.check_in.astimezone(timezone)
                in_date                 = attendance_rec.check_in.strftime("%Y-%m-%d")
                naive_in_datetime       = datetime.strptime(in_date+' '+str(resource.float_to_time(attendance_rec.shift_in_time)),"%Y-%m-%d %H:%M:%S")
                
                naive_early_entry_time  = naive_in_datetime+relativedelta(seconds=-float_time_to_seconds(attendance_rec.shift_id.early_entry_time)) if attendance_rec.shift_id.early_entry_time>0 else False #datetime.strptime(in_date+' '+str(resource.float_to_time(attendance_rec.shift_id.early_entry_time)),"%Y-%m-%d %H:%M:%S")
               
                naive_extra_late_entry_time  = naive_in_datetime+relativedelta(seconds=+float_time_to_seconds(attendance_rec.shift_id.late_entry_time)) if attendance_rec.shift_id.late_entry_time>0 else False

                naive_half_day_late_entry_time  = naive_in_datetime+relativedelta(seconds=+float_time_to_seconds(attendance_rec.shift_id.late_entry_half_leave_time)) if attendance_rec.shift_id.late_entry_half_leave_time>0 else False

                naive_full_day_late_entry_time  = naive_in_datetime+relativedelta(seconds=+float_time_to_seconds(attendance_rec.shift_id.late_entry_full_leave_time)) if attendance_rec.shift_id.late_entry_full_leave_time>0 else False
               
                if naive_early_entry_time and check_in_time_tz.replace(tzinfo=None) <= naive_early_entry_time:
                    check_in_status         = IN_STATUS_EARLY_ENTRY
                elif check_in_time_tz.replace(tzinfo=None) > naive_in_datetime and check_in_time_tz.replace(tzinfo=None) > naive_in_datetime+relativedelta(seconds=+float_time_to_seconds(attendance_rec.shift_id.grace_time)):
                    check_in_status         = IN_STATUS_LE 
                    if naive_extra_late_entry_time and check_in_time_tz.replace(tzinfo=None) >= naive_extra_late_entry_time: 
                        check_in_status     = IN_STATUS_EXTRA_LE
                    if naive_half_day_late_entry_time and check_in_time_tz.replace(tzinfo=None) >= naive_half_day_late_entry_time: 
                        check_in_status     = IN_STATUS_LE_HALF_DAY
                    if naive_full_day_late_entry_time and check_in_time_tz.replace(tzinfo=None) >= naive_full_day_late_entry_time: 
                        check_in_status     = IN_STATUS_LE_FULL_DAY
                # print(check_in_status)
                attendance_rec.check_in_status = check_in_status
               
                ##END: chec-in time status updation ##
                
    @api.depends('check_in_status','late_entry_reason')
    def _compute_leapproval_status(self):
        for attendance_rec in self:
            if attendance_rec.check_in_status and attendance_rec.check_in_status not in [IN_STATUS_EARLY_ENTRY,IN_STATUS_ON_TIME]:
               
                if attendance_rec.late_entry_reason:
                    attendance_rec.le_forward_to = attendance_rec.employee_id.parent_id.id

                attendance_rec.le_action_status = '1'

    @api.depends('check_out','day_status','shift_out_time')
    def _compute_attendance_check_out_status(self):
        for attendance_rec in self: 
            ##check for attendance mode, for no attednace mode enabled , late entry status will not be calculated :: and the day is not holiday/weekoff
            if not attendance_rec.employee_id.no_attendance and attendance_rec.day_status  in [DAY_STATUS_WORKING,DAY_STATUS_RWORKING]:

                timezone            = pytz.timezone(attendance_rec.tz or attendance_rec.employee_id.tz or 'UTC')
                
                ##Start :: check-out time status updation ##  
                if attendance_rec.check_out:          
                    check_out_status         = OUT_STATUS_ON_TIME
                    check_out_time_tz        = attendance_rec.check_out.astimezone(timezone)
                    out_date                 = attendance_rec.check_out.strftime("%Y-%m-%d")
                    naive_out_datetime       = datetime.strptime(out_date+' '+str(resource.float_to_time(attendance_rec.shift_out_time)),"%Y-%m-%d %H:%M:%S")

                    naive_late_exit_time     = naive_out_datetime+relativedelta(seconds=+float_time_to_seconds(attendance_rec.shift_id.late_exit_time)) if attendance_rec.shift_id.late_exit_time>0 else False                    

                    naive_extra_late_exit_time  = naive_out_datetime+relativedelta(seconds=+float_time_to_seconds(attendance_rec.shift_id.extra_late_exit_time)) if attendance_rec.shift_id.extra_late_exit_time>0 else False 

                    naive_half_day_early_exit_time  = naive_out_datetime+relativedelta(seconds=-float_time_to_seconds(attendance_rec.shift_id.early_exit_half_leave_time)) if attendance_rec.shift_id.early_exit_half_leave_time>0 else False 

                    if check_out_time_tz.replace(tzinfo=None) < naive_out_datetime:
                        check_out_status         = OUT_STATUS_EARLY_EXIT

                        if naive_half_day_early_exit_time and check_out_time_tz.replace(tzinfo=None) <= naive_half_day_early_exit_time:
                            check_out_status     = OUT_STATUS_EE_HALF_DAY
                    elif naive_late_exit_time and check_out_time_tz.replace(tzinfo=None) >= naive_late_exit_time:
                        check_out_status         = OUT_STATUS_LE
                        if naive_extra_late_exit_time and check_out_time_tz.replace(tzinfo=None) >= naive_extra_late_exit_time:
                            check_out_status     = OUT_STATUS_EXTRA_LE

                    attendance_rec.check_out_status = check_out_status
                ##END: check_out  time status updation ##

    @api.depends('check_in_status','day_status','check_out_status')
    def _compute_attendance_status(self):
        for attendance_rec in self:          

            attendance_status =  ATD_STATUS_PRESENT if attendance_rec.check_in else ATD_STATUS_ABSENT
            ##if its a working day 
            if attendance_rec.day_status in [DAY_STATUS_WORKING,DAY_STATUS_RWORKING]:
                
                if attendance_rec.check_in_status in [IN_STATUS_LE_HALF_DAY]:
                    attendance_status =  ATD_STATUS_FHALF_ABSENT

                if  attendance_rec.check_out_status in [OUT_STATUS_EE_HALF_DAY]:
                    attendance_status =  ATD_STATUS_SHALF_ABSENT
                
                if attendance_rec.check_in_status in [IN_STATUS_LE_FULL_DAY] or (attendance_rec.check_in_status in [IN_STATUS_LE_HALF_DAY] and attendance_rec.check_out_status in [OUT_STATUS_EE_HALF_DAY]):
                    attendance_status =  ATD_STATUS_ABSENT

            # print('------------inside state change ------------------')
            attendance_rec.state = attendance_status

    ##create daily attendance record for pay roll as per the log-in & log-out
    def create_daily_attendance(self,attendance_log_records):
        ## get the attendance date from record
        ## if no cross shift then normal flow
        ## if cross shift current date or previous date, then conditiona applied
        attendance_log_records.ensure_one()

        atd_employee_id         = attendance_log_records.employee_id
        attendance_date         = attendance_log_records.check_in.date()

        daily_emp_attendance    = self.env['kw_daily_employee_attendance']        
        emp_tz                  = atd_employee_id.tz or atd_employee_id.resource_calendar_id.tz or 'UTC' 
        

        ##get today's shift info       
        current_day_shift_info     = self._get_employee_shift(atd_employee_id,attendance_date)
        # shift_in_out_info          = self._get_shift_in_out_time(current_day_shift_info)
        # current_day_ofc_hours      = shift_in_out_info[1]  
        current_day_shift          = current_day_shift_info[3]       
        curr_shift_in_time         = current_day_shift_info[4]
        curr_shift_out_time        = current_day_shift_info[5]
        
        curr_naive_in_datetime     = datetime.strptime(attendance_date.strftime("%Y-%m-%d")+' '+str(resource.float_to_time(curr_shift_in_time)),"%Y-%m-%d %H:%M:%S")
        curr_naive_early_entry_time = curr_naive_in_datetime-relativedelta(seconds=+float_time_to_seconds(current_day_shift.early_entry_time)) 

        ##get previous day shift info 
        previous_day_date           = attendance_date - timedelta(days=1)
        previous_day                = datetime.strftime(previous_day_date, '%Y-%m-%d')
        previous_day_shift_info     = self._get_employee_shift(atd_employee_id,previous_day_date)
        # previous_day_ofc_hours      = previous_day_shift_info[1]  
        previous_day_shift          = previous_day_shift_info[3]
        
        ## if previous day is cross shift then ....
        if previous_day_shift.cross_shift:
            cross_shift_out_time   = previous_day_shift_info[5]
            cross_shift_in_time    = previous_day_shift_info[4]
            ##out date time should be today 
            prev_naive_out_datetime            = datetime.strptime(attendance_date+' '+str(resource.float_to_time(cross_shift_out_time)),"%Y-%m-%d %H:%M:%S")
            prev_naive_in_datetime             = datetime.strptime(previous_day+' '+str(resource.float_to_time(cross_shift_in_time)),"%Y-%m-%d %H:%M:%S")

            prev_naive_extra_late_exit_time    = prev_naive_out_datetime+relativedelta(seconds=+float_time_to_seconds(previous_day_shift.extra_late_exit_time)) 
            prev_naive_early_entry_time        = prev_naive_in_datetime-relativedelta(seconds=+float_time_to_seconds(previous_day_shift.early_entry_time)) 

            prev_attendance_rec   = daily_emp_attendance.search([('employee_id','=',atd_employee_id.id),('attendance_recorded_date','=',previous_day)])

            ##previous day attendance logs
            prev_attendance_logs         = self.env['hr.attendance'].search(['&',('employee_id','=',atd_employee_id.id),'&',('check_in','>=',prev_naive_early_entry_time),'|',('check_out','<=',prev_naive_extra_late_exit_time),('check_out','<=',curr_naive_early_entry_time)]) #atd_employee_id.attendance_ids.filtered(lambda rec: rec.check_in >= prev_naive_early_entry_time and (rec.check_out <= prev_naive_extra_late_exit_time or rec.check_out <= curr_naive_early_entry_time))
            if prev_attendance_logs:
                prev_first_check_in      = min(prev_attendance_logs.mapped('check_in'))
                all_check_outs           = prev_attendance_logs.filtered(lambda rec:rec.check_out !=False).mapped('check_out')
                prev_last_check_out      = max(all_check_outs) if all_check_outs else False
                prev_last_check_in       = max(prev_attendance_logs.mapped('check_in')) 
                prev_check_out           = prev_last_check_out if prev_last_check_out  and prev_last_check_out > prev_last_check_in else prev_last_check_in

                ##if previous day record exists then update the  check-out
                if prev_attendance_rec:
                    prev_attendance_rec.write({'check_in':prev_first_check_in,'check_out':prev_check_out})

                ##else insert a new one
                else: 
                    ##create yesterday's attendance
                    if prev_first_check_in <= prev_naive_extra_late_exit_time and prev_first_check_in >= prev_naive_early_entry_time:
                        daily_emp_attendance.create({'employee_id':atd_employee_id.id,'shift_id':previous_day_shift.id,'shift_name':previous_day_shift.name,'is_cross_shift':previous_day_shift.cross_shift,'shift_in_time':cross_shift_in_time,'shift_out_time':cross_shift_out_time,'attendance_recorded_date':previous_day,'check_in':prev_first_check_in,'check_out':prev_check_out,'tz':emp_tz, 'le_state':'0' }) 

            # print('ooooooooooooooooooooooooooooooo')
            ## insert for today's attendance if the time is greater than or same as early entry time

            # if attendance_log_records.check_in >= curr_naive_early_entry_time or attendance_log_records.check_in >= prev_naive_extra_late_exit_time:

            cur_attendance_logs         = atd_employee_id.attendance_ids.filtered(lambda rec: (rec.check_in >= curr_naive_early_entry_time or rec.check_in >= prev_naive_extra_late_exit_time) and rec.check_in.date() == attendance_date)

            if cur_attendance_logs:
                cur_first_check_in      = min(cur_attendance_logs.mapped('check_in'))
                cur_all_check_outs      = cur_attendance_logs.filtered(lambda rec:rec.check_out !=False and rec.check_out.date() == attendance_date).mapped('check_out')
                cur_last_check_out      = max(cur_all_check_outs) if cur_all_check_outs else False
                cur_last_check_in       = max(cur_attendance_logs.mapped('check_in')) 
                cur_check_out           = cur_last_check_out if cur_last_check_out and cur_last_check_out > cur_last_check_in else cur_last_check_in 

                cur_check_out           = False 
                if cur_last_check_out and cur_last_check_out > cur_last_check_in:
                    cur_check_out       = cur_last_check_out
                elif cur_last_check_in and len(cur_attendance_logs)>1:
                    cur_check_out       = cur_last_check_in
           

                day_rec                 = daily_emp_attendance.search([('employee_id','=',atd_employee_id.id),('attendance_recorded_date','=',attendance_date)])

                ##get all the log records of the day
                # attendance_logs         = atd_employee_id.attendance_ids.filtered(lambda rec: rec.check_in.date() == attendance_date)                

                if day_rec:                                           
                    day_rec.write({'check_in':cur_first_check_in,'check_out': cur_check_out})
                else:
                    daily_emp_attendance.create({'employee_id':atd_employee_id.id,'shift_id':current_day_shift.id,'shift_name':current_day_shift.name,'is_cross_shift':current_day_shift.cross_shift,'shift_in_time':curr_shift_in_time,'shift_out_time':curr_shift_out_time,'attendance_recorded_date':attendance_date,'check_in':cur_first_check_in,'check_out':cur_check_out,'tz':emp_tz,'le_state':'0' }) 

        ##if previous day was not cross-shift
        else:

            attendance_logs         = atd_employee_id.attendance_ids.filtered(lambda rec: rec.check_in.date() == attendance_date)

            if attendance_logs:
                first_check_in          = min(attendance_logs.mapped('check_in'))
                all_check_outs          = attendance_logs.filtered(lambda rec:rec.check_out !=False and rec.check_out.date() == attendance_date).mapped('check_out')
                last_check_out          = max(all_check_outs) if all_check_outs else False
                last_check_in           = max(attendance_logs.mapped('check_in'))     

                flast_check_out         = False 
                if last_check_out and last_check_out > last_check_in:
                    flast_check_out     = last_check_out
                elif last_check_in and len(attendance_logs)>1:
                    flast_check_out     = last_check_in

                # print('------ssssss----------------------------------')                

                day_rec                 = daily_emp_attendance.search([('employee_id','=',atd_employee_id.id),('attendance_recorded_date','=',attendance_date)])

                if day_rec:
                    day_rec.write({'check_in':first_check_in,'check_out': flast_check_out})
                  
                else:
                    # print("Shift nfo --")                
                    daily_emp_attendance.create({'employee_id':atd_employee_id.id,'shift_id':current_day_shift.id,'shift_name':current_day_shift.name,'is_cross_shift':current_day_shift.cross_shift,'shift_in_time':curr_shift_in_time,'shift_out_time':curr_shift_out_time,'attendance_recorded_date':first_check_in.date(),'check_in':first_check_in,'check_out':flast_check_out,'tz':emp_tz,'le_state':'0' })                              

        #return day_rec

    ##method to get the employee shift and office hour details as per shift/flexi/Roaster
    def _get_employee_shift(self,employee,check_in_time):
        """
        get the detail shift information (regular/roaster/flexi)
        @param values: provides attendance time / resource working hours

        @return: returns [shift_type,work_hours,roaster_shift_rec,shift record, shift in time, shift out time]
        """
        # print('inside shift employee ------')
        # print(check_in_time.weekday())
        roaster_shift       = self._get_employee_roaster_shift(employee.id,check_in_time)
        flexi_shift         = self._get_employee_flexi_shift(employee.id)

        employee_shift      = employee.resource_calendar_id

        timezone            = pytz.timezone(employee.tz or employee.resource_calendar_id.tz or 'UTC')
        start_dt            = datetime.combine(check_in_time, resource.float_to_time(0.0))
        end_dt              = datetime.combine(check_in_time, resource.float_to_time(23.98))
        attendance_intervals = False
        shift_type          = 'default' ##regular shift
        
        if roaster_shift:
            employee_shift  = roaster_shift.shift_id
            shift_type      = 'roaster' ##roaster shift

        if flexi_shift: 
            attendance_intervals    = flexi_shift.with_context(employee_id=employee.id)._attendance_intervals(start_dt.replace(tzinfo=timezone), end_dt.replace(tzinfo=timezone), resource=None)
            # print("---inside flexi")
            # print(len(attendance_intervals))
            if attendance_intervals:
                employee_shift  = flexi_shift
                shift_type      = 'flexi' ##flexi shift

        if not attendance_intervals :
            ##Check employee shift chnage history and effective date 
            employee_shift      = self._get_employee_assigned_shift(employee,check_in_time)

            attendance_intervals  = employee_shift.with_context(employee_id=employee.id)._attendance_intervals(start_dt.replace(tzinfo=timezone), end_dt.replace(tzinfo=timezone), resource=None)

        all_ofc_hours           = self.env['resource.calendar.attendance']
        exceptional_ofc_hours   = self.env['resource.calendar.attendance']
        regular_ofc_hours       = self.env['resource.calendar.attendance']

        for _,_,rec in attendance_intervals:           
            all_ofc_hours  |= rec           
          
        # print(all_ofc_hours)

        exceptional_ofc_hours  = all_ofc_hours.filtered(lambda rec: rec.date_from and rec.date_to )
        regular_ofc_hours      = all_ofc_hours.filtered(lambda rec: not rec.date_from)

        # for rec in all_ofc_hours:
        #     print(rec) 
        ofc_hours              = exceptional_ofc_hours if exceptional_ofc_hours else regular_ofc_hours
        shift_inout_info       = self._get_shift_in_out_time(ofc_hours)

        return [shift_type,ofc_hours,roaster_shift if roaster_shift else False]+shift_inout_info

    ##method to return the flexi shift of employee
    def _get_employee_flexi_shift(self,employee_id):
        return self.env['resource.calendar'].search([('employee_id','=',employee_id)],limit=1)

    ##method to return the roaster shift of employee
    def _get_employee_roaster_shift(self,employee_id,search_date):
        search_date         = search_date.strftime("%Y-%m-%d")
        return self.env['kw_employee_roaster_shift'].search([('employee_id','=',employee_id),('date','=',search_date)],limit=1)

    ##method to return employee current shift as per the given date
    def _get_employee_assigned_shift(self,employee_id,search_date):
        # print(search_date,employee_id)
        search_date         = search_date #.strftime("%Y-%m-%d")
        if employee_id.effective_from and employee_id.effective_from <= search_date:
            return employee_id.resource_calendar_id
        else:
            emp_history_log = self.env['kw_attendance_shift_log'].search([('employee_id','=',employee_id.id),('effective_from','<=',search_date),('effective_to','>=',search_date)],order='effective_from desc',limit=1)
           
            # print(emp_history_log)
            return emp_history_log.shift_id if emp_history_log else employee_id.resource_calendar_id

    ## method to get the shift information ,as per the shift type
    def _get_shift_in_out_time(self,ofc_hours):
        """
        get the shift in/out time as per the cross shift/normal shift type
        @param values: provides attendnace time / resource working hours

        @return: returns [shift record, shift in time, shift out time]
        """
        day_shift,shift_in_time,shift_out_time = None,None,None
        if ofc_hours:
            day_shift                  = ofc_hours.mapped('calendar_id')[0]
            if day_shift.cross_shift:
                shift_in_time          = max(ofc_hours.mapped('hour_from'))
                shift_out_time         = min(ofc_hours.mapped('hour_to'))
            else:
                shift_in_time          = min(ofc_hours.mapped('hour_from'))
                shift_out_time         = max(ofc_hours.mapped('hour_to'))

        return [day_shift,shift_in_time,shift_out_time]
        


    @api.model
    def _get_date_formats(self):
        """ get current date and time format, according to the context lang
            :return: a tuple with (format date, format time)
        """
        lang = self._context.get("lang")
        lang_params = {}
        if lang:
            record_lang = self.env['res.lang'].search([("code", "=", lang)], limit=1)
            lang_params = {
                'date_format': record_lang.date_format,
                'time_format': record_lang.time_format
            }

        # formats will be used for str{f,p}time() which do not support unicode in Python 2, coerce to str
        format_date = pycompat.to_native(lang_params.get("date_format", '%B-%d-%Y'))
        format_time = pycompat.to_native(lang_params.get("time_format", '%I-%M %p'))
        return (format_date, format_time)

    @api.model
    def get_current_user_details(self):
        emp_record = self.env['kw_daily_employee_attendance'].search([('employee_id.user_id','=',self.env.uid),('attendance_recorded_date','=',datetime.date(datetime.now()))])
        
        emp_attendance_mode = self.env['hr.employee'].search([('id','=',emp_record.employee_id.id)])
        portal_mode = emp_attendance_mode.attendance_mode_ids.filtered(lambda r: r.alias == ATD_MODE_PORTAL)

        lunch_in_time,lunch_out_time,error_msg,check_in_time,check_out_time = '','','','',''
        emp_timezone = pytz.timezone(emp_record.employee_id.tz or 'UTC')
        if emp_record.check_in:
            check_in_time = datetime.strftime(emp_record.check_in.replace(tzinfo=pytz.utc).astimezone(emp_timezone), "%H:%M:%S")
        if emp_record.check_out:
            check_out_time = datetime.strftime(emp_record.check_out.replace(tzinfo=pytz.utc).astimezone(emp_timezone), "%H:%M:%S")
        if emp_record.lunch_in:
            lunch_in_time = datetime.strftime(emp_record.lunch_in.replace(tzinfo=pytz.utc).astimezone(emp_timezone), "%H:%M:%S")
        if emp_record.lunch_out:
            lunch_out_time = datetime.strftime(emp_record.lunch_out.replace(tzinfo=pytz.utc).astimezone(emp_timezone), "%H:%M:%S")
        if emp_record.lunch_in and emp_record.lunch_in < emp_record.lunch_out:
            error_msg = "Lunch In time should be greater than Lunch Out."

        data = {'employee_id':emp_record.employee_id.id,
            'employee_name':emp_record.employee_id.name,
            'check_in': check_in_time,
            'check_out': check_out_time,
            'portal_mode':1 if  portal_mode else 0,
            'lunch_in_time': lunch_in_time,
            'lunch_out_time':lunch_out_time,
            'error_msg':error_msg
            }
        return data
    
    @api.model
    def get_lunch_timings(self,args):
        lunch_in_status     = args.get('lunch_in_status',False)
        lunch_in_time       = args.get('lunch_in', False)
        lunch_out_status    = args.get('lunch_out_status',False)
        lunch_out_time      = args.get('lunch_out', False)
        check_out_status    = args.get('check_out_status', False)
        emp_record          = self.env['kw_daily_employee_attendance'].search([('employee_id.user_id','=',self.env.uid),('attendance_recorded_date','=',datetime.date(datetime.now()))])

        if emp_record and emp_record.check_in:

            if lunch_out_status == 'lunchOUT':
                emp_record.lunch_out = lunch_out_time

            if lunch_in_status == 'lunchIN':
                if not emp_record.lunch_out:
                    raise ValidationError("Lunch-out time should be updated first.")
                if emp_record.lunch_out > datetime.strptime(lunch_in_time,'%Y-%m-%d %H:%M:%S.%f'):
                    raise ValidationError("Lunch in time should be greater than Lunch out time.")
                emp_record.lunch_in = lunch_in_time
            
            if check_out_status == 'checkOUT':
                # emp_attendance_mode = self.env['hr.employee'].search([('id','=',emp_record.employee_id.id)])
                portal_mode = emp_record.employee_id.attendance_mode_ids.filtered(lambda r: r.alias == ATD_MODE_PORTAL)
                if portal_mode:
                    emp_record.check_out = datetime.now()
                    # self.env['kw_hr_attendance_log'].employee_check_out(emp_record.employee_id.user_id.id)
                else:
                    raise ValidationError("Sorry, your office out time can not be recorded through portal.")
        else:
            raise ValidationError("Attendance details are not available for today.")

   
    def _compute_day_status(self,employee_id,attendance_recorded_date):
        """
          @params  : employee record set, attedance_date
          @returns : day status,rest_hour
        """
        
        shift_info           = self._get_employee_shift(employee_id,attendance_recorded_date)
        if shift_info:
            # print(ofc_hours.mapped('id'))  
            shift_type       = shift_info[0] ##default/flexi/roaster
            roaster_shift    = shift_info[2] 
            ofc_hours        = shift_info[1]

            shift_id         = shift_info[3]
            shift_in_time    = shift_info[4]
            shift_out_time   = shift_info[5]

            shift_rest_time  = max(ofc_hours.mapped('rest_time'))            

            ###day status            
            if shift_type == 'roaster' or roaster_shift:
                day_status              = DAY_STATUS_RHOLIDAY if roaster_shift.week_off_status else DAY_STATUS_RWORKING #3, rosterworking day, 4: roaster week off
            else:
                # week_offs               = set(ofc_hours.mapped('week_off'))
                # unique_week_offs        = (list(week_offs)) 
                day_status              = DAY_STATUS_WORKING #'1' if len(unique_week_offs) == 1 and True in unique_week_offs else '0'
                
                timezone                = pytz.timezone(employee_id.tz or 'UTC')
                #in_date                 = attendance_rec.check_in.strftime("%Y-%m-%d")
                naive_in_datetime       = datetime.combine(attendance_recorded_date,resource.float_to_time(shift_in_time))
                naive_out_datetime      = datetime.combine(attendance_recorded_date,resource.float_to_time(shift_out_time))

                ##check if the shift is flexi:: if flexi shift then get the work intervals from employee's default shift
                work_shift_employee     = shift_id
                if work_shift_employee.employee_id:
                    work_shift_employee = employee_id.resource_calendar_id
                                                        
                work_interval           = work_shift_employee.with_context(employee_id=employee_id.id)._work_intervals(naive_in_datetime.replace(tzinfo=timezone), naive_out_datetime.replace(tzinfo=timezone), resource=None, domain=None)
                # print('work interval --------------')
                # print(len(work_interval))    
                
                ##if there are no working attedance record found then holiday
                if len(work_interval) == 0 : #or is_public_holiday
                    day_status      = DAY_STATUS_HOLIDAY

            return [day_status,shift_rest_time,shift_id,shift_in_time,shift_out_time]

        return False

    ## method to recompute the daily attendance fields of the employees
    def recompute_employee_daily_attendance(self):
        """
            recompute the daily attendance data of each employee
            -   checks if roaster assigned
            -   checks if flexi assigned
            -   checks if exceptional office hours assigned
            -   checks if holiday exists
             
            -   checks if shift changed
            -   checks if attendance mode changed

            ** Assumptions
            -   Default Working hours does not change
            -   Grace Period & Other Timings (EE,LE,ELE etc) does not change
            ** It will consider the latest info same for all days
        """
        daily_attendance      = self.env['kw_daily_employee_attendance']
        # all_employees         = self.env['hr.employee'].search([])

        end_date              = datetime.today().date() 
        start_date            = end_date.replace(day=1)
        emp_data              = []
        for day in range(int ((end_date - start_date).days)+1):

            attendance_date            = start_date + timedelta(day)
            employee_attendance_data   = daily_attendance.search([('attendance_recorded_date','=',attendance_date)])

            for emp_attendance_rec in employee_attendance_data:

                ##check shift details 
                #shift_info    = self._get_employee_shift(emp_attendance_rec.employee_id,emp_attendance_rec.attendance_recorded_date)
                shift_info    = self._compute_day_status(emp_attendance_rec.employee_id,emp_attendance_rec.attendance_recorded_date)
                # print(shift_info)
                if shift_info :
                     
                    emp_attendance_rec.write({'day_status':shift_info[0],'shift_id':shift_info[2].id,'shift_name':shift_info[2].name,'shift_rest_time':shift_info[1],'shift_in_time':shift_info[3],'shift_out_time':shift_info[4]})

                






   