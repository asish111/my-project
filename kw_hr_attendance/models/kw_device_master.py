from odoo import models, fields, api

class DeviceMaster(models.Model):
    _name = 'kw_device_master'
    _description = 'Device Master'
    _rec_name = 'device_id'

    device_id = fields.Integer("Device Id",required=True)
    sync_status = fields.Boolean("Synchronization status",)
    location = fields.Selection(string="Location" ,selection=[('ho', 'HO'),('branch', 'Branch')], requried=True)

    _sql_constraints = [
        ('device_id_unique', 'unique (device_id)', 'Device Id already exists.'),
    ]