from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'
        
    module_kw_hr_attendance_status = fields.Boolean(string="Enable Portal Attendance")
    late_entry_screen_enable = fields.Boolean(string="Enable Late Entry Screen")

    @api.model
    def get_values(self):
        res = super(ResConfigSettings, self).get_values()
        res.update(
            module_kw_hr_attendance_status=self.env['ir.config_parameter'].sudo().get_param(
                'kw_hr_attendance.module_kw_hr_attendance_status'),
            late_entry_screen_enable=self.env['ir.config_parameter'].sudo().get_param(
            'kw_hr_attendance.late_entry_screen_enable'),
        )
        return res

    @api.multi
    def set_values(self):
        super(ResConfigSettings, self).set_values()
        param = self.env['ir.config_parameter'].sudo()
        field1 = self.module_kw_hr_attendance_status or False
        field2 = self.late_entry_screen_enable or False
        param.set_param('kw_hr_attendance.module_kw_hr_attendance_status', field1)
        param.set_param('kw_hr_attendance.late_entry_screen_enable',field2)
