# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class ResourceCalendar(models.Model):
    
    _inherit                = 'resource.calendar'    

    # branch_id               = fields.Many2one(string ='Branch ', comodel_name='kw_res_branch', ondelete='restrict')
    employee_id             = fields.Many2one('hr.employee', string="Employee",)

    first_half_hour_from    = fields.Float(string='First Half Start Time')
    first_half_hour_to      = fields.Float(string='First Half End Time')
    rest_hour               = fields.Float(string='Rest Time')
    second_half_hour_from   = fields.Float(string='Second Half Start Time')
    second_half_hour_to     = fields.Float(string='Second Half End Time')    

    attendance_ids          = fields.One2many(
        'resource.calendar.attendance', 'calendar_id', 'Working Time',
        copy=True, default=False)


    @api.onchange('employee_id')
    def get_resource_calendar_details(self):

        if self.employee_id:

            self.name                   = f"Flexi Timing for {self.employee_id.name}"
            if self.employee_id.resource_calendar_id:
                employeee_shift         = self.employee_id.resource_calendar_id
                self.hours_per_day      = employeee_shift.hours_per_day
                self.branch_id          = employeee_shift.branch_id
                self.tz                 = employeee_shift.tz
                self.late_exit_time     = employeee_shift.late_exit_time
                self.late_entry_time    = employeee_shift.late_entry_time
                self.early_entry_time   = employeee_shift.early_entry_time
                self.extra_late_exit_time = employeee_shift.extra_late_exit_time

                self.grace_time         = employeee_shift.grace_time
                self.cross_shift        = employeee_shift.cross_shift

                self.late_entry_half_leave_time = employeee_shift.late_entry_half_leave_time
                self.late_entry_full_leave_time = employeee_shift.late_entry_full_leave_time
                self.early_exit_half_leave_time = employeee_shift.early_exit_half_leave_time

                self.attendance_ids     = False
                # attendance_ids          = employeee_shift.attendance_ids.filtered(lambda rec: rec.date_from == False and rec.date_to == False)

                # attendance_list    = []
                # for attendance in attendance_ids: 

                #     attendance_list.append((0, 0, {'name':attendance.name, 'dayofweek':attendance.dayofweek, 'hour_from': attendance.hour_from, 'hour_to': attendance.hour_to, 'rest_time': attendance.rest_time, 'day_period': attendance.day_period}))

                # if attendance_list:
                #     self.attendance_ids = False
                #     self.attendance_ids = attendance_list


    @api.constrains('employee_id')
    def check_duplicate(self):
        for record in self:
           
            record.check_duplicate_employee()
            if record.employee_id and record.attendance_ids:
                for attendance in record.attendance_ids:
                    if not (attendance.date_from or attendance.date_to):
                        raise ValidationError(f"Please specify both starting date and end date for {attendance.name}. Duration is mandatory for Flexi Time Management")
    @api.multi                    
    def check_duplicate_employee(self):        
        duplicate_rec = self.env['resource.calendar'].search_count([('employee_id', '!=',False),('employee_id', '=', self.employee_id.id), ('id', '!=', self.id)])
       
        if duplicate_rec:
            raise ValidationError(f"Flexi Timing has already been assigned for {self.employee_id.name}")
    
    ##action to assign flexi time 
    def action_assign_flexi(self):

        if not self.employee_id:
            raise ValidationError("Please select employee.")
        else:
            self.check_duplicate_employee()

        if not any([self.start_date,self.end_date,self.first_half_hour_from,self.first_half_hour_to,self.second_half_hour_from,self.second_half_hour_to]):
            raise ValidationError("Please enter all the fields required to assign flexi hours (Start Date, End Date, Working Hour Details).")
        if self.end_date < self.start_date:
            raise ValidationError("End date should not be less than start date.")

        if self.first_half_hour_from >= self.first_half_hour_to :
            raise ValidationError("First half start time should not be greater than end time.")

        if self.second_half_hour_from >= self.second_half_hour_to :
            raise ValidationError("Second half start time should not be greater than end time.") 

        if not self.cross_shift and (self.second_half_hour_from <= self.first_half_hour_to or self.second_half_hour_from <= self.first_half_hour_from):
            raise ValidationError("Second half should start after first half time.")        


        flexi_working_hours  = []
        resource_rec         = self.env['resource.calendar'].browse(self.id)
        # print(resource_rec)
        # print(self.id)
        week_days            = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday','Sunday']
        for week_day in range(7):

            flexi_working_hours.append((0, 0, {'name': week_days[week_day]+' First Half', 'dayofweek': str(week_day), 'hour_from':self.first_half_hour_from, 'hour_to': self.first_half_hour_to, 'day_period': 'morning','date_from':self.start_date,'date_to':self.end_date}))

            flexi_working_hours.append((0, 0, {'name': week_days[week_day]+' Second Half', 'dayofweek': str(week_day), 'hour_from':self.second_half_hour_from, 'hour_to': self.second_half_hour_to, 'day_period': 'afternoon','date_from':self.start_date,'date_to':self.end_date}))

        # print(flexi_working_hours)
        
            
        if flexi_working_hours:
            self.attendance_ids = flexi_working_hours

        return 

