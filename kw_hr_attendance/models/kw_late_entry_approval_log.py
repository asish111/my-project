# -*- coding: utf-8 -*-

from odoo import models, fields, api

class LateEntryApprovalLog(models.Model):
    _name           = "kw_late_entry_approval_log"
    _description    = "Late Entry Approval Log"
    #_order          = "check_in desc"
    _rec_name       = 'action_taken_by'

   
    daily_attendance_id = fields.Many2one('kw_daily_employee_attendance', string="Daily Attendance Id",ondelete='cascade', required=True)
    forward_to          = fields.Many2one('hr.employee', string="Forwarded To",ondelete='cascade')
    

    authority_remark    = fields.Text(string='Remark')
    action_taken_by     = fields.Many2one('hr.employee', string="Action Taken By",ondelete='cascade')  
    state               = fields.Selection(string="Action Status" ,selection=[('3','Forward'),('1', 'LateWPC'),('2', 'LateWOPC')]) ##LateWOPC  LateWPC
