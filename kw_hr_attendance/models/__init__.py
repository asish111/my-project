# -*- coding: utf-8 -*-


from . import resource_calendar
from . import hr_employee
from . import kw_daily_employee_attendance
from . import kw_late_entry_approval_log
from . import kw_hr_attendance_log ##stores all log of check-in and check out

from . import kw_roaster_group_config
from . import kw_employee_roaster_shift

from . import hr_attendance
from . import kw_attendance_mode_master
from . import kw_flexi_timing_manage
#from . import resource_calendar_attendance
from . import resource_calendar_leaves
from . import kw_res_branch

from .import kw_employee_apply_attendance
from . import res_config_settings
# from . import kw_employee_manual_attendance

from . import kw_bio_attendance_log
from . import kw_device_master

from . import kw_attendance_grace_time_log
