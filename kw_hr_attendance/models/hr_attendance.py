# -*- coding: utf-8 -*-

from odoo import models, fields, api

class HrAttendance(models.Model):
    _inherit        = "hr.attendance"
    _description    = "Attendance"
    
    check_in_mode   = fields.Integer(string='Log-in Mode',default=0) ##0- web, 1- bio, 2- manual
    check_out_mode  = fields.Integer(string='Log-out Mode',default=0) 

    check_in_mode_display   = fields.Char(string="Mode",compute="_compute_mode_display")
    
    @api.constrains('check_in', 'check_out', 'employee_id')
    def _check_validity(self):
        
        """ If this is an automatic checkout the constraint is invalid
        as there may be old attendances not closed
        """
       
        return True 


    @api.depends('check_in_mode')
    def _compute_mode_display(self):
        modes = ['Portal','Bio-metric','Manual']
        for rec in self:            
            rec.check_in_mode_display = modes[rec.check_in_mode] if rec.check_in_mode>=0 else ''

    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """
    
        result = super(HrAttendance, self).create(values)

        # self.env['kw_daily_employee_attendance'].create_daily_attendance(result)
        for attendance_rec in result:
            self.env['kw_daily_employee_attendance'].create_daily_attendance(attendance_rec)
    
        return result
    
    
    @api.multi
    def write(self, values):
        """
            Update all record(s) in recordset, with new value comes as {values}
            return True on success, False otherwise
    
            @param values: dict of new values to be set
    
            @return: True on success, False otherwise
        """
    
        result = super(HrAttendance, self).write(values)        
        # self.env['kw_daily_employee_attendance'].create_daily_attendance(self)

        for attendance_rec in self:
            self.env['kw_daily_employee_attendance'].create_daily_attendance(attendance_rec)
    
        return result
    