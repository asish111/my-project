# -*- coding: utf-8 -*-
import base64
from datetime import date, datetime, timezone, timedelta
from odoo.addons.kw_utility_tools import kw_helpers

from odoo.http import request
from odoo import models, fields, api

from odoo.addons.kw_hr_attendance.models.kw_daily_employee_attendance import IN_STATUS_LE,IN_STATUS_EXTRA_LE,IN_STATUS_LE_HALF_DAY,IN_STATUS_LE_FULL_DAY,ATD_MODE_BIO,ATD_MODE_PORTAL

class HrAttendanceLog(models.Model):
    _name           = "kw_hr_attendance_log"
    _description    = "Daily Attendance Log"
    _order          = "check_in desc"
    _rec_name       = 'employee_id'


    def _default_employee(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1)

    employee_id     = fields.Many2one('hr.employee', string="Employee", default=_default_employee, required=True, ondelete='cascade', index=True)
    
    check_in        = fields.Datetime(string="Check In", default=fields.Datetime.now, required=True)
    check_out       = fields.Datetime(string="Check Out")
    
    check_in_mode   = fields.Integer(string='Log-in Mode',default=0) ##0- web, 1- bio, 2- manual
    check_in_mode_name = fields.Char(string="Log-In Mode",compute="compute_check_in_mode_name")
    check_out_mode  = fields.Integer(string='Log-out Mode',default=0) 
    check_out_mode_name = fields.Char(string="Log-Out Mode",compute="compute_check_out_mode_name")

    in_ip_address   = fields.Char(string="Client IP In Mode")
    out_ip_address  = fields.Char(string="Client IP Out Mode")

    @api.model
    def compute_check_in_mode_name(self):
        modes = ['Portal','Bio-metric','Manual']
        for record in self:
            record.check_in_mode_name = modes[record.check_in_mode] if record.check_in and record.check_in_mode>=0 else ''
                            
    @api.model
    def compute_check_out_mode_name(self):
        modes = ['Portal','Bio-metric','Manual']
        for record in self:                
            record.check_out_mode_name = modes[record.check_out_mode] if record.check_out and record.check_out_mode>=0 else ''
           

    @api.model
    ##portal log-in
    def employee_check_in(self, employee_id):
        # print("attendance log",employee_id)
        check_in_utc = datetime.now()
        self.env['kw_hr_attendance_log'].create({'employee_id': employee_id, 
            'check_in': check_in_utc, 'in_ip_address': request.httprequest.remote_addr})
    
    @api.model
    ##portal log-out
    def employee_check_out(self, user_id):
        check_out_utc        = datetime.now()
        user = self.env['res.users'].sudo().browse(user_id)
        latest_log_rec          = self.env['kw_hr_attendance_log'].sudo(user.id).search([('employee_id', '=', user.employee_ids.id),('check_out','=',False),('check_in_mode','=',0)], limit=1)
        if latest_log_rec and  latest_log_rec.check_in and not latest_log_rec.check_out :
            latest_log_rec.sudo(user.id).write({'check_out':check_out_utc,'out_ip_address':request.httprequest.remote_addr}) 

    @api.model
    def show_late_entry_reason(self, employee_id):
        url = False
        check_in_utc        = datetime.now()
        curr_date           = check_in_utc.date()
        prev_date           = check_in_utc.date() - timedelta(days=1)
        kw_attendance_log   = self.env['kw_daily_employee_attendance'].search([('employee_id', '=', employee_id),], limit=1)
        
        if ((kw_attendance_log.is_cross_shift and kw_attendance_log.attendance_recorded_date in [curr_date,prev_date]) or (not kw_attendance_log.is_cross_shift and kw_attendance_log.attendance_recorded_date == curr_date)) and kw_attendance_log.check_in_status in [IN_STATUS_LE,IN_STATUS_EXTRA_LE,IN_STATUS_LE_HALF_DAY,IN_STATUS_LE_FULL_DAY] and kw_attendance_log.le_state in ['0',False]:
            if kw_attendance_log.late_entry_reason == False:
                # print("Yes")
                raw_string  = """{0}##{1}##{2}""".format(curr_date,kw_attendance_log.id,datetime.now())
                # raw_string  = bytes(raw_string, 'utf-8') 
                # print(raw_string)
                enc_string  = kw_helpers.encrypt_msg(raw_string)
                enc_uid     = base64.b64encode(enc_string).decode('utf8')
                # print(enc_uid) #
                url = f'/late_entry/{enc_uid}'
        return url   
    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """
    
        result = super(HrAttendanceLog, self).create(values)

        result.createOdooAttendance('check_in')    
        return result

    
    @api.multi
    def write(self, values):
        """
            Update all record(s) in recordset, with new value comes as {values}
            return True on success, False otherwise
    
            @param values: dict of new values to be set
    
            @return: True on success, False otherwise
        """
    
        result = super(HrAttendanceLog, self).write(values)
        self.createOdooAttendance('check_out')    
        return result
    

    @api.multi
    def createOdooAttendance(self,mode=False):

        hr_attendance          = self.env['hr.attendance']  

        bio_attendance_mode    = self.env.ref('kw_hr_attendance.kw_attendance_mode_bio').alias or ATD_MODE_BIO
        portal_attendance_mode = self.env.ref('kw_hr_attendance.kw_attendance_mode_portal').alias or ATD_MODE_PORTAL

        for log_rec in self:
            
            all_attendance_modes   = log_rec.employee_id.attendance_mode_ids.mapped('alias')

            # has_only_bio_mode      = True if bio_attendance_mode in all_attendance_modes and len(all_attendance_modes) ==1 else False
            # ##if the attendance mode is bio-metric only 
            # if has_only_bio_mode and log_rec.check_in_mode !=1:
            #     continue
            # else:

            if (log_rec.check_in_mode == 0 and portal_attendance_mode in all_attendance_modes) or (log_rec.check_in_mode == 1 and bio_attendance_mode in all_attendance_modes):

                
                daily_attendance_latest      = hr_attendance.search([('employee_id', '=',log_rec.employee_id.id),('check_in', '>=',log_rec.check_in.date()),('check_in', '<=',log_rec.check_in.date())], limit=1)      #             
                
                # updation_status     = 0
                # if not last_attedance or (last_attedance and last_attedance.check_in.date() != check_in_utc.date()):
                if mode== 'check_in' and (not daily_attendance_latest or (daily_attendance_latest and daily_attendance_latest.check_out)):
                    daily_attendance_latest      = hr_attendance.create({'employee_id':log_rec.employee_id.id,'check_in':log_rec.check_in,'check_in_mode':log_rec.check_in_mode,'check_out':log_rec.check_out,'check_out_mode':log_rec.check_out_mode})
                    # updation_status     = 1
                    #print('------------------')
                elif daily_attendance_latest and not daily_attendance_latest.check_out :

                    check_out        = log_rec.check_in if not log_rec.check_out else log_rec.check_out
                    check_out_mode   = log_rec.check_in_mode if not log_rec.check_out else log_rec.check_out_mode

                    if daily_attendance_latest.check_in < check_out:
                        daily_attendance_latest.write({'check_out':check_out,'check_out_mode':check_out_mode})


