# -*- coding: utf-8 -*-
from datetime import date, datetime
from odoo import models, fields, api
from odoo.exceptions import ValidationError


from odoo.addons.kw_hr_attendance.models.kw_daily_employee_attendance import DAY_STATUS_RWORKING,DAY_STATUS_RHOLIDAY

class EmployeeRoasterShift(models.Model):
    _name           = 'kw_employee_roaster_shift'
    _description    = 'Employee Roaster Shift'
    _rec_name       = "employee_id"
    _order = 'date asc'

    employee_id     = fields.Many2one('hr.employee',string="Employee Name",required=True)
    date            = fields.Date(string="Date",required=True,autocomplete="off",index=True)
    shift_id        = fields.Many2one('resource.calendar',string="Shift",required=True)
    week_off_status = fields.Boolean(string="Week off")

    @api.constrains('employee_id','date','shift_id','week_off_status')
    def check_redundancy(self):
        for roaster in self: 
            if roaster.employee_id and roaster.date:
                # print(self.env['kw_daily_employee_attendance']._get_employee_shift(roaster.employee_id,roaster.date))
                
                if roaster.date.month < (datetime.now().month-1):
                    raise ValidationError("You can not assign/modify roaster shift to more than 1 month older record(s).")

                ers_record = self.env['kw_employee_roaster_shift'].search([('employee_id', '=', roaster.employee_id.id), ('date', '=', roaster.date)]) - roaster
                if ers_record:
                    raise ValidationError("Shift has been already assigned on selected dates.\n Please change the From Date and To Date.")


    

    ##update the daily attendance table status as per the week off state , in case of backward update/create        
    @api.multi
    def change_attendance_status(self):
        """
            update the daily attendance table status as per the week off state , in case of backward update/create  
        """
        for roaster_shift in self:
            if roaster_shift.date <= datetime.now().date():

                attendance_rec     = self.env['kw_daily_employee_attendance'].search([('employee_id', '=', roaster_shift.employee_id.id),('attendance_recorded_date', '=', roaster_shift.date)])

                roaster_day_status = DAY_STATUS_RHOLIDAY if roaster_shift.week_off_status else DAY_STATUS_RWORKING

                ##if attendnace record is there and shift differs or day status differs then
                if attendance_rec and (roaster_shift.shift_id != attendance_rec.shift_id or attendance_rec.day_status !=roaster_day_status):
                    
                    shift_info     = self.env['kw_daily_employee_attendance']._get_employee_shift(roaster_shift.employee_id,roaster_shift.date)

                    attendance_rec.write({'shift_id':shift_info[3].id,'shift_name':shift_info[3].name,'is_cross_shift':shift_info[3].cross_shift,'shift_in_time':shift_info[4],'shift_out_time':shift_info[5],'day_status':roaster_day_status})
                    ##update the log
                    body        = ("The attendance record of <b>%s</b> for <b>%s</b> has been updated as per the roaster  shift assignment. <b>Action Taken By :</b> %s")%(roaster_shift.employee_id.name,roaster_shift.date,self.env.user.employee_ids.name)        
                    attendance_rec.message_post(body=body)      
     
    @api.model
    def create(self, values):
        """
            Create a new record for a model ModelName
            @param values: provides a data for new record
    
            @return: returns a id of new record
        """
    
        result = super(EmployeeRoasterShift, self).create(values)

        ##update the attendance status
        result.change_attendance_status()
        return result

     
    @api.multi
    def write(self, values):
        """
            Update all record(s) in recordset, with new value comes as {values}
            return True on success, False otherwise
    
            @param values: dict of new values to be set
    
            @return: True on success, False otherwise
        """
    
        result = super(EmployeeRoasterShift, self).write(values)

        ##update the attendance status
        self.change_attendance_status()

        return result
    
    

                   

