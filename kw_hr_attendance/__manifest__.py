# -*- coding: utf-8 -*-
{
    'name'      : "Kwantify Employee Attendance ",

    'summary'   : """
        Track employee attendance""",

    'description': """
        Track employee attendance details
    """,

    'author'    : "CSM Technologies",
    'website'   : "http://www.yourcompany.com",

    'category'  : 'Kwantify/HR+',
    'version'   : '0.1',

    # any module necessary for this one to work correctly
    'depends'   : ['base','hr_attendance','hr_calendar_rest_time','kw_branch_master','kw_utility_tools','kw_hr_holidays_public'],

    # always loaded
    'data'      : [
        'security/kw_hr_attendance_security.xml',
        'security/ir.model.access.csv',
        # 'data/attendance_mode_data.xml',
        'views/kw_holiday_client_action.xml',
        'views/resource_calendar_views.xml',
        'views/hr_employee_view.xml',
        'views/kw_attendance_asset_back_end.xml',
        'views/kw_roaster_group_config.xml',
        'wizard/views/kw_shift_assignment.xml',
        'wizard/views/kw_attendance_mode.xml',
        'wizard/views/kw_roaster_manage.xml',
        'views/kw_employee_roaster_shift.xml',
        'views/kw_attendance_mode_master.xml',
        'views/kw_flexi_timing_manage.xml',
        'views/qweb/kw_late_entry.xml',
        'views/kw_res_branch.xml',
        # 'wizard/kw_fixed_holiday_assignment.xml',
        'views/kw_late_entry_approval_log.xml',
        'wizard/views/kw_late_entry_approval_wizard.xml',
        'views/late_entry_view.xml',
        'views/kw_daily_employee_attendance.xml',
        'views/resource_calendar_leaves.xml',
        'views/res_config_settings_view.xml',
        # 'views/kw_employee_manual_attendance_request.xml',

        'wizard/views/kw_manual_attendance_hr_wizard.xml',
        
        'data/attendance_mode_data.xml',
        # 'wizard/views/kw_holiday_year_view.xml',
        
        'views/kw_lunch_client_action.xml',

        'wizard/views/kw_employee_attendance_approval_wizard.xml',
        'views/kw_employee_apply_attendance.xml',
        'wizard/views/kw_shift_holidays_assignment_wizard.xml',
        'views/kw_attendance_shift_views.xml',
        'wizard/views/kw_shift_weekoff_assign.xml',

        'report/kw_attendance_log.xml',
        'report/kw_employee_shift_assignment.xml',
        'report/kw_attendance_summary_report.xml',
        'report/kw_employee_attendance_summary_report.xml',
        'report/kw_dept_attendnace_summary_report.xml',
        'report/kw_late_attendance_summary_report.xml',
        'report/kw_productive_summary_report.xml',
        'report/kw_effort_estimation_report.xml',
        'report/kw_attendance_shift_log.xml',

        'views/kw_bio_attendance_log.xml',
        'views/kw_device_master.xml',
        'views/kw_attendance_grace_time_log.xml',
        'views/kw_hr_attendance_menu.xml',

        'data/attendance_scheduler_data.xml',

    ],
    'qweb': ['static/src/xml/*.xml', ],
    'installable': True,
    'application': False,
}
