# -*- coding: utf-8 -*-

# from . import kw_fixed_holiday_assignment
from . import shift_assignment
from . import kw_attendance_mode
from . import kw_roaster_manage

from . import kw_late_entry_approval_wizard
from . import kw_manual_attendance_hr_wizard

from . import kw_employee_attendance_approval_wizard
from . import kw_shift_holidays_assignment_wizard
from . import kw_shift_weekoff_assign