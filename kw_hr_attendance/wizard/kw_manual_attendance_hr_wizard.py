# -*- coding: utf-8 -*-
import pytz
from datetime import datetime,timedelta

from odoo.addons.resource.models import resource

from odoo import models, fields, api
from odoo.exceptions import ValidationError
from odoo.addons.kw_hr_attendance.models.kw_daily_employee_attendance import DAY_STATUS_WORKING,DAY_STATUS_LEAVE,DAY_STATUS_HOLIDAY,DAY_STATUS_RWORKING,DAY_STATUS_RHOLIDAY ,ATD_MODE_MANUAL ,ATD_STATUS_ABSENT

class ManualHrAttendanceWizard(models.TransientModel):
    _name           = 'kw_manual_attendance_hr_wizard'
    _description    = 'HR Manual Attendance Wizard'
    _rec_name       = 'branch_id'


    from_date       = fields.Date(string="From Date",required=True,autocomplete="off")
    to_date         = fields.Date(string="To Date",required=True,autocomplete="off")
    branch_id       = fields.Many2one('kw_res_branch',string="Branch/SBU")
    employee_ids    = fields.Many2many('hr.employee',string="Employees",required=True)
    
    emp_attendance_ids    = fields.One2many(comodel_name = 'kw_manual_attendance_employee_info',inverse_name='manual_attendance_id',) #compute='_get_employee_attendance_ids',store=True,inverse='_set_employee_attendance_ids'
 


    @api.onchange('branch_id')
    def show_employee(self):
        
        manual_attendance_mode      = self.env.ref('kw_hr_attendance.kw_attendance_mode_manual').alias or ATD_MODE_MANUAL
        # print(manual_attendance_mode)

        self.employee_ids           = self.env['hr.employee'].search([('user_id.branch_id', '=', self.branch_id.id),('attendance_mode_ids.alias','in',[manual_attendance_mode])])

        return {'domain': {'employee_ids': ([('user_id.branch_id', '=', self.branch_id.id),('attendance_mode_ids.alias','in',[manual_attendance_mode])])}}

    @api.multi
    def generate_attendance(self):
        for record in self:
            if record.employee_ids and record.from_date and record.to_date:
                ##Start :: create dates for the selected date range
                all_dates   = self.env['kw_manual_attendance_dates'].search([])
                date_list   = []
                for day in range(int ((record.to_date - record.from_date).days)+1):

                    search_date = record.from_date + timedelta(day)
                    if not all_dates.filtered(lambda rec : rec.name == search_date):
                        date_list.append({'name':search_date})
                if date_list:
                    self.env['kw_manual_attendance_dates'].create(date_list)
                ##End :: create dates for the selected date range

                ##Start :: create employee attendnace for the selected date-range
                attendance_dates          = self.env['kw_manual_attendance_dates'].search([('name','>=',record.from_date),('name','<=',record.to_date)])
                record.emp_attendance_ids = False

                record.emp_attendance_ids = [[0, 0,{'employee_id': data.id,'present_status':True,'attendance_dates':attendance_dates.ids}] for data in record.employee_ids]

            else:
                raise ValidationError("Please enter all the required fields!\n (1) From Date\n (2) To Date\n (2) Employees")

           
    @api.multi        
    def create_manual_attendance(self):
        for rec in self:
            if rec.emp_attendance_ids:
                # emp_attendance_list = []
                for emp_attendance in rec.emp_attendance_ids:
                    ##Start :: check duplicate dates for the employees   
                    except_current = rec.emp_attendance_ids.filtered(lambda r:r.employee_id==emp_attendance.employee_id) - emp_attendance                    
                    duplicate       =  emp_attendance.attendance_dates.filtered(lambda record: record.id in except_current.mapped('attendance_dates').ids)
                    if duplicate:
                        raise ValidationError(f'Duplicate date for "{emp_attendance.employee_id.name}" is not allowed.')
                    ##End :: check duplicate dates for the employees       

                    ##Start :: create manual attendance for the selected employee, for each date
                    for attendance_date in emp_attendance.attendance_dates:
                        if emp_attendance.employee_id.resource_calendar_id:
                            self.create_employee_manual_attendance(emp_attendance.employee_id,attendance_date.name,emp_attendance.present_status) 
                        else:
                            raise ValidationError(f'There is no shift assigned for "{emp_attendance.employee_id.name}". \n Please assign shift to the employee(s) before proceeding for manual attendance')
                    
            else:               

                raise ValidationError("Please enter all the required fields!")

        self.env.user.notify_success(message='Manual attendance updated for the selected employees successfully.')
        # return new_record

        return {'type': 'ir.actions.act_window_close'}

    ##method to create employee manual attendance ## enter record except the holidays.
    def create_employee_manual_attendance(self,employee,attendance_date,present_status=True):

        daily_emp_attendance    = self.env['kw_daily_employee_attendance']
        shift_info              = daily_emp_attendance._compute_day_status(employee,attendance_date)

        # day_status,shift_rest_time,shift_id,shift_in_time,shift_out_time
        day_status              = shift_info[0]
        emp_shift               = shift_info[2] #ofc_hours.mapped('calendar_id')[0]
        
        shift_in_time           = shift_info[3]
        shift_out_time          = shift_info[4]

        emp_tz                  = employee.tz or emp_shift.tz or 'UTC'        

        if day_status in [DAY_STATUS_WORKING,DAY_STATUS_RWORKING] and present_status:            

            check_in_datetime   = datetime.combine(attendance_date, resource.float_to_time(shift_in_time))
            check_out_datetime  = datetime.combine(attendance_date, resource.float_to_time(shift_out_time))
            # print("----------------------------------------")
            # print(shift_in_time)
            in_datetime_emp_tz  = pytz.timezone(emp_tz).localize(check_in_datetime)
            out_datetime_emp_tz = pytz.timezone(emp_tz).localize(check_out_datetime)
            # print("emp time sone ---")
            
            # Convert to US/Pacific time zone
            utc_in_datetime     = in_datetime_emp_tz.astimezone(pytz.timezone('UTC'))
            utc_out_datetime    = out_datetime_emp_tz.astimezone(pytz.timezone('UTC'))

            attendance_data     = {'employee_id':employee.id,'shift_id':emp_shift.id,'shift_name':emp_shift.name,'is_cross_shift':emp_shift.cross_shift,'shift_in_time':shift_in_time,'shift_out_time':shift_out_time,'attendance_recorded_date':attendance_date,'check_in':utc_in_datetime,'check_in_mode':2,'check_out':utc_out_datetime,'check_out_mode':2,'tz':emp_tz}

        else:
            
            attendance_data         = {'employee_id':employee.id,'shift_id':emp_shift.id,'shift_name':emp_shift.name,'is_cross_shift':emp_shift.cross_shift,'shift_in_time':shift_in_time,'shift_out_time':shift_out_time,'attendance_recorded_date':attendance_date,'check_in':None,'check_out':None,'check_in_mode':2,'check_out_mode':2,'tz':emp_tz}

        day_rec                 = daily_emp_attendance.search([('employee_id','=',employee.id),('attendance_recorded_date','=',attendance_date)])
        # print(day_rec)
        body        = ("This record has been updated through manual attendance wizard.<br\> <b>Action Taken By :</b> %s")%(self.env.user.name)

        if day_rec:
            day_rec.write(attendance_data)
            day_rec.message_post(body=body) 
        else:
            # print("Shift nfo --")            
            daily_emp_attendance.create(attendance_data)   
            daily_emp_attendance.message_post(body=body) 


    ##method to insert each day record for every employee/ monthly -- Everyday scheduler
    def auto_attendance_status_update(self):

        daily_attendance      = self.env['kw_daily_employee_attendance']
        all_employees         = self.env['hr.employee'].search([])

        end_date              = datetime.today().date() 
        start_date            = end_date.replace(day=1)
        emp_data              = []
        for day in range(int ((end_date - start_date).days)+1):

            attendance_date   = start_date + timedelta(day)
            # print(attendance_date)
            day_all_employee_rec            = daily_attendance.search([('attendance_recorded_date','=',attendance_date)])
            
            for employee in all_employees:
                emp_day_rec                 = day_all_employee_rec.filtered(lambda r: r.employee_id == employee)

                if not emp_day_rec:
                    shift_info              = daily_attendance._get_employee_shift(employee,attendance_date)
                    # ofc_hours               = shift_info[1]  
                    emp_tz                  = employee.tz or employee.resource_calendar_id.tz or 'UTC'
                    day_shift               = shift_info[3]

                    emp_data.append({'employee_id':employee.id,'shift_id':day_shift.id,'shift_name':day_shift.name,'is_cross_shift':day_shift.cross_shift,'shift_in_time':shift_info[4],'shift_out_time':shift_info[5],'attendance_recorded_date':attendance_date,'tz':emp_tz,'state':ATD_STATUS_ABSENT})
        # print(emp_data)
        if emp_data:
            daily_attendance.create(emp_data)



####### as per the change request#######
class ManualHrAttendanceEmployeeInfo(models.TransientModel):
    _name           = 'kw_manual_attendance_employee_info'
    _description    = 'HR Manual Attendance Employee Info'

    manual_attendance_id    = fields.Many2one('kw_manual_attendance_hr_wizard',string="Attendance Wizard Id")
    attendance_dates        = fields.Many2many(comodel_name='kw_manual_attendance_dates',
        relation='kw_manual_attendance_dates_wizard_rel',
        column1='did',
        column2='eid',string="Dates")    
    employee_id             = fields.Many2one('hr.employee',string="Employees",required=True)     
    present_status          = fields.Boolean(string='Present',default=True)

    from_date       = fields.Date(string="From Date",related="manual_attendance_id.from_date")
    to_date         = fields.Date(string="To Date",related="manual_attendance_id.to_date")
    
  

class ManualAttendanceDates(models.TransientModel):
    _name           = 'kw_manual_attendance_dates'
    _description    = 'HR Manual Attendance Date Info'
    

    name            = fields.Date(string="Date",required=True)   
    color           = fields.Integer(string="Color",default=4)
    
    
    