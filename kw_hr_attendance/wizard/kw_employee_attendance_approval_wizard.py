# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class EmpAttendanceAprrovalWizard(models.TransientModel):
    _name        = 'kw_employee_attendance_approval_wizard'
    _description = 'Employee Attendance Approval Wizard'


    @api.model
    def default_get(self, fields):
        res             = super(EmpAttendanceAprrovalWizard, self).default_get(fields)
        active_ids      = self.env.context.get('active_ids', [])
        #print(active_ids)
        
        res.update({
              
                'attendance_request_ids': active_ids,
            })
        return res
    
    
    remark                  = fields.Text(string='Authority Remark',required=True)
    action_type             = fields.Char(string="Action Type")
    
    attendance_request_ids  = fields.Many2many(
        string='Application Request',
        comodel_name='kw_employee_apply_attendance',
        relation='kw_employee_attendance_approval_wizard_rel',
        column1='request_id',
        column2='approval_id',
    )    
    
    
    @api.multi
    def save_action_details(self):
        self.ensure_one()
        context         = dict(self._context or {})
        active_ids      = context.get('active_ids', [])
        # attendance_request_ids  = self.env['kw_daily_employee_attendance'].browse(active_ids)

        if self.action_type == 'cancel':
            state   = '6'      
        if self.action_type == 'reject':
            state   = '5'
        elif self.action_type == 'approve':
            state   = '3' #'4' if self.env.user.has_group('hr_attendance.group_hr_attendance_manager') else '3'
              
        
        self.attendance_request_ids.write({'authority_remark':self.remark,'action_taken_by':self.env.user.employee_ids.id,'state':state,'action_taken_on':datetime.now()})

        if self.action_type == 'approve' and state == '3':
            self.attendance_request_ids.create_daily_attendance()

            
        
        return {'type': 'ir.actions.act_window_close'}

    