from odoo import models, fields, api
from odoo.exceptions import ValidationError
from odoo.addons.kw_hr_attendance.models.kw_daily_employee_attendance import IN_STATUS_EARLY_ENTRY,IN_STATUS_ON_TIME

class LateEntryAprrovalWizard(models.TransientModel):
    _name        = 'kw_late_entry_approval_wizard'
    _description = 'Late Entry Approval Wizard'


    @api.model
    def default_get(self, fields):
        res             = super(LateEntryAprrovalWizard, self).default_get(fields)
        active_ids      = self.env.context.get('active_ids', [])
        #print(active_ids)
        if len(active_ids)==1:
            res.update({
                'daily_attendance_id': active_ids[0] if active_ids else False,
                'daily_attendance_ids': [],
            })
        else:
            res.update({
                'daily_attendance_id': False,
                'daily_attendance_ids': active_ids,
            })
        return res
    
    daily_attendance_id     = fields.Many2one('kw_daily_employee_attendance',string="Employee",readonly=True)
    attendance_recorded_date= fields.Date(string="Date",related="daily_attendance_id.attendance_recorded_date")
    shift_in_time           = fields.Float(string="Office Time",related="daily_attendance_id.shift_in_time")
    check_in                = fields.Datetime(string="Office In",related="daily_attendance_id.check_in")
    check_out               = fields.Datetime(string="Office Out",related="daily_attendance_id.check_out")
    #in_time                 = fields.Char(string="In Time",compute="_compute_last_late_entries") #
    late_entry_reason       = fields.Text(string="Late Entry Reason",related="daily_attendance_id.late_entry_reason")

    daily_attendance_ids    = fields.Many2many('kw_daily_employee_attendance',string="Attendance Ids")

    #le_action               = fields.Selection(string="Late Entry Action Status" ,selection=[('3','Forward'),('1', 'LateWPC'), ('2', 'LateWOPC'),],required=True)
    remark                  = fields.Text(string='Authority Remark',required=True)
    forward_to              = fields.Many2one('hr.employee', string="Forwarded To",ondelete='cascade')
    
    forward_late_entry      = fields.Boolean(string="Forward Late Entry")
    
    last_le_ids             = fields.Many2many('kw_daily_employee_attendance',
        string='Last 7 late entry details',compute="_compute_last_late_entries"
    )
    

    @api.depends('daily_attendance_id')
    def _compute_last_late_entries(self):
        for rec in self:
            if rec.daily_attendance_id:
                rec.last_le_ids = self.env['kw_daily_employee_attendance'].search([('employee_id','=',rec.daily_attendance_id.employee_id.id),('attendance_recorded_date','<',rec.daily_attendance_id.attendance_recorded_date),('check_in_status','not in',[IN_STATUS_EARLY_ENTRY,IN_STATUS_ON_TIME,False])],limit=7).ids 

    ##late entry without paycut
    @api.multi
    def late_entry_wdout_paycut(self):
        return self.late_entry_take_action('LateWOPC') 

    ##late entry with paycut
    @api.multi
    def late_entry_wd_paycut(self):
        return self.late_entry_take_action('LateWPC')     

    ##late entry forward
    @api.multi
    def late_entry_forward(self):
        return self.late_entry_take_action('forward')     
        
    
    @api.multi
    def late_entry_take_action(self,action_type):
        self.ensure_one()
        context         = dict(self._context or {})
        active_ids      = context.get('active_ids', [])
        attendance_ids  = self.env['kw_daily_employee_attendance'].browse(active_ids)

        ##authenticate /validate the action user  
        # print(self.authority_status)
        all_authorities = attendance_ids.mapped('le_forward_to')
        
        if all_authorities:
            #distinct_authorities = set(all_authorities)
           
            if all_authorities not in self.env.user.employee_ids or (attendance_ids.filtered(lambda rec : rec.le_state not in ['1','3'])) or (attendance_ids.filtered(lambda rec : rec.check_in_status in [IN_STATUS_EARLY_ENTRY,IN_STATUS_ON_TIME])):
                raise ValidationError("You are not authorized to take action for the selected request !")
        else:
            raise ValidationError("You are not authorized to take action for the selected request !")

        state   = '1'
        if action_type == 'LateWOPC':
            state   = '2'
        elif action_type == 'forward':
            state   = '3'

        #create approval log and  update the attendace model  'forward'
        log_records     = []
        for attendance_id in active_ids:
            log_records.append({'daily_attendance_id':attendance_id,'forward_to':self.forward_to.id if self.forward_late_entry else False,'authority_remark':self.remark,'action_taken_by':self.env.user.employee_ids.id,'state':state})
        # print(log_records)
        if log_records:
            self.env['kw_late_entry_approval_log'].create(log_records) 
        
       
        update_res      = {'le_authority_remark':self.remark,'le_action_taken_by':self.env.user.employee_ids.id,'le_state':'3' if self.forward_late_entry else '2','le_action_status':False if self.forward_late_entry else state}

        #print(action_selection.get(self.le_action))
        body             = ("The late entry request has been approved with <b>%s</b> by <b>%s</b> , <br\> <b>Remark :</b> %s")%(action_type,self.env.user.employee_ids.name,self.remark)

        if action_type == 'forward':
            del update_res['le_action_status']
            update_res.update({'le_forward_to':self.forward_to.id,'le_forward_reason':self.remark,'le_authority_remark':''}) ##5 - forwarded
        
            body        = ("The late entry request has been forwarded to <b>%s</b> . <br\> <b>Remark :</b> %s <br\> <b>Action Taken By :</b> %s")%(self.forward_to.name,self.remark,self.env.user.employee_ids.name)
        
        attendance_ids.write(update_res)

        # Log the payment in the chatter
        for attendance_rec in attendance_ids:
            attendance_rec.message_post(body=body)       

        return {'type': 'ir.actions.act_window_close'}

    