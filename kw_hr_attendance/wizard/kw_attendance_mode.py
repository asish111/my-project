from odoo import models, fields, api
from odoo.exceptions import ValidationError

class AttendanceMode(models.TransientModel):
    _name = 'kw_attendance_mode_wizard'
    _description = 'Attendance Mode Wizard'

    branch_id = fields.Many2one('kw_res_branch',string="Branch/SBU")
    employee_id = fields.Many2many('hr.employee',string="Employee")
    no_attendance = fields.Boolean(string="No Attendance")
    attendance_mode = fields.Many2many('kw_attendance_mode_master', string="Attendance Mode")

    @api.onchange('branch_id')
    def show_employee(self):
        self.employee_id = False
        return {'domain': {'employee_id': ([('user_id.branch_id', '=', self.branch_id.id)])}}

    @api.model
    def create(self, vals):
        new_record = super(AttendanceMode, self).create(vals)
        if new_record.employee_id:
            if new_record.no_attendance:
                
                new_record.employee_id.write(
                    {'no_attendance': True, 'attendance_mode_ids': False})
                
            else:
                new_record.employee_id.write({'no_attendance': False,'attendance_mode_ids': [[6, False, new_record.attendance_mode.ids]]})
        self.env.user.notify_success(message='Employee Attendance Mode Assigned successfully.')
        return new_record
