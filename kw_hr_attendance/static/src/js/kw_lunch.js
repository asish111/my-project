odoo.define('hr_attendance.lunchIn_lunchOut', function (require) {
    "use strict";
    
    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    
    var QWeb = core.qweb;
    var _t = core._t;

    var lunchin_lunchout = AbstractAction.extend({
        events: {
            "click #lunch_in": _.debounce(function() {
                var status = 'lunch_in';
                this.update_myoffice_time(status);
            }, 200, true),

            "click #lunch_out": _.debounce(function() {
                var status = 'lunch_out';
                this.update_myoffice_time(status);
            }, 200, true),

            "click #check_out": _.debounce(function() {
                var status = 'check_out';
                this.update_myoffice_time(status);
            }, 200, true),
        },
        start: function () {
            var self = this;
            var def = this._rpc({
                    model: 'kw_daily_employee_attendance',
                    method: 'get_current_user_details',
                })
                .then(function (res) {
                    self.employee = res;
                    // console.log(res);
                    self.$el.html(QWeb.render("LunchInLunchOut", {widget: self}));
                });
    
            return $.when(def, this._super.apply(this, arguments));
        },
        update_myoffice_time: function(status){
            var parameter = {}
            var self = this;
            var currentdate = new Date();
            var datetime = currentdate.getUTCFullYear()+"-"+
                            ('0' + (currentdate.getUTCMonth()+1) ).slice( -2 )+"-"+
                            ('0' + (currentdate.getUTCDate()) ).slice( -2 )+" "+
                            ('0' + (currentdate.getUTCHours()) ).slice( -2 )+":"+
                            ('0' + (currentdate.getUTCMinutes()) ).slice( -2 )+":"+
                            ('0' + (currentdate.getUTCSeconds()) ).slice( -2 )+"."+
                            currentdate.getUTCMilliseconds();
            if (status == 'lunch_in'){
                parameter = {
                    'lunch_in_status':'lunchIN',
                    'lunch_in': datetime,
                };
            }
            if (status == 'lunch_out'){
                parameter = {
                    'lunch_out_status':'lunchOUT',
                    'lunch_out': datetime,
                };
            }
            if (status == 'check_out'){
                parameter = {
                    'check_out_status':'checkOUT',
                };
            }
            // console.log("out",parameter); 
            
            this._rpc({
                model: 'kw_daily_employee_attendance',
                method: 'get_lunch_timings',
                args: [parameter],
            }).then(function(res){
                self.lunchIn = res;
                self.start();
            });
        },
    });

    core.action_registry.add('lunch_in_lunch_out', lunchin_lunchout);
    return lunchin_lunchout;
});