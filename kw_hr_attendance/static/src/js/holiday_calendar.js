odoo.define('kw_hr_attendance.holiday_calendar', function(require) {
    "use strict";

    var core = require('web.core');
    var concurrency = require('web.concurrency');

    var AbstractAction = require('web.AbstractAction');
    var QWeb = core.qweb;

    var HolidayCalendarView = AbstractAction.extend({
        // template: "kw_hr_attendance.holiday_calendar",
        // jsLibs: [
        //     '/kw_hr_attendance/static/src/js/year-calendar.js',
        // ],
        events: {
            "change #sel_branch_master": "_onBranchChange",
            "click #btnSearchHoliday": "_onSearchbuttonClick",
            "click #btnSearchMyHoliday": "_onMyHolidaybuttonClick",
        },
        init: function(parent, value) {
            this._super(parent, value);
            // var data = [];
            var self = this;
            var context = {}
            if ('context' in value){
                context = value['context']
            }
            this.dm = new concurrency.DropMisordered();
            this.calendar = ''
            if (value.tag == 'kw_hr_attendance.holiday_calendar') {
                var branch_id = 0
                var shift_id = 0
                if('branch_id' in context) {
                    branch_id = parseInt(context['branch_id']);
                 }
                 if('shift_id' in context) {
                    shift_id = parseInt(context['shift_id']);
                 }

                var personal_calendar = 0 
                if (branch_id ==0 & shift_id==0){
                    personal_calendar = 1 
                } 

                this._getCalendarMasterdata(branch_id, shift_id).then(function(result) {

                    self.data = result;
                    self.render();
                    console.log(self.data.default_branch)
                    self._create_calender(self.data.default_branch,self.data.default_shift,personal_calendar,0);
                });

            }

        },
        // willStart: function () {

        //     return $.when(ajax.loadLibs(this), this._super());            
        // },

        render: function() {
            var super_render = this._super;
            var self = this;
            var kw_calendar = QWeb.render('kw_hr_attendance.holiday_calendar', {
                master_data: self,
            });
            $(kw_calendar).prependTo(self.$el);

            $('#sel_branch_master').val($('#sel_branch_master').attr('data-selected'));
            $('#sel_shift').val($('#sel_shift').attr('data-selected'));
            $('#sel_employee').val($('#sel_employee').attr('data-selected'));
            return kw_calendar;
        },
        _create_calender: function(branch_id, shift_id,personal_calendar,employee_id) {
            var currentYear = new Date().getFullYear();
            // var yearSave    = new Date().getFullYear();
            var calender = new Calendar('#holiday_calender', {
                style: 'custom',
                maxDate: new Date(currentYear, 11, 31),
                dataSource: [],
                customDataSourceRenderer: function(element, date, event) {
                    if(event[0].color == '#d83d2b') { $(element).css('color', '#FFF');}
                    $(element).css('background-color', event[0].color);
                    $(element).css('border-radius', '15px');
                },
                mouseOnDay: function(e) {
                    if (e.events.length > 0) {
                        var content = '';

                        for (var i in e.events) {
                            content += '<div class="event-tooltip-content">' +
                                '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                                // + '<div class="event-details">' + e.events[i].details + '</div>'
                                +
                                '</div>';
                        }
                        $(e.element).popover({
                            trigger: 'manual',
                            container: 'body',
                            html: true,
                            content: content
                        });

                        $(e.element).popover('show');

                    }
                },
                mouseOutDay: function(e) {
                    if (e.events.length > 0) {
                        $(e.element).popover('hide');
                    }
                },
                yearChanged: function(e) {

                    $('.holiday_year').hide()
                    $('.' + e.currentYear).show()
                        // $('#holiday_calender').data('calendar').setDataSource(dataSource);
                },

            });
            // console.log('---personla calendar')
            // console.log(personal_calendar)
            this._getHolidayData(branch_id, shift_id,personal_calendar,employee_id).then(function(result) {
                
                $('.holidays').html('')
                if (result) {
                    var holiday_div = '<ul>'
                    $.each(result, function(key, value) {
                        // console.log(value)
                        if (value.week_off != '1') {
                            // $('.justify-content-center').after("<b> Name: </b>" + value.name + " <b>Start Date:</b> " + value.startDate + " <b>End Date:</b> " +value.endDate + "</br>");
                            holiday_div += '<li class="' + value.year + ' holiday_year"><b>' + value.startDate + '</b> :' + value.name + '</li>'
                        }


                    });

                    holiday_div += '</ul>'
                        // $('.holidays').html(holiday_div)
                }
                calender.setDataSource(result)


            });

        },
        _getCalendarMasterdata: function(branch_id, shift_id) {

            return this._rpc({
                model: 'resource.calendar.leaves',
                method: 'get_calendar_master_data',
                args: [branch_id, shift_id],
            }, []).then(function(result) {
                return result;
            });
        },

        _getHolidayData: function(branch_id, shift_id,personal_calendar,employee_id) {
            var self = this;

            return this.dm.add(this._rpc({
                model: 'resource.calendar.leaves',
                method: 'get_calendar_global_leaves',
                args: [branch_id, shift_id,personal_calendar,employee_id],

            }, [])).then(function(result) {

                // console.log('sssssss')
                if (result.holiday_calendar) {
                    return result.holiday_calendar.map(r => ({
                        startDate: new Date(r.date_from),
                        endDate: new Date(r.date_to),
                        name: r.name,
                        week_off: r.week_off,
                        color: r.color,
                        year: r.year,
                    }));
                }
                return [];

                // self.holiday_data = data;
            });
        },
        _onSearchbuttonClick: function(event) {

            // event.preventDefault();

            var branch_id = $('#sel_branch_master').val();
            var shift_id = $('#sel_shift').val();
            // console.log(branch_id)

            if (typeof branch_id === 'undefined') {
                alert("Please select a branch ")
                return false
            } else if (typeof shift_id === 'undefined' || shift_id == null) {
                alert("Please select a shift")
                return false
            }

            this._create_calender(branch_id, shift_id,0,0)
        },
        _onMyHolidaybuttonClick:function(event){
            var employee_id = $('#sel_employee').val();
            if (typeof employee_id === 'undefined') {
                alert("Please select employee")
                return false
            } 

            this._create_calender(0,0,1,employee_id)
        },

        _onBranchChange: function(event) {

            // event.preventDefault();
            var self = this;
            var branch_id = $('#sel_branch_master').val();
            // var shift_id = $('#sel_shift').val();
            // console.log(branch_id)

            if (typeof branch_id === 'undefined') {
                alert("Please select a branch ")
                return False
            }
            this._getCalendarMasterdata(branch_id,0).then(function(result) {
                // console.log(result)
                self.data = result;

                var tabDiv = '';
                $(self.data.shift_master).each(function(j, shift_master) {

                    tabDiv += '<option value="' + shift_master.id + '" >';
                    tabDiv += shift_master.name;
                    tabDiv += '</option>';
                });
                // console.log(tabDiv)
                $("#sel_shift").html(tabDiv);

            });



        },
    });
    core.action_registry.add('kw_hr_attendance.holiday_calendar', HolidayCalendarView);
    return HolidayCalendarView;



});

$(document).ready(function() {

});