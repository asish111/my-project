# -*- coding: utf-8 -*-
from calendar import monthrange
from datetime import date 
from odoo import tools
from odoo import models, fields, api
from odoo.addons.kw_hr_attendance.models.kw_daily_employee_attendance import ATD_STATUS_PRESENT,ATD_STATUS_ABSENT,ATD_STATUS_FHALF_ABSENT,ATD_STATUS_SHALF_ABSENT,DAY_STATUS_WORKING,DAY_STATUS_RWORKING,IN_STATUS_EARLY_ENTRY,IN_STATUS_ON_TIME,ATD_STATUS_TOUR,ATD_STATUS_LEAVE,ATD_STATUS_FHALF_LEAVE,ATD_STATUS_SHALF_LEAVE

class EmployeeAttendanceSummary(models.Model):
    _name           = "kw_employee_attendance_summary_report"
    _description    = "Employee Attendance Summary Report"
    _auto           = False
    _rec_name       = 'employee_id'

   
    employee_id                 = fields.Many2one(string='Employee',comodel_name='hr.employee',) 
    department_id               = fields.Many2one(string='Department',comodel_name='hr.department') 
    attendance_year             = fields.Char(string="Attendance Year",)
    attendance_month            = fields.Char(string="Attendance Month")    
    month_number                = fields.Char(string="Month Index")
    working_days                = fields.Integer(string="Working Days")
    present_state               = fields.Integer(string='Present',)
    absent_state                = fields.Integer(string='Absent',)
    on_leave_state              = fields.Integer(string="On Leave")
    on_tour_state               = fields.Integer(string="On Tour")
    normal_entry_state          = fields.Integer(string='Normal Entry',)
    lwpc_state                  = fields.Integer(string='With Pay Cut',)
    lwopc_state                 = fields.Integer(string='With out Pay Cut',)
    reporting_authority         = fields.Many2one(string='Reporting Authority',comodel_name='hr.employee',) 
    present_child_ids           = fields.Many2many('kw_daily_employee_attendance',string="Child Ids",compute="get_no_of_present_days")
   

    @api.model
    def get_no_of_present_days(self):
        start_date,end_date = self.env['kw_late_attendance_summary_report']._get_month_range(self.attendance_year,self.month_number)
        daily_attendance_records = self.env['kw_daily_employee_attendance'].search([('employee_id','=',self.employee_id.id),('attendance_recorded_date','>=',start_date),('attendance_recorded_date','<=',end_date)],order="attendance_recorded_date asc",)
        # filtered_data = daily_attendance_records.filtered(lambda r: str(r.attendance_recorded_date.year) == self.attendance_year and r.attendance_recorded_date.strftime('%B') == self.attendance_month)
        self.present_child_ids = daily_attendance_records.ids
    
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(f""" CREATE or REPLACE VIEW %s as (
            with attendance as
            (
                select employee_id,
                to_char(attendance_recorded_date,'MM') as month_number,
                Cast(TRIM(to_char(attendance_recorded_date,'Month')) as varchar) as attendance_month,
                cast(extract(year from attendance_recorded_date) as varchar) as attendance_year,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') then 1 else 0 end) as present_state,
                sum(case when state in ('{ATD_STATUS_LEAVE}','{ATD_STATUS_FHALF_LEAVE}','{ATD_STATUS_SHALF_LEAVE}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') then 1 else 0 end) as on_leave_state,
                sum(case when state = '{ATD_STATUS_TOUR}' then 1 else 0 end) as on_tour_state,
                sum(case when day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') then 1 else 0 end) as working_days,
                sum(case when state = '{ATD_STATUS_ABSENT}' and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') then 1 else 0 end) as absent_state,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') and (check_in_status ='{IN_STATUS_EARLY_ENTRY}' or check_in_status = '{IN_STATUS_ON_TIME}') then 1 else 0 end) as normal_entry_state,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') and le_action_status ='1' then 1 else 0 end) as lwpc_state,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') and le_action_status ='2' then 1 else 0 end) as lwopc_state
                from kw_daily_employee_attendance
                group by employee_id, extract(year from attendance_recorded_date), to_char(attendance_recorded_date,'Month'),  to_char(attendance_recorded_date,'MM')
            )
            select ROW_NUMBER () OVER (ORDER BY e.id) as id, e.id as employee_id, e.department_id,e.parent_id as reporting_authority, month_number ,coalesce(present_state,0) as present_state, working_days, normal_entry_state , lwpc_state , on_leave_state, on_tour_state,lwopc_state, absent_state,attendance_year, attendance_month from hr_employee e
            left join attendance a on a.employee_id = e.id
            WHERE e.department_id is not null order by e.name
        )""" % (self._table))
