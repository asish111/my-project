from calendar import monthrange
from datetime import date 
from odoo import tools
from odoo import models, fields, api
from odoo.addons.kw_hr_attendance.models.kw_daily_employee_attendance import ATD_STATUS_PRESENT,ATD_STATUS_ABSENT,ATD_STATUS_FHALF_ABSENT,DAY_STATUS_WORKING,DAY_STATUS_RWORKING,ATD_STATUS_SHALF_ABSENT,IN_STATUS_LE,IN_STATUS_EXTRA_LE,IN_STATUS_LE_HALF_DAY,IN_STATUS_LE_FULL_DAY

class EmployeeLateAttendanceSummary(models.Model):
    _name           = "kw_late_attendance_summary_report"
    _description    = "Employee Late Attendance Summary Report"
    _auto           = False
    _rec_name       = 'employee_id'

    employee_id                 = fields.Many2one(string='Employee',comodel_name='hr.employee',) 
    department_id               = fields.Many2one(string='Department',comodel_name='hr.department') 
    attendance_year             = fields.Char(string="Attendance Year",)
    attendance_month            = fields.Char(string="Attendance Month")    
    month_number                = fields.Char(string="Month Index")
    working_days                = fields.Integer(string="Working Days")
    total_late_entries          = fields.Integer(string="Total Late Entry")
    lwpc_state                  = fields.Integer(string="Late With Pay Cut")
    lwopc_state                 = fields.Integer(string="Late Without Pay Cut")
    pending_at_ra               = fields.Integer(string="Pending At RA")
    reporting_authority         = fields.Many2one(string='Reporting Authority',comodel_name='hr.employee',) 
    total_late_entries_ids      = fields.Many2many('kw_daily_employee_attendance',compute="get_total_late_entries")
    lwpc_state_ids              = fields.Many2many('kw_daily_employee_attendance',compute="get_lwpc_state_ids")
    lwopc_state_ids             = fields.Many2many('kw_daily_employee_attendance',compute="get_lwopc_state_ids")
    pending_at_ra_ids           = fields.Many2many('kw_daily_employee_attendance',compute="get_pending_at_ra_ids")

    # @api.model
    def _get_month_range(self,attendance_year=False,month_number=False):
        attendance_year = attendance_year or self.attendance_year
        month_number = month_number or self.month_number 
        _, num_days = monthrange(int(attendance_year),int(month_number))
        start_date  = date(int(attendance_year), int(month_number), 1)
        end_date    = date(int(attendance_year),int(month_number), num_days)
        return start_date,end_date

    def get_total_late_entries(self):
        start_date,end_date = self._get_month_range()
        late_entries_records = self.env['kw_daily_employee_attendance'].search([('employee_id','=',self.employee_id.id),('state','in',[ATD_STATUS_PRESENT,ATD_STATUS_FHALF_ABSENT,ATD_STATUS_SHALF_ABSENT]),('day_status','in',[DAY_STATUS_WORKING,DAY_STATUS_RWORKING]),('check_in_status','in',[IN_STATUS_LE,IN_STATUS_EXTRA_LE,IN_STATUS_LE_FULL_DAY,IN_STATUS_LE_HALF_DAY]),('attendance_recorded_date','>=',start_date),('attendance_recorded_date','<=',end_date)],order="attendance_recorded_date asc",)
        self.total_late_entries_ids = late_entries_records.ids
        

    #@api.model
    def get_lwpc_state_ids(self):
        start_date,end_date = self._get_month_range()
        lwpc_state_records = self.env['kw_daily_employee_attendance'].search([('employee_id','=',self.employee_id.id),('state','in',[ATD_STATUS_PRESENT,ATD_STATUS_FHALF_ABSENT,ATD_STATUS_SHALF_ABSENT]),('day_status','in',[DAY_STATUS_WORKING,DAY_STATUS_RWORKING]),('le_action_status','=','1'),('attendance_recorded_date','>=',start_date),('attendance_recorded_date','<=',end_date)])
        self.lwpc_state_ids = lwpc_state_records.ids

    
    #@api.model
    def get_lwopc_state_ids(self):
        start_date,end_date = self._get_month_range()
        lwopc_state_records = self.env['kw_daily_employee_attendance'].search([('employee_id','=',self.employee_id.id),('state','in',[ATD_STATUS_PRESENT,ATD_STATUS_FHALF_ABSENT,ATD_STATUS_SHALF_ABSENT]),('day_status','in',[DAY_STATUS_WORKING,DAY_STATUS_RWORKING]),('le_action_status','=','2'),('attendance_recorded_date','>=',start_date),('attendance_recorded_date','<=',end_date)])
        self.lwopc_state_ids = lwopc_state_records.ids

    #@api.model
    def get_pending_at_ra_ids(self):

        start_date,end_date = self._get_month_range()
        pending_at_ra_records = self.env['kw_daily_employee_attendance'].search([('employee_id','=',self.employee_id.id),('state','=',ATD_STATUS_PRESENT),('day_status','in',[DAY_STATUS_WORKING,DAY_STATUS_RWORKING]),('le_state','=','1'),('attendance_recorded_date','>=',start_date),('attendance_recorded_date','<=',end_date)])        
        self.pending_at_ra_ids = pending_at_ra_records.ids


    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(f""" CREATE or REPLACE VIEW %s as (
            with attendance as
            (
                select employee_id,
                to_char(attendance_recorded_date,'MM') as month_number,
                Cast(TRIM(to_char(attendance_recorded_date,'Month')) as varchar) as attendance_month,
                cast(extract(year from attendance_recorded_date) as varchar) as attendance_year,
                sum(case when day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') then 1 else 0 end) as working_days,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') and check_in_status in ('{IN_STATUS_LE}','{IN_STATUS_EXTRA_LE}','{IN_STATUS_LE_HALF_DAY}','{IN_STATUS_LE_FULL_DAY}') then 1 else 0 end) as total_late_entries,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') and le_action_status ='1' then 1 else 0 end) as lwpc_state,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') and le_action_status ='2' then 1 else 0 end) as lwopc_state,
                sum(case when state in ('{ATD_STATUS_PRESENT}','{ATD_STATUS_FHALF_ABSENT}','{ATD_STATUS_SHALF_ABSENT}') and day_status in ('{DAY_STATUS_WORKING}','{DAY_STATUS_RWORKING}') and le_state = '1' then 1 else 0 end) as pending_at_ra
                from kw_daily_employee_attendance
                group by employee_id, extract(year from attendance_recorded_date), to_char(attendance_recorded_date,'Month'),  to_char(attendance_recorded_date,'MM')
                )
                select ROW_NUMBER () OVER (ORDER BY e.id) as id, e.id as employee_id, e.department_id, e.parent_id as reporting_authority,
                coalesce(total_late_entries,0) as total_late_entries, working_days, lwpc_state, lwopc_state , pending_at_ra, month_number, attendance_month, attendance_year
                from hr_employee e
                left join attendance a on a.employee_id = e.id
                WHERE e.department_id is not null order by e.name
            )""" % (self._table))

