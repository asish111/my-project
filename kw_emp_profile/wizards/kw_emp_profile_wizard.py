from odoo import api, models, fields, _
from odoo.exceptions import UserError



class ProfileWizard(models.TransientModel):
    _name = 'profile_wizard'
    _description = 'Creates Profile Wizard'

    profile_rel = fields.Many2one('kw_emp_profile')
    work_email_id = fields.Char(string="Work Email", size=100)
    outlook_pwd = fields.Char(string=u'Outlook Password', size=100)
    personal_email = fields.Char(string='Personal EMail Id ',)
    marital = fields.Many2one('kwemp_maritial_master',string='Marital Status')
    marital_code = fields.Char(string=u'Marital Status Code ')
    wedding_anniversary = fields.Date(string=u'Wedding Anniversary')
    emp_religion = fields.Many2one('kwemp_religion_master', string="Religion")
    mobile_phone = fields.Char(string="Mobile No", size=15)
    personal_email = fields.Char(string='Personal EMail Id ',)
    present_addr_street = fields.Text(string="Present Address Line 1", size=500)
    present_addr_country_id = fields.Many2one('res.country', string="Present Address Country")
    present_addr_city = fields.Char(string="Present Address City", size=100)
    present_addr_state_id = fields.Many2one('res.country.state', string="Present Address State")
    present_addr_zip = fields.Char(string="Present Address ZIP", size=10)
    same_address = fields.Boolean(string=u'Same as Present Address', default=False)
    permanent_addr_country_id = fields.Many2one('res.country', string="Permanent Address Country")
    permanent_addr_street = fields.Text(string="Permanent Address Line 1", size=500)
    permanent_addr_city = fields.Char(string="Permanent Address City",  size=100)
    permanent_addr_state_id = fields.Many2one('res.country.state', string="Permanent Address State")
    permanent_addr_zip = fields.Char(string="Permanent Address ZIP", size=10)
    emergency_contact_name = fields.Char(string='Name')
    emergency_address = fields.Text(string='Address')
    emergencye_telephone = fields.Char(string='Telephone(R)')
    emergency_city = fields.Char(string='City')
    emergency_country = fields.Many2one('res.country',string='Country')
    emergency_state = fields.Many2one('res.country.state',string='State')
    emergency_mobile_no = fields.Char(string='Mobile No')





    @api.model
    def default_get(self, fields):
        res = super(ProfileWizard, self).default_get(fields)
        uid = self._uid
        print(uid)
        emp = self.env['kw_emp_profile'].sudo().search([('user_id','=',uid)],limit=1)
        if emp:
            res.update({
                'outlook_pwd': emp.outlook_pwd,
                'work_email_id': emp.work_email_id,
                'profile_rel':emp.id,
               
            	})
        return res

        


   

   
         