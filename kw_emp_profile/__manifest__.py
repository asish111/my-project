# -*- coding: utf-8 -*-
{
    'name': "Kwantify Employee Profile",
    'summary': "Advance features of employee",
    'description': "Employee details module",
    'author': "CSM Tech",
    'website': "http://www.csm.co.in",

  
    'category': 'Kwantify',
    'version': '0.1',

    'depends': ['base', 'website','web','kw_skill_assessment','kw_employee','hr'],

    'data': [
        'data/get_employee.xml',
        'security/ir.model.access.csv',
        # 'views/template.xml',
        # 'wizards/kw_emp_profile_wizard_view.xml',
        'views/kw_emp_profile.xml',
        # 'views/kw_emp_approval_view.xml',
        'views/kw_emp_profile_new_data_view.xml',

        

        
    

    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
