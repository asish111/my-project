from odoo import models, fields, api


class hr_employee_in(models.Model):
    _inherit = 'hr.employee'

    @api.multi
    def write(self, vals):

        list_1 = []
        list_2 = []
        list_3 = []
        list_4 = []
        list_5 = []
        list_6 = []
        list_7 = []
        list_8 = []
        
        
       
        for rec in self.family_details_ids:
            list_1.append(rec.id)
        for language in self.known_language_ids:
            list_3.append(language.id)
        for work in self.work_experience_ids:
            list_4.append(work.id)
        for identification in self.identification_ids:
            list_5.append(identification.id)
        for education in self.educational_details_ids:
            list_6.append(education.id)
        for skill in self.technical_skill_ids:
            list_7.append(skill.id)
        for membership in self.membership_assc_ids:
            list_8.append(membership.id)
        
        
        
        emp_prpf_rec = self.env['kw_emp_profile'].sudo().search([('emp_id','=',self.id)])
        val = {}
    
        if 'family_details_ids' in vals:
            for record in vals['family_details_ids']:
                if record[0] == 2:
                    profile_data = self.env['kw_emp_profile_family_info'].search([('family_id','=',record[1])]).unlink()
           
            
        if 'known_language_ids' in vals:
            for record in vals['known_language_ids']:
                if record[0] == 2:
                    profile_data = self.env['kw_emp_profile_language_known'].search([('emp_language_id','=',record[1])]).unlink()
          
        if 'membership_assc_ids' in vals:
            for record in vals['membership_assc_ids']:
                if record[0] == 2:
                    profile_data = self.env['kw_emp_profile_membership_assc'].search([('emp_membership_id','=',record[1])]).unlink()

        if 'educational_details_ids' in vals:
            for record in vals['educational_details_ids']:
                if record[0] == 2:
                    profile_data = self.env['kw_emp_profile_qualification'].search([('emp_educational_id','=',record[1])]).unlink()
        if 'identification_ids' in vals:
            for record in vals['identification_ids']:
                if record[0] == 2:
                    profile_data = self.env['kw_emp_profile_identity_docs'].search([('emp_document_id','=',record[1])]).unlink()
        if 'technical_skill_ids' in vals:  
            for record in vals['technical_skill_ids']:
                if record[0] == 2:
                    profile_data = self.env['kw_emp_profile_technical_skills'].search([('emp_technical_id','=',record[1])]).unlink()
        if 'work_experience_ids' in vals:    
            for record in vals['work_experience_ids']:
                if record[0] == 2:
                    profile_data = self.env['kw_emp_profile_work_experience'].search([('emp_work_id','=',record[1])]).unlink()

            

            
        emp_rec = super(hr_employee_in, self).write(vals)
        
        if 'membership_assc_ids' in vals:
            for rec in self.membership_assc_ids:
                list_2.append(rec.id)

            for res in list_8:
                if res in list_2:
                    list_2.remove(res)


            for record in vals['membership_assc_ids']:
                if record[0] == 1:
                    values = {}

                    profile_data = self.env['kw_emp_profile_membership_assc'].sudo().search([('emp_membership_id','=',record[1])])
                    if 'name' in record[2]:
                        values.update({'name':record[2]['name']})
                    if 'date_of_issue' in record[2]:
                        values.update({'date_of_issue':record[2]['date_of_issuedate_of_issuedate_of_issue']})
                    if 'date_of_expiry' in record[2]:
                        values.update({'date_of_expiry':record[2]['date_of_expiry']})
                    if 'renewal_sts' in record[2]:
                        values.update({'renewal_sts':record[2]['renewal_sts']})
                    if 'uploaded_doc' in record[2]:
                        values.update({'uploaded_doc':record[2]['uploaded_doc']})
                    profile_data.update(values)
                if record[0] == 0:
                    profile_data = self.env['kw_emp_profile_membership_assc'].sudo().search([])
                    values = {
                        'uploaded_doc': record[2]['uploaded_doc'] if record[2]['uploaded_doc'] else False,
                        'renewal_sts': record[2]['renewal_sts'] if record[2]['renewal_sts'] else False,
                        'date_of_expiry': record[2]['date_of_expiry'] if record[2]['date_of_expiry'] else False,
                        'name': record[2]['name'] if record[2]['name'] else False,
                        'date_of_issue': record[2]['date_of_issue'] if record[2]['date_of_issue'] else False,
                        'emp_membership_id': list_2[0],
                        'emp_id': emp_prpf_rec.id if emp_prpf_rec.id else False,                
                    }
                    profile_data.create(values)
                    list_2.pop(0)


        if 'technical_skill_ids' in vals:
            for rec in self.technical_skill_ids:
                list_2.append(rec.id)

            for res in list_7:
                if res in list_2:
                    list_2.remove(res)
            
            
            for record in vals['technical_skill_ids']:
                if record[0] == 1:
                    values = {}

                    profile_data = self.env['kw_emp_profile_technical_skills'].sudo().search([('emp_technical_id','=',record[1])])
                    if 'category_id' in record[2]:
                        values.update({'category_id':record[2]['category_id']})
                    if 'skill_id' in record[2]:
                        values.update({'skill_id':record[2]['skill_id']})
                    if 'proficiency' in record[2]:
                        values.update({'proficiency':record[2]['proficiency']})

                    profile_data.update(values)

                if record[0] == 0:
                    profile_data = self.env['kw_emp_profile_technical_skills'].sudo().search([])
                    values = {
                        'category_id': record[2]['category_id'] if record[2]['category_id'] else False,
                        'skill_id': record[2]['skill_id'] if record[2]['skill_id'] else False,
                        'proficiency': record[2]['proficiency'] if record[2]['proficiency'] else False,
                        'emp_technical_id': list_2[0],
                        'emp_id': emp_prpf_rec.id if emp_prpf_rec.id else False,              
                    }
                    profile_data.create(values)
                    list_2.pop(0)

        if 'educational_details_ids' in vals:
            for rec in self.educational_details_ids:
                list_2.append(rec.id)

            for res in list_6:
                if res in list_2:
                    list_2.remove(r)
            
            
            for record in vals['educational_details_ids']:
                if record[0] == 1:
                    values = {}

                    profile_data = self.env['kw_emp_profile_qualification'].sudo().search([('emp_educational_id','=',record[1])])
                    if 'course_type' in record[2]:
                        values.update({'course_type':record[2]['course_type']})
                    if 'course_id' in record[2]:
                        values.update({'course_id':record[2]['course_id']})
                    if 'uploaded_doc' in record[2]:
                        values.update({'uploaded_doc':record[2]['uploaded_doc']})
                    if 'stream_id' in record[2]:
                        values.update({'stream_id':record[2]['stream_id']})
                    if 'university_name' in record[2]:
                        values.update({'university_name':record[2]['university_name']})
                    if 'passing_year' in record[2]:
                        values.update({'passing_year':record[2]['passing_year']})
                    if 'division' in record[2]:
                        values.update({'division':record[2]['division']})
                    if 'marks_obtained' in record[2]:
                        values.update({'marks_obtained':record[2]['marks_obtained']})
                    
                   
                    profile_data.update(values)
                if record[0] == 0:
                    profile_data = self.env['kw_emp_profile_qualification'].sudo().search([])
                    values = {
                        'marks_obtained': record[2]['marks_obtained'] if record[2]['marks_obtained'] else False,
                        'division': record[2]['division'] if record[2]['division'] else False,
                        'passing_year': record[2]['passing_year'] if record[2]['passing_year'] else False,
                        'university_name': record[2]['university_name'] if record[2]['university_name'] else False,
                        'stream_id': record[2]['stream_id'] if record[2]['stream_id'] else False,
                        'course_id': record[2]['course_id'] if record[2]['course_id'] else False,
                        'course_type': record[2]['course_type'] if record[2]['course_type'] else False,
                        'uploaded_doc': record[2]['uploaded_doc'] if record[2]['uploaded_doc'] else False,
                        'emp_educational_id': list_2[0],
                        'emp_id': emp_prpf_rec.id if emp_prpf_rec.id else False,                
                    }
                    profile_data.create(values)
                    list_2.pop(0)

        if 'identification_ids' in vals:
            for rec in self.identification_ids:
                list_2.append(rec.id)

            for r in list_5:
                if r in list_2:
                    list_2.remove(r)
            
            
            for record in vals['identification_ids']:
                if record[0] == 1:
                    values = {}

                    profile_data = self.env['kw_emp_profile_identity_docs'].sudo().search([('emp_document_id','=',record[1])])
                    if 'name' in record[2]:
                        values.update({'name':record[2]['name']})
                    if 'doc_number' in record[2]:
                        values.update({'doc_number':record[2]['doc_number']})
                    if 'uploaded_doc' in record[2]:
                        values.update({'uploaded_doc':record[2]['uploaded_doc']})
                    if 'date_of_issue' in record[2]:
                        values.update({'date_of_issue':record[2]['date_of_issue']})
                    if 'date_of_expiry' in record[2]:
                        values.update({'date_of_expiry':record[2]['date_of_expiry']})
                    if 'renewal_sts' in record[2]:
                        values.update({'renewal_sts':record[2]['renewal_sts']})
                   
                    profile_data.update(values)
                if record[0] == 0:
                    profile_data = self.env['kw_emp_profile_identity_docs'].sudo().search([])
                    values = {
                        'name': record[2]['name'] if record[2]['name'] else False,
                        'doc_number': record[2]['doc_number'] if record[2]['doc_number'] else False,
                        'date_of_issue': record[2]['date_of_issue'] if record[2]['date_of_issue'] else False,
                        'date_of_expiry': record[2]['date_of_expiry'] if record[2]['date_of_expiry'] else False,
                        'renewal_sts': record[2]['renewal_sts'] if record[2]['renewal_sts'] else False,
                        'uploaded_doc': record[2]['uploaded_doc'] if record[2]['uploaded_doc'] else False,
                        'emp_document_id': list_2[0],
                         'emp_id': emp_prpf_rec.id if emp_prpf_rec.id else False,                
                     }
                    profile_data.create(values)
                    list_2.pop(0)


        if 'work_experience_ids' in vals:
            for rec in self.work_experience_ids:
                list_2.append(rec.id)

            for r in list_4:
                if r in list_2:
                    list_2.remove(r)
            for record in vals['work_experience_ids']:
                if record[0] == 1:
                    values = {}
                    
                    profile_data = self.env['kw_emp_profile_work_experience'].sudo().search([('emp_work_id','=',record[1])])
                    if 'country_id' in record[2]:
                        values.update({'country_id':record[2]['country_id']})
                    if 'name' in record[2]:
                        values.update({'name':record[2]['name']})
                    if 'designation_name' in record[2]:
                        values.update({'designation_name':record[2]['designation_name']})
                    if 'organization_type' in record[2]:
                        values.update({'organization_type':record[2]['organization_type']})
                    if 'industry_type' in record[2]:
                        values.update({'industry_type':record[2]['industry_type']})
                    if 'effective_from' in record[2]:
                        values.update({'effective_from':record[2]['effective_from']})
                    if 'effective_to' in record[2]:
                        values.update({'effective_to':record[2]['effective_to']})
                    if 'uploaded_doc' in record[2]:
                        values.update({'uploaded_doc':record[2]['uploaded_doc']})
                   
                    profile_data.update(values)
                    
                if record[0] == 0:
                    profile_data = self.env['kw_emp_profile_work_experience'].sudo().search([])
                    values = {
                        'country_id': record[2]['country_id'] if record[2]['country_id'] else False,
                        'name': record[2]['name'] if record[2]['name'] else False,
                        'designation_name': record[2]['designation_name'] if record[2]['designation_name'] else False,
                        'organization_type': record[2]['organization_type'] if record[2]['organization_type'] else False,
                        'industry_type': record[2]['industry_type'] if record[2]['industry_type'] else False,
                        'effective_from': record[2]['effective_from'] if record[2]['effective_from'] else False,
                        'effective_to': record[2]['effective_to'] if record[2]['effective_to'] else False,
                        'uploaded_doc': record[2]['uploaded_doc'] if record[2]['uploaded_doc'] else False,
                        'emp_work_id': list_2[0],
                        'emp_id': emp_prpf_rec.id if emp_prpf_rec.id else False,                
                    }
                    profile_data.create(values)
                    list_2.pop(0)
               


        
        if 'known_language_ids' in vals:
            for rec in self.known_language_ids:
                list_2.append(rec.id)

            for r in list_3:
                if r in list_2:
                    list_2.remove(r)

            for record in vals['known_language_ids']:
                if record[0] == 1:
                    values = {}
                    
                    profile_data = self.env['kw_emp_profile_language_known'].sudo().search([('emp_language_id','=',record[1])])
                    if 'language_id' in record[2]:
                        values.update({'language_id':record[2]['language_id']})
                    if 'reading_status' in record[2]:
                        values.update({'reading_status':record[2]['reading_status']})
                    if 'writing_status' in record[2]:
                        values.update({'writing_status':record[2]['writing_status']})
                    if'speaking_status' in record[2]:
                        values.update({'speaking_status':record[2]['speaking_status']})
                    if 'understanding_status' in record[2]:
                        values.update({'understanding_status':record[2]['understanding_status']})
                    
                   
                    profile_data.update(values)
                
                if record[0] == 0:
                    profile_data = self.env['kw_emp_profile_language_known'].sudo().search([])
                    values = {
                        'language_id': record[2]['language_id'] if record[2]['language_id'] else False,
                        'reading_status': record[2]['reading_status'] if record[2]['reading_status'] else False,
                        'writing_status': record[2]['writing_status'] if record[2]['writing_status'] else False,
                        'speaking_status': record[2]['speaking_status'] if record[2]['speaking_status'] else False,
                        'understanding_status': record[2]['understanding_status'] if record[2]['understanding_status'] else False,
                        'emp_language_id': list_2[0],
                        'emp_id': emp_prpf_rec.id if emp_prpf_rec.id else False,                
                    }
                    profile_data.create(values)
                    list_2.pop(0)
            
        
       
        if 'family_details_ids' in vals:
            for rec in self.family_details_ids:
                list_2.append(rec.id)

            for r in list_1:
                if r in list_2:
                    list_2.remove(r)

        


            for record in vals['family_details_ids']:
                if record[0] == 1:
                    values = {}
                    
                    profile_data = self.env['kw_emp_profile_family_info'].sudo().search([('family_id','=',record[1])])
                    if 'relationship_id' in record[2]:
                        values.update({'relationship_id':record[2]['relationship_id']})
                    if 'name' in record[2]:
                        values.update({'name':record[2]['name']})
                    if 'gender' in record[2]:
                        values.update({'gender':record[2]['gender']})
                    if'date_of_birth' in record[2]:
                        values.update({'date_of_birth':record[2]['date_of_birth']})
                    if 'dependent' in record[2]:
                        values.update({'dependent':record[2]['dependent']})
                    
                   
                    profile_data.update(values)
                               
                if record[0] == 0:
                    profile_data = self.env['kw_emp_profile_family_info'].sudo().search([])
                    values = {
                        'relationship_id': record[2]['relationship_id'] if record[2]['relationship_id'] else False,
                        'name': record[2]['name'] if record[2]['name'] else False,
                        'gender': record[2]['gender'] if record[2]['gender'] else False,
                        'date_of_birth': record[2]['date_of_birth'] if record[2]['date_of_birth'] else False,
                        'dependent': record[2]['dependent'] if record[2]['dependent'] else False,
                        'family_id': list_2[0],
                        'emp_family_id': emp_prpf_rec.id if emp_prpf_rec.id else False,                
                    }
                    profile_data.create(values)
                    list_2.pop(0)       


                
        if 'name' in vals:
            val['name'] = vals['name']
        if 'job_id' in vals:
            val['job_position'] = vals['job_id']
        if 'emp_code' in vals:
            val['employee_code'] = vals['emp_code']
        if 'work_email' in vals:
            val['work_email_id'] = vals['work_email']
        if 'work_phone' in vals:
            val['work_phone'] = vals['work_phone']
        if 'user_id' in vals:
            val['user_id'] = vals['user_id']
        if 'date_of_joining' in vals:
            val['date_of_joining'] = vals['date_of_joining']
        if 'gender' in vals:
            val['gender'] = vals['gender']
        if 'permanent_addr_street' in vals:
            val['permanent_addr_street'] = vals['permanent_addr_street']
        if 'blood_group' in vals:
            val['blood_group'] = vals['blood_group']
        if 'emp_religion' in vals:
            val['emp_religion'] = vals['emp_religion']
        if 'id_card_no' in vals:
            val['id_card_no'] = vals['id_card_no']
        if 'country_id' in vals:
            val['country_id'] = vals['country_id']
        if 'outlook_pwd' in vals:
            val['outlook_pwd'] = vals['outlook_pwd']
        if 'emergency_contact' in vals:
            val['emergency_contact_name'] = vals['emergency_contact']
        if 'present_addr_street' in vals:
            val['present_addr_street'] = vals['present_addr_street']
        if 'present_addr_city' in vals:
            val['present_addr_city'] = vals['present_addr_city']
        if 'present_addr_state_id' in vals:
            val['present_addr_state_id'] = vals['present_addr_state_id']
        if 'present_addr_zip' in vals:
            val['present_addr_zip'] = vals['present_addr_zip']
        if 'whatsapp_no' in vals:
            val['whatsapp_no'] = vals['whatsapp_no']
        if 'marital' in vals:
            val['marital'] = vals['marital']
        if 'permanent_addr_street' in vals:
            val['permanent_addr_street'] = vals['permanent_addr_street']
        if 'personal_email' in vals:
            val['personal_email'] = vals['personal_email']
        if 'permanent_addr_zip' in vals:
            val['permanent_addr_zip'] = vals['permanent_addr_zip']
        if 'permanent_addr_country_id' in vals:
            val['permanent_addr_country_id'] = vals['permanent_addr_country_id']
        if 'permanent_addr_state_id' in vals:
            val['permanent_addr_state_id'] = vals['permanent_addr_state_id']
        if 'permanent_addr_city' in vals:
            val['permanent_addr_city'] = vals['permanent_addr_city']
        if 'emergency_phone' in vals:
            val['emergency_mobile_no'] = vals['emergency_phone']
        if 'same_address' in vals:
            val['same_address'] = vals['same_address']
        if 'image' in vals:
            val['image'] = vals['image']
        if 'worked_country_ids' in vals:
            val['worked_country_ids'] = vals['worked_country_ids']
        if 'birthday' in vals:
            val['birthday'] = vals['birthday']
        if 'mobile_phone' in vals:
            val['mobile_phone'] = vals['mobile_phone']
       
        res = emp_prpf_rec.write(val)
        return emp_rec