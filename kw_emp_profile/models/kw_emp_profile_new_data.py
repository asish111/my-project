from odoo import models, fields, api

class EmployeeApproval(models.Model):
    _name = 'kw_emp_profile_new_data'
    _description = "New Data Approval Details"

    emp_prfl_id = fields.Many2one('kw_emp_profile',string="Employee Profile Id")
    name = fields.Char(string='Employee')
    user_id = fields.Char('User Id')
    work_email_id = fields.Char(string="Work Email", size=100)
    date_of_joining = fields.Date(string="Joining Date") 
    birthday = fields.Date(groups="base.group_user")
    work_phone = fields.Char(string="Work Phone No",)
    whatsapp_no = fields.Char(string='WhatsApp No.', size=15)
    extn_no = fields.Char(string ='Extn No.')

    new_name = fields.Char(string='Employee')
    new_user_id = fields.Char('User Id')

    new_work_email_id = fields.Char(string="Work Email", size=100)
    new_date_of_joining = fields.Date(string="Joining Date") 
    new_birthday = fields.Date(groups="base.group_user")
    new_work_phone = fields.Char(string="Work Phone No",)
    new_whatsapp_no = fields.Char(string='WhatsApp No.', size=15)
    new_extn_no = fields.Char(string ='Extn No.')

    user_name = fields.Char(string='User Name')
    digital_signature = fields.Binary(string="Digital Signature")
    id_card_no = fields.Char(string=u'ID Card No', size=100)
    outlook_pwd = fields.Char(string=u'Outlook Password', size=100)

    new_user_name = fields.Char(string='New User Name')
    new_digital_signature = fields.Binary(string="New Digital Signature")
    new_id_card_no = fields.Char(string=u'New ID Card No', size=100)
    new_outlook_pwd = fields.Char(string=u'Outlook Password', size=100)
    
    personal_email = fields.Char(string='Personal EMail Id ',)
    country_id = fields.Many2one('res.country',string="Nationality")
    marital = fields.Many2one('kwemp_maritial_master',string='Marital Status')
    marital_code = fields.Char(string=u'Marital Status Code ')
    wedding_anniversary = fields.Date(string=u'Wedding Anniversary')
    emp_religion = fields.Many2one('kwemp_religion_master', string="Religion")
    # gender = fields.Char(string='Gender')
    mobile_phone = fields.Char(string="Mobile No", size=15)
    present_addr_street = fields.Text(string="Present Address Line 1", size=500)
    present_addr_country_id = fields.Many2one('res.country', string="Present Address Country")
    present_addr_city = fields.Char(string="Present Address City", size=100)
    present_addr_state_id = fields.Many2one('res.country.state', string="Present Address State")
    present_addr_zip = fields.Char(string="Present Address ZIP", size=10)
    same_address = fields.Boolean(string=u'Same as Present Address', default=False)
    permanent_addr_country_id = fields.Many2one('res.country', string="Permanent Address Country")
    permanent_addr_street = fields.Text(string="Permanent Address Line 1", size=500)
    permanent_addr_city = fields.Char(string="Permanent Address City",  size=100)
    permanent_addr_state_id = fields.Many2one('res.country.state', string="Permanent Address State")
    permanent_addr_zip = fields.Char(string="Permanent Address ZIP", size=10)
    emergency_contact_name = fields.Char(string='Name')
    emergency_address = fields.Text(string='Address')
    emergencye_telephone = fields.Char(string='Telephone(R)')
    emergency_city = fields.Char(string='City')
    emergency_country = fields.Many2one('res.country',string='Country')
    emergency_state = fields.Many2one('res.country.state',string='State')
    emergency_mobile_no = fields.Char(string='Mobile No')
    known_language_ids = fields.One2many('kw_emp_profile_language_known', 'approve_id', string='Language Known')

    new_known_language_ids = fields.One2many('kw_emp_profile_language_known', 'new_approve_id', string='New Language Known')

    new_personal_email = fields.Char(string='New Personal EMail Id ',)
    new_country_id = fields.Many2one('res.country',string="New Nationality")
    new_marital = fields.Many2one('kwemp_maritial_master',string='New Marital Status')
    new_marital_code = fields.Char(string=u' New Marital Status Code ')
    new_wedding_anniversary = fields.Date(string=u'New Wedding Anniversary')
    new_emp_religion = fields.Many2one('kwemp_religion_master', string="New Religion")
    # new_gender = fields.Char(string='Gender')
    new_mobile_phone = fields.Char(string="New Mobile No", size=15)
    new_present_addr_street = fields.Text(string=" New Present Address Line 1", size=500)
    new_present_addr_country_id = fields.Many2one('res.country', string="New Present Address Country")
    new_present_addr_city = fields.Char(string="New Present Address City", size=100)
    new_present_addr_state_id = fields.Many2one('res.country.state', string="New Present Address State")
    new_present_addr_zip = fields.Char(string="New Present Address ZIP", size=10)
    new_same_address = fields.Boolean(string=u'Same as Present Address', default=False)
    new_permanent_addr_country_id = fields.Many2one('res.country', string="New Permanent Address Country")
    new_permanent_addr_street = fields.Text(string="New Permanent Address Line 1", size=500)
    new_permanent_addr_city = fields.Char(string="New Permanent Address City",  size=100)
    new_permanent_addr_state_id = fields.Many2one('res.country.state', string="New Permanent Address State")
    new_permanent_addr_zip = fields.Char(string="New Permanent Address ZIP", size=10)
    new_emergency_contact_name = fields.Char(string='New Name')
    new_emergency_address = fields.Text(string='New Address')
    new_emergencye_telephone = fields.Char(string='New Telephone(R)')
    new_emergency_city = fields.Char(string='New City')
    new_emergency_country = fields.Many2one('res.country',string='New Country')
    new_emergency_state = fields.Many2one('res.country.state',string='New State')
    new_emergency_mobile_no = fields.Char(string='New Mobile No')

    blood_group = fields.Many2one('kwemp_blood_group_master', string="Blood Group")
    new_blood_group = fields.Many2one('kwemp_blood_group_master', string="New Blood Group")

    experience_sts = fields.Selection(string="Work Experience Details ",
                                      selection=[('1', 'Fresher'), ('2', 'Experience')])
    new_experience_sts = fields.Selection(string="New Work Experience Details ",
                                      selection=[('1', 'Fresher'), ('2', 'Experience')])

    # check_all_boolean = fields.Char(compute='_get_value')
    digital_signature_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    personal_email_boolean = fields.Boolean(string='Check Availability',compute='_get_value')
    country_id_boolean = fields.Boolean(string='Check Availability',compute='_get_value')
    marital_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    wedding_anniversary_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emp_religion_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    mobile_phone_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    present_addr_street_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    present_addr_country_id_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    present_addr_city_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    present_addr_state_id_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    present_addr_zip_boolean= fields.Boolean(string="Check Availability",compute='_get_value')
    permanent_addr_state_id_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    permanent_addr_country_id_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    permanent_addr_city_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    permanent_addr_street_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    permanent_addr_zip_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emergency_address_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emergency_city_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emergency_contact_name_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emergency_mobile_no_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emergency_state_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emergencye_telephone_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    emergency_country_boolean = fields.Boolean(string="Check Availability",compute='_get_value')
    known_language_ids_boolean = fields.Boolean(string="Check Availability",compute='_get_value')


    def _get_value(self):
        for record in self:
            record.known_language_ids_boolean = True if len(record.new_known_language_ids)>0 else False
            record.digital_signature_boolean = True if record.new_digital_signature else False
            record.personal_email_boolean = True if record.new_personal_email else False
            record.country_id_boolean = True if record.new_country_id else False
            record.marital_boolean = True if record.new_marital else False
            record.wedding_anniversary_boolean = True if record.new_wedding_anniversary else False
            record.emp_religion_boolean = True if record.new_emp_religion else False
            record.mobile_phone_boolean = True if record.new_mobile_phone else False
            record.present_addr_street_boolean = True if record.new_present_addr_street else False
            record.present_addr_country_id_boolean = True if record.new_present_addr_country_id else False
            record.present_addr_city_boolean = True if record.new_present_addr_city else False
            record.present_addr_state_id_boolean = True if record.new_present_addr_state_id else False
            record.present_addr_zip_boolean = True if record.new_present_addr_zip else False
            record.permanent_addr_state_id_boolean = True if record.new_permanent_addr_state_id else False
            record.permanent_addr_country_id_boolean = True if record.new_permanent_addr_country_id else False
            record.permanent_addr_city_boolean = True if record.new_permanent_addr_city else False
            record.permanent_addr_zip_boolean = True if record.new_permanent_addr_zip else False
            record.permanent_addr_street_boolean = True if record.new_permanent_addr_street else False
            record.emergency_city_boolean = True if record.new_emergency_city else False
            record.emergency_address_boolean = True if record.new_emergency_address else False
            record.emergencye_telephone_boolean = True if record.new_emergencye_telephone else False
            record.emergency_state_boolean = True if record.new_emergency_state else False
            record.emergency_mobile_no_boolean = True if record.new_emergency_mobile_no else False
            record.emergency_contact_name_boolean = True if record.new_emergency_contact_name else False
            record.emergency_country_boolean = True if record.new_emergency_country else False








    @api.multi
    def take_action(self):
        view_id = self.env.ref('kw_emp_profile.kw_emp_profile_new_data_view_form').id
        target_id = self.id
        return {
            'name': 'Take Action',
            'type': 'ir.actions.act_window',
            'res_model': 'kw_emp_profile_new_data',
            'res_id': target_id,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'views': [(view_id, 'form')],
            'target': 'self',
            'view_id': view_id,
        }


   

    












    