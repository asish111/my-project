from odoo import models, fields, api

class EmployeeApproval(models.Model):
    _name = 'kw_emp_profile_approval'
    _description = "Employee Details Approval"

    profile_ids = fields.One2many('kw_emp_profile', 'approval_id', string='Profile Approval')
    old_data = fields.Char(string='Old Data')
    new_data = fields.Char(string='New Data')
    employee_id = fields.Many2one('hr.employee',string='Employee Name')

    