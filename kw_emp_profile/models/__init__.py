# -*- coding: utf-8 -*-
from . import kw_emp_profile_family_info
from . import kw_emp_profile_identity_docs
from . import kw_emp_profile_work_experience
from . import kw_emp_profile_educational_qualification
from . import kw_emp_profile_technical_skills
from . import kw_emp_profile_membership_assc
from . import kw_emp_profile_language_known
from . import kw_emp_profile
from . import kw_emp_profile_course_setting
from . import kw_emp_profile_inherit_hr_emp
# from . import kw_emp_profile_approval
from . import kw_emp_profile_new_data

