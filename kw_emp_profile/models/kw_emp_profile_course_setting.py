from odoo import models, fields, api
from odoo.exceptions import ValidationError


class kw_course_setting(models.Model):
    _name = 'kw_emp_profile_course_setting'
    _description = "A master model to created for courses year difference."
    _rec_name = "course_id"

    course_id = fields.Many2one('kwmaster_course_name', string='Course', required=True,
                                domain="[('course_type','=','1')]")
    child_id = fields.Many2one('kwmaster_course_name', string='Child Course Name', domain="[('course_type','=','1')]")
    diff_year = fields.Integer(string="Difference year")
