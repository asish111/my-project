# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions, SUPERUSER_ID
from odoo import tools, _
from odoo.exceptions import ValidationError, AccessError
import re
from lxml import etree
from datetime import date, datetime
from dateutil import relativedelta
import requests, json
from kw_utility_tools import kw_validations
from odoo import http

DEFAULT_INTERNAL_GRP = 1


class EmployeeProfile(models.Model):
    _name = 'kw_emp_profile'
    _description = 'employee profile details'

    name = fields.Char(string='Employee')
    emp_id = fields.Many2one('hr.employee',string = "Employee Id")
    employee_code = fields.Char(string='Employee Code')
    user_name = fields.Char(string='User Name')
    digital_signature = fields.Binary(string="Digital Signature")
    job_position = fields.Many2one('hr.job', string='Job Position')
    country_id = fields.Many2one('res.country',string="Nationality")

    date_of_joining = fields.Date(string="Joining Date") 

    id_card_no = fields.Char(string=u'ID Card No', size=100)
    outlook_pwd = fields.Char(string=u'Outlook Password', size=100)
    mobile_phone = fields.Char(string="Mobile No", size=15)
    work_phone = fields.Char(string="Work Phone No",)
    work_location_id = fields.Many2one('res.partner', string="Work Location ID")
    work_location = fields.Many2one('kw_re_branch',string="Work Location",)
    work_email_id = fields.Char(string="Work Email", size=100)
    gender = fields.Char(string='Gender')
    image = fields.Binary(string="Upload Photo",
                          help="Only .jpeg,.png,.jpg format are allowed. Maximum file size is 1 MB")
    marital = fields.Many2one('kwemp_maritial_master',string='Marital Status')
    marital_code = fields.Char(string=u'Marital Status Code ')
    wedding_anniversary = fields.Date(string=u'Wedding Anniversary')
    user_id = fields.Char('User')
    birthday = fields.Date(groups="base.group_user")

    personal_email = fields.Char(string='Personal EMail Id ',)
    present_addr_street = fields.Text(string="Present Address Line 1", size=500)
    present_addr_country_id = fields.Many2one('res.country', string="Present Address Country")
    present_addr_city = fields.Char(string="Present Address City", size=100)
    present_addr_state_id = fields.Many2one('res.country.state', string="Present Address State")
    present_addr_zip = fields.Char(string="Present Address ZIP", size=10)
    same_address = fields.Boolean(string=u'Same as Present Address', default=False)
    permanent_addr_country_id = fields.Many2one('res.country', string="Permanent Address Country")
    permanent_addr_street = fields.Text(string="Permanent Address Line 1", size=500)
    permanent_addr_city = fields.Char(string="Permanent Address City",  size=100)
    permanent_addr_state_id = fields.Many2one('res.country.state', string="Permanent Address State")
    permanent_addr_zip = fields.Char(string="Permanent Address ZIP", size=10)
    emp_religion = fields.Many2one('kwemp_religion_master', string="Religion")
    emergency_contact_name = fields.Char(string='Name')
    emergency_address = fields.Text(string='Address')
    emergencye_telephone = fields.Char(string='Telephone(R)')
    emergency_city = fields.Char(string='City')
    emergency_country = fields.Many2one('res.country',string='Country')
    emergency_state = fields.Many2one('res.country.state',string='State')
    emergency_mobile_no = fields.Char(string='Mobile No')

    known_language_ids = fields.One2many('kw_emp_profile_language_known', 'emp_id', string='Language Known')
    experience_sts = fields.Selection(string="Work Experience Details ",
                                      selection=[('1', 'Fresher'), ('2', 'Experience')])
    worked_country_ids = fields.Many2many('res.country', string='Countries Of Work Experience',
                                          groups="hr.group_hr_user")
    work_experience = fields.Char(string='Work Experience Details')
    technical_skill_ids = fields.One2many('kw_emp_profile_technical_skills', 'emp_id', string='Technical Skills')
    educational_details_ids = fields.One2many('kw_emp_profile_qualification', 'emp_id', string="Educational Details")
    blood_group = fields.Many2one('kwemp_blood_group_master', string="Blood Group")
    identification_ids = fields.One2many('kw_emp_profile_identity_docs', 'emp_id', string='Indentification Documents')
    membership_assc_ids = fields.One2many('kw_emp_profile_membership_assc', 'emp_id', string='Membership Association Details')
    family_details_ids = fields.One2many('kw_emp_profile_family_info', 'emp_family_id', string="Family Info Details")
    total_experience_display = fields.Char('Total Experience', readonly=True, compute='_compute_experience_display',
                                           help="Field allowing to see the total experience in years and months depending upon joining date and experience details")
    work_experience_ids = fields.One2many('kw_emp_profile_work_experience', 'emp_id', string='Work Experience Details')

    survey_investigation = fields.Boolean(string='Survey and Investigation ')
    design = fields.Boolean(string='Design')
    quality_control = fields.Boolean(string='Quality Control')
    project_work = fields.Boolean(string='Project Work')
    admin_legal_finance = fields.Boolean(string='Admin/Legal/Finance')
    research = fields.Boolean(string='Research')
    technology = fields.Boolean(string='Technology')

    employee_id = fields.Many2one('hr.employee',string='Name',default=lambda self: self.env.user.employee_ids)
    whatsapp_no = fields.Char(string='WhatsApp No.', size=15)
    extn_no = fields.Char(string ='Extn No.')
    approval_id = fields.Many2one('kw_emp_profile_approval',string='Profile Approval Id')

    account_percentage = fields.Float(compute='calculate_accountinfo_percentage')
    identification_percentage = fields.Float(compute='calculate_identification_percentage')
    personal_info_percentage = fields.Float(compute='calculate_personal_info_percentage')
    workinfo_percentage = fields.Float(compute='calculate_workinfo_percentage')
    familyinfo_percentage = fields.Float(compute='calculate_familyinfo_percentage')
    education_percentage = fields.Float(compute='calculate_education_percentage')
    

    @api.model
    def _add_employee_data(self):
        res = self.env['kw_emp_profile']
        uid = self._uid
        employees = self.env['hr.employee'].sudo().search([])
        for emp in employees:
            if emp.id:
                profile_data = self.env['kw_emp_profile'].sudo().search([('emp_id','=',emp.id)])
                if len(profile_data) == 0:
                    country_list = []
                    for r in emp.worked_country_ids:
                        country_list.append(r.id)
                    res.create({
                        'total_experience_display':emp.total_experience_display,
                        'emp_id': emp.id,
                        'name': emp.name,
                        'job_position': emp.job_id.id,
                        'work_email_id': emp.work_email,
                        'employee_code': emp.emp_code,
                        'mobile_phone': emp.mobile_phone,
                        'work_phone': emp.work_phone,
                        'user_id': emp.user_id.id,
                        'gender': emp.gender,
                        'permanent_addr_street':emp.permanent_addr_street,
                        'personal_email': emp.personal_email,
                        'permanent_addr_zip': emp.permanent_addr_zip,
                        'permanent_addr_country_id': emp.permanent_addr_country_id.id,
                        'permanent_addr_state_id': emp.permanent_addr_state_id.id,
                        'permanent_addr_city': emp.permanent_addr_city,
                        'date_of_joining': emp.date_of_joining,
                        'outlook_pwd': emp.outlook_pwd,
                        'birthday': emp.birthday,
                        'same_address': emp.same_address,
                        'emp_religion': emp.emp_religion.id,
                        'emergency_contact_name': emp.emergency_contact,
                        'emergency_mobile_no': emp.emergency_phone,
                        'id_card_no': emp.id_card_no,
                        # 'country_id': emp.country_id.name,
                        'present_addr_street': emp.present_addr_street,
                        'blood_group': emp.blood_group.id,
                        'present_addr_country_id': emp.present_addr_country_id.id,
                        'present_addr_city': emp.present_addr_city,
                        'present_addr_state_id': emp.present_addr_state_id.id,
                        'present_addr_zip': emp.present_addr_zip,
                        'whatsapp_no': emp.whatsapp_no,
                        'marital': emp.marital.id,
                        'experience_sts': emp.experience_sts,
                        'image': emp.image,
                         'identification_ids': [[0,0,{
                            'name':r.name,
                            'doc_number':r.doc_number,
                            'date_of_issue':r.date_of_issue,
                            'date_of_expiry':r.date_of_expiry,
                            'renewal_sts':r.renewal_sts,
                            'uploaded_doc':r.uploaded_doc,
                            'emp_document_id':r.id,
                            }]for r in emp.identification_ids],
                        'family_details_ids': [[0, 0, {
                            'relationship_id': r.relationship_id.id,
                            'name': r.name,
                            'gender': r.gender,
                            'date_of_birth': r.date_of_birth,
                            'dependent': r.dependent,
                            'family_id':r.id,
                        }] for r in emp.family_details_ids],
                        'work_experience_ids': [[0, 0, {
                            'country_id': r.country_id.id,
                            'name': r.name,
                            'designation_name': r.designation_name,
                            'organization_type': r.organization_type.id,
                            'industry_type': r.industry_type.id,
                            'effective_from': r.effective_from,
                            'effective_to': r.effective_to,
                            'uploaded_doc': r.uploaded_doc,
                            'emp_work_id':r.id,
                        }] for r in emp.work_experience_ids],
                        'membership_assc_ids': [[0, 0, {
                            'date_of_issue': r.date_of_issue,
                            'name': r.name,
                            'date_of_expiry': r.date_of_expiry,
                            'renewal_sts': r.renewal_sts,
                            'uploaded_doc': r.uploaded_doc,
                            'emp_membership_id':r.id,
                        }] for r in emp.membership_assc_ids],
                         'worked_country_ids': [[6, False,country_list]],
                           
                        'technical_skill_ids': [[0, 0, {
                            'category_id': r.category_id.id,
                            'skill_id': r.skill_id.id,
                            'proficiency': r.proficiency,
                            'emp_technical_id':r.id,
                        }] for r in emp.technical_skill_ids],
                        'known_language_ids': [[0, 0, {
                            'language_id': r.language_id.id,
                            'reading_status': r.reading_status,
                            'writing_status': r.writing_status,
                            'speaking_status': r.speaking_status,
                            'understanding_status': r.understanding_status,
                            'emp_language_id':r.id,
                        }] for r in emp.known_language_ids],
                        'educational_details_ids': [[0, 0, {
                            'course_type': r.course_type,
                            'course_id': r.course_id.id,
                            'stream_id': 1,
                            'university_name': r.university_name.id,
                            'passing_year': str(r.passing_year),
                            'division': r.division,
                            'marks_obtained': r.marks_obtained,
                            'uploaded_doc': r.uploaded_doc,
                            'emp_educational_id':r.id,
                        }] for r in emp.educational_details_ids],
                    })

               
                    

    def test_function(self):
        check_active_id = self.env['kw_emp_profile'].sudo().search([('user_id','=',self.env.uid)])
        if check_active_id:
            form_view_id = self.env.ref("kw_emp_profile.kw_emp_profile_form_view").id
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'kw_emp_profile',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': check_active_id.id,
                'view_id': form_view_id,
                'target': 'self',
                "domain": [('user_id', '!=', self.env.uid)],
            }
        else:
            pass

    @api.constrains('image')
    def _check_employee_photo(self):
        allowed_file_list = ['image/jpeg', 'image/jpg', 'image/png']
        for record in self:
            kw_validations.validate_file_mimetype(record.image, allowed_file_list)
            kw_validations.validate_file_size(record.image, 1)
   

    


    def calculate_education_percentage(self):
        for rec in self:
            count = 0
            if rec.educational_details_ids:
                education = rec.educational_details_ids.filtered(lambda r: r.course_type == '1')            
                if education:
                    rec.education_percentage = 100
                else:
                    rec.education_percentage = 0

    def calculate_identification_percentage(self):
        for rec in self:
            count = 0
            if rec.identification_ids:
                aadhar = rec.identification_ids.filtered(lambda r: r.name == '5')
                if aadhar:
                    count += 1
            if rec.blood_group:
                count += 1
            rec.identification_percentage = (count/2)*100

    def calculate_familyinfo_percentage(self):
        for rec in self:
            if rec.family_details_ids:
                rec.familyinfo_percentage = 100

    def calculate_workinfo_percentage(self):
        for rec in self:
            count = 0
            if rec.experience_sts == '2':
                count += 1
                # if rec.worked_country_ids.name != False and rec.worked_country_ids.code != False:
                #     count += 1
                if rec.work_experience_ids:
                    count += 1
            rec.workinfo_percentage = (count/2)*100
            if rec.experience_sts == '1':
                rec.workinfo_percentage = 100

    def calculate_accountinfo_percentage(self):
        for rec in self:
            count = 0
            if rec.employee_code:
                count += 1
            if rec.user_name:
                count += 1           
            if rec.digital_signature:
                count += 1
            if rec.date_of_joining:
                count += 1
            if rec.work_email_id:
                    count += 1
            if rec.id_card_no:
                count += 1
            if rec.outlook_pwd:
                count += 1
        rec.account_percentage = (count/6)*100

    def calculate_personal_info_percentage(self):
        for rec in self:
            count = 0
            if rec.birthday:
                count += 1
            if rec.country_id:
                count += 1
            if rec.personal_email:
                count += 1
            if rec.marital:
                count += 1
            if rec.gender:
                count += 1
            if rec.emp_religion:
                count += 1
            if rec.mobile_phone:
                count += 1

        rec.personal_info_percentage = (count / 7) * 100

    total_percentage = fields.Float(compute='calculate_total_percentage')

    # def calculate_total_percentage(self):
    #     for rec in self:
    #         count = 0
    #         if rec.personal_info_percentage:
    #             count += 1
    #         if rec.account_percentage:
    #             count += 1
    #         if rec.identification_percentage:
    #             count += 1
    #         if rec.workinfo_percentage:
    #             count += 1
    #         if rec.familyinfo_percentage:
    #             count += 1
    #         if rec.education_percentage:
    #             count += 1
    #     rec.total_percentage = (count / 6) * 100

   
    @api.onchange('same_address')
    def _change_permanent_address(self):
        if self.same_address:
            self.permanent_addr_country_id = self.present_addr_country_id
            self.permanent_addr_street = self.present_addr_street
            self.permanent_addr_city = self.present_addr_city
            self.permanent_addr_state_id = self.present_addr_state_id
            self.permanent_addr_zip = self.present_addr_zip

    @api.onchange('present_addr_country_id')
    def _change_present_address_state(self):
        country_id = self.present_addr_country_id.id
        self.present_addr_state_id = False
        return {'domain': {'present_addr_state_id': [('country_id', '=', country_id)], }}

    @api.onchange('permanent_addr_country_id')
    def _change_permanent_address_state(self):
        country_id = self.permanent_addr_country_id.id
        if self.same_address and self.present_addr_state_id and (
                self.permanent_addr_country_id == self.present_addr_country_id):
            self.permanent_addr_state_id = self.present_addr_state_id
        else:
            self.permanent_addr_state_id = False
        return {'domain': {'permanent_addr_state_id': [('country_id', '=', country_id)], }}
    @api.onchange('emergency_country')
    def _change_emergency_address_state(self):
        country_id = self.emergency_country.id
        self.present_addr_state_id = False
        return {'domain': {'present_addr_state_id': [('country_id', '=', country_id)], }}

    @api.onchange('marital')
    def _compute_marital_status_code(self):
        if self.marital:
            self.marital_code = self.marital.code
        else:
            self.marital_code = ''

    @api.depends('date_of_joining', 'work_experience_ids')
    def _compute_experience_display(self):
        for rec in self:
            total_years, total_months = 0, 0
            if rec.date_of_joining:
                difference = relativedelta.relativedelta(datetime.today(), rec.date_of_joining)
                total_years += difference.years
                total_months += difference.months

            if rec.work_experience_ids:
                for exp_data in rec.work_experience_ids:
                    exp_difference = relativedelta.relativedelta(exp_data.effective_to, exp_data.effective_from)
                    total_years += exp_difference.years
                    total_months += exp_difference.months
                 

            if total_months >= 12:
                total_years += total_months // 12
                total_months = total_months % 12

            if total_years > 0 or total_months > 0:
                rec.total_experience_display = " %s Years and %s Months " % (total_years, total_months)
            else:
                rec.total_experience_display = ''

    @api.constrains('wedding_anniversary', 'birthday')
    def validate_Birthdate_data(self):
        current_date = str(datetime.now().date())
        today = date.today()
        for record in self:
            if record.birthday:
                if today.year - record.birthday.year - (
                        (today.month, today.day) < (record.birthday.month, record.birthday.day)) < 18:
                    raise ValidationError("You must be 18 years old.")
                if record.wedding_anniversary:
                    if str(record.wedding_anniversary) <= str(record.birthday):
                        raise ValidationError("Wedding Anniversary date should not be less than birth date.")
            if record.birthday:
                if str(record.birthday) >= current_date:
                    raise ValidationError("The date of birth should be less than current date.")


    @api.constrains('work_email_id')
    def check_work_email(self):
        for record in self:
            kw_validations.validate_email(record.work_email_id)
            if record.work_email_id:
                records = self.env['kw_emp_profile'].search([('work_email_id', '=', record.work_email_id)]) - self
                if records:
                    raise ValidationError("This email id is already existing.")

    @api.constrains('work_experience_ids','birthday')
    def validate_experience(self):
        if self.work_experience_ids:
            if not self.birthday:
                raise ValidationError("Please enter your date of birthday.")
            for experience in self.work_experience_ids:
                if str(experience.effective_from) < str(self.birthday):
                    raise ValidationError("Work experience date should not be less than date of birth.")
                except_experience = self.work_experience_ids - experience
                overlap_experience = except_experience.filtered(
                    lambda r: r.effective_from <= experience.effective_from <= r.effective_to or r.effective_from <= experience.effective_to <= r.effective_to )
                if overlap_experience:
                    raise ValidationError(f"Overlapping experiences are not allowed.")

    @api.constrains('membership_assc_ids')
    def validate_membership_assc(self):
        for emp_rec in self:
            if emp_rec.membership_assc_ids and emp_rec.birthday:
                for record in emp_rec.membership_assc_ids:
                    if str(record.date_of_issue) <= str(emp_rec.birthday):
                        raise ValidationError("Membership issue date should not be less than date of birth.")

    @api.constrains('educational_details_ids')
    def validate_edu_data(self):
        if self.educational_details_ids:
            for record in self.educational_details_ids:
                if self.birthday:
                    if str(record.passing_year) < str(self.birthday):
                        raise ValidationError("Passing year should not be less than date of birth.")

    @api.constrains('identification_ids')
    def validate_issue_date(self):
        if self.identification_ids and self.birthday:
            for record in self.identification_ids:
                if str(record.date_of_issue) < str(self.birthday):
                    raise ValidationError("Date of issue should not be less than date of birth.")

    @api.constrains('mobile_phone')
    def check_mobile(self):
        for record in self:
            if record.mobile_phone:
                if not len(record.mobile_phone) == 10:
                    raise ValidationError("Your mobile number is invalid for: %s" % record.mobile_phone)
                elif not re.match("^[0-9]*$", str(record.mobile_phone)) != None:
                    raise ValidationError("Your mobile number is invalid for: %s" % record.mobile_phone)

    @api.constrains('work_phone')
    def check_phone(self):
        for record in self:
            if record.work_phone:
                if not len(record.work_phone) <= 18:
                    raise ValidationError("Your work phone no is invalid for: %s" % record.work_phone)
                elif re.match("^(\+?[\d ])+$",str(record.work_phone)) == None:
                    raise ValidationError("Your work phone no is invalid for: %s" % record.work_phone)
    @api.constrains('image')
    def _check_employee_photo(self):
        allowed_file_list = ['image/jpeg', 'image/jpg', 'image/png']
        for record in self:
            kw_validations.validate_file_mimetype(record.image, allowed_file_list)
            kw_validations.validate_file_size(record.image, 1)
            
    @api.constrains('emergency_mobile_no')
    def check_emergency_phone(self):
        for record in self:
            if record.emergency_mobile_no:
                if not len(record.emergency_mobile_no) <= 10:
                    raise ValidationError("Your emergency phone no is invalid for: %s" % record.emergency_mobile_no)
                elif not re.match("^[0-9]*$", str(record.emergency_mobile_no)) != None:
                    raise ValidationError("Your Emergency phone no is invalid for: %s" % record.emergency_mobile_no)

    @api.constrains('personal_email')
    def check_personal_email(self):
        for record in self:
            kw_validations.validate_email(record.personal_email)

    @api.constrains('present_addr_zip')
    def check_present_pincode(self):
        for record in self:
            if record.present_addr_zip:
                if not re.match("^[0-9]*$", str(record.present_addr_zip)) != None:
                    raise ValidationError("Present pincode is not Valid")

    @api.constrains('permanent_addr_zip')
    def check_permanent_pincode(self):
        for record in self:
            if record.permanent_addr_zip:
                if not re.match("^[0-9]*$", str(record.permanent_addr_zip)) != None:
                    raise ValidationError("Permanent pincode is not valid")

    
    def edit_profile(self):
        check_active_id = self.env['kw_emp_profile'].sudo().search([('user_id','=',self.env.uid)])
        print('active_id is========',check_active_id)
        if check_active_id:
            form_view_id = self.env.ref("kw_emp_profile.kw_emp_profile_form_view").id
            print('form_view_id is========',form_view_id)
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'kw_emp_profile',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': check_active_id.id,
                'view_id': form_view_id,
                'target': 'self',
                'flags': {'mode': 'edit', "toolbar": False}
            }
        else:
            pass  

    
    def write(self, vals):
        old_lang_ids = []
        print('self is=====',self.permanent_addr_state_id)

    # --------------Assigning Old Value------------
        old_email_id = self.work_email_id
        old_date_of_joining = self.date_of_joining
        old_birthday  = self.birthday
        old_work_phone = self.work_phone
        old_whatsapp_no = self.whatsapp_no
        old_extn_no = self.extn_no
        old_user_name = self.user_name
        old_digital_signature = self.digital_signature
        old_id_card_no = self.id_card_no
        old_outlook_pwd = self.outlook_pwd
        old_country_id = self.country_id.id
        old_personal_email = self.personal_email
        old_marital = self.marital.id
        old_marital_code = self.marital_code
        old_wedding_anniversary = self.wedding_anniversary
        old_emp_religion = self.emp_religion.id
        old_mobile_phone = self.mobile_phone
        old_present_addr_street = self.present_addr_street
        old_present_addr_country_id = self.present_addr_country_id.id
        old_present_addr_city = self.present_addr_city
        old_present_addr_state_id = self.present_addr_state_id.id
        old_present_addr_zip = self.present_addr_zip
        old_same_address = self.same_address
        old_permanent_addr_country_id = self.permanent_addr_country_id.id
        old_permanent_addr_street = self.permanent_addr_street
        old_permanent_addr_city = self.permanent_addr_city
        old_permanent_addr_state_id = self.permanent_addr_state_id.id
        old_permanent_addr_zip = self.permanent_addr_zip
        old_emergency_contact_name = self.emergency_contact_name
        old_emergency_address = self.emergency_address
        old_emergencye_telephone = self.emergencye_telephone
        old_emergency_city = self.emergency_city
        old_emergency_country = self.emergency_country.id
        old_emergency_state = self.emergency_state.id
        old_emergency_mobile_no = self.emergency_mobile_no 
        old_name = self.name
        old_user_id = self.user_id
        old_permanent_addr_street = self.permanent_addr_street


        old_lang_values = {}
        for rec in self.known_language_ids:
                # print(rec.language_id.id,rec.reading_status,rec.writing_status,rec.speaking_status,rec.understanding_status)
                old_lang_values[rec.language_id.id] = [rec.reading_status,rec.writing_status,
                    rec.speaking_status,rec.understanding_status]
       
        

        emp_rec = super(EmployeeProfile, self).write(vals)

        
                
        values={}   
        if 'known_language_ids' in vals:
            for record in vals['known_language_ids']:
                if record[0] == 1:
                    profile_data = self.env['kw_emp_profile_language_known'].sudo().search([('id','=',int(record[1]))])
                    values[profile_data.language_id.id] = [profile_data.reading_status,profile_data.writing_status,
                    profile_data.speaking_status,profile_data.understanding_status]

                if record[0] == 0:
                    values[record[2]['language_id']] = [record[2]['reading_status'],record[2]['writing_status'],
                    record[2]['speaking_status'],record[2]['understanding_status']]
                    
        emp_prfl_aprv = self.env['kw_emp_profile_new_data']
        aprv_record = emp_prfl_aprv.create({
            'emp_prfl_id':self.id,
            'permanent_addr_street':old_permanent_addr_street,
            'new_permanent_addr_street':vals['permanent_addr_street'] if 'permanent_addr_street' in vals else '', 
            'user_id':old_user_id,
            'new_user_id':vals['user_id'] if 'user_id' in vals else '', 
            'name':old_name, 
            'new_name': vals['name'] if 'name' in vals else '', 
            'emergency_contact_name':old_emergency_contact_name,
            'new_emergency_contact_name':vals['emergency_contact_name'] if 'emergency_contact_name' in vals else '','emergency_address':old_emergency_address,
            'new_emergency_address':vals['emergency_address'] if 'emergency_address' in vals else '','emergencye_telephone':old_emergencye_telephone,
            'new_emergencye_telephone':vals['emergencye_telephone'] if 'emergencye_telephone' in vals else '','emergency_city':old_emergency_city,
            'new_emergency_city':vals['emergency_city'] if 'emergency_city' in vals else '','emergency_country':old_emergency_country,
            'new_emergency_country':vals['emergency_country'] if 'emergency_country' in vals else '','emergency_state':old_emergency_state,
            'new_emergency_state':vals['emergency_state'] if 'emergency_state' in vals else '',
            'emergency_mobile_no':old_emergency_mobile_no,
            'new_emergency_mobile_no':vals['emergency_mobile_no'] if 'emergency_mobile_no' in vals else '',
            'permanent_addr_zip':old_permanent_addr_zip,
            'new_permanent_addr_zip':vals['permanent_addr_zip'] if 'permanent_addr_zip' in vals else '',
            'permanent_addr_state_id':old_permanent_addr_state_id,
            'new_permanent_addr_state_id':vals['permanent_addr_state_id'] if 'permanent_addr_state_id' in vals else 
            '',
            'permanent_addr_city':old_permanent_addr_city,
            'new_permanent_addr_city':vals['permanent_addr_city'] if 'permanent_addr_city' in vals else '','permanent_addr_country_id':old_permanent_addr_country_id,
            'new_permanent_addr_country_id':vals['permanent_addr_country_id'] if 'permanent_addr_country_id' in vals else '',
            'same_address':old_same_address,
            'new_same_address':vals['same_address'] if 'same_address' in vals else '',
            'present_addr_zip':old_present_addr_zip,
            'new_present_addr_zip':vals['present_addr_zip'] if 'present_addr_zip' in vals else '',
            'present_addr_state_id':old_present_addr_state_id,
            'new_present_addr_state_id':vals['present_addr_state_id'] if 'present_addr_state_id' in vals else '',
            'present_addr_city':old_present_addr_city,
            'new_present_addr_city':vals['present_addr_city'] if 'present_addr_city' in vals else '',
            'present_addr_country_id':old_present_addr_country_id,
            'new_present_addr_country_id':vals['present_addr_country_id'] if 'present_addr_country_id' in vals else '',
            'present_addr_street':old_present_addr_street,
            'new_present_addr_street':vals['present_addr_street'] if 'present_addr_street' in vals else '',
            'mobile_phone':old_mobile_phone,
            'new_mobile_phone':vals['mobile_phone'] if 'mobile_phone' in vals else '',
            'emp_religion':old_emp_religion,
            'new_emp_religion':vals['emp_religion'] if 'emp_religion' in vals else False,
            'wedding_anniversary':old_wedding_anniversary,
            'new_wedding_anniversary':vals['wedding_anniversary'] if 'wedding_anniversary' in vals else False,
            'marital_code':old_marital_code,
            'new_marital_code':vals['marital_code'] if 'marital_code' in vals else '',
            'marital':old_marital,
            'new_marital':vals['marital'] if 'marital' in vals else '',
            'personal_email':old_personal_email,
            'new_personal_email':vals['personal_email'] if 'personal_email' in vals else '',
            'work_email_id':old_email_id,
            'new_work_email_id':vals['work_email_id'] if 'work_email_id' in vals else '',
            'date_of_joining':old_date_of_joining,
            'new_date_of_joining':vals['date_of_joining'] if 'date_of_joining' in vals else False,
            'birthday':old_birthday,
            'new_birthday':vals['birthday'] if 'birthday' in vals else False,
            'work_phone':old_work_phone,
            'new_work_phone':vals['work_phone'] if 'work_phone' in vals else '',
            'whatsapp_no':old_whatsapp_no,
            'new_whatsapp_no':vals['whatsapp_no'] if 'whatsapp_no' in vals else '',
            'extn_no':old_extn_no,
            'new_extn_no': vals['extn_no'] if 'extn_no' in vals else '',
            'user_name':old_user_name,
            'new_user_name':vals['user_name'] if 'user_name' in vals else '',
            'digital_signature':old_digital_signature,
            'new_digital_signature':vals['digital_signature'] if 'digital_signature' in vals else '',
            'id_card_no':old_id_card_no,
            'new_id_card_no':vals['id_card_no'] if 'id_card_no' in vals else '',
            'outlook_pwd':old_outlook_pwd,
            'new_outlook_pwd':vals['outlook_pwd'] if 'outlook_pwd' in vals else '',
            'country_id':old_country_id,
            'new_country_id':vals['country_id'] if 'country_id' in vals else '',
            'new_known_language_ids': [[0, 0, {
                            'language_id': r,
                            'reading_status': values[r][0],
                            'writing_status': values[r][1],
                            'speaking_status': values[r][2],
                            'understanding_status': values[r][3],
                        }] for r in values] if values else False,
            'known_language_ids': [[0, 0, {
                            'language_id': r,
                            'reading_status': old_lang_values[r][0],
                            'writing_status': old_lang_values[r][1],
                            'speaking_status': old_lang_values[r][2],
                            'understanding_status': old_lang_values[r][3],
                        }] for r in old_lang_values],
                               
        })
        
        return emp_rec


    


   