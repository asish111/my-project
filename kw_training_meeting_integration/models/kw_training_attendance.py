# -*- coding: utf-8 -*-
from odoo import models, fields, api
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError


class TrainingAttendanceMeetingIntegration(models.Model):
    _inherit = 'kw_training_attendance'


    @api.model
    def create(self, values):
        print("meeting attendance create method called")
        result = super(TrainingAttendanceMeetingIntegration, self).create(values)
        if 'active_model' and 'active_id' in self._context:
            if self._context['active_model'] == 'kw_training_schedule':
                schedule_id = self._context['active_id']
                schedule_rec = self.env['kw_training_schedule'].browse(
                    schedule_id)
                if schedule_rec and not schedule_rec.attendance_id:
                    schedule_rec.attendance_id = result.id
        plan = result.training_id.plan_ids[-1] if result.training_id.plan_ids else False
        if plan:
            # plan.write({'participant_ids': [(6,0,[result.attendance_detail_ids.mapped('participant_id').ids])]})
            # new_participants = result.attendance_detail_ids.mapped(
            #     'participant_id') - plan.participant_ids
            new_participants = result.attendance_detail_ids.mapped('participant_id')

            # survey_user_group = self.env.ref('survey.group_survey_user')
            # dms_user_group = self.env.ref('kw_dms.group_dms_user')
            # training_employee_group = self.env.ref(
            #     'kw_training.group_kw_training_employee')
            # skill_assessment_user_group = self.env.ref(
            #     'kw_skill_assessment.group_kw_skill_assessment_user')
            meeting_user_group = self.env.ref(
                'kw_meeting_schedule.group_kw_meeting_schedule_user')

            for participant_emp in new_participants:
                plan.write({'participant_ids': [(4, participant_emp.id)]})

                if participant_emp.user_id:
                    p_user = participant_emp.user_id

                    # if not p_user.has_group('kw_training.group_kw_training_employee'):
                    #     training_employee_group.sudo().write(
                    #         {'users': [(4, p_user.id)]})
                    # if not p_user.has_group('survey.group_survey_user'):
                    #     survey_user_group.sudo().write(
                    #         {'users': [(4, p_user.id)]})
                    # if not p_user.has_group('kw_dms.group_dms_user'):
                    #     dms_user_group.sudo().write(
                    #         {'users': [(4, p_user.id)]})
                    # if not p_user.has_group('kw_skill_assessment.group_kw_skill_assessment_user'):
                    #     skill_assessment_user_group.sudo().write(
                    #         {'users': [(4, p_user.id)]})
                    if not p_user.has_group('kw_meeting_schedule.group_kw_meeting_schedule_user'):
                        meeting_user_group.sudo().write(
                            {'users': [(4, p_user.id)]})
        return result

    @api.multi
    def write(self, values):
        # print("values are",values)
        print("meeting attendance write called")
        # old_participants = 
        result = super(TrainingAttendanceMeetingIntegration, self).write(values)
        for record in self:
            plan = record.training_id.plan_ids[-1] if record.training_id.plan_ids else False
            if plan:
                # new_participants = record.attendance_detail_ids.mapped(
                #     'participant_id') - plan.participant_ids
                new_participants = record.attendance_detail_ids.mapped('participant_id')
                print("new participant inside meeting",new_participants)

                # survey_user_group = self.env.ref('survey.group_survey_user')
                # dms_user_group = self.env.ref('kw_dms.group_dms_user')
                # training_employee_group = self.env.ref(
                #     'kw_training.group_kw_training_employee')
                # skill_assessment_user_group = self.env.ref(
                #     'kw_skill_assessment.group_kw_skill_assessment_user')

                meeting_user_group = self.env.ref(
                    'kw_meeting_schedule.group_kw_meeting_schedule_user')
                print("meeting user grup is",meeting_user_group)

                for participant_emp in new_participants:
                    print("participant is",participant_emp)
                    plan.write({'participant_ids': [(4, participant_emp.id)]})

                    if participant_emp.user_id:
                        print("emp with user id",participant_emp.user_id)
                        p_user = participant_emp.user_id

                        # if not p_user.has_group('kw_training.group_kw_training_employee'):
                        #     print("not in employee group")
                        #     training_employee_group.sudo().write(
                        #         {'users': [(4, p_user.id)]})
                        # if not p_user.has_group('survey.group_survey_user'):
                        #     print("not in survey group")
                        #     survey_user_group.sudo().write(
                        #         {'users': [(4, p_user.id)]})
                        # if not p_user.has_group('kw_dms.group_dms_user'):
                        #     print("not in dms group")
                        #     dms_user_group.sudo().write(
                        #         {'users': [(4, p_user.id)]})
                        # if not p_user.has_group('kw_skill_assessment.group_kw_skill_assessment_user'):
                        #     print("not in skill group")
                        #     skill_assessment_user_group.sudo().write(
                        #         {'users': [(4, p_user.id)]})
                        if not p_user.has_group('kw_meeting_schedule.group_kw_meeting_schedule_user'):
                            print("not in meeting group")
                            meeting_user_group.sudo().write(
                                {'users': [(4, p_user.id)]})
                            print("executed")

        return result
