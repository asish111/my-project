# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError
from odoo.tools.translate import _

class MeetingScheduleIn(models.Model):
    _inherit = "kw_meeting_events"

    @api.model
    def create(self, values):
        result = super(MeetingScheduleIn, self).create(values)
        if 'active_model' and 'active_id' in self._context and self._context['active_model'] == 'kw_training_schedule':
            session = self.env['kw_training_schedule'].browse(self._context['active_id'])
            if not session.meeting_id:
                session.meeting_id = result.id
        return result

    @api.multi
    def action_meeting_attendance_completed(self):
        for record in self:
            attendance_status = False
            for attendee in record.attendee_ids:
                if attendee.attendance_status:
                    attendance_status = True

            if attendance_status:
                record.write({'state': 'attendance_complete',
                              'employee_ids': [[6, 'false', record.attendee_ids.mapped('employee_id').ids]],
                              'external_participant_ids': [[6, 'false', record.external_attendee_ids.mapped('partner_id').ids]]
                              })
            else:
                raise ValidationError(
                    _('At least one participant must have attended the meeting. '))

            training_schedule = self.env['kw_training_schedule'].sudo().search([('meeting_id','=',record.id)])
            if training_schedule:
                plan = training_schedule.training_id.plan_ids[-1] if training_schedule.training_id.plan_ids else False
                if plan:
                    survey_user_group = self.env.ref('survey.group_survey_user')
                    dms_user_group = self.env.ref('kw_dms.group_dms_user')
                    training_employee_group = self.env.ref('kw_training.group_kw_training_employee')
                    skill_assessment_user_group = self.env.ref('kw_skill_assessment.group_kw_skill_assessment_user')
                    meeting_user_group = self.env.ref(
                                'kw_meeting_schedule.group_kw_meeting_schedule_user')
                    new_participants = record.attendee_ids.mapped('employee_id') - plan.participant_ids

                    
                    for participant_emp in new_participants:

                        plan.write({'participant_ids': [(4, participant_emp.id)]})

                        if participant_emp.user_id:
                            p_user = participant_emp.user_id
                            if not p_user.has_group('kw_training.group_kw_training_employee'):
                                training_employee_group.sudo().write({'users': [(4, p_user.id)]})
                            if not p_user.has_group('survey.group_survey_user'):
                                survey_user_group.sudo().write({'users': [(4, p_user.id)]})
                            if not p_user.has_group('kw_dms.group_dms_user'):
                                dms_user_group.sudo().write({'users': [(4, p_user.id)]})
                            if not p_user.has_group('kw_skill_assessment.group_kw_skill_assessment_user'):
                                skill_assessment_user_group.sudo().write({'users': [(4, p_user.id)]})
                            if not p_user.has_group('kw_meeting_schedule.group_kw_meeting_schedule_user'):
                                meeting_user_group.sudo().write({'users': [(4, p_user.id)]})
        return True
