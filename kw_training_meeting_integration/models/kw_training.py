# -*- coding: utf-8 -*-
from odoo import models,api
from odoo.exceptions import ValidationError

class TrainingIn(models.Model):
    _inherit = "kw_training"

    @api.multi
    def _compute_subject_attended(self):
        uid = self._uid
        emp_id = self.env['hr.employee'].search(
            [('user_id', '=', uid)], limit=1)
        emp_partner_id = emp_id.user_id.partner_id.id if emp_id.user_id else False
        for record in self:
            if record.session_ids:
                record.total_subjects = len(record.session_ids)
                if emp_partner_id:
                    for r in record.session_ids:
                        if r.meeting_id and r.meeting_id.attendee_ids:
                            emp_attended = r.meeting_id.attendee_ids.filtered(
                                lambda r: r.partner_id.id == emp_partner_id and r.attendance_status == True)
                            if emp_attended:
                            # for partner in r.meeting_id.attendee_ids:
                            #     if emp_partner_id == partner.partner_id.id and partner.attendance_status == True:
                                record.subjects_attended += 1
                        elif r.attendance_id and r.attendance_id.attendance_detail_ids:
                            attendance_present = r.attendance_id.attendance_detail_ids.filtered(
                                lambda recs: recs.participant_id.id == emp_id.id and recs.attended == True)
                            if attendance_present:
                                record.subjects_attended += 1
            else:
                record.subjects_attended = 0

    @api.multi
    def unlink(self):
        '''
        cases when a training can't be deleted.
            1-assessment having assessment id from skill
            2-attendance updated for a session
            3-meeting is booked for a session i.e meeting id from meeting schedule
         '''
        for training in self:
            if training.session_ids:
                print('session id is',training.session_ids.mapped('attendance_id'))
                session_having_attendance_id = training.session_ids.filtered(
                    lambda r: r.attendance_id.id >0)
                print("session with attendance id is",session_having_attendance_id)
                if session_having_attendance_id:
                    raise ValidationError(f"Training {training.name} can't be deleted due to \
                        attendance is updated for session {session_having_attendance_id[0].subject}")
                session_having_meeting_id = training.session_ids.filtered(
                    lambda r: r.meeting_id.id >0)
                print('session having meeting id is',session_having_meeting_id)
                if session_having_meeting_id:
                    raise ValidationError(f"Training {training.name} can't be deleted due to \
                        meeting is booked for session {session_having_meeting_id[0].subject}")
            if training.assessment_ids:
                assessment_having_assessment_id = training.assessment_ids.filtered(
                    lambda r: r.assessment_id.id >0)
                print("assessment having assessment id is",assessment_having_assessment_id)
                if assessment_having_assessment_id:
                    raise ValidationError(f"Training {training.name} can't be deleted due to \
                        test is configured for assessment {assessment_having_assessment_id[0].name}")
        result = super(TrainingIn, self).unlink()
        return result
