# -*- coding: utf-8 -*-
from odoo import models,fields,api
from datetime import datetime
from odoo.exceptions import ValidationError

class TrainingSchedule(models.Model):
    _inherit = "kw_training_schedule"

    session_type = fields.Selection(string="Class Room",selection=[
                                        ('inside', 'Indoor'), ('outside', 'Outdoor'), ('online', 'Online')],required=True)
    meeting_id = fields.Many2one(
        "kw_meeting_events",string="Meeting",)
    venue = fields.Char(string="Venue", compute="_compute_if_venue")

    @api.multi
    def _compute_if_venue(self):
        print("venue called")
        for session in self:
            if session.meeting_id:
                print("venue present", session.meeting_id.meeting_room_id.name)
                session.venue = session.meeting_id.meeting_room_id.name
    
    @api.multi
    def _compute_is_attended(self):
        uid = self._uid
        emp_id = self.env['hr.employee'].search(
            [('user_id', '=', uid)], limit=1)
        for record in self:
            if emp_id:
                if record.training_id and record.training_id.plan_ids and record.training_id.plan_ids[0].participant_ids:
                    emp_is_attendee = record.training_id.plan_ids[0].participant_ids.filtered(
                        lambda r: r.id == emp_id.id)
                    if emp_is_attendee:
                        if record.session_type == 'inside':
                            if record.meeting_id and record.meeting_id.state == 'attendance_complete' and record.meeting_id.attendee_ids:
                                emp_attended = record.meeting_id.attendee_ids.filtered(
                                    lambda r: r.employee_id.id == emp_id.id and r.attendance_status == True)
                                record.is_attended = True if emp_attended else False
                                record.attendance_present = "Attended" if emp_attended else "Not Attended"
                            elif record.attendance_id and record.attendance_id.attendance_detail_ids:
                                emp_attended = record.attendance_id.attendance_detail_ids.filtered(
                                    lambda r: r.participant_id.id == emp_id.id and r.attended == True)
                                record.is_attended = True if emp_attended else False
                                record.attendance_present = "Attended" if emp_attended else "Not Attended"
                        else:
                            if record.attendance_id and record.attendance_id.attendance_detail_ids:
                                emp_attended = record.attendance_id.attendance_detail_ids.filtered(
                                    lambda r: r.participant_id.id == emp_id.id and r.attended == True)
                                record.is_attended = True if emp_attended else False
                                record.attendance_present = "Attended" if emp_attended else "Not Attended"
    
    @api.multi
    def schedule_meeting(self):
        current_date = datetime.now().date()
        from_time = self.from_time
        to_time = self.to_time
        meeting_date = self.date
        a = datetime(meeting_date.year, meeting_date.month, meeting_date.day, int(
            from_time.split(':')[0]), int(from_time.split(':')[1]), 0)
        b = datetime(meeting_date.year, meeting_date.month, meeting_date.day, int(
            to_time.split(':')[0]), int(to_time.split(':')[1]), 0)
        c = b-a
        duration = c.seconds/3600
        if duration < 0.5:
            raise ValidationError(
                "At least 30 minutes duration should be given.")
        if to_time < from_time:
            raise ValidationError("To time can't be less than from time.")
        if from_time == to_time:
            raise ValidationError("To time should be greater than from time.")
        if self.date < current_date:
            raise ValidationError(
                "Meeting date should not be less than current date.")
        participants = []
        if self.training_id.plan_ids and self.training_id.plan_ids[-1].participant_ids:
            participants = self.training_id.plan_ids[-1].participant_ids.ids
        view_id = self.env.ref(
            'kw_meeting_schedule.view_kw_meeting_calendar_event_form').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'kw_meeting_events',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': view_id,
            'target': 'self',
            'context': {'default_kw_start_meeting_date': self.date,
                        'default_kw_start_meeting_time': self.from_time,
                        'default_name': self.subject,
                        'default_kw_duration': str(duration),
                        'default_duration': duration,
                        'default_employee_ids': participants,
                        'default_training_session_id': self.id,
                        'default_meeting_category': 'general',
                        # 'default_meeting_type_id': self.env.ref('kw_training_meeting_integration.kw_training_meeting_tag').id,
                        # 'default_categ_ids': [self.env.ref('kw_training_meeting_integration.kw_training_meeting_tag').id],
                        }}

    @api.multi
    def view_meeting(self):
        view_id = self.env.ref(
            'kw_training_meeting_integration.view_kw_training_meeting_calendar_event_form').id
        target_id = self.meeting_id.id
        return {
            'name': 'Meeting Activities',
            'type': 'ir.actions.act_window',
            'res_model': 'kw_meeting_events',
            'res_id': target_id,
            'view_type': 'form',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'flags': {'mode': 'readonly'},
        }

    @api.multi
    def view_attendance(self):
        '''Check if session has meeting_id if so, then open the attendance view of the meeting.'''
        if self.session_type == 'inside':
            # Check if meeting is booked
            if self.meeting_id:
                meeting_attendance_view_id = self.env.ref(
                    'kw_meeting_schedule.view_meeting_schedule_take_action_form').id
                target_id = self.meeting_id.id
                return {
                    'name': 'Meeting Activities',
                    'type': 'ir.actions.act_window',
                    'res_model': 'kw_meeting_events',
                    'res_id': target_id,
                    'view_type': 'form',
                    'views': [(meeting_attendance_view_id, 'form')],
                    'view_id': meeting_attendance_view_id,
                    'flags': {'action_buttons': True, 'mode': 'edit'},
                }
            # Check if attendance is taken manually
            elif self.attendance_id:
                attendance_form_view_id = self.env.ref(
                    "kw_training.view_kw_training_attendance_form").id
                # participants = self.training_id.plan_ids[0].participant_ids
                return {
                    'name': 'Attendance',
                    'type': 'ir.actions.act_window',
                    'res_model': 'kw_training_attendance',
                    'res_id': self.attendance_id.id,
                    'view_type': 'form',
                    'views': [(attendance_form_view_id, 'form')],
                    'view_id': attendance_form_view_id,
                    'flags': {'action_buttons': True, 'mode': 'edit'},
                }
            # Return form view for attendance
            else:
                attendance_form_view_id = self.env.ref(
                    "kw_training.view_kw_training_attendance_form").id
                participants = self.training_id.plan_ids[0].participant_ids
                return {
                    'name': 'Attendance',
                    'type': 'ir.actions.act_window',
                    'res_model': 'kw_training_attendance',
                    'view_type': 'form',
                    'views': [(attendance_form_view_id, 'form')],
                    'view_id': attendance_form_view_id,
                    'flags': {'action_buttons': True, 'mode': 'edit'},
                    'context': {
                        'default_training_id': self.training_id.id,
                        'default_attendance_detail_ids': [[0, 0, {"participant_id": participant.id, "attended": False}] for participant in participants],
                        'default_session_id': self.id,
                    }
                }

        else:
            attendance_form_view_id = self.env.ref(
                "kw_training.view_kw_training_attendance_form").id
            participants = self.training_id.plan_ids[0].participant_ids
            _action = {
                'name': 'Attendance',
                'type': 'ir.actions.act_window',
                'res_model': 'kw_training_attendance',
                'view_type': 'form',
                'views': [(attendance_form_view_id, 'form')],
                'view_id': attendance_form_view_id,
                'flags': {'action_buttons': True, 'mode': 'edit'},
            }
            if self.attendance_id:
                _action['res_id'] = self.attendance_id.id
            else:
                _action['context'] = {
                    'default_training_id': self.training_id.id,
                    'default_attendance_detail_ids': [[0, 0, {"participant_id": participant.id, "attended": False}] for participant in participants],
                    'default_session_id': self.id,
                }
            return _action

    @api.multi
    def unlink(self):
        for session in self:
            if session.session_type == 'inside':
                # if attendance is taken in meeting
                if session.meeting_id and session.meeting_id.state == 'attendance_complete':
                    raise ValidationError(
                        f"Attendance for  {session.subject} is updated.Hence it can't be deleted.")
                # if Attendance is taken manually
                elif session.attendance_id:
                    raise ValidationError(
                        f"Attendance for  {session.subject} is updated.Hence it can't be deleted.")
            else:
                if session.attendance_id:
                    raise ValidationError(
                        f"Attendance for  {session.subject} is updated.Hence it can't be deleted.")

        result = super(TrainingSchedule, self).unlink()

        return result
