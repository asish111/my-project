# -*- coding: utf-8 -*-
{
    'name': "Kwantify Training Meeting Integration",
    'summary': """This module is used to integrate training module with meeting schedule module.""",
    'author': "CSM Technologies",
    'website': "http://www.csm.co.in",
    'category': 'Meeting/Integration',
    'version': '0.1',
    'depends': ['base','kw_training','kw_meeting_schedule'],
    'data': [
            'views/kw_training_schedule_view.xml',
            # 'data/kw_meeting_tag_data.xml',
            'views/kw_training_meeting_view.xml',
            ],
    "application": False,
    "installable": True,
    "auto_install": False,
}
