{
    'name': "Kwantify Project Dashboard",
    'description': "Dashboard Panel of Kwantify",
    'website': "www.csm.co.in",
    'category': 'Kwantify',
    'version': '0.1',
    'depends': [
        'base',
        'hr'
    ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/kw_project_dashboard_report.xml',
    ],
    'qweb': [
        "static/src/xml/kw_project_dashboard.xml",
    ],
    'installable': True,
    'application': True,
}