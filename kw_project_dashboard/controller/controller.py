from odoo import http
from odoo.http import request
import requests

class kw_project_dashboard_data(http.Controller):
    @http.route('/get_project_dashboard_data', type='json', auth="user", website=True)
    def get_project_dashboard_data(self, **args):
        project_dashboard_data = dict()
        project_dashboard_data['projectinfo_dict'] = []
        project_dashboard_data['moduleinfo_dict'] = []
        project_dashboard_data['billinginfo_dict'] = []
        project_dashboard_data['comp_budget_exp_dict'] = []

        #  calling api to pull data for project info
        try:
            proj_info_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetProjectInfoData/522").json()
            #  selecting clsProjectInfo list
            proj_info = proj_info_result["GetProjectInfoDataResult"][0]["clsProjectInfo"][0]

            #  mapping values of dictionary to kw_project_info model
            # vals = {
            #     'effort': proj_info['Effort'] if 'Effort' in proj_info else False,
            #     'lifecycle': proj_info['LifeCycle'] if 'LifeCycle' in proj_info else False,
            #     'proj_manager': proj_info['ProjectManager'] if 'ProjectManager' in proj_info else False,
            #     'proj_name': proj_info['ProjectName'] if 'ProjectName' in proj_info else False,
            #     'schedule': proj_info['Schedule'] if 'Schedule' in proj_info else False,
            #     'size': proj_info['Size'] if 'Size' in proj_info else False,
            #     'wo_code': proj_info['WorkOrderCode'] if 'WorkOrderCode' in proj_info else False
            # }

            # request.env['kw_project_info'].sudo().create(vals)

            # projects = request.env['kw_project_info'].sudo().search([],limit=1)

            project_dashboard_data['projectinfo_dict'].append({'effort': proj_info['Effort'], 'lifecycle': proj_info['LifeCycle'],
                                                        'proj_manager': proj_info['ProjectManager'], 'proj_name': proj_info['ProjectName'],
                                                        'schedule': proj_info['Schedule'],'size': proj_info['Size'],
                                                        'wo_code': proj_info['WorkOrderCode']})
        except:
            print("Project Info service is not working!")
            pass

        # Project Goal
        # try:
        #     proj_role_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/ProjectGoal/522").json()
        #     proj_role = proj_role_result['']

        # except:
        #     print("Project Goal service is not working!")
        #     pass

        #  Module Info
        try:
            modules_info_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetModuleInfoData/522").json()
            modules_info = modules_info_result['GetModuleInfoDataResult']

            # for module_info in modules_info:
            #     vals = {
            #         'code_prod': module_info['CodeProd'] if 'CodeProd' in module_info else False,
            #         'dre': module_info['DRE'] if 'DRE' in module_info else False,
            #         'name': module_info['ModuleName'] if 'ModuleName' in module_info else False,
            #         'productivity': module_info['Productivity'] if 'Productivity' in module_info else False,
            #         'rdc': module_info['RDC'] if 'RDC' in module_info else False,
            #         'size': module_info['Size'] if 'Size' in module_info else False,
            #         'status': module_info['Status'] if 'Status' in module_info else False,
            #         'tdl': module_info['TDL'] if 'TDL' in module_info else False,
            #         'testing_prod': module_info['TestingProd'] if 'TestingProd' in module_info else False
            #     }
            #     request.env['kw_module_info'].sudo().create(vals)
            #
            # modules = request.env['kw_module_info'].sudo().search([], limit=15)

            # for record in modules:
            for module in modules_info:
                project_dashboard_data['moduleinfo_dict'].append({'code_prod': module['CodeProd'],
                                                                 'dre': module['DRE'], 'name': module['ModuleName'], 'productivity': module['Productivity'],
                                                                  'rdc': module['RDC'], 'size': module['Size'], 'status': module['Status'],
                                                                  'tdl': module['TDL'], 'testing_prod': module['TestingProd']})
        except:
            print("Module info service is not working")
            pass

        #  Billing Info
        try:
            billings_info_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/BillingCollectionInfo/522").json()
            billings_info = billings_info_result['BillingCollectionInfoResult']
            # for billing_info in billings_info:
            #     vals = {
            #         'collection_amnt': billing_info['CollectionAmt'] if 'CollectionAmt' in billing_info else False,
            #         'invoice_amnt': billing_info['InvoiceAmount'] if 'InvoiceAmount' in billing_info else False,
            #         'invoice_dt': billing_info['InvoiceDate'] if 'InvoiceDate' in billing_info else False,
            #         'invoice_details': billing_info['InvoiceDetails'] if 'InvoiceDetails' in billing_info else False,
            #         'invoice_no': billing_info['InvoiceNo'] if 'InvoiceNo' in billing_info else False,
            #         'pending_amnt': billing_info['PendingAmt'] if 'PendingAmt' in billing_info else False,
            #         'wo_code': billing_info['WorkorderCode'] if 'WorkorderCode' in billing_info else False
            #     }
            #     request.env['kw_billing_collection'].sudo().create(vals)

            # billings = request.env['kw_billing_collection'].sudo().search([], limit=8)
            for billing in billings_info:
                project_dashboard_data['billinginfo_dict'].append({'collection_amnt': billing['CollectionAmt'],
                                                                   'invoice_amnt': billing['InvoiceAmount'], 'invoice_dt': billing['InvoiceDate'],
                                                                   'invoice_details': billing['InvoiceDetails'], 'invoice_no': billing['InvoiceNo'],
                                                                   'pending_amnt': billing['PendingAmt'], 'wo_code': billing['WorkorderCode']})
        except:
            print("Billing Info service is not working")
            pass

        #  component wise budget vs expense
        comp_budget_exps_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/BudgetExp/522").json()
        comp_budget_exps = comp_budget_exps_result['BudgetExpResult']

        # for comp_budget_exp in comp_budget_exps:
        #     vals = {
        #         'budget': comp_budget_exp['Budget'] if 'Budget' in comp_budget_exp else False,
        #         'component': comp_budget_exp['ComponentName'] if 'ComponentName' in comp_budget_exp else False,
        #         'expenses': comp_budget_exp['Expenses'] if 'Expenses' in comp_budget_exp else False,
        #     }
        #     request.env['kw_budget_exp'].sudo().create(vals)
        #
        # budget_exps = request.env['kw_budget_exp'].sudo().search([])
        # for record in budget_exps:
        #     for budget_exp in record:
        #         project_dashboard_data['comp_budget_exp_dict'].append({'id': budget_exp.id, 'budget': budget_exp.budget,
        #                                                                'component': budget_exp.component, 'expenses': budget_exp.expenses})

        total_exp, total_budge = 0.00, 0.00
        for comp_budget_exp in comp_budget_exps:
            total_exp += float(comp_budget_exp['Expenses'])
            total_budge += float(comp_budget_exp['Budget'])
            project_dashboard_data['comp_budget_exp_dict'].append({'budget': comp_budget_exp['Budget'], 'component': comp_budget_exp['ComponentName'],
                                                                   'expenses': comp_budget_exp['Expenses']})
        project_dashboard_data['comp_budget_exp_dict'].append({'total_exp': total_exp, 'total_budge': total_budge})

        # resourcewise count
        resource_wise_count_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/ResourcewiseCount/522?_=1591941548890").json()
        resource_wise_count_results = resource_wise_count_result['ResourcewiseCountResult']

        request.env['kw_resourcewise_count'].sudo().search([]).unlink()

        for resource_count in resource_wise_count_results:
            vals = {
                'count': resource_count['Count'], 
                'proj_role': resource_count['ProjectRole'],
            }
            request.env['kw_resourcewise_count'].sudo().create(vals)

        # rolewise task percent
        rolewise_task_percent_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetRoleWiseTaskPercentage/522").json()
        rolewise_task_percents = rolewise_task_percent_result['GetRoleWiseTaskPercentageResult']

        request.env['kw_rolewise_task_percentage'].sudo().search([]).unlink()

        for rolewise_task_percent in rolewise_task_percents:
            vals = {
                'min_effort': rolewise_task_percent['EffortInMin'],
                'proj_role': rolewise_task_percent['ProjRole'],
                'sl_no': rolewise_task_percent['SlNo']
            }
            
            request.env['kw_rolewise_task_percentage'].sudo().create(vals)

        # task status
        task_status_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetTaskStatus/522").json()
        task_status = task_status_result["GetTaskStatusResult"][0]
        
        request.env['kw_task_status'].sudo().search([]).unlink()
        
        for k, v in task_status.items():
            vals = {
                'stage': k,
                'value': v
            }
            request.env['kw_task_status'].sudo().create(vals)
        
        # risk status
        risk_status_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetRiskStatus/522").json()
        risk_status = risk_status_result['GetRiskStatusResult']

        request.env['kw_risk_status'].sudo().search([]).unlink()

        for record in risk_status:
            vals = {
                'count': record['Count'],
                'status': record['Status']
            }
            request.env['kw_risk_status'].sudo().create(vals)

        # issue status
        issue_status_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetIssueStatus/522?_=1591941549872").json()
        issue_status_results = issue_status_result['GetIssueStatusResult']

        request.env['kw_issue_status'].sudo().search([]).unlink()

        for issue_status in issue_status_results:
            vals = {
                'issue_count1': issue_status['IssueCount1'], 
                'issue_count2': issue_status['IssueCount2'],
            }
            request.env['kw_issue_status'].sudo().create(vals)

        # issue review detail
        issue_review_detail = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetIssueReviewDetails/522?_=1591941550042").json()
        issue_review_details = issue_review_detail['GetIssueReviewDetailsResult']

        request.env['kw_issue_review_details'].sudo().search([]).unlink()

        for issue_review in issue_review_details:
            vals = {
                'issue_count1': issue_review['IssueCount1'],
            }
            request.env['kw_issue_review_details'].sudo().create(vals)
            
        # issue status result
        issue_status_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetDependencyStatus/522?_=1591941593450").json()
        issue_status_results = issue_status_result['GetDependencyStatusResult']

        request.env['kw_dependency_status'].sudo().search([]).unlink()

        for issue_status in issue_status_results:
            vals = {
                'dependency_count1': issue_status['DependencyCount1'], 
                'dependency_count2': issue_status['DependencyCount2'],
            }
            request.env['kw_dependency_status'].sudo().create(vals)

        # test case type
        test_case_type_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetTestCaseType/522").json()
        test_case_type = test_case_type_result['GetTestCaseTypeResult'][0]

        request.env['kw_testcase_type'].sudo().search([]).unlink()

        for k,v in test_case_type.items():
            vals = {
                'name': k,
                'value': v
            }
            request.env['kw_testcase_type'].sudo().create(vals)

        # defects (injected stage wise)
        injected_stagewise_defect_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetDefects/522").json()
        injected_stagewise_defect = injected_stagewise_defect_result['GetDefectsResult']
        
        request.env['kw_defects_injected_stagewise'].sudo().search([]).unlink()

        for stagewise_defect in injected_stagewise_defect:
            vals = {
                'close_count': stagewise_defect['CloseCount'],
                'count': stagewise_defect['Count'],
                'open_count': stagewise_defect['OpenCount'],
                'phase_name': stagewise_defect['PhaseName']
            }
    
            request.env['kw_defects_injected_stagewise'].sudo().create(vals)

        # defects (defected stage wise)
        defected_stagewise_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetDefectedStageWise/522").json()
        defected_stagewise = defected_stagewise_result['GetDefectedStageWiseResult']
        
        request.env['kw_defected_stagewise'].sudo().search([]).unlink()

        for defect in defected_stagewise:
            vals = {
                'close_count': defect['CloseCount'],
                'count': defect['Count'],
                'open_count': defect['OpenCount'],
                'phase_name': defect['PhaseName']
            }
    
            request.env['kw_defected_stagewise'].sudo().create(vals)

        # review coverage
        review_coverage_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetAllReviewCoverage/522").json()
        review_coverage = review_coverage_result['GetAllReviewCoverageResult'][0]

        request.env['kw_all_review_coverage'].sudo().search([]).unlink()

        for k, v in review_coverage.items():
            vals = {
                'name': k,
                'value': v
            }

            request.env['kw_all_review_coverage'].sudo().create(vals)

        # modulewise testcase summary
        modulewise_testcase_result = requests.get("https://kwportalservice.csmpl.com/DashboardSVC.svc/GetModuleWiseTestCaseSummary/522").json()
        modulewise_testcase = modulewise_testcase_result['GetModuleWiseTestCaseSummaryResult']

        request.env['kw_modulewise_testcase_summary'].sudo().search([]).unlink()

        for module in modulewise_testcase:
            vals = {
                'module': module['ModuleName'],
                'total_bug_count': module['TotalBugCount'],
                'total_fun_bug': module['TotalFunBug'],
                'total_non_fun_bug': module['TotalNonFunBug']
            }
            request.env['kw_modulewise_testcase_summary'].sudo().create(vals)



        return project_dashboard_data