odoo.define('kw_project_dashboard.kw_project_dashboard_report', function (require) {
    "use strict";

    var core = require('web.core');
    var framework = require('web.framework');
    var session = require('web.session');
    var ajax = require('web.ajax');
    var ActionManager = require('web.ActionManager');
    var view_registry = require('web.view_registry');
    var Widget = require('web.Widget');
    var AbstractAction = require('web.AbstractAction');
    var ControlPanelMixin = require('web.ControlPanelMixin');
    var QWeb = core.qweb;
    var rpc = require('web.rpc')

    var KwantifyProjectDashboardView = AbstractAction.extend(ControlPanelMixin, {

        init: function(parent, value) {
            this._super(parent, value);
            var employee_data = [];
            var self = this;
            if (value.tag == 'kw_project_dashboard.kw_project_dashboard_report') {
                ajax.jsonRpc("/get_project_dashboard_data", 'call')
                .then(function(result){
                    // console.log("Results",result)
                    self.dashboard_data = result
                }).done(function(){
                    self.render();
                    self.render_rolewise_task_graph();
                    self.render_issue_status();
                    self.render_dependency_status();
                    self.render_resource_wise_count();
                    self.render_issue_review_status();
                    self.get_task_status();
                    self.get_risk_status();
                    self.get_testcase_type();
                    self.render_defects_injected_stagewise();
                    self.render_defected_stagewise();
                    self.render_review_coverage();
                    self.render_modulewise_testcase_summary();
                    self.href = window.location.href;
                });
            }
        },
        willStart: function() {
             return $.when(ajax.loadLibs(this), this._super());
        },
        start: function() {
            var self = this;
            return this._super();
        },
        render: function() {
            var super_render = this._super;
            var self = this;
            var kw_project_dashboard = QWeb.render( 'kw_project_dashboard.kw_project_dashboard_report', {
                widget: self,
            });
            $( ".o_control_panel" ).addClass( "o_hidden" );
            $(kw_project_dashboard).prependTo(self.$el);
            return kw_project_dashboard
        },
        reload: function () {
                window.location.href = this.href;
        },
        render_resource_wise_count:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_resource_wise_count",
            }).then(function (data) {
                console.log("resourcewise count", data);
                Highcharts.chart('project_team_structure_graph', {
                    chart: {
                      plotBackgroundColor: null,
                      plotBorderWidth: null,
                      plotShadow: false,
                      type: 'pie'
                    },
                    title: {
                      text: 'Project Team Structure'
                    },
                    tooltip: {
                      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                      point: {
                        valueSuffix: '%'
                      }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                      pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                          enabled: true,
                          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                      }
                    },
                    series: [{
                      name: null,
                      colorByPoint: true,
                      data: [{
                        name: data[0]['name'],
                        y: data[0]['value'],
                        sliced: true,
                        selected: true
                      }, {
                        name: data[1]['name'],
                        y: data[1]['value']
                      }, {
                        name: data[2]['name'],
                        y: data[2]['value']
                      }, {
                        name: data[3]['name'],
                        y: data[3]['value']
                      }, {
                        name: data[4]['name'],
                        y: data[4]['value']
                      }, {
                        name: data[5]['name'],
                        y: data[5]['value']
                      }]
                    }]
                  });
            });
        },

        render_rolewise_task_graph:function(){
           rpc.query({
               model: "kw_project_dashboard",
               method: "get_rolewise_task",
           }).then(function (data) {
               console.log("role wise task", data)
                Highcharts.chart('rolewise_task_graph', {
                    chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                    },
                    title: {
                    text: 'Role Wise Task Percent'
                    },
                    tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                      enabled: false
                    },
                    accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                    },
                    plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                    },
                    series: [{
                    name: null,
                    colorByPoint: true,
                    data: [{
                        name: data[0]['name'],
                        y: data[0]['value'],
                        sliced: true,
                        selected: true
                    }, {
                        name: data[1]['name'],
                        y: data[1]['value']
                    }, {
                        name: data[2]['name'],
                        y: data[2]['value']
                    }, {
                        name: data[3]['name'],
                        y: data[3]['value']
                    }, {
                        name: data[4]['name'],
                        y: data[4]['value']
                    }, {
                        name: data[5]['name'],
                        y: data[5]['value']
                    }, {
                        name: data[6]['name'],
                        y: data[6]['value']
                    }, {
                        name: data[7]['name'],
                        y: data[7]['value']
                    }, {
                        name: data[8]['name'],
                        y: data[8]['value']
                    }]
                    }]
                });
           });
        },

        get_task_status:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_task_status",
            }).then(function (data) {
                console.log(data)
                Highcharts.chart('task_status', {
                    chart: {
                      type: 'column'
                    },
                    title: {
                        text: 'Task Status'
                    },
                    accessibility: {
                      announceNewData: {
                        enabled: true
                      }
                    },
                    xAxis: {
                      type: 'category'
                    },
                    yAxis: {
                        text: false
                    },
                    legend: {
                      enabled: true
                    },
                    plotOptions: {
                      series: {
                        borderWidth: 0,
                        dataLabels: {
                          enabled: true,
                          format: '{point.y}'
                        }
                      }
                    },
                  
                    tooltip: {
                      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                    },
                    credits: {
                        enabled: false
                    },
                  
                    series: [
                      {
                        colorByPoint: true,
                        data: [
                            {
                              name: data[0].stage,
                              y: data[0].value,
                              drilldown: data[0].stage
                            },
                            {
                              name: data[1].stage,
                              y: data[1].value,
                              drilldown: data[1].stage
                            },
                            {
                              name: data[2].stage,
                              y: data[2].value,
                              drilldown: data[2].stage
                            },
                            {
                              name: data[3].stage,
                              y: data[3].value,
                              drilldown: data[3].stage
                            }
                          ]
                      }
                    ],
                  });
            });
        },

        get_risk_status:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "risk_status",
            }).then(function(data){
                console.log("risk status", data)
                Highcharts.chart('risk_status', {
                    chart: {
                      type: 'column'
                    },
                    title: {
                      text: 'Risk Status'
                    },
                    xAxis: {
                      categories: [
                        'Low',
                        'Medium',
                        'High'
                      ],
                      crosshair: true
                    },
                    yAxis: {
                      min: 0,
                      title: {
                        text: 'Values'
                      }
                    },
                    tooltip: {
                      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                      footerFormat: '</table>',
                      shared: true,
                      useHTML: true
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                      column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                      }
                    },
                    series: [{
                      name: 'Open',
                      data: [0.0, 0.0, 0.0]
                  
                    }, {
                      name: 'Close',
                      data: [data[0]['counts'][0], data[0]['counts'][1], data[0]['counts'][2]]
                  
                    }]
                  });
            })
        },
           
        render_issue_status:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_issue_status",
            }).then(function (data) {
                console.log("issue status", data);
                Highcharts.chart('issue_status_graph', {
                    chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                    },
                    title: {
                    text: 'Issue Status'
                    },
                    tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                        enabled: false
                    },
                    accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                    },
                    plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                        enabled: false
                        },
                        showInLegend: true
                    }
                    },
                    series: [{
                    colorByPoint: true,
                    data: [{
                        name: data[0]['name'],
                        y: data[0]['value'],
                        sliced: true,
                        selected: true
                    }]
                    }]
                });
            });
        },

        render_dependency_status:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_dependency_status",
            }).then(function (data) {
                console.log("dependency status", data);
                Highcharts.chart('dependency_status_graph', {
                    chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                    },
                    title: {
                    text: 'Dependency Status'
                    },
                    tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                        enabled: false
                    },
                    accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                    },
                    plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                        enabled: false
                        },
                        showInLegend: true
                    }
                    },
                    series: [{
                    colorByPoint: true,
                    data: [{
                        name: data[0]['name'],
                        y: data[0]['value'],
                        sliced: true,
                        selected: true
                    }]
                    }]
                });
            });
         },

        render_issue_review_status:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_issue_review",
            }).then(function(data){
                console.log("issue_review_status", data)
                Highcharts.chart('issue_review_details_graph', {
                    chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                    },
                    title: {
                    text: 'Issue Review Status'
                    },
                    tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    credits: {
                        enabled: false
                    },
                    accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                    },
                    plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                        enabled: false
                        },
                        showInLegend: true
                    }
                    },
                    series: [{
                    colorByPoint: true,
                    data: [{
                        name: data[0]['name'],
                        y: data[0]['value'],
                        sliced: true,
                        selected: true
                    },
                    {
                        name: data[1]['name'],
                        y: data[1]['value'],
                        sliced: true,
                        selected: true
                    },
                    {
                        name: data[2]['name'],
                        y: data[2]['value'],
                        sliced: true,
                        selected: true
                    }]
                    }]
                });
            });
        },
        get_testcase_type:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_testcase_type",
            }).then(function(data){
                console.log("testcase type", data)
                Highcharts.chart('test_case_type', {
                    chart: {
                      type: 'pie',
                      options3d: {
                        enabled: true,
                        alpha: 45
                      }
                    },
                    title: {
                      text: 'Test Case Type'
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                      pie: {
                        innerSize: 100,
                        depth: 45
                      }
                    },
                    series: [{
                      name: 'Delivered amount',
                      data: [
                        [data[0]['name'], data[0]['value']],
                        [data[1]['name'], data[1]['value']]
                      ]
                    }]
                  });

            })
        },

        render_defects_injected_stagewise:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_injected_stagewise_defects",
            }).then(function(data){
                console.log("defects_injected_stagewise", data)
                Highcharts.chart('defects_injected_stagewise', {
                    colors: ['#FFD700', '#CD7F32'],
                    chart: {
                      type: 'column',
                      inverted: true,
                      polar: true
                    },
                    title: {
                      text: 'Injected Stage Wise Defects'
                    },
                    tooltip: {
                      outside: true
                    },
                    credits: {
                        enabled: false
                    },
                    pane: {
                      size: '85%',
                      innerSize: '20%',
                      endAngle: 270
                    },
                    xAxis: {
                      tickInterval: 1,
                      labels: {
                        align: 'right',
                        useHTML: true,
                        allowOverlap: true,
                        step: 1,
                        y: 3,
                        style: {
                          fontSize: '13px'
                        }
                      },
                      lineWidth: 0,
                      categories: [
                        data[0]['name'],
                        data[1]['name'],
                        data[2]['name'],
                        data[3]['name'],
                        data[4]['name']
                      ]
                    },
                    yAxis: {
                      crosshair: {
                        enabled: true,
                        color: '#333'
                      },
                      lineWidth: 0,
                      tickInterval: 25,
                      reversedStacks: false,
                      endOnTick: true,
                      showLastLabel: true
                    },
                    plotOptions: {
                      column: {
                        stacking: 'normal',
                        borderWidth: 0,
                        pointPadding: 0,
                        groupPadding: 0.15
                      }
                    },
                    series: [{
                      name: 'Injected Stage Open',
                      data: [data[0]['open_count'], data[1]['open_count'],
                            data[2]['open_count'], data[3]['open_count'], 
                            data[4]['open_count']]
                    }, {
                      name: 'Injected Stage Close',
                      data: [data[0]['close_count'], data[1]['close_count'],
                            data[2]['close_count'], data[3]['close_count'], 
                            data[4]['close_count']]
                    }]
                  });
            });
        },

        render_defected_stagewise:function(){
            rpc.query({
                model: "kw_project_dashboard",
                method: "get_defected_stagewise",
            }).then(function(data){
                console.log("defected_stagewise", data)
                Highcharts.chart('stagewise_defected', {
                    colors: ['#FFD700', '#CD7F32'],
                    chart: {
                      type: 'column',
                      inverted: true,
                      polar: true
                    },
                    title: {
                      text: 'Injected Stage Wise Defects'
                    },
                    tooltip: {
                      outside: true
                    },
                    credits: {
                        enabled: false
                    },
                    pane: {
                      size: '85%',
                      innerSize: '20%',
                      endAngle: 270
                    },
                    xAxis: {
                      tickInterval: 1,
                      labels: {
                        align: 'right',
                        useHTML: true,
                        allowOverlap: true,
                        step: 1,
                        y: 3,
                        style: {
                          fontSize: '13px'
                        }
                      },
                      lineWidth: 0,
                      categories: [
                        data[0]['name'],
                        data[1]['name'],
                        data[2]['name'],
                        data[3]['name'],
                        data[4]['name']
                      ]
                    },
                    yAxis: {
                      crosshair: {
                        enabled: true,
                        color: '#333'
                      },
                      lineWidth: 0,
                      tickInterval: 25,
                      reversedStacks: false,
                      endOnTick: true,
                      showLastLabel: true
                    },
                    plotOptions: {
                      column: {
                        stacking: 'normal',
                        borderWidth: 0,
                        pointPadding: 0,
                        groupPadding: 0.15
                      }
                    },
                    series: [{
                      name: 'Defected Stage Open',
                      data: [data[0]['open_count'], data[1]['open_count'],
                            data[2]['open_count'], data[3]['open_count'], 
                            data[4]['open_count']]
                    }, {
                      name: 'Defected Stage Close',
                      data: [data[0]['close_count'], data[1]['close_count'],
                            data[2]['close_count'], data[3]['close_count'], 
                            data[4]['close_count']]
                    }]
                  });
            });
        },

        render_review_coverage:function(){
          rpc.query({
              model: "kw_project_dashboard",
              method: "get_review_coverage",
          }).then(function (data) {
              console.log("review_coverage", data)
              function renderIcons() {

                // Move icon
                if (!this.series[0].icon) {
                  this.series[0].icon = this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
                    .attr({
                      stroke: '#303030',
                      'stroke-linecap': 'round',
                      'stroke-linejoin': 'round',
                      'stroke-width': 2,
                      zIndex: 10
                    })
                    .add(this.series[2].group);
                }
                this.series[0].icon.translate(
                  this.chartWidth / 2 - 10,
                  this.plotHeight / 2 - this.series[0].points[0].shapeArgs.innerR -
                    (this.series[0].points[0].shapeArgs.r - this.series[0].points[0].shapeArgs.innerR) / 2
                );

                // Exercise icon
                if (!this.series[1].icon) {
                  this.series[1].icon = this.renderer.path(
                    ['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8,
                      'M', 8, -8, 'L', 16, 0, 8, 8]
                  )
                    .attr({
                      stroke: '#ffffff',
                      'stroke-linecap': 'round',
                      'stroke-linejoin': 'round',
                      'stroke-width': 2,
                      zIndex: 10
                    })
                    .add(this.series[2].group);
                }
                this.series[1].icon.translate(
                  this.chartWidth / 2 - 10,
                  this.plotHeight / 2 - this.series[1].points[0].shapeArgs.innerR -
                    (this.series[1].points[0].shapeArgs.r - this.series[1].points[0].shapeArgs.innerR) / 2
                );

                // Stand icon
                if (!this.series[2].icon) {
                  this.series[2].icon = this.renderer.path(['M', 0, 8, 'L', 0, -8, 'M', -8, 0, 'L', 0, -8, 8, 0])
                    .attr({
                      stroke: '#303030',
                      'stroke-linecap': 'round',
                      'stroke-linejoin': 'round',
                      'stroke-width': 2,
                      zIndex: 10
                    })
                    .add(this.series[2].group);
                }

                this.series[2].icon.translate(
                  this.chartWidth / 2 - 10,
                  this.plotHeight / 2 - this.series[2].points[0].shapeArgs.innerR -
                    (this.series[2].points[0].shapeArgs.r - this.series[2].points[0].shapeArgs.innerR) / 2
                );

                // Stand icon
                if (!this.series[2].icon) {
                  this.series[2].icon = this.renderer.path(['M', 0, 8, 'L', 0, -8, 'M', -8, 0, 'L', 0, -8, 8, 0])
                    .attr({
                      stroke: '#303030',
                      'stroke-linecap': 'round',
                      'stroke-linejoin': 'round',
                      'stroke-width': 2,
                      zIndex: 10
                    })
                    .add(this.series[2].group);
                }

                this.series[2].icon.translate(
                  this.chartWidth / 2 - 10,
                  this.plotHeight / 2 - this.series[2].points[0].shapeArgs.innerR -
                    (this.series[2].points[0].shapeArgs.r - this.series[2].points[0].shapeArgs.innerR) / 2
                );

              }

              Highcharts.chart('review_coverage', {

                chart: {
                  type: 'solidgauge',
                  // height: '110%',
                  events: {
                    render: renderIcons
                  }
                },

                title: {
                  text: 'Activity',
                  style: {
                    fontSize: '24px'
                  }
                },
                credits: {
                    enabled: false
                },

                tooltip: {
                  borderWidth: 0,
                  backgroundColor: 'none',
                  shadow: false,
                  style: {
                    fontSize: '16px'
                  },
                  valueSuffix: '%',
                  pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
                  positioner: function (labelWidth) {
                    return {
                      x: (this.chart.chartWidth - labelWidth) / 2,
                      y: (this.chart.plotHeight / 2) + 15
                    };
                  }
                },

                pane: {
                  startAngle: 0,
                  endAngle: 360,
                  background: [{ // Track for Move
                    outerRadius: '120%',
                    innerRadius: '105%',
                    backgroundColor: Highcharts.color(Highcharts.getOptions().colors[0])
                      .setOpacity(0.3)
                      .get(),
                    borderWidth: 0
                  }, { // Track for Exercise
                    outerRadius: '100%',
                    innerRadius: '85%',
                    backgroundColor: Highcharts.color(Highcharts.getOptions().colors[2])
                      .setOpacity(0.3)
                      .get(),
                    borderWidth: 0
                  }, { // Track for Stand
                    outerRadius: '80%',
                    innerRadius: '65%',
                    backgroundColor: Highcharts.color(Highcharts.getOptions().colors[6])
                      .setOpacity(0.3)
                      .get(),
                    borderWidth: 0
                  }, { // Track for Stand
                    outerRadius: '60%',
                    innerRadius: '45%',
                    backgroundColor: Highcharts.color(Highcharts.getOptions().colors[5])
                      .setOpacity(0.3)
                      .get(),
                    borderWidth: 0
                  }
                ]
                },

                yAxis: {
                  min: 0,
                  max: 100,
                  lineWidth: 0,
                  tickPositions: []
                },

                plotOptions: {
                  solidgauge: {
                    dataLabels: {
                      enabled: false
                    },
                    linecap: 'round',
                    stickyTracking: false,
                    rounded: true
                  }
                },

                series: [{
                  name: data[3]['name'],
                  data: [{
                    color: Highcharts.getOptions().colors[0],
                    radius: '120%',
                    innerRadius: '105%',
                    y: data[3]['value']
                  }]
                }, {
                  name: data[1]['name'],
                  data: [{
                    color: Highcharts.getOptions().colors[2],
                    radius: '100%',
                    innerRadius: '85%',
                    y: data[1]['value']
                  }]
                }, {
                  name: data[0]['name'],
                  data: [{
                    color: Highcharts.getOptions().colors[6],
                    radius: '80%',
                    innerRadius: '65%',
                    y: data[0]['value']
                  }]
                },{
                  name: data[2]['name'],
                  data: [{
                    color: Highcharts.getOptions().colors[5],
                    radius: '60%',
                    innerRadius: '45%',
                    y: data[2]['value']
                  }]
                }]
              });
          });
        },

        render_modulewise_testcase_summary:function(){
          rpc.query({
            model: "kw_project_dashboard",
            method: "get_modulewise_testcase_summary",
          }).then(function (data) {
              console.log("modulewise_testcase_summary", data)
              Highcharts.chart('modulewise_testcase', {
                chart: {
                  type: 'column'
                },
                title: {
                  text: 'Modulewise Testcase Summary'
                },
                xAxis: {
                  categories: [data[0]['module'], data[1]['module'], data[2]['module'], data[3]['module'],
                              data[4]['module'], data[5]['module'], data[6]['module'], data[7]['module'],
                              data[8]['module'], data[9]['module'], data[10]['module'], data[11]['module'],
                              data[12]['module'], data[13]['module'], data[14]['module']]
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: null
                  },
                  stackLabels: {
                    enabled: true,
                    style: {
                      fontWeight: 'bold',
                      color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                      ) || 'gray'
                    }
                  }
                },
                legend: {
                  align: 'right',
                  x: -30,
                  verticalAlign: 'top',
                  y: 25,
                  floating: true,
                  backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || 'white',
                  borderColor: '#CCC',
                  borderWidth: 1,
                  shadow: false
                },
                tooltip: {
                  headerFormat: '<b>{point.x}</b><br/>',
                  pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                  column: {
                    stacking: 'normal',
                    dataLabels: {
                      enabled: true
                    }
                  }
                },
                series: [{
                  name: 'Functional Bug',
                  data: [data[0]['total_fun_bug'],data[1]['total_fun_bug'],data[2]['total_fun_bug'],data[3]['total_fun_bug'],
                        data[4]['total_fun_bug'],data[5]['total_fun_bug'],data[6]['total_fun_bug'],data[7]['total_fun_bug'],
                        data[8]['total_fun_bug'],data[9]['total_fun_bug'],data[10]['total_fun_bug'],data[11]['total_fun_bug'],
                        data[12]['total_fun_bug'],data[13]['total_fun_bug'],data[14]['total_fun_bug']]
                }, {
                  name: 'Non-Functional Bug',
                  data: [data[0]['total_non_fun_bug'],data[1]['total_non_fun_bug'],data[2]['total_non_fun_bug'],data[3]['total_non_fun_bug'],
                        data[4]['total_non_fun_bug'],data[5]['total_non_fun_bug'],data[6]['total_non_fun_bug'],data[7]['total_non_fun_bug'],
                        data[8]['total_non_fun_bug'],data[9]['total_non_fun_bug'],data[10]['total_non_fun_bug'],data[11]['total_non_fun_bug'],
                        data[12]['total_non_fun_bug'],data[13]['total_non_fun_bug'],data[14]['total_non_fun_bug']]
                }]
              });
          });
        }

    });
    core.action_registry.add('kw_project_dashboard.kw_project_dashboard_report', KwantifyProjectDashboardView);
    return KwantifyProjectDashboardView
   });