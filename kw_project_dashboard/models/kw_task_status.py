from odoo import models, fields


class kw_task_status(models.Model):
    _name = 'kw_task_status'
    _description = "Status of Task"

    stage = fields.Char(string="Stage")
    value = fields.Integer(string="Close")