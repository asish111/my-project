from odoo import models, fields


class kw_testcase_categorization(models.Model):
    _name = 'kw_testcase_categorization'
    _description = "Categorization of Testcase"

    func_count = fields.Integer(string="Functional Count")
    non_func_count = fields.Integer(string="Non Functional Count")