from odoo import models, fields


class kw_resourcewise_count(models.Model):
    _name = 'kw_resourcewise_count'
    _description = "Resourcewise Count"


    count = fields.Integer(string="Count")
    proj_role = fields.Char(string="Project Role")