from odoo import models, fields


class kw_project_schedule(models.Model):
    _name = 'kw_project_schedule'
    _description = "Project Schedule"

    dtm_plan_end = fields.Datetime(string="Dtm Plan End Date")
    dtm_plan_start = fields.Datetime(string="Dtm Plan Start Date")
    int_phase_order = fields.Char(string="Int Phase Order")
    phase = fields.Char(string="Phase")