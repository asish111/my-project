from odoo import models, fields


class kw_module_info(models.Model):
    _name = 'kw_module_info'
    _description = "Module Information"


    code_prod = fields.Float(string="Code Prod")
    dre = fields.Float(string="DRE")
    name = fields.Char(string="Module")
    productivity = fields.Float(string="Productivity")
    rdc = fields.Float(string="RDC")
    size = fields.Float(string="Size")
    status = fields.Char(string="Status")
    tdl = fields.Float(string="TDL")
    testing_prod = fields.Float(string="Testing Prod")