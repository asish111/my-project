from odoo import models, fields


class kw_testcase_summary(models.Model):
    _name = 'kw_testcase_summary'
    _description = "Summary of Testcase"

    fail_count = fields.Integer(string="Fail Count")
    pass_count = fields.Integer(string="Pass Count")
    reopen_count = fields.Integer(string="Reopen Count")