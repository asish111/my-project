from odoo import models, fields


class kw_priority_classification(models.Model):
    _name = 'kw_priority_classification'
    _description = "Classification of Priority"

    high_percent = fields.Integer(string="High Percentage")
    med_percent = fields.Integer(string="Medium Percentage")
    low_percent = fields.Integer(string="Low Percentage")