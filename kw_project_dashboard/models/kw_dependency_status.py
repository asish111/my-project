from odoo import models, fields


class kw_dependency_status(models.Model):
    _name = 'kw_dependency_status'
    _description = "Dependency Status"

    dependency_count1 = fields.Char(string="Dependency Count1")
    dependency_count2 = fields.Integer(string="Dependency Count2")