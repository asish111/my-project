from odoo import models, fields


class kw_project_goal(models.Model):
    _name = 'kw_project_goal'
    _description = "Project Goal"

    code_productivity = fields.Float(string="Code Productivity")
    drf = fields.Integer(string="DRF")
    productivity = fields.Float(string="Productivity")
    rdc = fields.Integer(string="RDC")
    tdl = fields.Integer(string="TDL")
    testing_productivity = fields.Float(string="Testing Productivity")
    target = fields.Float(string="Target")
    vchqpmparameter = fields.Char(string="VCHQPM Parameter")