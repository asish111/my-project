from odoo import models, fields


class kw_theme_setting_details(models.Model):
    _name = 'kw_theme_setting_details'
    _description = "Details of Theme Setting"

    collapsible = fields.Char(string="Collapsible")
    filter_theme = fields.Boolean(string="Filter")
    maximizable = fields.Boolean(string="Maximizable")
    name = fields.Char(string="Project Info")
    page_name = fields.Char(string="Page Name")
    portlet_id = fields.Integer()
    refresh = fields.Boolean()
    removable = fields.Char(string="Removable")
    settings = fields.Boolean(string="Settings")
    tabular = fields.Char(string="Tabular")
    title = fields.Char(string="Project Info")
    width = fields.Float(string="Width")