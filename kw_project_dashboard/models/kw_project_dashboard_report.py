from odoo import models, fields, api


class KwantifyProjectDashboard(models.Model):
    _name = "kw_project_dashboard"
    _description = "Kwantify Project Dashboard"

    name = fields.Char("")

    @api.model
    def get_resource_wise_count(self):
        data = []
        resource_wise_tasks = self.env['kw_resourcewise_count'].sudo().search([])
        for record in resource_wise_tasks:
            for resource_wise_task in record:
                data.append({'name': resource_wise_task.proj_role,
                             'value': resource_wise_task.count})

        return data

    @api.model
    def get_rolewise_task(self):
        data = []
        rolewise_tasks = self.env['kw_rolewise_task_percentage'].sudo().search([])
        for record in rolewise_tasks:
            for rolewise_task in record:
                data.append({'name': rolewise_task.proj_role,
                             'value': rolewise_task.min_effort})

        return data

    @api.model
    def get_task_status(self):
        data = []
        task_status = self.env['kw_task_status'].sudo().search([])
        for record in task_status:
            for task in record:
                data.append({'stage': task.stage, 'value': task.value, 'drilldown': task.stage})
        return data
    
    @api.model
    def get_count_format(self, count):
        lis = []
        for num in count.split(','):
            lis.append(float(num))
        return lis

    @api.model
    def risk_status(self):
        data = []
        risk_status = self.env['kw_risk_status'].sudo().search([])
        for record in risk_status:
            for task in record:
                counts = self.get_count_format(task.count)
                data.append({'status': task.status, 'counts': counts})
        return data

    @api.model
    def get_issue_status(self):
        data = []
        issue_stat = self.env['kw_issue_status'].sudo().search([])
        for record in issue_stat:
            for issue in record:
                data.append({'name': issue.issue_count1,
                            'value': issue.issue_count2})
        return data

    def get_issue_review_value(self, value):
        lis = value.split(',')
        return lis

    @api.model
    def get_issue_review(self):
        data = []
        issue_review = self.env['kw_issue_review_details'].sudo().search([])
        for record in issue_review:
            for issue in record:
                value = self.get_issue_review_value(issue.issue_count1)
                data.append({'name': value[0],
                            'value': float(value[1])})

        return data

    @api.model
    def get_dependency_status(self):
        data = []
        dependency_stat = self.env['kw_dependency_status'].sudo().search([])
        for record in dependency_stat:
            for dependency in record:
                data.append({'name': dependency.dependency_count1,
                            'value': dependency.dependency_count2})

        return data

    @api.model
    def get_testcase_type(self):
        data = []
        testcase_type = self.env['kw_testcase_type'].sudo().search([])
        for testcase in testcase_type:
            for record in testcase:
                data.append({'name': record.name, 'value': record.value})

        return data

    @api.model
    def get_injected_stagewise_defects(self):
        data = []
        injected_stagewise_defects = self.env['kw_defects_injected_stagewise'].sudo().search([])
        for injected_stage in injected_stagewise_defects:
            for record in injected_stage:
                data.append({'name': record.phase_name, 'close_count': record.close_count, 
                'open_count': record.open_count})
        return data

    @api.model
    def get_defected_stagewise(self):
        data = []
        defected_stagewise = self.env['kw_defected_stagewise'].sudo().search([])
        for defected in defected_stagewise:
            for record in defected:
                data.append({'name': record.phase_name, 'close_count': record.close_count, 
                'open_count': record.open_count})
        return data

    @api.model
    def get_review_coverage(self):
        data = []
        review_coverage = self.env['kw_all_review_coverage'].sudo().search([])
        for review in review_coverage:
            for record in review:
                data.append({'name': record.name, 'value': record.value})

        return data

    @api.model
    def get_modulewise_testcase_summary(self):
        data = []
        modulewise_testcase_summary = self.env['kw_modulewise_testcase_summary'].sudo().search([])

        for module in modulewise_testcase_summary:
            for record in module:
                data.append({'module': record.module, 'total_bug_count': record.total_bug_count,
                            'total_fun_bug': record.total_fun_bug, 'total_non_fun_bug': record.total_non_fun_bug})
        
        return data