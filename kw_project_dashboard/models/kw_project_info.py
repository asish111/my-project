from odoo import models, fields


class kw_project_info(models.Model):
    _name = 'kw_project_info'
    _description = "Project Inforamtion"

    effort = fields.Integer(string="Effort")
    lifecycle = fields.Char(string="Life Cycle")
    proj_manager = fields.Char(string="Project Manager")
    proj_name = fields.Char(string="Project Name")
    schedule = fields.Char(string="Schedule")
    size = fields.Integer(string="Size")
    wo_code = fields.Char(string="WorkOrder Code")