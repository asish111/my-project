from odoo import models, fields


class kw_billing_collection(models.Model):
    _name = 'kw_billing_collection'
    _description = "Billing Collection"

    collection_amnt = fields.Float(string="Collection Amount")
    invoice_amnt = fields.Float(string="Invoice Amount")
    invoice_dt = fields.Date(string="Invoice Date")
    invoice_details = fields.Char(string="Invoice Details")
    invoice_no = fields.Char(string="Invoice No")
    pending_amnt = fields.Float(string="Pending Amount")
    wo_code = fields.Char(string="Workorder Code")