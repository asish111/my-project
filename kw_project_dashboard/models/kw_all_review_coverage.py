from odoo import models, fields


class kw_all_review_coverage(models.Model):
    _name = 'kw_all_review_coverage'
    _description = "Review Coverage"

    name = fields.Char(string="Name")
    value = fields.Float(string="Value")