from odoo import models, fields


class kw_bug_classification(models.Model):
    _name = 'kw_bug_classification'
    _description = "Bug Classsification"

    browser_comp_bug = fields.Integer(string="Browser Compatible Bug")
    cosm_bug = fields.Integer(string="Cosmetic Bug")
    logic_bug = fields.Integer(string="Logical Bug")
    secu_bug = fields.Integer(string="Security Bug")