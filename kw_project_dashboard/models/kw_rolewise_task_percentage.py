from odoo import models, fields


class kw_rolewise_task_percentage(models.Model):
    _name = 'kw_rolewise_task_percentage'
    _description = "Percentage of rolewise task"


    min_effort = fields.Integer(string="Effort in Min")
    proj_role = fields.Char(string="Project Role")
    sl_no = fields.Integer(string="Sl No")