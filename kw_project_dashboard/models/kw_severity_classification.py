from odoo import models, fields


class kw_severity_classification(models.Model):
    _name = 'kw_severity_classification'
    _description = "Classification of Severity"

    cosmetic_per = fields.Integer(string="Cosmetic Per")
    critical_per = fields.Integer(string="Critical Per")
    major_per = fields.Integer(string="Major Per")
    minor_per = fields.Integer(string="Minor Per")