from odoo import models, fields


class kw_issue_status(models.Model):
    _name = 'kw_issue_status'
    _description = "Status of Issue"

    issue_count1 = fields.Char(string="Issue Count1")
    issue_count2 = fields.Integer(string="Issue Count2")