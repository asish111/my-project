from odoo import models, fields


class kw_defect_summary(models.Model):
    _name = 'kw_defect_summary'
    _description = "Defect Summary"

    close = fields.Integer(string="Close")
    open = fields.Integer(string="Open")