from odoo import models, fields


class kw_testcase_type(models.Model):
    _name = 'kw_testcase_type'
    _description = "Type of Testcase"

    name = fields.Char(string="Name")
    value = fields.Float(string="Value")