from odoo import models, fields


class kw_risk_status(models.Model):
    _name = 'kw_risk_status'
    _description = "Risk Status"

    count = fields.Char(string="Count")
    status = fields.Char(string="Status")