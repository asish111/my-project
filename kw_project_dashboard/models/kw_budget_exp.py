from odoo import models, fields


class kw_budget_exp(models.Model):
    _name = 'kw_budget_exp'
    _description = "Budget Expenses Comparision"


    budget = fields.Float(string="Budget")
    component = fields.Char(string="Component Name")
    expenses = fields.Float(string="Expenses")