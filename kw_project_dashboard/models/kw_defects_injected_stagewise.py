from odoo import models, fields


class kw_get_defects(models.Model):
    _name = 'kw_defects_injected_stagewise'  # fields same as kw_defected_stagewise
    _description = "Get Defects"

    close_count = fields.Integer(string="Close Count")
    count = fields.Integer(string="Count")
    open_count = fields.Integer(string="Open Count")
    phase_name = fields.Char(string="Phase Name")