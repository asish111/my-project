from odoo import models, fields


class kw_modulewise_testcase_summary(models.Model):
    _name = 'kw_modulewise_testcase_summary'
    _description = "Modulewise Summary of Testcase"

    module = fields.Char(string="Module Name")
    total_bug_count = fields.Integer(string="Total Bug Count")
    total_fun_bug = fields.Integer(string="Total Fun Bug")
    total_non_fun_bug = fields.Integer(string="Total Non Fun Bug")