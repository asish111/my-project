from odoo import models, fields


class kw_defected_stagewise(models.Model):
    _name = 'kw_defected_stagewise'
    _description = "Stagewise Defected"

    close_count = fields.Integer(string="Close Count")
    count = fields.Integer(string="Count")
    open_count = fields.Integer(string="Open Count")
    phase_name = fields.Char(string="Phase Name")