# -*- coding: utf-8 -*-

from . import hr_leave_allocation
# from . import hr_leave_type
from . import kw_leave_type_master
from . import kw_leave_cycle_master
from . import kw_leave_entitlements
from . import kw_credit_rule_config

from . import hr_leave_type
from . import resource_calendar_mixin
from . import hr_leave

from . import kw_set_encashment_period
from . import kw_leave_type_config
from . import kw_cancel_leave
from . import hr_employee_in
