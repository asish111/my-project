from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re


class KwLeaveType(models.Model):
    _inherit = 'hr.leave.type'

    

    leave_type_config_ids = fields.One2many(
        string='Leave Configuration Ids',
        comodel_name='kw_leave_type_config',
        inverse_name='leave_type_id',
    )
    leave_code = fields.Char(string='Leave Code',  required=True)


    _sql_constraints = [
        ('name_uniq', 'unique(leave_code)',
         'The leave code must be unique!'),
    ]

    @api.constrains('leave_code')
    def check_code(self):
        for record in self:
            if not re.match("^[0-9a-zA-Z_]+$", str(record.leave_code)) != None:
                raise ValidationError(
                    "Only alphanumeric characters and underscore allowed.")    