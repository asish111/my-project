from odoo import models,fields,api
from odoo.exceptions import ValidationError


class KwEncashmentPeriod(models.Model):
    _name = 'kw_set_encashment_period'
    _description = "Encashment Period Set"
    _rec_name = "financial_year_id"


    financial_year_id = fields.Many2one('account.fiscalyear', 'Financial Year', required=True)
    user_id = fields.Many2many('hr.employee',string='Employee')
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    first_year_service = fields.Boolean(string='Applicable To First year Service')


    _sql_constraints = [('financial_year_uniq', 'unique (financial_year_id)',
                 'Record with this financial year already exist.. !')]


    @api.constrains('start_date', 'end_date')
    def validate_date(self):
        for record in self:
            if record.start_date > record.end_date:
                raise ValidationError(
                    f'Start date should not greater than End date : {record.start_date} > {record.end_date}.')


