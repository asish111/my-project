from odoo import models,fields,api
import datetime
from odoo.exceptions import ValidationError


class KwLeaveCycleMaster(models.Model):
    _name = 'kw_leave_cycle_master'
    _description = "Leave Cycle master sheet"
    _rec_name       = 'branch_id'

    branch_id = fields.Many2one('kw_res_branch',string="Branch", required=True)
    from_month =  fields.Selection( selection='_get_month_name_list',string='Month',  required=True)

    to_month = fields.Selection( selection='_get_month_name_list',string='Month',  required=True)

    from_date = fields.Selection(string='Date', selection='_get_date_list',  required=True)

    to_date = fields.Selection(string='Date', selection='_get_date_list',  required=True)


    _sql_constraints = [
        ('name_uniq', 'unique(branch_id)',
        "This Branch is already existing.."),
    ]

    @api.model
    def _get_date_list(self):
        date = 31
        return [(str(i), i) for i in range(1,date + 1)]


    @api.model
    def _get_month_name_list(self):
        months_choices = []
        for i in range(1,13):
            months_choices.append((str(i), datetime.date(2008, i, 1).strftime('%B')))
        return months_choices


