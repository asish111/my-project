from odoo import models, fields, api
import datetime
from odoo.exceptions import ValidationError


class KwLeaveTypeConfig(models.Model):
    _name = 'kw_leave_type_config'
    _description = "Leave type configuration"
    _rec_name = "branch_id"

    branch_id = fields.Many2one(
        'kw_res_branch', string="Branch", required=True)

    leave_type_id = fields.Many2one(
        'hr.leave.type', string="Leave Type", required=True)

    # entitlement = fields.Selection([('monthly', 'Monthly'),
    #                                 ('quarterly', 'Quarterly'),
    #                                 ('yearly', 'Yearly'),
    #                                 ('others', 'Others')],
    #                                string='Entitlement')

    carry_forward = fields.Boolean(string='Carry Forward')

    carry_forward_percentage = fields.Float(string='Carry Forward Percentage',  required=True)

    carry_forward_lapsed = fields.Integer(
        string='Carry Forward leave lapsed after (In Months)' , required=True)

    encashable = fields.Boolean(string='Encashable')

    encashable_percentage = fields.Float(string='Encashable Percentage' , required=True)

    absent_deduction = fields.Boolean(string='Absent Deduction')

    exclude_holiday = fields.Boolean(string='Exclude Holiday')

    applicable_for = fields.Selection([('m', 'Male'),
                                       ('f', 'Female'),
                                       ('b', 'Both')],
                                      string='Applicable For' , default ='b')

    first_year_service = fields.Boolean(
        string='Applicable To First year Service')

    # allow_negative = fields.Boolean(string='Allow Negative')

    description = fields.Char(string='Description')

    credit_day = fields.Integer(string='Credit Day',  required=True)

    credit_month = fields.Selection(
        selection='_get_month_name_list', string='Credit Month',  required=True)

    lapse_day = fields.Integer(string='Lapse Day',  required=True)

    lapse_month = fields.Selection(
        selection='_get_month_name_list', string='Lapse Month',  required=True)

    @api.model
    def _get_month_name_list(self):
        months_choices = []
        for i in range(1, 13):
            months_choices.append(
                (str(i), datetime.date(2008, i, 1).strftime('%B')))
        return months_choices

    @api.constrains('credit_day')
    def check_credit_day(self):
        for record in self:
            if record.credit_day > 31 or record.credit_day < 1:
                raise ValidationError(
                    "The credit day should be between 1-31.")

    @api.constrains('lapse_day')
    def check_lapse_day(self):
        for record in self:
            if record.lapse_day > 31 or record.lapse_day < 1:
                raise ValidationError(
                    "The lapse day should be between 1-31.")

    _sql_constraints = [('branch_leave_uniq', 'unique (branch_id,leave_type_id)',
                         'This leave type already exist under same branch.. !')]

    @api.constrains('carry_forward_percentage', 'carry_forward')
    def check_carry_forward_percentage(self):
        for record in self:
             if self.carry_forward == True and record.carry_forward_percentage < 1 or record.carry_forward_percentage > 100.00:
                raise ValidationError(
                    "Carry Forward Percentage should be between 1-100% .")

    @api.constrains('encashable_percentage', 'encashable')
    def check_encashable_percentage(self):
        for record in self:
             if self.encashable == True and record.encashable_percentage < 1 or record.encashable_percentage > 100.00:
                raise ValidationError(
                    "Encashable Percentage should be between 1-100% .")


    @api.constrains('carry_forward_lapsed', 'carry_forward')
    def check_carry_forward_lapsed(self):
        for record in self:
            if record.carry_forward == True:
                if record.carry_forward_lapsed > 12 or record.carry_forward_lapsed < 1:
                    raise ValidationError(
                        "The caeey forward lapsed month should be between 1-12.")

    @api.onchange('carry_forward')
    def onchange_carry_forward(self):
        if self.carry_forward == False:
            self.carry_forward_percentage = 0.00
            self.carry_forward_allowed = False
            self.carry_forward_lapsed = 0

    @api.onchange('encashable')
    def onchange_encashable(self):
        if self.encashable == False:
            self.encashable_percentage = 0.00
