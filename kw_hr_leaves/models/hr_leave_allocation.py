from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import date, timedelta
import fiscalyear
from dateutil import relativedelta


class HrLeaveAllocation(models.Model):
    _inherit = 'hr.leave.allocation'

    holiday_type = fields.Selection(selection_add=[('grade', 'By Grade')])
    grade_id = fields.Many2many('kwemp_grade', string="Grade")
    expire_date = fields.Date('Expire date')

    _sql_constraints = [
        ('type_value',
         "CHECK( (holiday_type='employee' AND employee_id IS NOT NULL) or "
         "(holiday_type='category' AND category_id IS NOT NULL) or "
         "(holiday_type='department' AND department_id IS NOT NULL) or "
         "(holiday_type='grade' AND employee_id IS NULL and category_id IS NULL and department_id IS NULL and mode_company_id IS NULL) or "
         "(holiday_type='company' AND mode_company_id IS NOT NULL))",
         "The employee, department, company or employee category of this request is missing. Please make sure that your user login is linked to an employee."),

    ]

    @api.onchange('holiday_type')
    def _onchange_type(self):
        print('method called')
        if self.holiday_type == 'employee':
            if not self.employee_id:
                self.employee_id = self.env.user.employee_ids[:1].id
            self.mode_company_id = False
            self.category_id = False
            self.grade_id = False
        elif self.holiday_type == 'company':
            self.employee_id = False
            if not self.mode_company_id:
                self.mode_company_id = self.env.user.company_id.id
            self.category_id = False
            self.grade_id = False
        elif self.holiday_type == 'department':
            self.employee_id = False
            self.mode_company_id = False
            self.category_id = False
            self.grade_id = False
            if not self.department_id:
                self.department_id = self.env.user.employee_ids[:1].department_id.id
        elif self.holiday_type == 'category':
            self.employee_id = False
            self.mode_company_id = False
            self.department_id = False
            self.grade_id = False
        elif self.holiday_type == 'grade':
            self.employee_id = False
            self.mode_company_id = False
            self.department_id = False
            self.category_id = False

    @api.constrains('holiday_type', 'grade_id')
    def validate_grade(self):

        for record in self:
            if record.holiday_type == 'grade' and record.grade_id == '':
                raise ValidationError("Please select at least one grade")

    def _action_validate_create_childs(self):
        childs = self.env['hr.leave.allocation']
        if self.state == 'validate' and self.holiday_type in ['category', 'department', 'company', 'grade']:
            if self.holiday_type == 'category':
                employees = self.category_id.employee_ids
            elif self.holiday_type == 'department':
                employees = self.department_id.member_ids
            elif self.holiday_type == 'grade':
                employees = self.env['hr.employee'].search(
                    [('emp_grade', 'in', self.grade_id.ids)])
            else:
                employees = self.env['hr.employee'].search(
                    [('company_id', '=', self.mode_company_id.id)])
            for employee in employees:
                childs += self.with_context(
                    mail_notify_force_send=False,
                    mail_activity_automation_skip=True
                ).create(self._prepare_holiday_values(employee))
            # TODO is it necessary to interleave the calls?
            childs.action_approve()
            if childs and self.holiday_status_id.validation_type == 'both':
                childs.action_validate()
        return childs


    @api.model
    def add_allocation(self):
        last_date = date.today() - timedelta(14)
        employees = self.env['hr.employee'].search([('user_id','!=',False),('date_of_joining','!=',False),('date_of_joining','>',last_date)])
        
        for employee in employees:
            allocation =  self.env['hr.leave.allocation'].search([('employee_id','=',employee.id)])
            if not allocation:
                entitlement =  self.env['kw_leave_entitlements'].search([('grade_id','in',employee.emp_grade.ids),('branch_id','=',employee.user_id.branch_id.id)])

                leave_types = entitlement and entitlement.entitlement_ids.read(['leave_type_id','quantity']) or False
                if leave_types:
                    for leave_type in leave_types:
                        
                        leave_config = self.env['kw_leave_type_config'].search([('branch_id','=',employee.user_id.branch_id.id),('leave_type_id','=',leave_type['leave_type_id'][0])])

                        fiscalyear.setup_fiscal_calendar(start_month=4)
                        fyear =fiscalyear.FiscalYear.current()

                        if int(leave_config.lapse_month) < 4 :
                            year =fyear.end.year
                        else:
                            year =fyear.start.year
                        expire_date = date(year=year,month=int(leave_config.lapse_month),day=int(leave_config.lapse_day))

                        credit_rule = self.env['kw_credit_rule_config'].search([('leave_type_id','=',leave_type['leave_type_id'][0])])

                        rules = credit_rule and credit_rule.rule_ids.read(['from_day','to_day','percentage']) or False
                        for rule in rules:
                            remaining_months = relativedelta.relativedelta(fyear.end, employee.date_of_joining).months
                            leave_per_month = leave_type['quantity'] / 12

                            if rule['from_day'] <= employee.date_of_joining.day <= rule['to_day']:
                                leave = leave_per_month * rule['percentage']
                                quantity = (leave_per_month * remaining_months) + leave

                                self.env['hr.leave.allocation'].create({
                                        'state':'validate',
                                        'employee_id':employee.id,
                                        'number_of_days':quantity,
                                        'holiday_type':'employee',
                                        'expire_date' : expire_date,
                                        'holiday_status_id':leave_type['leave_type_id'][0]})

                                

          



                    