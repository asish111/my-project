# -*- coding: utf-8 -*-
from datetime import datetime, time
from pytz import timezone, UTC
from odoo import models,fields,api
from odoo.addons.resource.models.resource import float_to_time, HOURS_PER_DAY
from odoo.exceptions import AccessError, UserError, ValidationError

class HrLeave(models.Model):
    _inherit = 'hr.leave'


    request_date_from_period    = fields.Selection([
        ('am', 'First Half'), ('pm', 'Second Half')],
        string="Date Period Start", default='am')
    
    request_date_to_period      = fields.Selection([
        ('am', 'First Half'), ('pm', 'Second Half')],
        string="Date Period End", default='am')
    # request type
    request_unit_half_to_period = fields.Boolean('Half Day')
    name                        = fields.Text('Reason',size=500,required=True)

    leave_address1              = fields.Text(string="Leave Address(1)",size=300,)
    leave_address2              = fields.Text(string="Leave Address(2)",size=300)
    phone_no                    = fields.Char(string="Telephone",size=15)
    medical_doc                 = fields.Binary(string='Document', attachment=True)
    
    leave_code                  = fields.Char(string="Leave Code",related="holiday_status_id.leave_code")
    
    # @api.onchange('date_from', 'date_to', 'employee_id')
    # def _onchange_leave_dates(self):
    #     if self.date_from and self.date_to:
    #         self.number_of_days = self._get_number_of_days(self.date_from, self.date_to, self.employee_id.id)
    #     else:
    #         self.number_of_days = 0

    @api.constrains('name')
    def _check_name(self):
        for holiday in self:
            if holiday.name and len(holiday.name)>500:
               raise ValidationError('Please enter the reason within 500 characters only') 

    @api.constrains('number_of_days','number_of_days_display')
    def _check_no_of_days(self):
        for holiday in self:
            # print(holiday.number_of_days)
            if holiday.number_of_days ==0 or holiday.number_of_days_display == 0:
                raise ValidationError('No of days should be grater than 0')

    @api.constrains('request_date_from_period','request_date_to_period')
    def _check_no_of_days(self):
        for holiday in self:
            if holiday.request_date_to and holiday.request_date_from:
                if holiday.request_date_to == holiday.request_date_from :
                    if (holiday.request_unit_half_to_period and not holiday.request_unit_half) or (not holiday.request_unit_half_to_period and holiday.request_unit_half):
                        raise ValidationError('Leave Duration combination is not correct.')

                    if holiday.request_date_from_period == 'pm' and holiday.request_date_to_period == 'am':
                        raise ValidationError('Leave Duration combination is not correct.')
    ##validate if two type of leaves taken on a single day                    
    @api.constrains('request_unit_half', 'request_unit_half_to_period', 'holiday_status_id')
    def _check_duplicate_half_holiday_type(self):        
        for holiday in self:
            domain = [                   
                    ('holiday_status_id', '!=', holiday.holiday_status_id.id),
                    ('employee_id', '=', holiday.employee_id.id),
                    ('id', '!=', holiday.id),
                    ('state', 'not in', ['cancel', 'refuse']),
                ]
            if holiday.request_unit_half:
                fdomain = domain+[
                    ('request_date_from', '=', holiday.request_date_from),
                    ('request_date_from_period', '!=', holiday.request_date_from_period),                    
                ]
                
                nfholidays = self.search_count(fdomain)               
                if nfholidays:
                    raise ValidationError('You can not have 2 type of leaves on the same day.')
            if holiday.request_unit_half_to_period:
                tdomain = domain+[
                    ('request_date_to', '=', holiday.request_date_to),
                    ('request_date_to_period', '!=', holiday.request_date_to_period),                    
                ]
                ntholidays = self.search_count(tdomain)
                if ntholidays:
                    raise ValidationError('You can not have 2 type of leaves on the same day.')

    @api.onchange('request_unit_half_to_period')
    def _onchange_request_unit_half_to(self):
        if self.request_unit_half_to_period:
            self.request_unit_hours = False
            self.request_unit_custom = False
        self._onchange_request_parameters()
    
    @api.onchange('holiday_status_id')
    def _onchange_holiday_status_id(self):
        if self.holiday_status_id:           
            self._onchange_request_parameters()

    #@api.multi
    def _get_number_of_days(self, date_from, date_to, employee_id):
        """ Returns a float equals to the timedelta between two dates given as string."""
        # print(datetime.combine(date_from.date(), time.min))
        # print(datetime.combine(date_to.date(), time.max))
        if employee_id:
            employee = self.env['hr.employee'].browse(employee_id)
           
            exclude_leaves  = self.holiday_status_id.leave_type_config_ids.filtered(lambda rec: rec.branch_id == employee.user_id.branch_id).exclude_holiday 
           
            total_days      = employee.kw_get_work_days_data(datetime.combine(date_from.date(), time.min),
            datetime.combine(date_to.date(), time.max),exclude_leaves)['days']

            # print(self.request_date_from_period)
            if self.request_unit_half and self.request_date_from_period:
                if self.request_date_from_period == 'pm':
                    total_days-=.5
            if self.request_unit_half_to_period and self.request_date_to_period:
                if self.request_date_to_period == 'am':
                    total_days-=.5

            return total_days

        today_hours = self.env.user.company_id.resource_calendar_id.get_work_hours_count(
            datetime.combine(date_from.date(), time.min),
            datetime.combine(date_from.date(), time.max),
            False)

        return self.env.user.company_id.resource_calendar_id.get_work_hours_count(date_from, date_to) / (today_hours or HOURS_PER_DAY)



    ##override the existing method
    @api.onchange('request_date_from_period', 'request_hour_from', 'request_hour_to',
                  'request_date_from', 'request_date_to','request_date_to_period','request_unit_half_to_period',
                  'employee_id')
    def _onchange_request_parameters(self):
        if not self.request_date_from:
            self.date_from = False
            return

        # if self.request_unit_half or self.request_unit_hours:
        #     self.request_date_to = self.request_date_from

        if not self.request_date_to:
            self.date_to = False
            return

        domain = [('calendar_id', '=', self.employee_id.resource_calendar_id.id or self.env.user.company_id.resource_calendar_id.id)]
        attendances = self.env['resource.calendar.attendance'].search(domain, order='dayofweek, day_period DESC')

        # find first attendance coming after first_day
        attendance_from = next((att for att in attendances if int(att.dayofweek) >= self.request_date_from.weekday()), attendances[0])
        # find last attendance coming before last_day
        attendance_to = next((att for att in reversed(attendances) if int(att.dayofweek) <= self.request_date_to.weekday()), attendances[-1])

        if self.request_unit_half:
            if self.request_date_from_period == 'am':
                hour_from = float_to_time(attendance_from.hour_from)
                hour_to = float_to_time(attendance_from.hour_to)
            else:
                hour_from = float_to_time(attendance_to.hour_from)
                hour_to = float_to_time(attendance_to.hour_to)
        elif self.request_unit_hours:
            # This hack is related to the definition of the field, basically we convert
            # the negative integer into .5 floats
            hour_from = float_to_time(abs(self.request_hour_from) - 0.5 if self.request_hour_from < 0 else self.request_hour_from)
            hour_to = float_to_time(abs(self.request_hour_to) - 0.5 if self.request_hour_to < 0 else self.request_hour_to)
        elif self.request_unit_custom:
            hour_from = self.date_from.time()
            hour_to = self.date_to.time()
        else:
            hour_from = float_to_time(attendance_from.hour_from)
            hour_to = float_to_time(attendance_to.hour_to)

        tz = self.env.user.tz if self.env.user.tz and not self.request_unit_custom else 'UTC'  # custom -> already in UTC
        self.date_from = timezone(tz).localize(datetime.combine(self.request_date_from, hour_from)).astimezone(UTC).replace(tzinfo=None)
        self.date_to = timezone(tz).localize(datetime.combine(self.request_date_to, hour_to)).astimezone(UTC).replace(tzinfo=None)
        self._onchange_leave_dates()


    # @api.multi
    # def cancel_leave(self):
    #     return {
    #         'name': ('Cancel Leave'),
    #         'view_type': 'form',
    #         'view_mode': 'tree,form',
    #         'res_model': 'kw_cancel_leave',
    #         'view_id': False,
    #         # 'domain':  [('approved_leave_id','=',self.id),('approved_leave_id.employee_id','in',self.env.user.employee_ids.id)],
    #         'type': 'ir.actions.act_window',
    #     }


    # @api.multi
    # def take_action(self):
    #     return {
    #         'name': ('Take Action'),
    #         'view_type': 'form',
    #         'view_mode': 'tree,form',
    #         'res_model': 'kw_cancel_leave',
    #         'view_id': False,
    #         'domain': [('approved_leave_id','=',self.id),('approved_leave_id.employee_id','in',self.env.user.employee_ids.child_ids.ids)],
    #         'type': 'ir.actions.act_window',
    #     }