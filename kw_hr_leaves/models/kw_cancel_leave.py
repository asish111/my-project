# -*- coding: utf-8 -*-


from odoo import api, fields, models
from odoo.exceptions import ValidationError


class CancelLeave(models.Model):

    _name           = 'kw_cancel_leave'
    _description    = "Leave Cancel after approval."

    _rec_name       = 'approved_leave_id'
    
    @api.model
    def _get_domain(self):
        return [('state','=','validate'),('employee_id','=',self.env.user.employee_ids.id)]


    approved_leave_id            = fields.Many2one('hr.leave', string="Approved Leave", required=True ,  domain=_get_domain)
    description                  = fields.Text('Reason of Cancellation',size=500,required=True) 
    comment                      = fields.Text(string='Comments By Manager' ,size=500,required=True) 
    check_parent                 = fields.Boolean(string="Check Parent", compute='_compute_parent')
    status                       = fields.Selection([('apply', 'Applied'),('approve', 'Approved'),('reject', 'Rejected')],string="Status" , default='apply')
    duration                     = fields.Float(string='Duration', related= 'approved_leave_id.number_of_days_display')
    from_day                     = fields.Date(string="From", related= 'approved_leave_id.request_date_from')
    from_half_day                = fields.Selection([('am', '  First Half'),('pm', '  Second Half')],string="Half day", related= 'approved_leave_id.request_date_from_period')
    to_day                       = fields.Date(string="To", related= 'approved_leave_id.request_date_to')
    to_half_day                  = fields.Selection([('am', '  First Half'),('pm', '  Second Half')],string="Half day", related= 'approved_leave_id.request_date_to_period')
    request_unit_half            = fields.Boolean(string="From Half day", related= 'approved_leave_id.request_unit_half')
    request_unit_half_to_period  = fields.Boolean(string="To Half day", related= 'approved_leave_id.request_unit_half_to_period')
    reason                       = fields.Text( related= 'approved_leave_id.name')
    

    _sql_constraints = [
        ('leave_uniq', 'unique(approved_leave_id)',
         'The Leave you have selected already exist .'),
    ]

    @api.multi
    def _compute_parent(self):
        current_user= self.env.user
        for record in self:
            user= record.create_uid
            employee= self.env['hr.employee'].search([('user_id','=',user.id)],limit=1)
            ra = employee and employee.parent_id or False
            record.check_parent = True if ra and ra.user_id and ra.user_id.id == current_user.id else False

    @api.multi
    def action_approve(self):
        self.status='approve'
        self.approved_leave_id.state='cancel'
    
    @api.multi
    def action_reject(self):
        self.status='reject'