# -*- coding: utf-8 -*-
{
    'name': "kw_hr_leaves",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "CSM Technologies",
    'website': "http://www.yourcompany.com",

    
    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','hr_holidays','kw_account_fiscal_year'],

    'data': [
        'security/ir.model.access.csv',
        'security/kw_hr_leaves_security.xml',
        'views/hr_leave_allocation.xml',
        'views/hr_leave_type.xml',
        'views/kw_leave_type_master.xml',
        'views/kw_leave_cycle_master.xml',
        'views/kw_leave_entitlements.xml',
        'views/kw_credit_rule_config.xml',

        'views/hr_leave_views.xml',
        'views/kw_set_encashment_period.xml',
        'views/kw_leave_type_config.xml',
        'views/kw_cancel_leave.xml',
        'data/kw_leave_allocation_cron.xml',


        'views/kw_leave_menu.xml'
    ],
    
}