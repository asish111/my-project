# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request


class kw_workstation_seatmap(http.Controller):

    @http.route('/seat-map', auth="public", website=True)
    def workstation_seat_map(self, **args):
        if http.request.session.uid is None:
            return http.request.redirect('/web')
        infra = dict()
        infra_code = (args['code']) if 'code' in args.keys() and args['code'] else 'sixth-c'
        infra['infrastructure'] = http.request.env['kw_workstation_infrastructure'].search([])
        infra['infra_code'] = infra_code
        infra['employees'] = http.request.env['hr.employee'].search([])
        if infra_code and infra_code != 'all':
            workstation_data = http.request.env['kw_workstation_master'].search([('infrastructure.code', '=', infra_code)])
        else:
            workstation_data = http.request.env['kw_workstation_master'].search([])
        infra['workstations'] = [(station.id, station.name) for station in workstation_data]
        return http.request.render('kw_workstation.kw_workstation_seat_map', infra)
