# -*- coding: utf-8 -*-
{
    'name': "Kwantify Workstation",

    'summary': """
       Kwantify Workstation Management System
    """,

    'description': """
       Manage the Workstations
    """,

    'author': "CSM Technologies",
    'website': "https://www.csm.co.in",

    'category': 'Kwantify',
    'version': '12.0.0.3',

    # any module necessary for this one to work correctly
    'depends': ['base', 'kw_employee'],

    # always loaded
    'data': [
        'security/kw_workstation_security.xml',
        'security/ir.model.access.csv',
        # 'views/kw_workstation_employee.xml',
        # 'views/kw_workstation_emp_tagging.xml',
        'views/kw_workstation_master.xml',
        'views/kw_infrastructure_master.xml',
        'views/kw_workstation_type_master.xml',
        # 'views/kw_workstation_config.xml',
        'views/kw_workstation_seatmap.xml',
        'views/kw_workstation_menu.xml',
        'views/employee_system_view.xml',
        'static/src/layouts/seat_map_sixth_floor_c_block.xml',
        'static/src/layouts/seat_map_sixth_floor_s_block.xml',
        'static/src/layouts/seat_map_sixth_floor_m_block.xml',
        'static/src/layouts/seat_map_fifth_floor.xml',
        'static/src/layouts/seat_map_coe.xml',

    ],
    'application': False,
    'installable': True,
    'auto_install': False,
}