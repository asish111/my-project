odoo.define('kw_workstation.seat_map', function (require) {
    'use strict';
    var Widget = require('web.Widget');
    var session = require('web.session');
    var core = require('web.core');
    var rpc = require('web.rpc');
    var QWeb = core.qweb;

    var seatmap = {
        svg_id : '',
        infra_id : 0,
        assigned: '#108fbb',
        //not_assigned: '#faa635',
        not_assigned: '#ff0800',
        //not_mapped: '#d7182a',
        not_mapped: '#cccccc',
        wfo: '#228b22',
        wfh: '#ffc30b',
        init: function(){
            // $('path.workstation').removeAttr('style');
            seatmap.get_infrastructure();
            seatmap._get_context();
            // seatmap.get_workstations();
            $('.btn-wfo').css({'background-color':seatmap.wfo});
            $('.btn-wfh').css({'background-color':seatmap.wfh});
            $('.btn-not-assigned').css({'background-color':seatmap.not_assigned,});
//            $('.btn-assigned').css({'background-color':seatmap.assigned});
//            $('.btn-not-mapped').css({'background-color':seatmap.not_mapped});
            $('select.select2').select2({
                placeholder: "Select an employee",
                //multiple:true,
                maximumSelectionSize: 2,
            });
            $('path.workstation').dblclick(function(e){
                seatmap.showResource( $(this), e);
            });
            $('#btn-ws-tag-save').click(function(){
                seatmap.saveResource();
            });
            $('#btn-ws-tag-cancel').click(function(){
                seatmap.resetResource();
            });
        },
        refresh: function(){
            seatmap.get_infrastructure();
        },
        get_workstations: function(){
            $('#infraSelect').change(function(){
                seatmap.infra_id = $('option:selected',this).val();
            });
            if($('#infraSelect').val() != 0){
                return rpc.query({
                    model: 'kw_workstation_master',
                    method: 'get_workstations',
                    args: [{infra_id: $('#infraSelect > option:selected').attr('id')}],
                }).then(function (val) {
                    $('select.select2').select2({
                        placeholder: "Select an employee",
                        //multiple:true,
                        maximumSelectionSize: 2,
                    });
                    $.each(val, function(k, v){
                        $('select.select2').attr(k,v);
                        });
                });
            }
        },
        get_infrastructure: function(){
            $('#infraSelect').change(function(){
                seatmap.infra_id = $('option:selected',this).val();
                // history.pushState({id: 'homepage'}, '', "/seatmap?infrastructure_id="+seatmap.infra_id);
                window.location.href = "/seat-map?code="+seatmap.infra_id;
            });
            if($('#infraSelect').val() != 0){
                return rpc.query({
                    model: 'kw_workstation_master',
                    method: 'get_seat_map',
                    args: [{infra_id: $('#infraSelect > option:selected').attr('id')}],
                }).then(function (val) {
                    var tot_assign = 0;
                    var tot_not_assign = 0;
                    var tot_wfo = 0;
                    var tot_wfh = 0;
                    var infra = '';
//                    var tot_ceo = 0;
//                    var ceo_assign = 0;
//                    var ceo_notassign = 0;
                    var cab_assign = 0;
                    var cab_notassign = 0;
                    var cab_wfh = 0;
                    var sr_assign = 0;
                    var sr_notassign = 0;
                    var sr_wfh = 0;
                    var mid_assign = 0;
                    var mid_notassign = 0;
                    var mid_wfh = 0;
                    var run_assign = 0;
                    var run_notassign = 0;
                    var run_wfh = 0;
                    var in_assign = 0;
                    var in_notassign = 0;
                    var in_wfh = 0;
                    var reception = 0;
                    var rec_notassign = 0;
                    var rec_assign = 0;
                    var cab_occupied = 0;
                    var sr_occupied = 0;
                    var mid_occupied = 0;
                    var run_occupied = 0;
                    var int_occupied = 0;
                    var rec_occupied = 0;
                    var laptop_count = 0;
                    var pc_count = 0;
                    var onpremise_count= 0;
                    var remote_count = 0;
                    $('path.workstation').css({ 'fill': seatmap.not_mapped, 'stroke': seatmap.not_mapped});
                    $.each(val, function(k, v){
                        infra = v.infrastructure;
                        tot_wfo = v.tot_wfo;
                        tot_wfh = v.tot_wfh;
//                        tot_ceo = v.ceo_count;
                        cab_assign = v.cab_assign;
                        cab_notassign = v.cab_notassign;
                        cab_wfh = v.cab_wfh;
                        sr_assign = v.sr_assign;
                        sr_notassign = v.sr_notassign;
                        sr_wfh = v.sr_wfh;
                        mid_assign = v.mid_assign;
                        mid_notassign = v.mid_notassign;
                        mid_wfh = v.mid_wfh;
                        run_assign = v.run_assign;
                        run_notassign = v.run_notassign;
                        run_wfh = v.run_wfh;
                        in_assign = v.in_assign;
                        in_notassign = v.in_notassign;
                        in_wfh = v.in_wfh;
//                        ceo_assign = v.ceo_assign;
//                        ceo_notassign = v.ceo_notassign;
                        reception = v.reception;
                        rec_notassign = v.rec_notassign;
                        rec_assign = v.rec_assign;
                        cab_occupied = v.cab_occupied;
                        sr_occupied = v.sr_occupied;
                        mid_occupied = v.mid_occupied;
                        run_occupied = v.run_occupied;
                        int_occupied = v.int_occupied;
                        rec_occupied = v.rec_occupied;
                        laptop_count = v.laptop_count;
                        pc_count = v.pc_count;
                        onpremise_count = v.onpremise_count;
                        remote_count = v.remote_count;
                        if(typeof v.path_id != 'undefined' && v.path_id != ''){
                            var _el = $('path#'+v.path_id);
                            var _assign = v.is_wfh ? seatmap.wfh : seatmap.wfo;
                            var _title = (v.emp_name != '' ? '<b>'+v.emp_name+'</b>' + ' || ' : 'Not Assigned' + ' || ') + v.name;
                            _el.length > 0 ? (v.emp_name != '' ? tot_assign++ : tot_not_assign++) : '' ;

                            _el.attr({'title': _title, 'emp_ids': v.emp_ids, 'station_id': v.station_id});
                            _el.css({ 'fill': v.emp_name != '' ? _assign : seatmap.not_assigned, 'stroke': v.emp_name != '' ? _assign : seatmap.not_assigned});
                        }
                    });

                    $('#tot_wfo').html(tot_wfo);
                    $('#tot_wfh').html(tot_wfh);
                    $('#tot_people').html(parseInt(tot_wfo)+parseInt(tot_wfh));
                    $('#tot_wfo_ico').html(tot_wfo);
                    $('#tot_wfh_ico').html(tot_wfh);

                    $('#tot_ws_cabin').html(parseInt(cab_assign)+parseInt(cab_notassign));
                    $('#tot_ws_cabin_assign').html(cab_assign);
                    $('#tot_ws_cabin_notassign').html(cab_notassign);

                    $('#tot_ws_sr').html(parseInt(sr_assign)+parseInt(sr_notassign));
                    $('#tot_ws_sr_assign').html(sr_assign);
                    $('#tot_ws_sr_notassign').html(sr_notassign);

                    $('#tot_ws_mid').html(parseInt(mid_assign)+parseInt(mid_notassign));
                    $('#tot_ws_mid_assign').html(mid_assign);
                    $('#tot_ws_mid_notassign').html(mid_notassign);

                    $('#tot_ws_running').html(parseInt(run_assign)+parseInt(run_notassign));
                    $('#tot_ws_run_assign').html(run_assign);
                    $('#tot_ws_run_notassign').html(run_notassign);
                    $('#tot_ws_run_wfh').html(run_wfh);

                    $('#tot_ws_intern').html(parseInt(in_assign)+parseInt(in_notassign));
                    $('#tot_ws_intern_assign').html(in_assign);
                    $('#tot_ws_intern_notassign').html(in_notassign);

                    $('#tot_reception_ws').html(reception);
                    $('#tot_reception_ws_assign').html(rec_assign);
                    $('#tot_reception_ws_notassign').html(rec_notassign);

					var tot_ws_assigned = parseInt(cab_assign)+parseInt(sr_assign)+parseInt(mid_assign)+parseInt(run_assign)+parseInt(in_assign)+parseInt(rec_assign);
                    $('#tot_ws_assigned').html(tot_ws_assigned);
					$('#tot_assigned').html(tot_ws_assigned);

					var tot_ws_notassigned = parseInt(cab_notassign)+parseInt(sr_notassign)+parseInt(mid_notassign)+parseInt(run_notassign)+parseInt(in_notassign)+parseInt(rec_notassign);
                    $('#tot_ws_notassigned').html(tot_ws_notassigned);
                    $('#tot_not_assigned').html(tot_ws_notassigned);
                    $('#tot_not_assigned_ico').html(tot_ws_notassigned);

                    var tot_ws = parseInt(tot_ws_assigned)+parseInt(tot_ws_notassigned);
                    $('#tot_ws').html(tot_ws);
					$('#total_work_station').html(tot_ws);

                    $('#tot_sr_occupied').html(sr_occupied);
                    $('#tot_cab_occupied').html(cab_occupied);
                    $('#tot_mid_occupied').html(mid_occupied);
                    $('#tot_run_occupied').html(run_occupied);
                    $('#tot_int_occupied').html(int_occupied);
                    $('#tot_rec_occupied').html(rec_occupied);

                    var total_occupied = parseInt(int_occupied)+parseInt(sr_occupied)+parseInt(cab_occupied)+parseInt(mid_occupied)+parseInt(run_occupied)+parseInt(rec_occupied);
                    $('#total_occupied').html(total_occupied);
                    $('#tot_laptop').html(total_occupied);
                    /* System Status*/
                    $('#tot_laptop').html(laptop_count);
                    $('#tot_desktop').html(pc_count);
                    $('#tot_onpremise').html(onpremise_count);
                    $('#tot_remote').html(remote_count);

                });
            }
        },
        _get_context: function(){
            $('.hovertext').hide();
            $("path.workstation").mousemove(function(e)
            {
                var _el = jQuery(this);
                _el.css({ 'cursor':'pointer'});
                if(_el.attr('title') != '' && typeof _el.attr('title') != 'undefined')
                {
                    $(".hovertext").html(_el.attr('title').replace('||','<br/>'));
                    $(".hovertext").css({
                        'top': e.pageY - 20,
                        'left': e.pageX + 20
                    }).fadeIn('fast');
                    //_el.css({ 'fill': _el.attr('title').split('||').length > 1 ? seatmap.assigned : seatmap.not_assigned});
                }
                else
                {
                    $(".hovertext").fadeOut('fast');
                    //_el.css({ 'fill': seatmap.not_mapped});
                }
            });
            $("path.workstation").mouseout(function(e){
                var _el = jQuery(this);
                _el.css({'cursor':'initial'});
                //_el.css({'fill': ''});
                $(".hovertext").fadeOut('fast');
            });
        },
        showResource: function(_el, event){
            seatmap.resetResource();
            seatmap.svg_id = _el;
            // console.log(_el.attr('id'))
            var emp_ids = _el.attr('emp_ids');
            if(typeof emp_ids != 'undefined' && emp_ids != ''){
                $('#ws-employee-tag').val(_el.attr('emp_ids').split(',')).trigger('change');
            }
            var station_id = _el.attr('station_id');
            if(typeof station_id != 'undefined' && station_id != ''){
                $('#ws-workstation-tag').val(_el.attr('station_id')).trigger('change');
            }
            // console.log(_el.position())
            var offset = _el.offset();
            _el.css({'stroke-width':4});
            $('#ws-employee-tag-block').addClass('active').css({'top':offset.top+50, 'left':offset.left});
        },
        resetResource: function(){
            seatmap.svg_id = '';
            $('path.workstation').css({'stroke-width':''});
            $('#ws-employee-tag-block').removeClass('active');
            $('#ws-employee-tag').val('').trigger('change');
            $('#ws-workstation-tag').val('').trigger('change');
            $('#ws-employee-tag-msg, #ws-workstation-tag-msg').removeClass('active');
        },
        saveResource: function(){
            var status = '';
            if($('#ws-employee-wfh').prop("checked") == true){
                status = true;
            }else{
                status = false;
            }
            var params = {
                svg_id : seatmap.svg_id.attr('id'),
                emp_ids : $('#ws-employee-tag').val(),
                ws_id : $('#ws-workstation-tag').val(),
                wfh_status : status
            };
            var flag = false;
            $('#ws-employee-tag-msg, #ws-workstation-tag-msg').removeClass('active');
            /*if(params.emp_ids == '' || params.emp_ids == null){
                $('#ws-employee-tag-msg').addClass('active');
                flag = true;
            }*/

            if(params.ws_id == ''){
                $('#ws-workstation-tag-msg').addClass('active');
                flag = true;
            }

            if(flag === true){
                return false;
            }

            seatmap._save(params);
        },
        _save: function(params){
            // console.log(params);
            return rpc.query({
                    model: 'kw_workstation_master',
                    method: 'save_seat_map',
                    args: [params],
                }).then(function (response) {
                    $('#ws-employee-tag-block').removeClass('active');
                    console.log('then.....');
                    console.log(response);
                    seatmap.refresh();
                    //seatmap.alert({'type':'success', 'text':'Workstation details updated successfully.'});
                    seatmap.notify({'type':'success', 'text':'Workstation details updated successfully.'});
                    $('path.workstation').css({'stroke-width':''});
                }).fail(function(response){
                    console.log('fail.....');
                    seatmap.alert({'type':'error', 'text':response.data.arguments[0], 'title':"Are you sure to continue?"});
                    //seatmap.notify({'type':'error', 'text':response.data.arguments[0]});
                    //seatmap.alert({'type':'error', 'text':response.data.message});
                    console.log(response.data.arguments[0]);
                    console.log(this);
                });
        },
        alert: function(params){
            var config = {
              title: params.text,
              //text: params.text,
              // type: params.type || "warning",
              confirmButtonText: "Ok",
              confirmButtonClass: "btn-primary btn-sm",
              showCancelButton: true,
              cancelButtonText: "Cancel",
              cancelButtonClass: "btn-danger btn-sm",
              // closeOnConfirm: false,
              // closeOnCancel: false
            };
            params.title = params.title || false
            if(params.title){
                config.title = params.title || "Are you sure?";
                config.text = params.text
            }
            swal(config,
            function(isConfirm) {
              if (isConfirm) {
                console.log('Ok')
                //swal("Deleted!", "Your imaginary file has been deleted.", "success");
              } else {
                console.log('Cancel')
                //swal("Cancelled", "Your imaginary file is safe :)", "error");
              }
            });
        },
        notify:function(params){
            var dict = {'success':'md-toast-success','error':'md-toast-error','info':'md-toast-info','warning':'md-toast-warning'};

            /*
            <div class="md-toast md-toast-warning">
                <button type="button" class="close">x</button>
                <div class="md-toast-message">I was launched via jQuery!</div>
            </div>
            */
            var close = $('<button>').attr({'type':'button'}).addClass('close').text('x');
            var message = $('<div>').addClass('md-toast-message').text(params.text);
            var toast = $('<div>').attr('class', 'md-toast').addClass(dict[params.type]).append(message);//.append(close)
            $('div#toast-container').append(toast);
            setTimeout(function(){
                toast.slideUp('slow').remove();
            }, 5000);
            //$(document).on('click', close, function(){$(this).closest('.md-toast').slideUp('slow');});
        }
    };
    
    $(function(){
        seatmap.init();
    });
});