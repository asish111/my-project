# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re


class kw_workstation_master(models.Model):
    _name = 'kw_workstation_master'
    _description = "A master model to create the Work Station"

    name = fields.Char(string="Name", required=True, size=100)
    workstation_type = fields.Many2one('kw_workstation_type', string="Workstation Type", required=True)
    infrastructure = fields.Many2one('kw_workstation_infrastructure', string="Infrastructure", required=True)
    # employee_id = fields.Many2one('hr.employee', string="Employee")
    # employee_id = fields.Many2many('hr.employee', 'kw_workstation_hr_employee_rel', string="Employee")
    employee_id = fields.Many2many(comodel_name='hr.employee',
                                   relation='kw_workstation_hr_employee_rel',
                                   column1='wid',
                                   column2='eid',
                                   string='Employee')
    layout_id = fields.Char('Layout ID', )
    is_wfh = fields.Boolean('WFH ?')

    # @api.constrains('name')
    # def validate_workstation_name(self):
    #     if re.match("^[a-zA-Z0-9 ,./()_+-]+$", self.name) == None:
    #         raise ValidationError("Invalid Workstation Name! Please provide a valid Work Station Name.")
    #     record = self.env['kw_workstation_master'].search([]) - self
    #     for info in record:
    #         if info.name.lower() == self.name.lower():
    #             raise ValidationError(f"The Workstation {self.name} already exists.")

    @api.constrains('employee_id')
    def _check_employee_id(self):
        all_data = self.env['kw_workstation_master'].search([])
        for record in self:
            if len(record.employee_id) > 2:
                raise ValidationError("Maximum two employees allowed to tag.")
            if record.employee_id:
                all_rec = all_data - record
                for employees in record.employee_id:
                    existing_employee = all_rec.filtered(lambda rec: employees.id in rec.employee_id.ids)
                    # print(existing_employee,existing_employee.name)
                    if existing_employee:
                        raise ValidationError(f"{employees.name} has already assigned to {existing_employee[0].name}")

    # @api.constrains('layout_id')
    # def _check_layout_id(self):
    #     for record in self:
    #         if record.layout_id:
    #             layout_data = self.env['kw_workstation_master'].search([('layout_id', '=', record.layout_id)])
    #             print('layout_data : ', layout_data)
    #             if layout_data:
    #                 print('layout_data : ', record.name, ' : ', record.layout_id, ' : ', record.id)
    #                 raise ValidationError(f"{record.name} has already assigned.")

    @api.model
    def create(self, vals):
        new_record = super(kw_workstation_master, self).create(vals)
        self.env.user.notify_success(message='Workstation created successfully.')
        return new_record

    @api.multi
    def unlink(self):
        result = super(kw_workstation_master, self).unlink()
        self.env.user.notify_success(message='Workstation deleted successfully.')
        return result

    @api.multi
    def write(self, vals):
        res = super(kw_workstation_master, self).write(vals)
        self.env.user.notify_success(message='Workstation updated successfully.')
        return res

    @api.model
    def get_seat_map(self, args):
        infra_id = args.get('infra_id', False)
        print('infra_id ', infra_id)
        if infra_id == 'all':
            workstation = self.env['kw_workstation_master'].search([])
        else:
            workstation = self.env['kw_workstation_master'].search([('infrastructure.id', '=', infra_id)])
 
        seats = []
        wfo_count = 0
        wfh_count = 0
        cab_occupied = sr_occupied = mid_occupied = run_occupied = int_occupied = rec_occupied = 0
        cab_assign = cab_notassign = cab_wfh = sr_assign = sr_notassign = sr_wfh = mid_assign = mid_notassign = mid_wfh = run_assign = run_notassign = run_wfh = in_assign = in_notassign = in_wfh = reception = rec_assign = rec_notassign = 0
        laptop_count = pc_count = onpremise_count = remote_count = 0
        for record in workstation:
            is_whf = False
            if record.employee_id:
                for id_emp in record.employee_id:
                    if id_emp.is_wfh:
                        is_whf = True
            # if record.workstation_type.code == 'ceochamber':
            #     ceo_count += 1
            #     if record.employee_id:
            #         ceo_assign += 1
            #     else:
            #         ceo_notassign += 1
            if record.workstation_type.code == 'chamber':
                if record.employee_id:
                    for id_emp in record.employee_id:
                        cab_occupied += 1
                        if id_emp.is_wfh == True:
                            cab_wfh += 1
                            wfh_count += 1
                        else:
                            wfo_count += 1
                        if id_emp.issued_system == 'pc':
                            pc_count += 1
                            if id_emp.system_location == 'office':
                                onpremise_count += 1
                            if id_emp.system_location == 'home':
                                remote_count += 1
                        if id_emp.issued_system == 'laptop':
                            laptop_count += 1
                    cab_assign += 1
                else:
                    cab_notassign +=1
            elif record.workstation_type.code == 'srws':
                if record.employee_id:
                    for id_emp in record.employee_id:
                        sr_occupied += 1
                        if id_emp.is_wfh == True:
                            sr_wfh += 1
                            wfh_count += 1
                        else:
                            wfo_count += 1
                        if id_emp.issued_system == 'pc':
                            pc_count += 1
                            if id_emp.system_location == 'office':
                                onpremise_count += 1
                            if id_emp.system_location == 'home':
                                remote_count += 1
                        if id_emp.issued_system == 'laptop':
                            laptop_count += 1
                    sr_assign += 1
                else:
                    sr_notassign +=1
            elif record.workstation_type.code == 'midws':
                if record.employee_id:
                    for id_emp in record.employee_id:
                        mid_occupied += 1
                        if id_emp.is_wfh == True:
                            mid_wfh += 1
                            wfh_count += 1
                        else:
                            wfo_count += 1
                        if id_emp.issued_system == 'pc':
                            pc_count += 1
                            if id_emp.system_location == 'office':
                                onpremise_count += 1
                            if id_emp.system_location == 'home':
                                remote_count += 1
                        if id_emp.issued_system == 'laptop':
                            laptop_count += 1
                    mid_assign += 1
                else:
                    mid_notassign +=1
            elif record.workstation_type.code == 'runningws':
                if record.employee_id:
                    for id_emp in record.employee_id:
                        run_occupied += 1
                        if id_emp.is_wfh == True:
                            run_wfh += 1
                            wfh_count += 1
                        else:
                            wfo_count += 1
                        if id_emp.issued_system == 'pc':
                            pc_count += 1
                            if id_emp.system_location == 'office':
                                onpremise_count += 1
                            if id_emp.system_location == 'home':
                                remote_count += 1
                        if id_emp.issued_system == 'laptop':
                            laptop_count += 1
                    run_assign += 1
                else:
                    run_notassign +=1
            elif record.workstation_type.code == 'interns':
                if record.employee_id:
                    for id_emp in record.employee_id:
                        int_occupied += 1
                        if id_emp.is_wfh == True:
                            in_wfh += 1
                            wfh_count += 1
                        else:
                            wfo_count += 1
                        if id_emp.issued_system == 'pc':
                            pc_count += 1
                            if id_emp.system_location == 'office':
                                onpremise_count += 1
                            if id_emp.system_location == 'home':
                                remote_count += 1
                        if id_emp.issued_system == 'laptop':
                            laptop_count += 1
                    in_assign += 1
                else:
                    in_notassign += 1
            elif record.workstation_type.code == 'reception':
                reception += 1
                if record.employee_id:
                    rec_assign += 1
                    for id_emp in record.employee_id:
                        rec_occupied += 1
                        if id_emp.issued_system == 'pc':
                            pc_count += 1
                            if id_emp.system_location == 'office':
                                onpremise_count += 1
                            if id_emp.system_location == 'home':
                                remote_count += 1
                        if id_emp.issued_system == 'laptop':
                            laptop_count += 1
                else:
                    rec_notassign += 1
            emp_display_name = [[' '+emp.emp_display_name+'(WFH)' if emp.is_wfh else ' '+emp.emp_display_name+'(WFO)'] for emp in record.employee_id]
            
            seats.append({'path_id': record.layout_id if record.layout_id else '',
                          'emp_name': emp_display_name,
                          'emp_ids': [emp.id if emp.emp_display_name else '' for emp in record.employee_id],
                          'is_wfh': is_whf,
                          'name': record.name,
                          'station_id': record.id,
                          'infrastructure': workstation[0].infrastructure.name,
                          'tot_wfo':wfo_count,
                          'tot_wfh':wfh_count,
                          # 'ceo_count': ceo_count,
                          # 'ceo_assign': ceo_assign,
                          # 'ceo_notassign':ceo_notassign,
                          'cab_assign': cab_assign,
                          'cab_notassign': cab_notassign,
                          'cab_wfh': cab_wfh,
                          'sr_assign': sr_assign,
                          'sr_notassign': sr_notassign,
                          'sr_wfh': sr_wfh,
                          'mid_assign': mid_assign,
                          'mid_notassign': mid_notassign,
                          'mid_wfh': mid_wfh,
                          'run_assign': run_assign,
                          'run_notassign': run_notassign,
                          'run_wfh': run_wfh,
                          'in_assign': in_assign,
                          'in_notassign': in_notassign,
                          'in_wfh': in_wfh,
                          'reception': reception,
                          'rec_assign': rec_assign,
                          'rec_notassign':rec_notassign,
                          'sr_occupied': sr_occupied,
                          'int_occupied': int_occupied,
                          'mid_occupied': mid_occupied,
                          'cab_occupied':cab_occupied,
                          'run_occupied': run_occupied,
                          'rec_occupied': rec_occupied,
                          'laptop_count': laptop_count,
                          'pc_count': pc_count,
                          'onpremise_count': onpremise_count,
                          'remote_count': remote_count,
                          })

        return seats

    @api.model
    def save_seat_map(self, args):
        workstation = self.env['kw_workstation_master'].search([('id', '=', args['ws_id'])])
        workstation['layout_id'] = args['svg_id']
        if args['emp_ids']:
            workstation['employee_id'] = [(6, 0, [int(i) for i in args['emp_ids']])]
            # for emp in args['emp_ids']:
            #     employee = self.env['hr.employee'].search([('id', '=', int(emp))])
            #     if employee:
            #         if args['wfh_status'] == '':
            #             args['wfh_status'] = False
            #         employee.write({'is_wfh':args['wfh_status']})
        else:
            workstation['employee_id'] = [(5, 0, 0)]
        # workstation['is_wfh'] = args['wfh_status']
        res = self.write(workstation)
        return args

    @api.model
    def get_workstations(self, args):
        infra = dict()
        infra_id = self.env['kw_workstation_infrastructure'].search([('id','=',int(args.get('infra_id')))])
        infra['infrastructure'] = self.env['kw_workstation_infrastructure'].search([])
        infra['infra_code'] = infra_id.code
        infra['employees'] = self.env['hr.employee'].search([])
        if infra_id.code and infra_id.code != 'all':
            workstation_data = self.search([('infrastructure.code', '=', infra_id.code)])
        else:
            workstation_data = self.search([])
        infra['workstations'] = [(station.id, station.name) for station in workstation_data]
        # print(infra)
        return infra