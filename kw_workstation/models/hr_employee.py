from odoo import models, fields, api, tools, _
from odoo.exceptions import ValidationError


class Employee(models.Model):
    _inherit = 'hr.employee'

    issued_system = fields.Selection(string='System Issued',selection=[('pc', 'PC'), ('laptop', 'Laptop')],default='')
    system_location = fields.Selection(string='System Location',
    	selection=[('office', 'Office'), ('home', 'Remote')],default='')

    @api.multi
    def write(self, vals):
        if vals.get('is_wfh') == True or vals.get('is_wfh') == False:
            work_station = self.env['kw_workstation_master'].search([('employee_id', '=', self.id)])
            if work_station:
                work_station.write({'is_wfh': vals.get('is_wfh')})
        return super(Employee, self).write(vals)
