# -*- coding: utf-8 -*-
from . import kw_question_set_config
from . import kw_training_plan
# from . import kw_meeting_event_in
from . import kw_training_schedule
from . import kw_training_material
from . import  kw_training_feedback
from . import kw_training_attendance_details
from . import kw_training_attendance
from . import kw_training_report
from . import kw_training_assessment
from . import kw_training_score_details
from . import kw_training_score
from . import kw_question_bank_master
from . import kw_training_assessment_result
from . import kw_training
from . import res_config_settings