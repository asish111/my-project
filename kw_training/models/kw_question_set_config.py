from odoo import models, fields, api
from odoo.exceptions import ValidationError


class kw_question_set_config_training(models.Model):
    _inherit = 'kw_skill_question_set_config'

    # training_id = fields.Many2one(
    #     string='Training', comodel_name='kw_training',)

    # user_is_participant = fields.Boolean(
    #     string='Is Participant?', default=False, compute="_compute_if_participant"
    # )
    # user_has_given_assessment = fields.Boolean(
    #     string='Assessment Given?', default=False, compute="_compute_if_user_given_assessment"
    # )

    # user_is_trainer = fields.Boolean(
    #     string='Is Trainer?', default=False, compute="_compute_if_trainer"
    # )

    # user_is_manager = fields.Boolean(
    #     string='Is Manager?', default=False, compute="_compute_if_manager")
    
    @api.multi
    def _compute_if_participant(self):
        current_employee_id = self.env['hr.employee'].search([('user_id','=',self._uid)],limit=1)
        employee_id = current_employee_id.id if current_employee_id else False
        for assessment in self:
            if employee_id and assessment.assessment_type == 'training' and assessment.training_id:
                if assessment.training_id.plan_ids and assessment.training_id.plan_ids.participant_ids:
                    employee_participant = assessment.training_id.plan_ids.participant_ids.filtered(
                        lambda r: r.id == employee_id)
                    if employee_participant:
                        assessment.user_is_participant = True
    @api.multi
    def _compute_if_user_given_assessment(self):
        for assessment in self:
            # check if user has given assessment
            assessmnet_given = self.env['kw_skill_answer_master'].search(
                [('user_id', '=', self._uid), ('set_config_id','=',assessment.id)])
            if assessmnet_given:
                assessment.user_has_given_assessment = True


    @api.multi
    def _compute_if_trainer(self):
        current_employee_id = self.env['hr.employee'].search([('user_id','=',self._uid)],limit=1)
        employee_id = current_employee_id.id if current_employee_id else False
        for assessment in self:
            if employee_id and assessment.assessment_type == 'training' and assessment.training_id:
                if assessment.training_id.plan_ids and assessment.training_id.plan_ids.internal_user_ids:
                    employee_trainer = assessment.training_id.plan_ids.internal_user_ids.filtered(
                        lambda r: r.id == employee_id)
                    if employee_trainer:
                        assessment.user_is_trainer = True
                

    @api.multi
    def _compute_if_manager(self):
        for assessment in self:
            if assessment.assessment_type == 'training' and assessment.training_id:
                if self.env.user.has_group('kw_training.group_kw_training_manager'):
                    assessment.user_is_manager = True
    @api.multi
    def view_employeewise_answer(self):
        res = self.env['ir.actions.act_window'].for_xml_id(
            'kw_training', 'kw_user_trainingtest_report_action_window')
        res['domain'] = [('set_config_id', '=', self.id)]
        # if not self.select_individual:
        #     raise ValidationError("No individual participants found.Unable to show result.")
        # ctx = self.env.context.copy()
        # ctx.update({'set_config_id': self.id,
        #             'employee_ids': self.select_individual.ids})
        # self.env['kw_training_assessment_result'].with_context(ctx).init()
        # res = self.env['ir.actions.act_window'].for_xml_id(
        #     'kw_training', 'action_kw_training_assessment_result_act_window')
        return res


# access_kw_training_assessment_result_manager, kw_training_assessment_result manager, model_kw_training_assessment_result, group_kw_training_manager, 1, 0, 0, 0
# access_kw_training_assessment_result_employee, kw_training_assessment_result employee, model_kw_training_assessment_result, group_kw_training_employee, 1, 0, 0, 0


   
    @api.multi
    def view_assessment_answer(self):
        assessment_given = self.env['kw_skill_answer_master'].search(
            [('user_id', '=', self._uid), ('set_config_id', '=', self.id)],limit=1)
        view_id = self.env.ref(
            'kw_skill_assessment.kw_question_user_report_form_view').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'kw_skill_answer_master',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': view_id,
            'res_id': assessment_given.id,
            'target': 'self',
            'flags': {"toolbar": False}
        }


    @api.model
    def create(self, values):
        # print("context create",self._context)
        result = super(kw_question_set_config_training, self).create(values)
        if 'active_model' and 'active_id' in self._context and self._context['active_model'] == 'kw_training_assessment':
            assessment = self.env['kw_training_assessment'].browse(self._context['active_id'])
            if not assessment.assessment_id:
                assessment.assessment_id = result.id
        return result
