# -*- coding: utf-8 -*-
from datetime import date
from odoo import models, fields, api

class TrainingFeedbackDetails(models.Model):
    _name = "kw_training_feedback_details"
    _description = "Kwantify Training Feedback Details"

    
    feedback_id = fields.Many2one("kw_training_feedback",string="Feedback ID",ondelete='cascade')
    training_id = fields.Many2one("kw_training", related="feedback_id.training_id",string="Training ID")
    category_id = fields.Many2one("kw_feedback_category",string="Category",required=True,)
    description = fields.Char(string="Description", related="category_id.description", readonly=True,store=True)
    marks = fields.Selection(string="Marks", selection=[('1', 1), ('2', 2), ('3', 3), ('4', 4),('5', 5), ('6', 6),
                                                         ('7', 7), ('8', 8), ('9', 9), ('10', 10), ('NA', 'NA')],)
    remark = fields.Char("Remarks",)


