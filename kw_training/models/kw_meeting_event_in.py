# -*- coding: utf-8 -*-
from odoo import models, fields, api

class MeetingScheduleIn(models.Model):
    _inherit = "kw_meeting_events"

    @api.model
    def default_get(self, fields):
        if self.env.context.get('default_training_session_id'):
            self = self.with_context(
                default_res_model_id=self.env.ref(
                    'kw_training.model_kw_training_schedule').id,
                default_res_id=self.env.context['default_training_session_id']
            )

        defaults = super(MeetingScheduleIn, self).default_get(fields)
        return defaults

    training_session_id = fields.Many2one('kw_training_schedule', string="Session id",
    ondelete='set null'
    )
