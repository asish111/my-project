# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class TrainingScoreDetails(models.Model):
    _name = 'kw_training_score_details'
    _description = "Kwantify Training Score Details"
    _rec_name = "score_id"

    score_id = fields.Many2one("kw_training_score", string='Score',ondelete="cascade")
    training_id = fields.Many2one(related="score_id.training_id")
    participant_id = fields.Many2one("hr.employee", string='Employee',required=True)
    score = fields.Integer(string='Score', default=0,required=True,size=100)

    @api.constrains('score')
    def check_value(self):
        for rec in self:
            if rec.score > 100 or rec.score < 0:
                raise ValidationError('Enter score between 0-100.')
