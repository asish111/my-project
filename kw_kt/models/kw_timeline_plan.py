from odoo import models, fields, api


class kw_timeline_plan(models.Model):
    _name = "kw_timeline_plan" 
    _description = "KT Timeline Plan"
    _rec_name = 'employee_name'

    project = fields.Many2one('project.project',string="Project")
    employee_name = fields.Many2one('hr.employee', string='Employee Name')
    kt_date = fields.Date('KT Date')
    start_time = fields.Datetime('Start Time')
    end_time = fields.Datetime('End Time')
    description = fields.Text('Description')
