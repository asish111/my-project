# -*- coding: utf-8 -*-
from odoo import models, fields, api


class kw_kt_category(models.Model):
    _name = "kw_kt_category" 
    _description = "KT category model"
    _rec_name = 'category_name'

    category_name = fields.Char("Category Name")
    applied_by = fields.Selection([('applicant','Applicant'),('hr','HR')],"Applied By", default="applicant")
    fill_eos = fields.Selection([('yes','Yes'),('no','No')],"Fill EOS", default="no")


class kw_kt_tag(models.Model):
    _name = "kw_kt_tag" 
    _description = "User Tag to KT"
    _rec_name = 'category'

    category = fields.Many2one('kw_kt_category',string="Category")
    department = fields.Many2one('hr.department', string='Department')
    user_name = fields.Many2one('hr.employee', string='User Name')
    effective_date = fields.Date('Effective Date')
    last_date = fields.Date('Last Date')
