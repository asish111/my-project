from odoo import models, fields, api


class kw_project_document(models.Model):
    _name = "kw_project_document" 
    _description = " Upload Project Documents"
    _rec_name = 'for_project'

    document_attach = fields.Binary(string="Document Attach")
    for_project = fields.Many2one('project.project',string="For Project")
    description = fields.Char(string='Description')
    
class department_inherit(models.Model):
    _inherit = "hr.department"

    notice_period = fields.Char("Notice Period") 