# -*- coding: utf-8 -*-
{

    'name': "Knowledge Transfer",
    'summary': """Knowledge Transfer""",
    'description': """ Knowledge Transfer Module
        
    """,

    'author': "CSM Technologies",
    'website': "www.csm.co.in",

    'category': 'End of service',
    'version': '1',

    # any module necessary for this one to work correctly
    'depends': ['base','hr','mail','project'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/kw_kt_configuration.xml',
        'views/kw_user_tag_to_kt.xml',
        'views/kw_timeline_plan.xml',
        'views/kw_project_document.xml',
        'views/kw_menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}