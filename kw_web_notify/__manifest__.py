{
    'name': 'Kwantify Web Notify',
    'summary': "Send notification messages to user",
    'author': "CSM Technologies",
    'website': "http://www.csmpl.com",
    'category': 'Kwantify',
    'version': '0.1',
    'depends': ['web','bus','base'],
    'data': [
        'views/web_notify.xml'
    ],
    'demo': [
        'views/res_users_demo.xml'
    ],
    'installable': True,
}
