# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from ..models.zoomus.client import ZoomClient
import json, requests
from odoo.exceptions import UserError, ValidationError, Warning


class EmailZoom(models.TransientModel):
    _name = 'zoom_email_employee'
    _description = 'Zoom Email for employee'

    zoom_id = fields.Char('Zoom Email ID')

    @api.multi
    def add_zoom_details(self):
        """ Add zoom email to company zoom account"""
        if not self.zoom_id:
            raise UserError(_("Please add Zoom email ID."))
        current_user = self.env.user
        if current_user and self.zoom_id:
            client = ZoomClient(current_user.company_id.jwt_api_key, current_user.company_id.jwt_api_secret)
            user_info = {
                "email": self.zoom_id,
                "type": 1,
                "first_name": current_user.name[0:current_user.name.rindex(' ')] or current_user.name[0:],
                "last_name": current_user.name[current_user.name.rindex(' ')+1:] 
            }
            zoom_response = client.user.create(action="create", user_info=user_info)
            user_data = json.loads(zoom_response.content)
            if user_data.get('code') == 1005:
                raise UserError(_("Zoom User already exist in the account: %s.") % (self.zoom_id))
            else:
                if user_data.get('id'):
                    user_info['zoom_id'] = user_data.get('id')
                    zoom_user = self.env['kw_zoom_users'].find_or_create(user_info)
                    if zoom_user:
                        current_user.sudo().write({'zoom_id': zoom_user.id})
                    self.env.user.notify_success(message='You might have got an email from zoom to authenticate.\nPlease approve the request.')
        return True

