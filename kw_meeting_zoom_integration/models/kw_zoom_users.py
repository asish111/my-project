# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ZoomUsers(models.Model):
    _name = 'kw_zoom_users'
    _description = 'Zoom User Information'
    _order = 'first_name'

    zoom_id = fields.Char('Zoom ID')
    first_name = fields.Char('First Name')
    last_name = fields.Char('Last Name')
    email = fields.Char('Email', required=True)
    type = fields.Integer(string='Type', default=1)
    # type = fields.Selection([('1', 'Basic'),('2', 'Licensed'),('3', 'On-prem')], default=1, required=True,string='Type')
    role_name = fields.Char('Role Name')
    pmi = fields.Char('PMI')
    use_pmi = fields.Boolean('Use PMI', default=False)
    vanity_url = fields.Char('Vanity URL')
    personal_meeting_url = fields.Char('Personal Meeting URL')
    timezone = fields.Char('Time Zone')
    verified = fields.Char('Verified')
    dept = fields.Char('Dept')
    created_at = fields.Char('Created at')
    last_login_time = fields.Char('Last Login Time')
    last_client_version = fields.Char('Last Client Version')
    pic_url = fields.Char('pick URL')
    host_key = fields.Char('Host key')
    jid = fields.Char('JID')
    group_ids = fields.Text('Groups')
    im_group_ids = fields.Text('IM Groups')
    account_id = fields.Char('Account')
    language = fields.Char('Language')
    # language = fields.Many2one('res.country','Country')
    phone_country = fields.Text('Country')
    # phone_country = fields.Many2one('res.country','Country')
    phone_number = fields.Char('Phone No')
    status = fields.Selection([('active', 'Active'), ('inactive', 'InActive')], 'Status', default='active')
    user_id = fields.Many2one('res.users', string='User')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, readonly=True)
    type_id = fields.Char(compute='check_user', string="User Type", readonly=True, store=False, required=False)
    mapped_user = fields.Char(compute='check_user', string="User Mapped", readonly=True, store=False, required=False)
    active = fields.Boolean('Active', default=False)
    
    @api.model
    def create(self, values):
        if values.get('group_ids') == []:
            values['group_ids'] = ''
        if values.get('im_group_ids') == []:
            values['im_group_ids'] = ''
        if values.get('email') and not values.get('user_id'):
            user_rec = self.env['res.users'].search([('email', '=', values.get('email'))])
            values['user_id'] = user_rec.id if user_rec else None
        return super(ZoomUsers, self).create(values)

    @api.model
    def find_or_create(self, vals):
        zoom_id = vals.get('zoom_id') if vals.get('zoom_id') else False
        record = self.search([('zoom_id', '=', zoom_id), '|', ('active', '=', True), ('active', '=', False)], limit=1)
        res_user = self.env['res.users'].search([('email', '=', vals.get('email'))])
        if record and not record.user_id and res_user:
            record.write({'user_id': res_user.id})
        elif not record and res_user:
            vals['user_id'] = res_user.id
        return record or self.create(vals)

    @api.multi
    def check_user(self):
        opt = {1: 'Basic', 2: 'Licensed', 3: 'On-prem'}
        for rec in self:
            rec.type_id = opt.get(rec.type) if opt.get(rec.type) else 'N/A'
            rec.mapped_user = 'Yes' if rec.user_id else 'No'
        return True

    @api.multi
    def check_zoom_user(self, args):
        if self.env.user.zoom_id and self.env.user.zoom_id.active == True:
            return {'status': 'success'}
        elif self.env.user.zoom_id and self.env.user.zoom_id.active == False:
            return {'status': 'pending'}
        else:
            return {'status': 'not found'}

    @api.model
    def create_or_write(self, vals):
        zoom_id = vals.get('zoom_id') if vals.get('zoom_id') else False
        zoom_user = self.search([('zoom_id', '=', zoom_id), '|', ('active', '!=', False), ('active', '=', False)], limit=1)
        res_user = self.env['res.users'].search([('email', '=', vals.get('email'))])

        zoom_user_update = vals
        """Update res_user_id in zoom_user table"""
        if zoom_user and not zoom_user.user_id and res_user:
            zoom_user_update['user_id'] = res_user.id

        """Create or Update zoom user"""
        zoom_user_update['active'] = True
        if zoom_user:
            zoom_user.write(zoom_user_update)
        elif not zoom_user and res_user:
            vals['user_id'] = res_user.id
            vals['active'] = True
        return zoom_user or self.create(vals)
