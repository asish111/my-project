# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ZoomMeeting(models.Model):
    _name = 'kw_zoom_meeting'
    _description = 'Zoom Meeting Information'

    topic = fields.Char('Topic')
    type = fields.Integer('Type',
                          default=2)  # 1-Instant meeting 2-Scheduled meeting 3- Recurring meeting with no fixed time 8-Reccuring meeting with fix time
    start_time = fields.Char('Start Time')
    duration = fields.Char('Duration')  # In minutes
    schedule_for = fields.Char('Schedule For')
    timezone = fields.Char('Timezone')
    password = fields.Char('Password')
    agenda = fields.Char('Agenda')
    uuid_zoom = fields.Char('UUID')
    zoom_id = fields.Char('Zoom ID')
    host_id = fields.Char('Host ID')
    status = fields.Char('Status')
    created_at = fields.Char('Created at')
    start_url = fields.Char('Start URL')
    join_url = fields.Char('Join URL')
    h323_password = fields.Char('H323 Password')
    pstn_password = fields.Char('PSTN Password')
    encrypted_password = fields.Char('Encr Password')
    misc_settings = fields.Text('Settings')
    meeting_id = fields.Many2one('kw_meeting_events', string='Meeting')
