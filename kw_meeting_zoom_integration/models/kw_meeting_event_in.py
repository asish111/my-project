# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import datetime
from datetime import datetime
# from backports.datetime_fromisoformat import MonkeyPatch
import json
from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError
from .zoomus.client import ZoomClient

class MeetingEvent(models.Model):
    _inherit = 'kw_meeting_events'
    _order = 'id'

    """##Online zoom meeting integration"""

    @api.depends('logged_user_id')
    def _check_logged_user(self):
        """Check if logged user same as mmeeting crated user"""
        for res in self:
            # if self.env.user.zoom_id:
            #     res.update({'zoom_enabled_user': True})

            if res.create_uid.id == self.env.user.id:
                res.update({'is_create_user': True})
            else:
                res.update({'is_create_user': False})

    @api.model
    def _get_meeting_room_domain(self):
        if self.env.user.has_group('kw_meeting_schedule.group_kw_meeting_schedule_manager'):
            return [('location_id', '=', self.env.user.company_id.id)]
        else:
            return [('location_id', '=', self.env.user.company_id.id),
                    ('restricted_access', '=', False), ]


    online_meeting = fields.Selection(string='Online Meeting', selection=[('zoom', 'Zoom')])
    online_meeting_id = fields.Many2one('kw_zoom_meeting', string='Online Meeting ID')
    online_meeting_start_url = fields.Char('Online Meeting Start URL', readonly=True)
    online_meeting_join_url = fields.Char('Online Meeting Join URL', readonly=True)
    location_id = fields.Many2one('kw_res_branch', string="Location", required=False)  # domain=_get_location_domain,
    meeting_room_id = fields.Many2one(
        string='Meeting Room',
        comodel_name='kw_meeting_room_master',
        ondelete='restrict',
        domain=_get_meeting_room_domain,
        required=False
    )
    logged_user_id = fields.Many2one('res.users', default=lambda self: self.env.user)
    is_create_user = fields.Boolean('is created uid', default=True, compute='_check_logged_user')
    zoom_enabled_user = fields.Boolean('Enabled Zoom User', default=False)
    disable_meeeting = fields.Boolean('Enable', compute='_compute_disable_meeting',default=False)
    participant_meeeting = fields.Boolean('Enable', compute='_compute_is_participant',default=False)

    def _compute_is_participant(self):
        for res in self:
            if self.env.user.employee_ids in res.employee_ids:
                res.participant_meeeting = True
            else:
                res.participant_meeeting = False

    @api.onchange('online_meeting')
    def onchange_online_meeting(self):
        if self.online_meeting == 'zoom':
            if not self.env.user.zoom_id:
                raise UserError(_("Please link your zoom account to create zoom meeting."))
            if self.env.user.zoom_id.active == False:
                raise UserError(_("Zoom account activation still pending, Please wait before."))
                
    @api.depends('stop')
    def _compute_disable_meeting(self):
        """ Disable meeting button after meeting over"""
        for res in self:
            if res.stop:
                if res.stop < datetime.now():
                    res.disable_meeeting = True
                else:
                    res.disable_meeeting = False

    @api.constrains('agenda_ids', 'meeting_room_id', 'categ_ids', 'meeting_category')
    def validate_agenda(self):
        for record in self:
            if not record.meeting_room_id and not record.online_meeting:
                raise ValidationError("Please select meeting room.")

            if not record.agenda_ids:
                raise ValidationError("Please enter meeting agenda details.")

            if not record.categ_ids:
                raise ValidationError("Please select at least one Meeting Type.")

            if not record.meeting_category:
                raise ValidationError("Please select meeting category.")

    @api.constrains('meeting_room_id', 'start', 'stop', 'duration', 'recurrency', 'interval')
    def validate_meeting_room_availability(self):
        for record in self:
            meeting_events = self.env['kw_meeting_events']
            if not record.meeting_room_id and not record.online_meeting:
                raise ValidationError("Please select meeting room.")
            if record.meeting_room_id and record.start and record.stop and record.interval and record.duration:
                start_datetime = record.start.strftime("%Y-%m-%d %H:%M:%S")
                stop_datetime = record.stop.strftime("%Y-%m-%d %H:%M:%S")
                if record.recurrency:
                    rdates = record._get_recurrent_dates_by_event()
                    meeting_start_date = rdates[0][0].strftime("%Y-%m-%d")
                    meeting_end_date = rdates[-1][0].strftime("%Y-%m-%d")
                    existing_event_data = meeting_events.search(
                        [('recurrency', '=', False), ('meeting_room_id', '=', record.meeting_room_id.id),
                         ('start', '>=', meeting_start_date), ('start', '<=', meeting_end_date),
                         ('id', '<>', record.id)])
                    available_date_count = 0
                    for r_start_date, r_stop_date in rdates:
                        # #check for overlapping days events
                        overlapping_events = existing_event_data.filtered(lambda event: not (
                                event.stop <= r_start_date.replace(tzinfo=None) or event.start >= r_stop_date.replace(
                            tzinfo=None)))
                        if not overlapping_events:
                            available_date_count += 1
                    if available_date_count == 0:
                        raise ValidationError(
                            'There is no available date in the selected date range. Please choose another date range and try again. ')
                else:
                    calendar_events = meeting_events.search([('id', '<>', record.id), ('recurrency', '=', False),
                                                             ('meeting_room_id', '=', record.meeting_room_id.id), '|',
                                                             '&', ('start', '>=', start_datetime),
                                                             ('start', '<=', stop_datetime), '&',
                                                             ('stop', '>=', start_datetime),
                                                             ('stop', '<=', stop_datetime)])
                    if calendar_events:
                        raise ValidationError("The meeting room \"" + str(
                            record.meeting_room_id.name) + "\" has already been booked for \"" + str(
                            calendar_events.mapped('name')) + " on " + str(calendar_events.mapped(
                            'display_time')) + "\" . Please choose another time slot and try again .")

    @api.model
    def create(self, values):
        """
        @Create: Creation of zoom meeting if user is zoom user
        @Delete: Delete the created zoom meeting if meeting not created.
        """
        if values.get('online_meeting'):
            if values.get('company_id'):
                company = self.env['res.company'].search([('id', '=', values.get('company_id'))])
                if company:
                    client = ZoomClient(company.jwt_api_key, company.jwt_api_secret)
                    meeting_datetime = datetime.strptime(values.get('start_datetime'), "%Y-%m-%d %H:%M:%S")
                    # meeting_datetime = datetime.fromisoformat(values.get('start_datetime'))
                    if self.env.user.zoom_id:
                        zoom_response = client.meeting.create(
                            user_id=self.env.user.zoom_id.zoom_id,
                            topic=values.get('name'),
                            start_time=meeting_datetime,
                            duration=values.get('duration') * 60,
                            password="Csmpl@123")
                        zvalues = json.loads(zoom_response.content)
                        if zvalues.get('uuid'):
                            zvalues['zoom_id'] = zvalues.get('id')
                            zvalues['uuid_zoom'] = zvalues.get('uuid')
                            zvalues['misc_settings'] = zvalues.get('settings')
                            del zvalues['id']
                            del zvalues['uuid']
                            del zvalues['settings']
                            online_meet = self.env['kw_zoom_meeting'].create(zvalues)
                            values['online_meeting_id'] = online_meet.id
                            values['online_meeting_start_url'] = zvalues.get('start_url')
                            values['online_meeting_join_url'] = zvalues.get('join_url')

        meeting = super(MeetingEvent, self).create(values)
        
        # except Exception as e:
            # meeting = self
        if not meeting.online_meeting_id:
            zoom_response = client.meeting.delete(id=meeting.online_meeting_id.zoom_id) if meeting.online_meeting_id else False
            meeting.online_meeting_id.unlink()
            # pass
        return meeting
        # else:
        #     meeting = super(MeetingEvent, self).create(values)
        # return meeting

    @api.multi
    def action_start_meeting(self):
        return {
            'type': 'ir.actions.act_url',
            'url': self.online_meeting_start_url,
            'target': 'new',
        }

    @api.multi
    def action_join_meeting(self):
        return {
            'type': 'ir.actions.act_url',
            'url': self.online_meeting_join_url,
            'target': 'new',
        }

    @api.multi
    def unlink(self):
        for meeting in self:
            if meeting.online_meeting and meeting.online_meeting == 'zoom':
                company = self.env['res.company'].search([('id', '=', meeting.company_id.id)])
                client = ZoomClient(company.jwt_api_key, company.jwt_api_secret)
                if rec.online_meeting_id:
                    client.meeting.delete(id=meeting.online_meeting_id.zoom_id)
                    rec.online_meeting_id.unlink()
        return super(MeetingEvent, self).unlink()

    @api.multi
    def write(self, values):
        self.ensure_one()
        result = super(MeetingEvent, self).write(values)
        if result:
            if self.online_meeting and self.online_meeting == 'zoom':
                company = self.env['res.company'].search([('id', '=', self.company_id.id)])
                client = ZoomClient(company.jwt_api_key, company.jwt_api_secret)
                if self.online_meeting_id:
                    # meeting_datetime = datetime.strptime(self.start_datetime, "%Y-%m-%d %H:%M:%S")
                    client.meeting.update(id=self.online_meeting_id.zoom_id,
                                          topic=self.name,
                                          start_time=self.start_datetime,
                                          duration=self.duration * 60
                                          )

                    online_meet = self.env['kw_zoom_meeting'].search([('id', '=', self.online_meeting_id.id)])
                    online_meet.write({'topic': self.name,
                                       'start_time': self.start_datetime,
                                       'duration': self.duration * 60})
        return result


    @api.multi
    def update_participant_webhook(self, values):
        meeting_id = values.get('id')
        participant = values.get('participant')
        meeting = self.env['kw_zoom_meeting'].search([('zoom_id','=', meeting_id)])
        if meeting:
            event_id = self.search([('online_meeting_id','=', meeting.id)])
            if event_id:
                zoom_user = self.env['kw_zoom_users'].search([('zoom_id','=', participant.get('id'))],limit=1)
                if zoom_user:
                    for attendee in event_id.attendee_ids:
                        if attendee.employee_id in zoom_user.user_id.employee_ids:
                            attendee.write({'attendance_status': True})
        return
                    


