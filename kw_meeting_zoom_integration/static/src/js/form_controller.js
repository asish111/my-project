odoo.define('kw_meeting_zoom_integration.FormController', function (require) {
"use strict";

var core = require('web.core');
var FormController = require('web.FormController');
var rpc = require('web.rpc');
var session = require('web.session');
var _t = core._t;

FormController.include({
	renderButtons: function($node) {
	   this._super.apply(this, arguments);
	   if(this.modelName == 'kw_meeting_events' && this.viewType == 'form'){
	        this.check_zoom_user();
	    }
	   if (this.$buttons) {
	     this.$buttons.find('.oe_action_tag_zoom_button').click(this.proxy('on_action_tag_zoom'));
	   }
	},
	on_action_tag_zoom: function(e){
        e.preventDefault();
        this.do_action({
            name: _t("kw_meeting_zoom_integration.tag_zoom_mail_employee_account_view"),
            type: "ir.actions.act_window",
            res_model: "zoom_email_employee",
            domain : [],
            views: [[false, "form"]],
            target: 'new',
            context: {},
            view_type : 'form',
            view_mode : 'form'
        });
    },
    check_zoom_user: function(){
        var self = this;
        var user = session.uid;
        rpc.query({
                model: 'kw_zoom_users',
                method: 'check_zoom_user',
                args: [[user],{'id':user}],
            }).then(function (e) {
                $(".o_menu_apps").find('.dropdown-menu[role="menu"]').removeClass("show");
                if(e.status == 'success'){
                    self.$buttons.find('.oe_action_tag_zoom_button').hide();
                }else if(e.status == 'pending'){
                    self.$buttons.find(".oe_action_tag_zoom_button")
                            .html("Pending Zoom Activation")
                            .removeClass('btn-success')
                            .addClass('btn-warning')
                            .prop('disabled',true);
                }
            });
    }
});

return FormController;

});
