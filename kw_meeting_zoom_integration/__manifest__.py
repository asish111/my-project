# -*- coding: utf-8 -*-
{
    'name': "Kwantify Meeting Zoom Integration",

    'summary': """
        Schedule your meetings with zoom""",
    'description': """
        Scheduling of daily day to day meetings with Zoom 
    """,
    'author': "Kwantify",
    'website': "https://www.csm.co.in",
    'category': 'Kwantify',
    'version': '1.0',
    # 'external_dependencies': {
    #     'python': [
    #         'pyjwt',
    #     ],
    # },
    'depends': ['base', 'web', 'kw_meeting_schedule','hr'],
    'data': [
        'security/ir.model.access.csv',
        'data/cron_get_zoom_user_deatails.xml',
        'data/mail_meeting_invitation_data.xml',
        'data/mail_meeting_invitation_external.xml',
        'data/mail_meeting_invitation_admin_it_team.xml',
        'wizard/zoom_account_tag_view.xml',
        'views/templates.xml',
        'views/res_company.xml',
        'views/meeting_event.xml',
    ],
    'demo': [
    ],
    'qweb': [
     "static/src/xml/base.xml",
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
