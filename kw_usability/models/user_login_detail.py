# -*- coding: utf-8 -*-
from datetime import date
from odoo.http import request
from odoo import models, fields, api
from odoo.exceptions import AccessDenied

class LoginUserDetail(models.Model):
    _inherit = 'res.users'
    _description = "User login details"

    @api.model
    def _check_credentials(self, password):
        try:
            result = super(LoginUserDetail, self)._check_credentials(password)
            ip_address = request.httprequest.environ['REMOTE_ADDR']
            params = {'user_id': self._uid, 'name': self.name, 'ip_address': ip_address, 'status': "Success"}
            self.env['user_login_detail'].sudo().create(params)
            return result
        except Exception:
            ip_address = request.httprequest.environ['REMOTE_ADDR']
            params = {'user_id': self._uid, 'name': self.name, 'ip_address': ip_address, 'status': "Failed",}
            request.env['user_login_detail'].sudo().create(params)
            raise AccessDenied()


class user_login_detail(models.Model):
    _name = 'user_login_detail'
    _description = "User login details"
    _order = 'id DESC'

    user_id = fields.Integer("User ID")
    name = fields.Char(string="User Name")
    date_time = fields.Datetime(string="Login date / time", default=lambda self: fields.datetime.now())
    ip_address = fields.Char(string="IP Address")
    status = fields.Char(string="Login Status")
    emp_code = fields.Char('Employee Code', compute='_get_employee_code')

    @api.model
    def _get_year_list(self):
        current_year = date.today().year
        return [(str(i), i) for i in range(current_year, 1953, -1)]

    @api.model
    def get_year_options(self):
        return self._get_year_list()

    def _get_employee_code(self):
        for user in self:
            records = self.env['hr.employee'].search([('user_id', '=', user.user_id)])
            if records:
                user.emp_code = records.emp_code