from odoo import api, fields, models, tools
from odoo.exceptions import UserError,ValidationError
import re



class kw_asset(models.Model):
    _inherit = "account.asset.asset"

    name = fields.Many2one('product.product', string="Name",
    required=True,
     domain=[('product_tmpl_id.is_asset','=',True)])


    @api.onchange('name')
    def _name_onchange(self):
        for record in self:
            # print(record.name)
            asset_rec = self.env['account.asset.asset'].sudo().search([('name.id','=',record.name.id)])
            if len(asset_rec) > 0:
                raise ValidationError(f"Record for [{record.name.product_tmpl_id.default_code}] {record.name.product_tmpl_id.name} is already present please select another")






class kw_asset_report(models.Model):
    _name = "kw_asset_report"
    _description    = "Asset Custom Report"
    _auto           = False
    _order          = "category_id,product,depreciation_date asc"
    # _rec_name       = 'Product'

    category_id = fields.Char(string="Category Name")
    product = fields.Char(string="Asset Name")
    purchase_date = fields.Date(string="Purchase Date")
    value = fields.Float(string="Gross Amount")
    salvage_value = fields.Float(string="Salvage Value")
    value_residual = fields.Float(string="Residual Value")
    depreciation_date = fields.Date(string="Depreciation Dates")
    amount = fields.Float(string="Depreciation Amount")
    current_value = fields.Float(string="Current Value")
    boolean_invisible = fields.Boolean(string='Boolean Invisible',compute='_get_value' )
        
        
        
        
        
    def _get_value(self):
        dict_prod = {}
        for record in self:
            if record.product not in dict_prod:
                dict_prod[record.product] = [record.id]
        for dict_p in dict_prod:
            for rec in self:
                if rec.product == dict_p and rec.id == dict_prod[dict_p][0] :
                    rec.boolean_invisible = True
                # print(dict_p,dict_prod[dict_p][0])


    # @api.model_cr
    # def init(self):
    #     tools.drop_view_if_exists(self._cr, 'view_kw_asset_report')
    #     self._cr.execute("""
    #         create or replace view view_kw_asset_report as (
    #             select e.id as id, a.name as category_id,concat('[',d.default_code,']',' ', d.name) as product,b.date as purchase_date,b.value,b.salvage_value,(b.value-b.salvage_value) as value_residual,e.depreciation_date,e.depreciated_value as amount,e.remaining_value as current_value
    #             from account_asset_category a join account_asset_asset b on a.id = b.category_id
    #             join product_product c on c.id = b.name join product_template d on c.product_tmpl_id = d.id
    #             join account_asset_depreciation_line e  on b.id = e.asset_id)""")
    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute(""" CREATE or REPLACE VIEW %s as (
            select e.id as id, a.name as category_id,concat('[',d.default_code,']',' ', d.name) as product,b.date as purchase_date,b.value,b.salvage_value,(b.value-b.salvage_value) as value_residual,e.depreciation_date,e.amount as amount,e.remaining_value as current_value
            from account_asset_category a join account_asset_asset b on a.id = b.category_id
            join product_product c on c.id = b.name join product_template d on c.product_tmpl_id = d.id
            join account_asset_depreciation_line e  on b.id = e.asset_id)""" % (self._table))

        
        