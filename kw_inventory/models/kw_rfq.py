from odoo import api, fields, models
from odoo.addons import decimal_precision as dp
from datetime import date,datetime
from odoo.exceptions import ValidationError
import re
from odoo.addons.purchase.models.purchase import PurchaseOrder as Purchase

class kw_rfq(models.Model):
    _inherit = "purchase.order"

    


    boolean_create_po = fields.Boolean(string="Po create boolean",default=False)
    qc_ids = fields.Many2many('kw_quotation_consolidation','kw_qc_purchase_rel', string="Quotation Consolidation No")
    partner_id = fields.Many2one('res.partner', string='Vendor', help="You can find a vendor by its Name, TIN, Email or Internal Reference.",required=False)
    # qc_items_record_id = fields.Integer(string='Quotation Consolidation Items Record Id')
    state = fields.Selection([
        ('draft', 'PO'),
        ('sent', 'PO Sent'),
        ('to approve', 'To Approve'),
        ('purchase', 'Confirm PO'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ('reject','Rejected')
    ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')
    req_id_rel = fields.Many2many('kw_purchase_requisition','kw_rfq_po_rel',string='Purchase Requision Rel Id')

    # req_id = fields.Many2one('kw_purchase_requisition',string='Purchase Requision Id',domain="[('id','in',req_id_rel)]")
    cc_code = fields.Char('Cost Center Code')
    budget_code = fields.Char("Budget Code")
    estimated_value = fields.Integer("Estimated Value")
    budgeted_value = fields.Integer("Budgeted Value")
    
    contact_prsn_name = fields.Char('Contact Person Name')
    contact_prsn_number = fields.Char('Contact Person Number')

    
    # @api.onchange("req_id")
    # def _change_req(self):
    #     # req = self.env['purchase.order'].sudo().search([('req_id','=','self.id')])
    #     for record in self:
    #         print(record.req_id,"Record=====")
    #         print(self.cc_code,"CC code====")
    #         record.cc_code = str(record.req_id.cost_center_code.code) + ' ' + str(record.req_id.cost_center_code.name)
    #         record.budget_code = record.req_id.budget_code
    #         record.estimated_value = record.req_id.estimated_value
    #         record.budgeted_value = record.req_id.budgeted_value
    

        
        

    @api.multi
    def move_qc(self):
        self.state = 'reject'
        for items in self.order_line:
            qc_item = self.env['kw_quotation_consolidation_items'].sudo().search([('id','=',items.quotation_items_ids.id)])
            if qc_item:
                qc_item.po_create = False



    # @api.onchange("indent")
    # def _set_items(self):
    #     for record in self:
    #         record.order_line = False
    #         for rec in record.indent:
    #             if rec.add_product_consolidation_rel:
    #                 vals = []
    #                 for items in rec.add_product_consolidation_rel:
    #                     product = self.env['product.product'].sudo().search([('id','=',items.item_code.id)])
    #                     unit = product.uom_id.id
    #                     vals.append([0,0,{
    #                         'product_id':items.item_code.id if items.item_code else False,
    #                         'name':items.item_description if items.item_description else False,
    #                         'product_qty': items.quantity_required if items.quantity_required else False,
    #                         'date_planned':datetime.now(),
    #                         'product_uom': unit,
    #                         'price_unit':0,
    #                     }])
    #                 record.order_line = vals
    #             else:
    #                 record.order_line=False

class kw_purchase_order_line(models.Model):
    _inherit = "purchase.order.line"
        
    quotation_items_ids = fields.Many2one('kw_quotation_consolidation_items',string='Quotation items')
    boolean_create = fields.Boolean(string="Po boolean",default=False)