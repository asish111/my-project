from odoo import api, fields, models
from odoo.addons import decimal_precision as dp
from odoo.exceptions import ValidationError
import re

class ProductTemplate(models.Model):
    _inherit = "product.template"
    _order = 'default_code asc'
    

    # type = fields.Selection([
    #     ('consu', 'Consumable'),
    #     ('service', 'Service')], string='Product Type', default='consu', required=False,
    #     help='A storable product is a product for which you manage stock. The Inventory app has to be installed.\n'
    #          'A consumable product is a product for which stock is not managed.\n'
    #          'A service is a non-material product you provide.')
    # barcode = fields.Char('Barcode', oldname='ean13', related='product_variant_ids.barcode', readonly=False, size=250)

    # default_code = fields.Char('Product Code', compute='_compute_default_code', inverse='_set_default_code', store=True)
    critical_item = fields.Boolean(string="Critical Item")
    temporary_item = fields.Boolean(string="Temporary Item")
    old_item_code = fields.Char(string="Old Item Code", size=250)
    equipment_code = fields.Char(string="Equipment Code", size=250)
    ledger_number = fields.Char(string="Ledger Number", size=250)
    folio_number = fields.Char(string="Folio Number", size=250)
    current_price = fields.Float(string="Current Price")
    average_price = fields.Float(string="Average Price")
    purchase_price = fields.Float(string="Purchase Price")
    opening_balance_qty = fields.Float(string="Opening Balance Qty", digits=dp.get_precision('Product Unit of Measure'))
    closing_balance_qty = fields.Float(string="Closing Balance Qty", digits=dp.get_precision('Product Unit of Measure'))
    opening_balance_price = fields.Float(string="Opening Balance Price")
    closing_balance_price = fields.Float(string="Closing Balance Price")
    is_asset = fields.Boolean(string="Is Asset?")
    

   
    product_qutetion_ids = fields.One2many(
        string='product_qutetion',
        comodel_name='kw_quotation_items',
        inverse_name='product_id',
    )
    

    # _sql_constraints = [
    #     (
    #         'default_code',
    #         'unique(default_code)',
    #         'The product with above code exit'
    #     )
    # ]

    @api.constrains('default_code')
    def check_code(self):
        for template in self:
            duplicate = self.env['product.template'].sudo().search([('default_code','=',template.default_code)]) - self
            if duplicate:
                raise ValidationError('Above code is already exists.')
    
        


    @api.onchange('name')
    def convert_uppercase(self):
        for record in self:
            if record.name:
                record.name = record.name.upper()

    @api.multi
    def write(self, vals):
        if 'name' in vals:
            vals['name'] = vals['name'].upper()
        res = super(ProductTemplate, self).write(vals)
        return res
    
    # categ_id = fields.Many2one(
    #     'product.category', 'Product Category',
    #     change_default=True, default=_get_default_category_id,
    #     required=False, help="Select category for the current product")
    # , search='_search_qty_available'
    qty_ordered = fields.Float(
        'Quantity Ordered', compute='_compute_quantities',
        digits=dp.get_precision('Product Unit of Measure'),
        help="Ordered quantity of products.")
    # , search='_search_qty_available'
    qty_indented = fields.Float(
        'Quantity Indented', compute='_compute_quantities',
        digits=dp.get_precision('Product Unit of Measure'),
        help="Ordered quantity of products.")

    def _compute_quantities(self):
        for template in self:
            template.qty_ordered = 0
            template.qty_indented = 0

    def _search_qty_available(self, operator, value):
        if value == 0.0 and operator == '>' and not ({'from_date', 'to_date'} & set(self.env.context.keys())):
            product_ids = self._search_qty_available_new(
                operator, value, self.env.context.get('lot_id'), self.env.context.get('owner_id'),
                self.env.context.get('package_id')
            )
            return [('id', 'in', product_ids)]
        return self._search_product_quantity(operator, value, 'qty_available')

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            # default_code = f'({record.default_code})' if record.default_code else ''
            result.append((record.id, f'{record.default_code}'))
        return result
