from odoo import models, fields, api
from odoo.exceptions import ValidationError
import re


class kw_purchase_requisition(models.Model):
    _name = 'kw_purchase_requisition'
    _description = "A master model to create the Purchase Requisition"
    _rec_name = "requisition_number"
    _inherit =['mail.thread', 'mail.activity.mixin','portal.mixin']
    

    
    date = fields.Date(string='Date',default=fields.Date.context_today, required=True)
    requisition_number = fields.Char(string="Requisition Number",required=True,default="New",readonly="1")
    
    project_code =  fields.Char(string="Project Code")
    add_product_rel = fields.One2many("kw_add_product", 'pr_rel', string="Item Details")
    cost_center_code = fields.Many2one('account.account',string="Cost Center Code",track_visibility='always')
    budget_code = fields.Char(string="Budget code")
    estimated_value = fields.Integer(string="Estimated Value")
    budgeted_value = fields.Integer(string="Budgeted Value")
    can_edit_remark = fields.Boolean(compute='compute_can_edit_remark')
    status = fields.Char(string="Pr Status",compute="_get_status",store=True,default="Draft")
    state = fields.Selection([
        ('Pending','Pending'),
         ('Hold','Hold'),
        ('In progress','In progress'),
        ('Cancelled', 'Cancelled'),
        ('Completed', 'Completed'),
    ], string='Status', readonly=True, index=True, copy=False, default='Pending', track_visibility='onchange')   
    # get_department = fields.Boolean(compute="_get_department", string="Get Department", default=False)

    #pr sequence
    @api.model
    def create(self, vals):
        if vals.get('requisition_number', 'New') == 'New':
            vals['requisition_number'] = self.env['ir.sequence'].next_by_code('kw_requisition') or '/'
        self.env.user.notify_success(message='Requisition Created Successfully.')
        return super(kw_purchase_requisition, self).create(vals)

    @api.constrains('add_product_rel')
    def check_child_record(self):
        for record in self:
            if len(record.add_product_rel) == 0:
                print(len(record.add_product_rel))
                raise ValidationError("There is no Items added")

    # @api.onchange("department_name")
    # def _set_department_code(self):
    #     for record in self:
    #         if record.department_name:
    #             record.department_code = record.department_name.id
    
    @api.model
    def get_department(self):
        # print("get dept called")
        # print(self._uid)
        uid = self.env.user.id
        dept = self.env['hr.employee'].sudo().search([('user_id','=',self._uid)])
        # print("Department is",dept.department_id)
        return dept.department_id.id

    department_name = fields.Many2one('hr.department', string="Department Name",required=True,track_visibility='always',
     default=lambda self: self.get_department()
    )
    department_code = fields.Char(string="department Code",required=True,default=lambda self: self.get_department())
    indenting_department = fields.Many2one('hr.department', string="Indenting Department",required=True,track_visibility='always',default=lambda self: self.get_department())
               
    
    @api.model
    def compute_can_edit_remark(self):
        for record in self:
            record.can_edit_remark = record.env.user.has_group('stock.group_stock_manager')


    
    @api.multi
    @api.depends('add_product_rel')
    def _get_status(self):
        print('get status method called')
        for record in self:
            approv = []
            reject = []
            draf = []
            on_hold = []
            if record.add_product_rel:
                for rec in record.add_product_rel:
                    if rec.status == 'Draft':
                        draf.append(rec.status)
                    if rec.status == 'Approved':
                        approv.append(rec.status)
                    if rec.status == 'Rejected':
                        reject.append(rec.status)
                    if rec.status == 'Hold':
                        on_hold.append(rec.status)
                if len(record.add_product_rel) == len(draf):
                    print(len(draf))
                    record.status = 'Pending'
                    record.state = 'Pending'
                if len(record.add_product_rel) == len(reject):
                    print(len(reject))
                    record.status = 'Cancelled'
                    record.state = 'Cancelled'
                if len(record.add_product_rel) == len(approv):
                    print(len(approv))
                    record.status = 'Completed'
                    record.state = 'Completed'
                if len(record.add_product_rel) == len(on_hold):
                    record.status = 'Hold'
                    record.state = 'Hold'
                if len(draf) > 0 and len(approv) > 0 or len(reject) > 0 and len(approv) > 0 or len(reject) > 0 and len(draf) > 0 or len(draf) > 0 and len(on_hold) > 0 or len(on_hold) > 0 and len(approv) > 0 or len(on_hold) > 0 and len(reject) > 0:
                    record.status = 'In progress'
                    record.state = 'In progress'
                if len(draf) == 0 and len(on_hold) == 0 and len(approv) > 0 and len(reject) > 0:
                    record.status = 'Completed'
                    record.state = 'Completed'

            
                
            

    


    # @api.model
    # def create(self, vals):
    #     new_record = super(kw_purchase_requisition, self).create(vals)
    #     self.env.user.notify_success(message='Requisition Created Successfully.')
    #     return new_record

    @api.multi
    def write(self, vals):
        res = super(kw_purchase_requisition, self).write(vals)
        self.env.user.notify_success(message='Requisition Updated Successfully.')
        return res

    @api.onchange('add_product_rel')
    def validate_item(self):
        for record in self:
            lst = []
            for rec in record.add_product_rel:
                if rec.item_code.id in lst:
                    raise ValidationError("Same Item cannot be Added Twice")
                else:
                    lst.append(rec.item_code.id)


    @api.multi
    def unlink(self):
        for record in self:
            indent_rec = self.env['kw_consolidation'].sudo().search([])
            if indent_rec:
                for indent in indent_rec:
                    for req in indent.requisition_rel:
                        if record.requisition_number == req.requisition_number:
                            raise ValidationError(f"Record cannot be Deleted. Requisition No - {record.requisition_number} is referenced by Indent No - {indent.indent_number}")

        return super(kw_purchase_requisition, self).unlink()


    

    


    
   