from . import product_template
from . import stock_warehouse
from . import stock_inventory
from . import product
from . import uom_uom
from . import res_config_settings
from . import purchase_requisition,indent_consolidation,add_products,add_product_consolidation,kw_rfq,kw_inventory_res_partner,kw_quotation,kw_quotation_items,kw_inventory_wizard,kw_inventory_quotation_wizard
from . import quotation_consolidation,kw_quotation_consolidation_items,kw_inventory_quotation_consolidation_wizard,kw_inventory_report,kw_quotation_consolidation_items_wizard
from . import kw_negotiation,kw_negotiation_log
from . import kw_asset