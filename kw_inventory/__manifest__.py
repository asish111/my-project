# -*- coding: utf-8 -*-
{
    'name': "Kwantify Inventory",

    'summary': """
        Kwantify stock and logistics activities
    """,

    'description': """
        Kwantify stock and logistics activities
    """,

    'author': "CSM technology pvt.ltd.",
    'website': "https://www.csm.co.in",

    'category': 'Kwantify',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock', 'purchase', 'sale_management','product', 'uom', 'l10n_in','kw_account_accountant','payment','purchase_stock'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        # 'security/kw_inventory_security.xml',
        'views/product_views.xml',
        'views/kw_quotation_sequence.xml',
        'views/kw_pr_sequence.xml',
        'views/kw_indent_sequence.xml',
        'views/kw_quotation_consolidation_sequence.xml',
        'views/kw_quotation_action.xml',
        'views/kw_mode_of_negotiation_master.xml',
        'views/inventory_report.xml',
        'views/kw_remark.xml',
        'views/kw_remark_reject.xml',
        'views/inventory_adjustment_view.xml',
        # 'views/kw_product_template_views.xml',
        'views/stock_warehouse_orderpoint.xml',
        'views/kw_quotation_consolidation.xml',
        'views/add_product_consolidation.xml',
        'views/indent_consolidation.xml',
        'views/approved_requisition_items.xml',
        'views/add_product.xml',
        'data/kw_uom_data.xml',
        'views/purchase_requisition_view.xml',
        'views/kw_inventory_res_partner_view.xml',
        'views/kw_rfq.xml',
        'views/chart_of_account.xml',
        'views/kw_quotation.xml',
        'views/kw_quotation_items.xml',
        'views/kw_inventory_email.xml',
        'views/kw_inventory_wizard.xml',
        'views/kw_quotation_consolidation_items.xml',
        'views/kw_inventory_quotation_wizard.xml',
        'views/kw_approved_negotiations.xml',
        'views/kw_inventory_view_asset.xml',
        'views/kw_remark_onhold.xml',
        'views/kw_inventory_quotation_consolidation_wizard.xml',
        'views/kw_inventory_report_views.xml',
        'views/kw_quotation_consolidation_decision_stage.xml',
        'views/consolidation_template.xml',
        'views/kw_negotiation.xml',
        'views/kw_ready_for_po.xml',
        'views/kw_quotation_consolidation_items_wizard.xml',
        'views/kw_asset.xml',
        'views/kw_purchase_report.xml',
        'views/inventory_menu.xml',
    ],
    'qweb': ['static/src/xml/kw_inventory_report_view.xml'],
    'application': True,
    'installable': True,
    'auto_install': False,
}
