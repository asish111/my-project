from odoo import http
from odoo.http import request

class SearchReport(http.Controller):
    @http.route('/search-filter-report', type='json', auth="user", website=True)
    def filter_report(self, **args):
        query = "SELECT req_num AS Requisition_Number,pr_create_date as PR_Create_Date,requisition_Department as Requisition_Department,requisition_status as Requisition_Status,pr_approved_by as PR_Approved_By,\
        ind_num as Indent_Number,indent_date as Indent_Date,indent_department as Indent_Department,indent_status as Indent_Status,indent_approved_by as Indent_Approved_By,\
        quo_no as Quotation_Number,quotation_date as Quotation_Date,quotation_state as Quotation_State,quotation_approved_by as Quotation_Approved_By from get_inventory2()"
        request.cr.execute(query)    
        data = request.cr.fetchall()
        inventory_data = []
        for record in data:
            # print(record)
            inventory_data.append({'req_no':record[0], 'pr_create_date': record[1],'requisition_Department':record[2],'requisition_status':record[3],'pr_approved_by':record[4], 
            'indent_no':record[5],'indent_date':record[6],'indent_department':record[7],'indent_status':record[8],'indent_approved_by':record[9],'quo_no':record[10],'quotation_date':record[11],
            'quotation_state':record[12],'quotation_approved_by':record[13]})
        infodict = dict(report_inventory = inventory_data )
        return infodict
