from odoo import http
from odoo.http import request


class kw_quotation_consolidation(http.Controller):
    @http.route('/quotation-consolidation-decision', type='http', auth="user", website=True)
    def quotation_consolidation_decision_stage(self, **args):
        inventory_dict = dict()
        inventory_dict['quotation_dict'] = []
        inventory_dict['product_id'] = []
        inventory_dict['vendor'] = []
        inventory_dict['vendor_name'] = []
        inventory_dict['p_name'] = []
        inventory_dict['v_data'] = {}
        inventory_dict['min_value'] = {}
        qtions = http.request.env['kw_quotation'].search([])
        for record in qtions:
            inventory_dict['quotation_dict'].append(record.qo_no)
            if record.partner_id.id not in inventory_dict['vendor']:
                inventory_dict['vendor'].append(record.partner_id.id)
                inventory_dict['vendor_name'].append({"id":record.partner_id.id, "name":record.partner_id.name})


        qitems = http.request.env['kw_quotation_items'].search([])
        for rec in qitems:
            if rec.product_id.id not in inventory_dict['product_id']:
                inventory_dict['product_id'].append(rec.product_id.id)
                inventory_dict['p_name'].append({"id":rec.product_id.id, "name": rec.product_id.name})


        for product_id in inventory_dict['product_id']:
            items = request.env['kw_quotation_items'].search([('product_id.id', '=', product_id)])
            for item in items:
                for vendor_id in inventory_dict['vendor']:
                    if item.order_id.partner_id.id == vendor_id:
                        if product_id in inventory_dict['v_data']:
                            inventory_dict['v_data'][product_id].update({vendor_id: [item.price_unit,item.product_qty,item.price_subtotal]})
                        else:
                            inventory_dict['v_data'][product_id] = {vendor_id: [item.price_unit,item.product_qty,item.price_subtotal]}

        for p_id in inventory_dict['product_id']:
            if p_id in inventory_dict['v_data']:
                x = inventory_dict['v_data'][p_id]
                lst = []
                if x:
                    for i in x:
                        if x[i][0] not in lst:
                            lst.append(x[i][0]) 
                                            
                    lst.sort()
                    if len(lst) >= 3:
                        inventory_dict['min_value'].update({p_id: [lst[0],lst[1],lst[2]]})
                    if len(lst) >= 2 and len(lst) < 3:
                        inventory_dict['min_value'].update({p_id: [lst[0],lst[1],'NA']})
                    if len(lst) >= 1 and len(lst) < 2:
                        inventory_dict['min_value'].update({p_id: [lst[0],'NA','NA']})

            

        return http.request.render('kw_inventory.kw_consolidation_details',inventory_dict)


        