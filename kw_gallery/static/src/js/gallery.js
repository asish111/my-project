odoo.define('kw_gallery.kw_image_gallery', function (require) { 
    'use strict';
    var image = {
        init: function(){
            lightbox.option({
              'resizeDuration': 200,
              'wrapAround': true,
              'disableScrolling': true,
              'alwaysShowNavOnTouchDevices': true,
            });
        }
    };
    $(function(){
        image.init();
    });
});