# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime


class kw_images(models.Model):
    _name = 'kw_images'
    _description = 'All images'

    image_id = fields.Many2one('kw_image_albums')
    title = fields.Char('Title')
    image = fields.Binary('Upload Image', attachment=True)
    image_status = fields.Selection([('1', 'Published'), ('2', 'Unpublished')], default='2',
                                    track_visibility='onchange', string='Status')
    tag_employees = fields.Many2many('res.users')
    attachment_id = fields.Char(compute='_compute_attachment_id', string="Attachments")

    @api.multi
    def publish_image(self):
        attachment = self.env['ir.attachment'].sudo().search([('res_id', '=', self.id), ('res_model', '=', 'kw_images'), ('res_field', '=', 'image')])
        attachment.public = True
        self.image_status = '1'

    @api.multi
    def unpublish_image(self):
        self.image_status = '2'

    def _compute_attachment_id(self):
        for image in self:
            attachment_id = self.env['ir.attachment'].search([('res_id', '=', image.id), ('res_model', '=', 'kw_images'), ('res_field', '=', 'image')]).id
            image.attachment_id = attachment_id
