# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request


class Gallery(http.Controller):
    @http.route(['/gallery'], auth="public", website=True)
    def gallery(self, **kwargs):
        data = dict()
        data['gallery'] = request.env['kw_image_albums'].sudo().search([])
        return request.render("kw_gallery.gallery_template", data)

    @http.route(['/gallery-details-<string:album_id>'], auth="public", methods=['GET'], website=True)
    def gallery_images(self, **kwargs):
        values = dict(kwargs)
        data = dict()
        data['gallery'] = gallery = request.env['kw_image_albums'].sudo().search([('id', '=', values['album_id'])])
        # for image in gallery.gallery_id:
        #     print(image.attachment_id)
        return request.render("kw_gallery.gallery_detail_template", data)
