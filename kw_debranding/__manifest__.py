{
    'name': "Kwantify Debranding",
    'description': "Helps change the aesthetic look of Odoo software via customizing them with Logo and other branding changes.",
    'author': "CSM Technologies Pvt. Ltd.",
    'website': "www.csm.co.in",
    'depends': [
        'base_setup',
        'web',
        'website',
    ],
    'data': [
        'views/views.xml',
    ],
    'qweb' : ['static/src/xml/setting.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
}
