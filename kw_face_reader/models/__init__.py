# -*- coding: utf-8 -*-
from . import res_config_settings

from . import kw_face_matched_log
from . import kw_face_training_data
from . import kw_face_unmatched_log

from . import hr_employee

from . import kw_manage_iot_device
