# -*- coding: utf-8 -*-

from . import kw_synchronization
from . import kw_sync_grade
from . import kw_sync_blood_group
from . import kw_sync_profile_master
from . import kw_sync_religion
from . import kw_sync_maritial_status
from . import kw_sync_technical_cate
from . import kw_sync_org_type
from . import kw_sync_employee_tag
from . import kw_sync_industry_type
from . import kw_sync_hr_department
from . import kw_sync_hr_jobs
from . import kw_sync_hr_employee
