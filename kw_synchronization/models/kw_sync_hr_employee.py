# -*- coding: utf-8 -*-

from odoo import models, fields, api
import json
from datetime import date, datetime
import datetime
from dateutil.tz import tzutc

class kw_sync_hr_employee(models.Model):
    _inherit = 'hr.employee'

    @api.model
    def create(self, vals):
        # print('create called')
        # print('vals is',vals)\
        val_address_id = vals['address_id'] if 'address_id' in vals else ''
        val_work_location_id = vals['work_location_id'] if 'work_location_id' in vals else ''
        val_work_email = vals['work_email'] if 'work_email' in vals else ''
        val_mobile_phone = vals['mobile_phone'] if 'mobile_phone' in vals else ''
        val_work_phone = vals['work_phone'] if 'work_phone' in vals else ''
        val_department_id = vals['department_id'] if 'department_id' in vals else ''
        val_job_id = vals['job_id'] if 'job_id' in vals else ''
        val_job_title = vals['job_title'] if 'job_title' in vals else ''
        val_parent_id = vals['parent_id'] if 'parent_id' in vals else ''
        val_coach_id = vals['coach_id'] if 'coach_id' in vals else ''
        val_resource_calendar_id = vals['resource_calendar_id'] if 'resource_calendar_id' in vals else ''
        val_tz = vals['tz'] if 'tz' in vals else '' 

        val_user_id = vals['user_id'] if 'user_id' in vals else ''        
        val_emp_grade = vals['emp_grade'] if 'emp_grade' in vals else ''        
        val_date_of_joining = vals['date_of_joining'] if 'date_of_joining' in vals else ''        
        val_date_of_completed_probation = vals['date_of_completed_probation'] if 'date_of_completed_probation' in vals else ''        
        val_confirmation_sts = vals['confirmation_sts'] if 'confirmation_sts' in vals else ''        
        val_emp_code = vals['emp_code'] if 'emp_code' in vals else ''        
        val_emp_role = vals['emp_role'] if 'emp_role' in vals else ''        
        val_emp_category = vals['emp_category'] if 'emp_category' in vals else ''        
        val_employement_type = vals['employement_type'] if 'employement_type' in vals else ''        
        val_work_email = vals['work_email'] if 'work_email' in vals else ''        
        val_outlook_pwd = vals['outlook_pwd'] if 'outlook_pwd' in vals else ''        
        val_biometric_id = vals['biometric_id'] if 'biometric_id' in vals else ''        
        val_epbx_no = vals['epbx_no'] if 'epbx_no' in vals else ''        
        val_domain_login_id = vals['domain_login_id'] if 'domain_login_id' in vals else ''
        val_domain_login_pwd = vals['domain_login_pwd'] if 'domain_login_pwd' in vals else ''        
        val_id_card_no = vals['id_card_no'] if 'id_card_no' in vals else ''

        val_country_id = vals['country_id'] if 'country_id' in vals else ''        
        val_identification_id = vals['identification_id'] if 'identification_id' in vals else ''        
        val_emergency_contact = vals['emergency_contact'] if 'emergency_contact' in vals else ''        
        val_emergency_phone = vals['emergency_phone'] if 'emergency_phone' in vals else ''        
        val_km_home_work = vals['km_home_work'] if 'km_home_work' in vals else ''        
        val_personal_email = vals['personal_email'] if 'personal_email' in vals else ''
        val_birthday = vals['birthday'] if 'birthday' in vals else ''        
        val_gender = vals['gender'] if 'gender' in vals else ''        
        val_emp_religion = vals['emp_religion'] if 'emp_religion' in vals else ''        
        val_marital = vals['marital'] if 'marital' in vals else ''        
        val_marital_code = vals['marital_code'] if 'marital_code' in vals else ''        
        val_wedding_anniversary = vals['wedding_anniversary'] if 'wedding_anniversary' in vals else ''        
        val_visa_no = vals['visa_no'] if 'visa_no' in vals else ''        
        val_permit_no = vals['permit_no'] if 'permit_no' in vals else ''        
        val_visa_expire = vals['visa_expire'] if 'visa_expire' in vals else ''        
        val_emp_refered_from = vals['emp_refered_from'] if 'emp_refered_from' in vals else ''        
        val_emp_refered_detail = vals['emp_refered_detail'] if 'emp_refered_detail' in vals else ''        
        val_known_language_ids = vals['known_language_ids'] if 'known_language_ids' in vals else ''  

        val_present_addr_street = vals['present_addr_street'] if 'present_addr_street' in vals else ''        
        val_present_addr_street2 = vals['present_addr_street2'] if 'present_addr_street2' in vals else ''        
        val_present_addr_country_id = vals['present_addr_country_id'] if 'present_addr_country_id' in vals else ''        
        val_present_addr_city = vals['present_addr_city'] if 'present_addr_city' in vals else ''        
        val_present_addr_state_id = vals['present_addr_state_id'] if 'present_addr_state_id' in vals else ''        
        val_present_addr_zip = vals['present_addr_zip'] if 'present_addr_zip' in vals else '' 

        val_same_address = vals['same_address'] if 'same_address' in vals else ''       

        val_permanent_addr_street = vals['permanent_addr_street'] if 'permanent_addr_street' in vals else ''        
        val_permanent_addr_street2 = vals['permanent_addr_street2'] if 'permanent_addr_street2' in vals else ''        
        val_permanent_addr_country_id = vals['permanent_addr_country_id'] if 'permanent_addr_country_id' in vals else ''        
        val_permanent_addr_city = vals['permanent_addr_city'] if 'permanent_addr_city' in vals else ''        
        val_permanent_addr_state_id = vals['permanent_addr_state_id'] if 'permanent_addr_state_id' in vals else ''        
        val_permanent_addr_zip = vals['permanent_addr_zip'] if 'permanent_addr_zip' in vals else ''  

        val_blood_group = vals['blood_group'] if 'blood_group' in vals else ''
        val_identification_ids = vals['identification_ids'] if 'identification_ids' in vals else ''        
        val_membership_assc_ids = vals['membership_assc_ids'] if 'membership_assc_ids' in vals else '' 

        val_educational_details_ids = vals['educational_details_ids'] if 'educational_details_ids' in vals else ''

        val_experience_sts = vals['experience_sts'] if 'experience_sts' in vals else ''        
        val_technical_skill_ids = vals['technical_skill_ids'] if 'technical_skill_ids' in vals else ''        
        val_worked_country_ids = vals['worked_country_ids'] if 'worked_country_ids' in vals else ''        
        val_work_experience_ids = vals['work_experience_ids'] if 'work_experience_ids' in vals else ''

        val_family_details_ids = vals['family_details_ids'] if 'family_details_ids' in vals else ''   
        val_whatsapp_no = vals['whatsapp_no'] if 'whatsapp_no'    in vals else ''
        print(val_whatsapp_no)
        val_image=vals['image']
        record = super(kw_sync_hr_employee, self).create(vals)
        
        json_data = {"synchronized_by":self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)]).kw_id,
            "kw_emp_id":record.kw_id,
            "user_name":record.user_id.login,
            "kw_emp_grade":record.emp_grade.kw_id,
            "kw_work_location_id":record.work_location_id.kw_location_id,
            "kw_office_id":record.work_location_id.kw_office_id,
            "kw_dept_id":record.department_id.kw_id,
            "kw_deg_id":record.job_id.kw_id,
            "kw_parent_id":record.parent_id.kw_id,
            "kw_coach_id":record.coach_id.kw_id,
            "kw_emp_name":vals['name'],
            "work_email":val_work_email,
            "mobile_phone":val_mobile_phone,
            "work_phone":val_work_phone,
            "department_id":val_department_id,
            "job_id":val_job_id,
            "job_title":val_job_title,
            "parent_id":val_parent_id,
            "coach_id":val_coach_id,
            "resource_calendar_id":val_resource_calendar_id,
            "tz":val_tz,

            "emp_grade":val_emp_grade,
            "date_of_joining":val_date_of_joining,
            "date_of_completed_probation":val_date_of_completed_probation,
            "confirmation_sts":val_confirmation_sts,
            "emp_code":val_emp_code,
            "emp_role":val_emp_role,
            "emp_category":val_emp_category,
            "employement_type":record.employement_type.code,
            # "work_email":val_work_email,
            "outlook_pwd":val_outlook_pwd,
            "biometric_id":val_biometric_id,
            "epbx_no":val_epbx_no,
            "domain_login_id":val_domain_login_id,
            "domain_login_pwd":val_domain_login_pwd,
            "id_card_no":val_id_card_no,

            "country_id":record.country_id.name,
            "identification_id":val_identification_id,
            "emergency_contact":val_emergency_contact,
            "emergency_phone":val_emergency_phone,
            "km_home_work":val_km_home_work,
            "personal_email":val_personal_email,
            "whatsapp_no":val_whatsapp_no,
            "birthday":val_birthday,
            "gender":val_gender,
            "emp_religion":record.emp_religion.name,
            "marital":val_marital,
            "marital_code":val_marital_code,
            "wedding_anniversary":val_wedding_anniversary,
            "visa_no":val_visa_no,
            "permit_no":val_permit_no,
            "visa_expire":val_visa_expire,
            "emp_refered_from":val_emp_refered_from,
            "emp_refered_detail":val_emp_refered_detail,

            "present_addr_street" : val_present_addr_street,     
            "present_addr_street2" : val_present_addr_street2,
            "present_addr_country_id" : record.present_addr_country_id.name,
            "present_addr_city" : val_present_addr_city,
            "present_addr_state_id" : record.present_addr_state_id.name,
            "present_addr_zip" : val_present_addr_zip,

            "same_address" :val_same_address,

            "permanent_addr_street" : val_permanent_addr_street,
            "permanent_addr_street2" : val_permanent_addr_street2,
            "permanent_addr_country_id" : record.permanent_addr_country_id.name,
            "permanent_addr_city" : val_permanent_addr_city,
            "permanent_addr_state_id" : record.permanent_addr_state_id.name,
            "permanent_addr_zip":val_permanent_addr_zip,

            "blood_group":val_blood_group,
            "blood_group_name" : record.blood_group.name,
            "image":val_image,
             }
        serialized_data = json.dumps(json_data, sort_keys=False)
        print(serialized_data)
        new_data = self.env['kw_synchronization'].create({'new_data':serialized_data,'model_name':"hr.employee","operation":"I"})
        return record


    @api.multi
    def write(self, vals):
        new_json={'synchronized_by':self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)]).kw_id,'kw_emp_id':self.kw_id}
        new_json.update(vals)

        # Work information------------------
        if 'work_location_id' in new_json:
            record=self.env['res.partner'].sudo().browse(vals['work_location_id'])
            new_json['kw_office_id']=record.kw_office_id
            new_json['kw_location_id']=record.kw_location_id
            # new_json['work_location_id']={'kw_location_id':location_id,'kw_office_id':office_id}
        if 'department_id' in new_json:
            record=self.env['hr.department'].sudo().browse(vals['department_id'])
            new_json['department_id']=record.kw_id
        if 'job_id' in new_json:
            record=self.env['hr.job'].sudo().browse(vals['job_id'])
            new_json['job_id']=record.kw_id
        if 'parent_id' in new_json:
            record=self.env['hr.employee'].sudo().browse(vals['parent_id'])
            new_json['parent_id']=record.kw_id
        if 'coach_id' in new_json:
            record=self.env['hr.employee'].sudo().browse(vals['coach_id'])
            new_json['coach_id']=record.kw_id

        # Hr settings--------------------------
        if 'user_id' in new_json:
            record=self.env['res.users'].sudo().browse(vals['user_id'])
            new_json['user_id']=record.login
        if 'emp_grade' in new_json:
            record=self.env['kwemp_grade'].sudo().browse(vals['emp_grade'])
            new_json['emp_grade']=record.kw_id
        if 'emp_role' in new_json:
            record=self.env['kwmaster_role_name'].sudo().browse(vals['emp_role'])
            new_json['emp_role']=record.kw_id
        if 'emp_category' in new_json:
            record=self.env['kwmaster_category_name'].sudo().browse(vals['emp_category'])
            new_json['emp_category']=record.kw_id
        if 'employement_type' in new_json:
            record=self.env['kwemp_employment_type'].sudo().browse(vals['employement_type'])
            new_json['employement_type']=record.code

        # Personal Information------------------
        if 'country_id' in new_json:
            record = self.env['res.country'].sudo().browse(vals['country_id'])
            new_json['country_id'] = record.name
        if "present_addr_country_id" in new_json:
            record = self.env['res.country'].sudo().browse(vals['present_addr_country_id'])
            new_json['present_addr_country_id'] = record.name
        if "present_addr_state_id" in new_json:
            record = self.env['res.country.state'].sudo().browse(vals['present_addr_state_id'])
            new_json['present_addr_state_id'] = record.name
        if "permanent_addr_state_id" in new_json:
            record = self.env['res.country.state'].sudo().browse(vals['permanent_addr_state_id'])
            new_json['permanent_addr_state_id'] = record.name
        if "permanent_addr_country_id" in new_json:
            record = self.env['res.country'].sudo().browse(vals['permanent_addr_country_id'])
            new_json['permanent_addr_country_id'] = record.name
        if "emp_religion" in new_json:
            record = self.env['kwemp_religion_master'].sudo().browse(vals['emp_religion'])
            new_json['emp_religion'] = record.kw_id
        if "marital" in new_json:
            record = self.env['kwemp_maritial_master'].sudo().browse(vals['marital'])
            new_json['marital'] = record.kw_id
        if 'emp_refered_from' in new_json:
            record = self.env['kwemp_reference_mode_master'].sudo().browse(vals['emp_refered_from'])
            new_json['emp_refered_from'] = record.kw_id
        if 'blood_group' in new_json:
            record = self.env['kwemp_blood_group_master'].sudo().browse(vals['blood_group'])
            new_json['blood_group_name'] = record.name
        if 'whatsapp_number' in new_json:
            new_json['whatsapp_number'] = self.whatsapp_no
            
        print("Final new json data is",new_json)
        serialized_data = json.dumps(new_json, sort_keys=False)

        image=self.image
        img = image.decode('utf-8')

        old_json_data = {
                "synchronized_by":self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)]).kw_id,
                "kw_emp_id":self.kw_id,
                "user_name":self.user_id.login,
                "kw_emp_grade":self.emp_grade.kw_id,
                "kw_work_location_id":self.work_location_id.kw_location_id,
                "kw_office_id":self.work_location_id.kw_office_id,
                "kw_dept_id":self.department_id.kw_id,
                "kw_deg_id":self.job_id.kw_id,
                "kw_parent_id":self.parent_id.kw_id,
                "kw_coach_id":self.coach_id.kw_id,
                "kw_emp_name":self.name,
                "work_email":self.work_email,
                "mobile_phone":self.mobile_phone,
                "work_phone":self.work_phone,
                # "address_id":self.address_id.id,
                "department_id":self.department_id.id,
                "job_id":self.job_id.id,
                "job_title":self.job_title,
                "parent_id":self.parent_id.id,
                "coach_id":self.coach_id.id,
                "resource_calendar_id":self.resource_calendar_id.id,
                "tz":self.tz,

                # "user_id":self.user_id.id,
                "emp_grade":self.emp_grade.id,
                "date_of_joining":str(self.date_of_joining),
                "date_of_completed_probation":str(self.date_of_completed_probation),
                "confirmation_sts":self.confirmation_sts,
                "emp_code":self.emp_code,
                "emp_role":self.emp_role.id,
                "emp_category":self.emp_category.id,
                "employement_type":self.employement_type.code,
                "outlook_pwd":self.outlook_pwd,
                "biometric_id":self.biometric_id,
                "epbx_no":self.epbx_no,
                "domain_login_id":self.domain_login_id,
                "domain_login_pwd":self.domain_login_pwd,
                "id_card_no":self.id_card_no,

                "country_id":self.country_id.name,
                "identification_id":self.identification_id,
                "emergency_contact":self.emergency_contact,
                "emergency_phone":self.emergency_phone,
                "km_home_work":self.km_home_work,
                "personal_email":self.personal_email,
                "whatsapp_no":self.whatsapp_no,
                "birthday":str(self.birthday),
                "gender":self.gender,
                "emp_religion":self.emp_religion.id,
                "marital":self.marital.id,
                "marital_code":self.marital_code,
                "wedding_anniversary":str(self.wedding_anniversary),
                "visa_no":self.visa_no,
                "permit_no":self.permit_no,
                "visa_expire":str(self.visa_expire),
                "emp_refered_from":self.emp_refered_from.id,
                "emp_refered_detail":self.emp_refered_detail,

                "present_addr_street" : self.present_addr_street,     
                "present_addr_street2" : self.present_addr_street2,
                "present_addr_country_id" : self.present_addr_country_id.name,
                "present_addr_city" : self.present_addr_city,
                "present_addr_state_id" : self.present_addr_state_id.name,
                "present_addr_zip" : self.present_addr_zip,

                "same_address" :self.same_address,

                "permanent_addr_street" : self.permanent_addr_street,
                "permanent_addr_street2" : self.permanent_addr_street2,
                "permanent_addr_country_id" : self.permanent_addr_country_id.name,
                "permanent_addr_city" : self.permanent_addr_city,
                "permanent_addr_state_id" : self.permanent_addr_state_id.name,
                "permanent_addr_zip":self.permanent_addr_zip,
                "blood_group":self.blood_group.id,
                "blood_group_name":self.blood_group.name,
                "image": img,
                }
        # print(old_json_data)
        old_serialized_data = json.dumps(old_json_data, sort_keys=False)
        record = super(kw_sync_hr_employee, self).write(vals)
        new_data = self.env['kw_synchronization'].create({'new_data':serialized_data,'model_name':"hr.employee",'old_data':old_serialized_data,"operation":'U'})
        return record
        
    @api.multi
    def unlink(self,**vals):
        
        image=self.image
        img = image.decode('utf-8')
        json_data = { 
                "synchronized_by":self.env['hr.employee'].sudo().search([('user_id','=',self.env.user.id)]).kw_id,
                "kw_emp_id":self.kw_id,
                "user_name":self.user_id.login,
                "kw_emp_grade":self.emp_grade.kw_id,
                "kw_work_location_id":self.work_location_id.kw_location_id,
                "kw_office_id":self.work_location_id.kw_office_id,
                "kw_dept_id":self.department_id.kw_id,
                "kw_deg_id":self.job_id.kw_id,
                "kw_parent_id":self.parent_id.kw_id,
                "kw_coach_id":self.coach_id.kw_id,
                "kw_emp_name":self.name,
                "work_email":self.work_email,
                "mobile_phone":self.mobile_phone,
                "work_phone":self.work_phone,
                "address_id":self.address_id.id,
                "department_id":self.department_id.id,
                "job_id":self.job_id.id,
                "job_title":self.job_title,
                "parent_id":self.parent_id.id,
                "coach_id":self.coach_id.id,
                "resource_calendar_id":self.resource_calendar_id.id,
                "tz":self.tz,

                # "user_id":self.user_id.id,
                "emp_grade":self.emp_grade.id,
                "date_of_joining":str(self.date_of_joining),
                "date_of_completed_probation":str(self.date_of_completed_probation),
                "confirmation_sts":self.confirmation_sts,
                "emp_code":self.emp_code,
                "emp_role":self.emp_role.id,
                "emp_category":self.emp_category.id,
                "employement_type":self.employement_type.code,
                "outlook_pwd":self.outlook_pwd,
                "biometric_id":self.biometric_id,
                "epbx_no":self.epbx_no,
                "domain_login_id":self.domain_login_id,
                "domain_login_pwd":self.domain_login_pwd,
                "id_card_no":self.id_card_no,

                "country_id":self.country_id.name,
                "identification_id":self.identification_id,
                "emergency_contact":self.emergency_contact,
                "emergency_phone":self.emergency_phone,
                "km_home_work":self.km_home_work,
                "personal_email":self.personal_email,
                "whatsapp_no":self.whatsapp_no,
                "birthday":str(self.birthday),
                "gender":self.gender,
                "emp_religion":self.emp_religion.id,
                "marital":self.marital.id,
                "marital_code":self.marital_code,
                "wedding_anniversary":str(self.wedding_anniversary),
                "visa_no":self.visa_no,
                "permit_no":self.permit_no,
                "visa_expire":str(self.visa_expire),
                "emp_refered_from":self.emp_refered_from.id,
                "emp_refered_detail":self.emp_refered_detail,

                "present_addr_street" : self.present_addr_street,     
                "present_addr_street2" : self.present_addr_street2,
                "present_addr_country_id" : self.present_addr_country_id.name,
                "present_addr_city" : self.present_addr_city,
                "present_addr_state_id" : self.present_addr_state_id.name,
                "present_addr_zip" : self.present_addr_zip,

                "same_address" :self.same_address,

                "permanent_addr_street" : self.permanent_addr_street,
                "permanent_addr_street2" : self.permanent_addr_street2,
                "permanent_addr_country_id" : self.permanent_addr_country_id.name,
                "permanent_addr_city" : self.permanent_addr_city,
                "permanent_addr_state_id" : self.permanent_addr_state_id.name,
                "permanent_addr_zip":self.permanent_addr_zip,
                "blood_group":self.blood_group.id,
                "blood_group_name":self.blood_group.name,
                "image": img,
                }
        serialized_data = json.dumps(json_data, sort_keys=False)
        record = super(kw_sync_hr_employee, self).unlink()
        new_data = self.env['kw_synchronization'].create({'new_data':serialized_data,'model_name':"hr.employee","operation":'D'})
        return record   