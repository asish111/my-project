# -*- coding: utf-8 -*-
{
    'name': "Kwantify Synchronization",
    'summary': "Used to maintain the DB log files",
    'description': "It is used to map db entities and store log files.",
    'author': "CSM Technologies",
    'website': "http://www.csmpl.com",
    'category': 'Kwantify',
    'version': '0.1',
    # any module necessary for this one to work correctly
    'depends': ['base','hr','kw_employee'],
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/kw_synchronization.xml',
        'data/ir_cron_data.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
