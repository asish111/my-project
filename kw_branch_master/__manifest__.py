# -*- coding: utf-8 -*-
{
    'name': "Kwantify Branch",
    'summary': "Kwantify Branch Master",
    'description': "Kwantify Branch Master",
    'author': "CSM Technologies",
    'website': "http://www.csm.co.in",
    'category': 'Kwantify',
    'depends': ['base', 'hr'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/kw_res_branch.xml',
        'views/res_company.xml',
        'views/res_users.xml',
        'data/branch_master_data.xml',
    ],
    # only loaded in demonstration mode
    'installable': True,
    'application': False,
    'auto_install': False,
}
