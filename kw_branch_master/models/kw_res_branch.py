from odoo import api, fields, models


class KwResBranch(models.Model):
    _name = 'kw_res_branch'
    _description = "Branch"
    _rec_name = 'city'
    _order = 'city asc'

    branch_code = fields.Char('Branch Code/Ref.')
    kw_id = fields.Integer('Kwantify ID')
    name = fields.Char('Name', required=True)
    address = fields.Text('Branch Address', size=252)
    telephone_no = fields.Char('Telephone No.')
    gst_no = fields.Char('GSTIN')
    state_id = fields.Many2one('res.country.state', 'State')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id, readonly=True)
    email = fields.Char('Email')
    website = fields.Char('Website')
    country = fields.Many2one('res.country', 'Country')
    fax = fields.Char('Fax')
    city = fields.Char('City')
    zipcode = fields.Char('Zipcode')
    active = fields.Boolean('Active', default=True)

    @api.onchange('country')
    def fetch_states(self):
        if self.country:
            self.state_id = False
            return {'domain': {'state_id': ([('country_id', '=', self.country.id)])}}

    # def _get_name(self):
    #     """ Utility method to allow name_get to be overrided without re-browse the partner """
    #
    #     partner = self
    #     name = partner.city or ''
    #     if self._context.get('city'):
    #         name = partner.city
    #     return name
