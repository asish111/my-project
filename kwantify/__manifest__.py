# -*- coding: utf-8 -*-
{
    'name': "Kwantify",
    'sequence': 1,
    'summary': "ERP solution out of the box",
    'description': "Kwantify ERP application",
    'author': "CSM Technologies",
    'website': "http://www.csmpl.com",

    'category': 'Kwantify',
    'version': '0.1',

    'depends': ['base', 'mail', 'auth_signup', 'hr', 'portal', 'website', 'kw_sendsms', 'kw_debranding', 'kw_employee',
                'kw_handbook', 'kw_web_notify', 'survey', 'kw_dynamic_workflow',
                'kw_account_fiscal_year', 'kw_branch_master', 'kw_remove_export_option', 'kw_utility_tools', 'restful'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/menu.xml',
        'views/res_users.xml',
        'views/templates.xml',
        'views/hr_employee_template.xml',
        'views/portal_data.xml',
        'views/pwa_template.xml',
        'views/website_menu.xml',
        'views/empcode_of_users.xml',
        'views/kw_survey_survey_views.xml',
        'data/system_param_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
