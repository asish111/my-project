from odoo import models, fields, api, exceptions, SUPERUSER_ID
from odoo.http import request


class res_users(models.AbstractModel):
    _inherit = 'res.users'

    # emp_id = fields.One2many('hr.employee', 'user_id', string="Employee Code")
    # emp_code = fields.Char(related='emp_id.emp_code', store=True)
    emp_code = fields.Char('Employee Code', compute='_get_employee_code')

    def _get_employee_code(self):
        for user in self:
            records = self.env['hr.employee'].search([('user_id', '=', user.id)])
            if records:
                user.emp_code = records.emp_code
