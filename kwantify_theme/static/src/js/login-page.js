odoo.define('kwantify_theme.login_page', function (require) {
    "use strict";
    $(function() {
        $(document).on('mousedown','.passwordToggle', function(){
            $(this).removeClass('icon-eye').addClass('icon-eye-off');
            $('#password').attr('type','text');
        });
    
        $(document).on('mouseup','.passwordToggle', function(){
            $(this).removeClass('icon-eye-off').addClass('icon-eye');
            $('#password').attr('type','password');
        });

        $(document).on('mousedown','.passwordToggle1', function(){
            $(this).removeClass('icon-eye').addClass('icon-eye-off');
            $('#confirm_password').attr('type','text');
        });
    
        $(document).on('mouseup','.passwordToggle1', function(){
            $(this).removeClass('icon-eye-off').addClass('icon-eye');
            $('#confirm_password').attr('type','password');
        });
    });
});