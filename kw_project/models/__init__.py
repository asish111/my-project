# -*- coding: utf-8 -*-

from . import kw_project_inherit
from . import kw_project_crm_inherit
from . import kw_employee_inherits
from . import kw_project_tagging
from . import crm_team_inherit
from . import res_config_settings