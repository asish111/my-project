# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools


class kw_project_inherit(models.Model):
    _inherit = 'project.project'

    kw_id = fields.Integer("KW ID")
    code = fields.Char('Project Code', required=True)
    crm_id = fields.Many2one('crm.lead', "Reference Name")
    emp_id = fields.Many2one(string='Project Manager', comodel_name='hr.employee', ondelete='restrict')
    user_id = fields.Many2one(related='emp_id.user_id', store=True)
    resource_id = fields.One2many(string='Members', comodel_name='kw_project_resource_tagging',
                                  inverse_name='project_id', domain=[("active", "=", True)])
    stage_id = fields.Char(related='crm_id.stage_id.name', string='Reference Type')
    create_date_crm = fields.Date(related='crm_id.date', string='Work Order Date')
    sales_person = fields.Char(related='crm_id.user_id.name', string='Account Manager')

    @api.model
    def get_cc_emails(self):
        users = self.env.ref('kw_project.project_group_cc_users').users
        values = ','.join(str(user_email.email) for user_email in users)
        return values
