# -*- coding: utf-8 -*-

import requests, json, base64
import mimetypes
from urllib.request import urlopen
from datetime import datetime, timedelta,date
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api
from odoo import http

class kw_project_crm_inherit(models.Model):
    _inherit = 'crm.lead'

    kw_opportunity_id = fields.Integer('Opportunity Id')
    kw_workorder_id = fields.Integer('Workorder Id')
    code = fields.Char('Code')
    date = fields.Date("Work Order Date")

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.code:
                record_name = str(record.code) +' | '+str(record.name)
            else:
                record_name = str(record.name)
            result.append((record.id, record_name))
        return result

## Main method called from CRON Starts: ---
    def syncKwantifyProjectData(self):
        try:
            params = self.env['ir.config_parameter'].sudo()
            url = params.get_param('kw_proj.project_web_service_url', False)
            # print(url,'injjbhnj')
            day_diff = int(params.get_param('kw_proj.project_service_diff_days', 1))

            json_data = {
                "FromDate": (datetime.now() - relativedelta(days=+day_diff)).strftime('%Y-%m-%d'),
                "ToDate": datetime.now().strftime('%Y-%m-%d'),
            }
        ## CRM Method Call and update record data
            crm_result = self._ResponsefromKwantifyConfigCrm(url,json_data)
            self._CreateUpdateCRMData(crm_result)
        ## PROJECT Method Call and update record data
            project_result = self._ResponsefromKwantifyConfigProject(url,json_data)
            self._CreateUpdateProjectData(project_result)
        ## RESOURCE TAGGING Method Call and update record data
            resource_result = self._ResponsefromKwantifyConfigResourceTagging(url,json_data)
            self._CreateUpdateResourceTaggingData(resource_result)
        ## MANAGE RESOURCE DATE Method Call and send mail to PM(s)
            self._ManageResourceDate()
        except Exception as e:
            print("Error ocuures in main cron method : ",e)
            pass
## Main method called from CRON Ends: --- 

## Opportunity(CRM) URL call and Update record Starts :------        
    def _ResponsefromKwantifyConfigCrm(self,url,json_data):
        header = {'Content-type': 'application/json', }
        data = json.dumps(json_data)
        if url:
            crm_url = url + '/GetOpportunity'

        request_params_opp = " Service Url : " + crm_url + " " + str(data)
        try:
            response_result = requests.post(crm_url, data=data, headers=header)
            opportunity_resp = json.loads(response_result.text)
            return {'status': 200, 'result': opportunity_resp,'request_params_opp':request_params_opp}
        except Exception as e:
            return {'status': 500, 'error_log': str(e),'request_params_opp':request_params_opp}
    
    def _CreateUpdateCRMData(self,result):
        crm_lead = self.env['crm.lead']
        crm_stage = self.env['crm.stage']
        opp = crm_stage.search([('sequence','=',3)])
        wor = crm_stage.search([('sequence','=',70)])
        record_log = result['error_log'] if 'error_log' in result else ''
        update_record_log = ''
        new_record_log = ''
        try:
            if result['status'] == 200 and result['result']:
                result_set = result['result']
                for crm_data in result_set:
                    user_data = self.env['hr.employee'].search([('kw_id','=',int(crm_data['SalespersonId']))],limit=1)
                    vals={
                        'name':crm_data['OpportunityName'],
                        'date':crm_data['WorkOrderDate'] if crm_data['WorkOrderDate'] else False,
                        'code':crm_data['Code'] if crm_data['Code'] else False,
                        'active':True if crm_data['Status'] == '1' else False,
                        'user_id':user_data.user_id.id if len(user_data) else False,
                        }
                    if int(crm_data['OpportunityId']) != 0 and int(crm_data['WorkorderId']) == 0:
                        # print('Opp != 0 and work == 0')
                        opp1_id = crm_lead.search([('kw_opportunity_id','=',int(crm_data['OpportunityId'])),'|', ('active', '=', True), ('active', '=', False)])
                        # print(opp1_id)
                        if len(opp1_id):
                            vals.update({'stage_id': opp.id})
                            opp1_id.write(vals)
                            update_record_log += ' ### Start_rec ### ' + str(opp1_id.id) +' ###' + str(vals) + ' ### End_rec ### \n'
                        else:
                            vals.update({'kw_opportunity_id':int(crm_data['OpportunityId']),'kw_workorder_id':int(crm_data['WorkorderId']),'stage_id': opp.id})
                            new_created_record = crm_lead.create(vals)
                            new_record_log += ' ### Start_rec ### ' + str(new_created_record.id) + ' ###' + str(vals) + ' ### End_rec ### \n'
                    elif int(crm_data['OpportunityId']) != 0 and int(crm_data['WorkorderId']) != 0:
                        # print('Opp != 0 and work != 0')
                        opp2_id = crm_lead.search([('kw_opportunity_id','=',int(crm_data['OpportunityId'])),'|', ('active', '=', True), ('active', '=', False)])
                        # print(opp2_id)
                        if len(opp2_id):
                            vals.update({'kw_workorder_id':int(crm_data['WorkorderId']),'stage_id': wor.id})
                            opp2_id.write(vals)
                            update_record_log += ' ### Start_rec ### ' + str(opp2_id.id) +' ###' + str(vals) + ' ### End_rec ### \n'
                        else:
                            vals.update({'kw_opportunity_id':int(crm_data['OpportunityId']),'kw_workorder_id':int(crm_data['WorkorderId']),'stage_id': wor.id})
                            new_created_record = crm_lead.create(vals)
                            new_record_log += ' ### Start_rec ### ' + str(new_created_record.id) + ' ###' + str(vals) + ' ### End_rec ### \n'
                    elif int(crm_data['OpportunityId']) == 0 and int(crm_data['WorkorderId']) != 0:
                        # print('Opp == 0 and work != 0')
                        wor_id = crm_lead.search([('kw_workorder_id','=',int(crm_data['WorkorderId'])),'|', ('active', '=', True), ('active', '=', False)])
                        # print(wor_id)
                        if len(wor_id):
                            vals.update({'kw_opportunity_id':int(crm_data['OpportunityId']),'kw_workorder_id':int(crm_data['WorkorderId']),'stage_id': wor.id})
                            wor_id.write(vals)
                            update_record_log += ' ### Start_rec ### ' + str(wor_id.id) +' ###' + str(vals) + ' ### End_rec ### \n'
                        else:
                            vals.update({'kw_opportunity_id':int(crm_data['OpportunityId']),'kw_workorder_id':int(crm_data['WorkorderId']),'stage_id': wor.id})
                            new_created_record = crm_lead.create(vals)
                            new_record_log += ' ### Start_rec ### ' + str(new_created_record.id) + ' ###' + str(vals) + ' ### End_rec ### \n'
                print("Successfully Done With Opportunity and WorkOrder : ",len(result_set))
        except Exception as e:
            print('Error occures in CRM : ',e)
            pass
        finally:
            ## Enter data into log model
            synch_log = self.env['kw_kwantify_integration_log'].sudo()
            synch_log.create({'name': 'Kwantify Opportunity and WorkOrder Data', 'new_record_log': new_record_log, 'update_record_log': update_record_log,
                'error_log': record_log, 'request_params': result['request_params_opp'],
                'response_result': result['result'] if 'result' in result else []})

            ## If any record. delete last 15 days log record
            synch_log.search([('create_date', '<=', (datetime.now() - relativedelta(weeks=+2)).strftime('%Y-%m-%d'))]).unlink()

## Opportunity(CRM) URL call and Update record Ends :------    

## Project URL call and Update record Starts :------
    def _ResponsefromKwantifyConfigProject(self,url,json_data):  
        header = {'Content-type': 'application/json', }
        data = json.dumps(json_data)
        if url:
            project_url = url + '/GetProject'

        request_params_project = " Service Url : " + project_url + " " + str(data)
        try:
            response_result = requests.post(project_url, data=data, headers=header)
            project_resp = json.loads(response_result.text)
            return {'status': 200,'project_result':project_resp,'request_params_project':request_params_project}
        except Exception as e:
            return {'status': 500, 'error_log': str(e),'request_params_project':request_params_project}
                        
    def _CreateUpdateProjectData(self,result):
        crm_lead = self.env['crm.lead']
        project_project = self.env['project.project']
        record_log = result['error_log'] if 'error_log' in result else ''
        update_record_log = ''
        new_record_log = ''
        try:
            if result['status'] == 200 and result['project_result']:
                project_responce = result['project_result']
                for project_data in project_responce:
                    user_data = self.env['hr.employee'].search([('kw_id','=',int(project_data['PMId'])),'|', ('active', '=', True), ('active', '=', False)],limit=1)
                    vals={
                        'emp_id':user_data.id if len(user_data) else False,
                        'code':project_data['ProjCode'],
                        'kw_id':int(project_data['ProjId']),
                        'name':project_data['ProjName'],
                        'active':True if project_data['Status'] == '1' else False
                    }
                    if project_data['RefId'] and project_data['RefType'] and project_data['RefType'] == 'W':
                        word_order_id = crm_lead.search([('kw_workorder_id','=',int(project_data['RefId'])),'|', ('active', '=', True), ('active', '=', False)],limit=1)
                        if len(word_order_id):
                            # print('W found')
                            vals.update({'crm_id':word_order_id.id})
                    elif project_data['RefId'] and project_data['RefType'] and project_data['RefType'] == 'O':
                        opportunity_id = crm_lead.search([('kw_opportunity_id','=',int(project_data['RefId'])),'|', ('active', '=', True), ('active', '=', False)],limit=1)
                        if len(opportunity_id):
                            # print('O found')
                            vals.update({'crm_id':opportunity_id.id})

                    kw_ids = project_project.search([('kw_id','=',int(project_data['ProjId'])),'|', ('active', '=', True), ('active', '=', False)])
                    if len(kw_ids):
                        kw_ids.write(vals)
                        update_record_log += ' ### Start_rec ### ' + str(kw_ids.id) +' ###' + str(vals) + ' ### End_rec ### \n'
                    else:
                        new_created_record = project_project.create(vals)
                        new_record_log += ' ### Start_rec ### ' + str(new_created_record.id) + ' ###' + str(vals) + ' ### End_rec ### \n'
                print("Successfully Done With Project Data : ",len(project_responce))  
        except Exception as e:
            print('Error occures in Project : ',e)
            pass
        finally:
            ##enter data into log model
            synch_log = self.env['kw_kwantify_integration_log'].sudo()
            synch_log.create({'name': 'Kwantify Project Data', 'new_record_log': new_record_log, 'update_record_log': update_record_log,
                'error_log': record_log, 'request_params': result['request_params_project'],
                'response_result': result['project_result'] if 'project_result' in result else []})

            ##if any record.   delete last 15 days log record
            synch_log.search([('create_date', '<=', (datetime.now() - relativedelta(weeks=+2)).strftime('%Y-%m-%d'))]).unlink()

## Project URL call and Update record Ends :-------

## Resource Tagging URL Call and update record Starts :-----  
    def _ResponsefromKwantifyConfigResourceTagging(self,url,json_data):
        header = {'Content-type': 'application/json', }
        data = json.dumps(json_data)
        if url:
            resource_tagging_url = url + '/GetResourceTagging'

        request_params_resource = " Service Url : " + resource_tagging_url + " " + str(data)
        try:
            response_result = requests.post(resource_tagging_url, data=data, headers=header)
            resource_resp = json.loads(response_result.text)
            return {'status': 200,'resource_result':resource_resp,'request_params_resource':request_params_resource}
        except Exception as e:
            return {'status': 500, 'error_log': str(e),'request_params_resource':request_params_resource}

    def _CreateUpdateResourceTaggingData(self,result):
        project_project = self.env['project.project'].sudo()
        resource_tagging = self.env['kw_project_resource_tagging'].sudo()
        record_log = result['error_log'] if 'error_log' in result else ''
        update_record_log = ''
        new_record_log = ''
        try:
            if result['status'] == 200 and result['resource_result']:
                resource_responce = result['resource_result']
                for resource_data in resource_responce:
                    user_data = self.env['hr.employee'].search([('kw_id','=',int(resource_data['ResourceId'])),'|', ('active', '=', True), ('active', '=', False)],limit=1)
                    project_data = project_project.search([('kw_id','=',int(resource_data['ProjId'])),'|', ('active', '=', True), ('active', '=', False)])
                    vals={
                        'project_id':project_data.id if len(project_data) else False,
                        'emp_id':user_data.id if len(user_data) else False,
                        'start_date':resource_data['InvolvedFrom'],
                        'end_date':resource_data['InvolvedTo'],
                        'active':True if resource_data['Status'] == '1' else False
                    }
                    
                    kw_ids = resource_tagging.search([('kw_id','=',int(resource_data['RoleId'])),'|', ('active', '=', True), ('active', '=', False)],limit=1)
                    if len(kw_ids):
                        kw_ids.write(vals)
                        update_record_log += ' ### Start_rec ### ' + str(kw_ids.id) +' ###' + str(vals) + ' ### End_rec ### \n'
                    else:
                        vals.update({'kw_id':int(resource_data['RoleId'])})
                        new_created_record = resource_tagging.create(vals)
                        new_record_log += ' ### Start_rec ### ' + str(new_created_record.id) + ' ###' + str(vals) + ' ### End_rec ### \n'
                print("Successfully Done With Resource Tagging : ",len(resource_responce))  
        except Exception as e:
            print('Error occures in Resource Tagging : ',e)
            pass
        finally:
            ##enter data into log model
            synch_log = self.env['kw_kwantify_integration_log'].sudo()
            synch_log.create({'name': 'Kwantify Resource Tagging Data', 'new_record_log': new_record_log, 'update_record_log': update_record_log,
                'error_log': record_log, 'request_params': result['request_params_resource'],
                'response_result': result['resource_result'] if 'resource_result' in result else []})

            ##if any record.   delete last 15 days log record
            synch_log.search([('create_date', '<=', (datetime.now() - relativedelta(weeks=+2)).strftime('%Y-%m-%d'))]).unlink()

## Resource Tagging URL Call and update record Ends :----- 

## Manage Resource Date and send mail Starts :-----
    def _ManageResourceDate(self):
        resource_tagging = self.env['kw_project_resource_tagging']
        try:
            ## Making Resources status 'False' whose end_date already crossed
            resources_datas = resource_tagging.search([('end_date','<',date.today())])
            for resouce_date in resources_datas:
                resouce_date.write({'active':False})
            # Sending advanced 7,15,30 days reminder mail
            project_datas = self.env['project.project'].search([])
            template = self.env.ref("kw_project.kw_project_reminder_mail_template")
            for projects in project_datas:
                resource_records = projects.resource_id.filtered(lambda rec: rec.end_date == (date.today() + relativedelta(days=15)) or rec.end_date == (date.today() + relativedelta(days=30)))
                if len(resource_records):
                    self.env['mail.template'].browse(template.id).send_mail(projects.id)
                else:
                    pass
            print("Successfully Done with Mail And Resource Active/Inactive.")  
        except Exception as e:
            print('Error occures in Manage resource : ',e)
            pass
## Manage Resource Date and send mail Ends :-----