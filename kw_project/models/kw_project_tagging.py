# -*- coding: utf-8 -*-

from odoo import models, fields, api

class kw_project_resource_tagging(models.Model):
    _name       = 'kw_project_resource_tagging'
    _description= 'Resource tagging'
    _rec_name   = 'designation_id'

    kw_id           = fields.Integer(string='KW ID')
    designation_id  = fields.Many2one('hr.job',string='Designation')
    emp_id          = fields.Many2one('hr.employee',string='Employee')
    project_id      = fields.Many2one('project.project',string='Project Name')
    start_date      = fields.Date("Start Date")
    end_date        = fields.Date('End Date')
    active          = fields.Boolean('Status')
    # Related fields
    # skill_ids     = fields.Many2many(related='emp_id.skill_id',string='Skill')
    project_manager = fields.Char(related='project_id.emp_id.name',string='Project Manager')
    project_name    = fields.Char(related='project_id.name',string='Project Name')