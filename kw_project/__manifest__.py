# -*- coding: utf-8 -*-
{
    'name': "Kwantify Project",
    'version': '12.0.0.1',
    'summary': """Organize and schedule your projects""",
    'description': """Organize and schedule your projects""",
    'category': 'Generic Modules/Human Resources',
    'author': 'CSM technology pvt.ltd.',
    'company': 'CSM technology pvt.ltd.',
    'maintainer': 'CSM technology pvt.ltd.',
    'website': "https://www.csm.co.in",
    'depends': ['base', "project", 'crm', 'kw_employee', 'kw_kwantify_integration'],
    'data': [
        'security/project_security.xml',
        'security/ir.model.access.csv',
        'views/kw_project_inherit.xml',
        'views/kw_employee_inherits.xml',
        'views/kw_project_tagging.xml',
        'views/kw_project_crm_inherit.xml',
        'data/data_cron.xml',
        'views/res_config_settings_views.xml',

        # Email Templates
        'views/email/project_mail_template.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': False,
}